import _ from 'lodash'
import { normalize } from '../../helpers/screenHelper'

const fonts = {
    roboto: {
        // Regular
        regular: 'Roboto-Regular',

        // Thin
        thin: 'Roboto-Thin',
        thin_italic: 'Roboto-ThinItalic',

        // Light
        light: 'Roboto-Light',
        light_italic: 'Roboto-LightItalic',

        // Medium
        medium: 'Roboto-Medium',
        medium_italic: 'Roboto-MediumItalic',

        // Bold
        bold: 'Roboto-Bold',
        bold_italic: 'Roboto-BoldItalic',

        // Black
        black: 'Roboto-Black',
        black_italic: 'Roboto-BlackItalic',
    },
}

export const fontsSize = {
    xxxsmall: 'fs-xxxsmall',
    xxsmall: 'fs-xxsmall',
    xsmall: 'fs-xsmall',
    small: 'fs-small',
    medium: 'fs-medium',
    xmedium: 'fs-xmedium',
    large: 'fs-large',
    extraLarge: 'fs-xlarge',
}

export const getFontsSize = (fontSizeType = fontsSize.xsmall) => {
    switch (fontSizeType) {
    case fontsSize.xxxsmall: return normalize(8)
    case fontsSize.xxsmall: return normalize(10)
    case fontsSize.xsmall: return normalize(12)
    case fontsSize.small: return normalize(14)
    case fontsSize.medium: return normalize(16)
    case fontsSize.xmedium: return normalize(18)
    case fontsSize.large: return normalize(20)
    case fontsSize.extraLarge: return normalize(24)
    default: return Number(_.isNumber(Number(fontSizeType.split('-')?.[1])) ? normalize(fontSizeType.split('-')?.[1]) : normalize(12))
    }
}

export const fontsFamily = {
    roboto: {
        regular: 'ff-roboto-regular',
        thin: 'ff-roboto-thin',
        light: 'ff-roboto-light',
        medium: 'ff-roboto-medium',
        bold: 'ff-roboto-bold',
        extraBold: 'ff-roboto-black',
    },
}

export const getFontsFamily = (fontsFamilyType = fontsFamily.roboto.regular) => {
    const fontFamily = fontsFamilyType.split('-')?.[1]
    const fontType = fontsFamilyType.split('-')?.[2]
    return fonts?.[fontFamily]?.[fontType]
}

export default fonts
