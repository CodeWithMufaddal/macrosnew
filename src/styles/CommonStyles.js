import colors from '../constants/colors'
import { calenderDayType, isRTL, TEXT_INPUT_STYLE, appTextInputType } from '../constants/constants'

export const commonShadow = (props = {
    elevation: null,
    opacity: null,
    radius: null,
    color: null,
    offsetWidth: null,
    offsetHeight: null,
}) => ({
    elevation: props?.elevation ?? 2,
    shadowOpacity: props?.opacity ?? 0.2,
    shadowRadius: props?.radius ?? 2,
    shadowColor: props?.color ?? colors.shadowBlack,
    shadowOffset: { width: props?.offsetWidth ?? 0, height: props?.offsetHeight ?? 0 },
})

export const calenderDayStyle = ({
    currentSelectionDate: {
        customStyles: {
            container: { backgroundColor: calenderDayType.currentSelection.color },
            text: { color: colors.white },
        },
    },
    selectedDate: {
        customStyles: {
            container: { backgroundColor: calenderDayType.selectedDay.color },
            text: { color: colors.black },
        },
    },
    pausedDate: {
        customStyles: {
            container: { backgroundColor: calenderDayType.pausedDay.color },
            text: { color: colors.white },
        },
    },
    offDate: {
        customStyles: {
            container: { backgroundColor: calenderDayType.offDay.color },
            text: { color: colors.white },
        },
    },
})


export const getTextInputRTLDirection = (textInputType) => {
    return {
        textAlign: isRTL && textInputType === appTextInputType.TEXT ?  'right' : 'left',
        writingDirection: isRTL && textInputType === appTextInputType.TEXT  ? 'rtl' : 'ltr',
    }
}