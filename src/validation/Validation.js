import CustomToast from '../components/custom/CustomToast';
import { MOBILE_NUMBER_LENGTH, OTP_LENGTH } from '../constants/constants';
import i18n from '../i18n/i18n';

// Login
export const loginValidation = (mobileNumber) => {
    let isValid = false;
    let message = '';
    if (!mobileNumber) message = i18n.t('enter_mobile_number')
    else if (mobileNumber?.length !== MOBILE_NUMBER_LENGTH) {
        message = i18n.t('mobile_validation_length', { digit: MOBILE_NUMBER_LENGTH })
    }
    if (message) CustomToast.error(message);
    else isValid = true;
    return isValid
}

export const signupValidation = (signUpData) => {
    let isValid = false;
    let message = '';
    if (!signUpData?.name) message = i18n.t('enter_name')
    else if (!signUpData?.area) message = i18n.t('select_area')
    else if (!signUpData?.mobileNumber) message = i18n.t('enter_mobile_number')
    else if (signUpData.mobileNumber?.length !== MOBILE_NUMBER_LENGTH) {
        message = i18n.t('mobile_validation_length', { digit: MOBILE_NUMBER_LENGTH })
    }
    if (message) CustomToast.error(message);
    else isValid = true;
    return isValid
}

// Verify Otp
export const verifyOtpValidation = (otp) => {
    let isValid = false;
    let message = '';
    if (!otp) message = i18n.t('enter_otp_place_holder')
    else if (otp?.length !== OTP_LENGTH) message = i18n.t('otp_validation_length', { digit: OTP_LENGTH })
    if (message) CustomToast.error(message);
    else isValid = true;
    return isValid
}

// Purchase Subscription One
export const purchaseSubscriptionOneValidation = (validationProps) => {
    let isValid = false;
    let message = '';
    if (!validationProps.selectedGoal) message = i18n.t('select_package')
    else if (!validationProps.selectedMealCategory) message = i18n.t('select_meal_category')
    else if (!validationProps.selectedMealPlanDuration) message = i18n.t('select_no_of_days')
    else if (!validationProps.selectedPlan) message = i18n.t('select_plan')
    if (message) CustomToast.error(message);
    else isValid = true;
    return isValid
}

export const purchaseSubscriptionOneStartDateValidation = (validationProps) => {
    let isValid = false;
    let message = '';
    if (!validationProps.startDate) message = i18n.t('select_start_date')
    if (message) CustomToast.error(message);
    else isValid = true;
    return isValid
}

export const orderSummaryPromoCodeValidation = (validationProps) => {
    let isValid = false;
    let message = '';
    if (!validationProps.coupon) message = i18n.t('enter_promo_code')
    if (message) CustomToast.error(message);
    else isValid = true;
    return isValid
}

export const saveInbodySheetValidation = (validationProps) => {
    let isValid = false;
    const message = '';

    if (message) CustomToast.error(message);
    else isValid = true;
    return isValid
}

export const saveMyHealthDataValidation = () => {
    let isValid = false;
    const message = '';
    if (message) CustomToast.error(message);
    else isValid = true;
    return isValid
}

export const saveAddressValidation = (validationProps) => {
    let isValid = false;
    let message = '';
    // if (!validationProps.address_nickname) message = i18n.t('enter_address_nickname')
    if (!validationProps.area_id) message = i18n.t('select_area')
    else if (!validationProps.block) message = i18n.t('enter_block')
    else if (!validationProps.street_name) message = i18n.t('enter_street')
    else if (!validationProps.house_number) message = `${i18n.t('enter_house_number')} / ${i18n.t('enter_building')}`
    // else if (!validationProps.building) message = i18n.t('enter_building')
    else if (validationProps.pickup_phone_number && validationProps.pickup_phone_number?.length !== MOBILE_NUMBER_LENGTH) message = i18n.t('enter_valid_alternative_phone_number')
    if (message) CustomToast.error(message);
    else isValid = true;
    return isValid
}

export const saveMyProfileValidation = (validationProps) => {
    let isValid = false;
    let message = '';
    if (!validationProps.first_name) message = i18n.t('enter_name')
    else if (!validationProps.mobile_number) message = i18n.t('enter_phone')
    else if (validationProps.phoneVerificationData.oldMobileNumber !== validationProps.phoneVerificationData.newMobileNumber) message = i18n.t('phone_number_should_be_verified')
    // else if (!validationProps.address_nickname) message = i18n.t('enter_address_nickname')
    else if (!validationProps.area_id) message = i18n.t('select_area')
    else if (!validationProps.block) message = i18n.t('enter_block')
    else if (!validationProps.street_name) message = i18n.t('enter_street')
    else if (!validationProps.house_number) message = `${i18n.t('enter_house_number')} / ${i18n.t('enter_building')}`
    // else if (!validationProps.building) message = i18n.t('enter_building')
    else if (validationProps.pickup_phone_number && validationProps.pickup_phone_number?.length !== MOBILE_NUMBER_LENGTH) message = i18n.t('enter_valid_alternative_phone_number')

    if (message) CustomToast.error(message);
    else isValid = true;
    return isValid
}

export const generateCouponValidation = (validationProps) => {
    let message = null;
    if (!validationProps.amount) message = i18n.t('enter_amount')

    return message
}
