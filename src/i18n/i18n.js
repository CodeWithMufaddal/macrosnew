import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import ar from './locales/ar.json';
import en from './locales/en.json';

export const APP_DIRECTION = {
    ltr: 'ltr',
    rtl: 'rtl',
}
export const APP_LANGUAGES = {
    EN: 'en',
    AR: 'ar',
}

i18n
    .use(initReactI18next)
    .init({
        compatibilityJSON: 'v3',
        fallbackLng: 'en',
        resources: {
            en: { translation: en },
            ar: { translation: ar },
        },
        interpolation: {
            escapeValue: false
        },
        react: {
            useSuspense: false,
        }
    });

i18n.changeLanguage(APP_LANGUAGES.EN);

export default i18n;
