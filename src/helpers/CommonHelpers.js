/* eslint-disable no-plusplus */
/* eslint-disable no-unused-expressions */
import React from 'react';
import { Alert, Platform, View } from 'react-native';
import { openSettings } from 'react-native-permissions';
import moment from 'moment';
import Geolocation from '@react-native-community/geolocation'
import DeviceInfo from 'react-native-device-info';
import auth from '@react-native-firebase/auth';
import { getFontsFamily, getFontsSize } from '../assets/fonts/fonts';
import * as constants from '../constants/constants';
import { calenderDayStyle } from '../styles/CommonStyles';
import { isIOS } from './screenHelper';

export const getDeviceUniqueId = () => DeviceInfo.getUniqueId()
export const getOS = () => Platform.OS;

export const getFontsFamilyAndSize = (fontsProps = null) => {
    let fontFamily = getFontsFamily()
    let fontSize = getFontsSize()
    if (fontsProps) {
        fontsProps.map((key) => {
            if (key.startsWith('ff')) fontFamily = getFontsFamily(key)
            else if (key.startsWith('fs')) fontSize = getFontsSize(key)
            return key;
        })
    }
    return { fontFamily, fontSize }
}

export const generateUniqueId = (prefixString = '', suffixString = '') => `${prefixString}-${suffixString}`;

export const CommonHorizontalItemSeparator = () => <View style={{ width: 15 }} />

export const getDisabledDaysInMonth = (date = null) => {
    const month = date ? date.month - 1 : moment().month()
    const year = date ? date?.year : moment().year();
    const days = [constants.EVENT_OFF_DAY];
    const pivot = moment().month(month).year(year).startOf('month')
    const end = moment().month(month).year(year).endOf('month')

    const dates = {}
    const disabled = { disabled: true }
    while (pivot.isBefore(end)) {
        days.forEach((day) => {
            dates[pivot.day(day).format('YYYY-MM-DD')] = disabled
        })
        pivot.add(7, 'days')
    }

    return dates
}
export const getHitSlop = (slopValue) => {
    let hitSlop = {
        top: 0, bottom: 0, right: 0, left: 0,
    }
    if (['string', 'number']?.includes(typeof slopValue)) {
        hitSlop = {
            top: slopValue, bottom: slopValue, right: slopValue, left: slopValue,
        }
    } else if (typeof slopValue === 'object') hitSlop = { ...hitSlop, ...slopValue };
    return hitSlop;
}

// Get Coordinates
export const getRegionForCoordinates = (coordsPoints) => {
    const deltaFlag = 0.002;
    const points = [coordsPoints];
    // points should be an object of { latitude: X, longitude: Y }
    let minX, maxX, minY, maxY;

    // init first point
    ((point) => {
        minX = point.latitude;
        maxX = point.latitude;
        minY = point.longitude;
        maxY = point.longitude;
    })(points[0]);

    // calculate rect
    points.forEach((point) => {
        minX = Math.min(minX, point.latitude);
        maxX = Math.max(maxX, point.latitude);
        minY = Math.min(minY, point.longitude);
        maxY = Math.max(maxY, point.longitude);
        return point;
    });

    const midX = (minX + maxX) / 2;
    const midY = (minY + maxY) / 2;
    const deltaX = maxX - minX + deltaFlag;
    const deltaY = maxY - minY + deltaFlag;

    return {
        latitude: midX,
        longitude: midY,
        latitudeDelta: deltaX,
        longitudeDelta: deltaY,
        // latitude: points[0].latitude,
        // longitude: points[0].longitude,
        // latitudeDelta: 0.0922,
        // longitudeDelta: 0.0421,
    };
};

export const getCurrentLocation = () => new Promise((resolve, reject) => Geolocation.getCurrentPosition(
    (position) => resolve(position?.coords),
    (error) => reject(error),
    { enableHighAccuracy: true },
))

export const sendDataToReducer = (dispatch, type = null, payload = null) => {
    if (type) dispatch({ type, payload })
}

// eslint-disable-next-line max-len
export const getAmountWithCurrency = (amount = 0) => `${constants.APP_CURRENCY} ${Number(amount).toFixed(constants.NO_OF_DECIMAL_POINTS_IN_AMOUNT)}`

// Get Whole Plan for User
export const getWholePlanDuration = (subscriptionData) => {
    if (subscriptionData?.length > 0) {
        const startAndEndDateArray = subscriptionData.map((item) => ({
            start_time: getValidDate(item?.start_time, 'YYYY-MM-DD 00:00:00'),
            end_time: getValidDate(item?.end_time, 'YYYY-MM-DD 00:00:00'),
        }))

        return {
            start_time: startAndEndDateArray?.[0]?.start_time,
            end_time: startAndEndDateArray?.[startAndEndDateArray?.length - 1]?.end_time,
        }
    }
    return null;
}

// Get Subscription Data
export const getSingleSubscriptionData = (subscriptionData, date = null) => {
    if (date) {
        const bindDate = getValidDate(date, 'YYYY-MM-DD 00:00:00');
        if (subscriptionData?.length > 0) {
            const dummySubscriptionData = subscriptionData.map((item) => ({
                ...item,
                start_time: getValidDate(item?.start_time, 'YYYY-MM-DD 00:00:00'),
                end_time: getValidDate(item?.end_time, 'YYYY-MM-DD 00:00:00'),
            }))

            const singleSubscriptionData = dummySubscriptionData.find((item) => new Date(bindDate) >= new Date(item?.start_time) && new Date(bindDate) <= new Date(item?.end_time));
            return singleSubscriptionData
        }
        return null;
    }
    return subscriptionData?.[0];
}

// Get Calender Day Styles
const getCalenderDatesWithBackground = (dates = [], key, style) => {
    const currentDate = new Date(getValidDate(new Date(), 'YYYY-MM-DD 00:00:00'));
    const obj = {};
    dates.map((item) => {
        const occupiedDate = new Date(getValidDate(item[key], 'YYYY-MM-DD 00:00:00'))
        if (occupiedDate >= currentDate) obj[item[key]] = style;
    })
    return obj;
}

export const getCalenderMarkedDates = (currentSelectionDate, calenderDates) => {
    const selectedDates = getCalenderDatesWithBackground(calenderDates?.selected_date, 'selected_date', calenderDayStyle.selectedDate);
    const pausedDates = getCalenderDatesWithBackground(calenderDates?.paused_date, 'paused_date', calenderDayStyle.pausedDate);
    return ({
        ...selectedDates,
        ...pausedDates,
        [currentSelectionDate]: calenderDayStyle.currentSelectionDate,
    })
}

// Get Calender Day Styles
const getHorizontalCalenderDatesWithBackground = (dates = [], key, color) => {
    const currentDate = new Date(getValidDate(new Date(), 'YYYY-MM-DD 00:00:00'));
    const obj = []
    dates.map((item) => {
        const occupiedDate = new Date(getValidDate(item[key], 'YYYY-MM-DD 00:00:00'))
        if (occupiedDate >= currentDate) obj.push({ date: item[key], dots: [{ color }] });
    })
    return obj;
}
export const getHorizontalCalenderMarkedDates = (calenderDates) => {
    const selectedDates = getHorizontalCalenderDatesWithBackground(calenderDates?.selected_date, 'selected_date', constants.calenderDayType.selectedDay.color);
    const pausedDates = getHorizontalCalenderDatesWithBackground(calenderDates?.paused_date, 'paused_date', constants.calenderDayType.pausedDay.color);
    return ([
        ...selectedDates,
        ...pausedDates,
    ])
}

export const getCalenderDayType = (myDate, subData, calenderData) => {
    let dayType = null;
    const date = getValidDate(new Date(myDate), 'YYYY-MM-DD 00:00:00')
    let isPlanExistOnTodayDate = false;
    const subscriptionData = getSingleSubscriptionData(subData, date)
    const startDates = calenderData?.selected_date?.map((item) => getValidDate(item?.selected_date));
    const pausedDates = calenderData?.paused_date?.map((item) => getValidDate(item?.paused_date));
    const currentDate = new Date(getValidDate(new Date(), 'YYYY-MM-DD 00:00:00'));
    const subscriptionStartDate = new Date(getValidDate(subscriptionData?.start_time, 'YYYY-MM-DD 00:00:00'));
    const beginDate = currentDate > subscriptionStartDate ? currentDate : subscriptionStartDate;
    const after24HourDate = new Date(new Date().setDate(currentDate.getDate() + 1));

    if (new Date(date) <= new Date(subscriptionData?.end_time) && new Date(date) >= beginDate) {
        isPlanExistOnTodayDate = true
    }
    if (!isPlanExistOnTodayDate) dayType = constants.calenderDayType.noPlanExistDay.key;
    else if (new Date(date).getDay() === constants.EVENT_OFF_DAY) dayType = constants.calenderDayType.offDay.key
    else if (new Date(date) < after24HourDate) dayType = constants.calenderDayType.timeGoesForSelection.key
    else if (startDates?.includes(getValidDate(date, 'YYYY-MM-DD'))) dayType = constants.calenderDayType.selectedDay.key
    else if (pausedDates?.includes(getValidDate(date, 'YYYY-MM-DD'))) dayType = constants.calenderDayType.pausedDay.key;
    else dayType = constants.calenderDayType.noMealSelectedDay.key;
    return dayType;
}

export const getExpireInDays = (endDate) => {
    const currentDate = moment(moment(new Date(getValidDate(new Date(), 'YYYY-MM-DD 00:00:00'))), 'DD-MM-YYYY');
    const validUpto = moment(moment(new Date(getValidDate(endDate, 'YYYY-MM-DD 00:00:00'))), 'DD-MM-YYYY');
    return validUpto.diff(currentDate, 'days');
}

export const getMinimumStartDate = (leadTime, startDate = null, endDate = null) => new Promise((resolve) => {
    const expireIn = endDate ? getExpireInDays(endDate) : null;
    const currentDate = new Date(getValidDate(new Date(), 'YYYY-MM-DD 00:00:00'))
    let beginDate = new Date(getValidDate(startDate ?? new Date(), 'YYYY-MM-DD 00:00:00'));
    if (currentDate > beginDate) beginDate = currentDate
    const expireDate = endDate ? new Date(getValidDate(endDate, 'YYYY-MM-DD 00:00:00')) : null;
    const beforeLeadTimeDate = new Date(getValidDate(beginDate, 'YYYY-MM-DD 00:00:00'));
    let afterLeadTimeDate = new Date(getValidDate(beginDate, 'YYYY-MM-DD 00:00:00'));
    let count = 0;
    const isRenew = expireDate ? expireIn <= constants.PLAN_MINIMUM_EXPIRE_DAYS : false;
    if (isRenew && Number(leadTime) <= Number(expireIn)) {
        afterLeadTimeDate = new Date(expireDate);
        afterLeadTimeDate.setDate(afterLeadTimeDate.getDate() + 1);
        const dayOfWeek = afterLeadTimeDate.getDay();
        if (Number(dayOfWeek) === constants.EVENT_OFF_DAY) count++;
    } else {
        const dt = beforeLeadTimeDate.getDate()
            + (Number(leadTime) < constants.MINIMUM_START_DATE_DURATION_DAYS
                ? constants.MINIMUM_START_DATE_DURATION_DAYS
                : Number(leadTime)
            );
        afterLeadTimeDate.setDate(dt + 1);
        const curDate = beforeLeadTimeDate;
        while (curDate <= afterLeadTimeDate) {
            const dayOfWeek = curDate.getDay();
            Number(dayOfWeek) === constants.EVENT_OFF_DAY && count++;
            curDate.setDate(curDate.getDate() + 1);
        }
    }
    if (count > 0) afterLeadTimeDate.setDate(afterLeadTimeDate.getDate() + count);
    if (afterLeadTimeDate?.getDay() === constants.EVENT_OFF_DAY) afterLeadTimeDate.setDate(afterLeadTimeDate.getDate() + 1);
    resolve(afterLeadTimeDate);
})

export const countDayBetweenFromAndToDate = (DAY, startDate, endDate) => {
    let count = 0;
    while (startDate <= endDate) {
        if (startDate.getDay() === DAY) count++;
        startDate.setDate(startDate.getDate() + 1)
    }
    return Number(count);
}
export const getPlanDurationFromStartDate = (start_date, no_of_days) => {
    if (Number(no_of_days) > 1) {
        const dummyStartDate = new Date(start_date)
        const dummyEndDate = new Date(start_date)
        let count = Number(no_of_days)
        dummyEndDate.setDate(dummyStartDate.getDate() + Number(no_of_days))
        const offDayCount = countDayBetweenFromAndToDate(constants.EVENT_OFF_DAY, dummyStartDate, dummyEndDate);
        count += offDayCount;
        const finalEndDate = new Date(start_date);
        if (count > 0) finalEndDate.setDate(finalEndDate.getDate() + (count - 1))
        if (finalEndDate.getDay() === constants.EVENT_OFF_DAY) finalEndDate.setDate(finalEndDate.getDate() + 1);
        return { startDate: new Date(start_date), endDate: finalEndDate };
    }
    return { startDate: new Date(start_date), endDate: new Date(start_date) };
}

export const generateRandomNumber = (fromNumber = 100000, toNumber = 900000) => Math.floor(fromNumber + Math.random() * toNumber)

export const getFormattedAddress = (addressData) => {
    const addData = {};
    const typeOrder = [
        'street_number',
        'route',
        'administrative_area_level_2',
        'neighborhood',
        'administrative_area_level_1',
        'country',
    ];

    addressData.forEach((e) => {
        e?.address_components?.forEach((tp) => {
            const componentTypes = tp.types;
            typeOrder.forEach((type) => {
                if (componentTypes.includes(type) && !addData[type]) {
                    // addData[type] = tp.long_name;
                    if (type === 'neighborhood') {
                        addData['block_number'] = tp.long_name;
                    } else if (type === 'street_number') {
                        addData['house_number'] = tp.long_name;
                    } else if (type === 'route') {
                        addData['street_name'] = tp.long_name;
                    } else {
                        addData[type] = tp.long_name;
                    }
                }
            });
        })
    });

    return addData;
}

// eslint-disable-next-line no-sequences
// export const jsonToFormData = (jsonObj) => Object.entries(jsonObj).reduce((current, item) => (current.append(...item), current), new FormData())
export const jsonToFormData = (jsonObj) =>
    Object.entries(jsonObj).reduce((formData, [key, value]) => {
        formData.append(key, value);
        return formData;
    }, new FormData());

export const getRandomString = (length = 20) => {
    const randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';
    for (let i = 0; i < length; i++) result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
    return result;
}

export const getRandomFileName = (fileName) => {
    const fileNameArray = fileName?.split('.');
    const extenstion = fileNameArray?.[fileNameArray?.length - 1].toLowerCase();
    return `${getRandomString(40)}.${extenstion}`
}
export const getServerImageUploadFormat = (file) => ({
    name: getRandomFileName(file?.path),
    type: file.mime,
    uri: Platform.OS === 'ios' ? file.path.replace('file://', '') : file.path,
})

export const getWeekNumberFromDate = (passedDate) => {
    const date = new Date(passedDate);
    const startDateDay = moment(date).startOf('month').day();
    const weekNumber = Math.ceil((date.getDate() + startDateDay) / 7);
    return weekNumber > 4 ? weekNumber - 4 : weekNumber;
}

export const getPlanType = (selectedDate, subscriptionData = []) => {
    let planType = constants.PLAN_TYPE.REGULAR;
    if (subscriptionData?.length > 0) {
        const singleSubscriptionData = getSingleSubscriptionData(subscriptionData, selectedDate);
        if (singleSubscriptionData?.is_custom_grams == '1') planType = constants.PLAN_TYPE.CUSTOM_GRAMS;
    }
    return planType;
}

export const checkPermissionForGallery = () => {
    Alert.alert('You do not have library permission', 'Allow Permission from App Settings',
        [
            {
                text: 'Cancel',
                style: 'destructive',
            },
            {
                text: 'Open Setting',
                onPress: openSettings,
                style: 'default',
            },
        ], {
        cancelable: true,
    });
}

export const authSignOut = () => auth().signOut()
export const preFBPhoneAuthConfig = () => {
    // auth().settings.appVerificationDisabledForTesting = constants.FIREBASE_APP_VERIFICATION_DISABLED_FOR_TESTING;
}

export const checkTimezoneIsKuwaitOrNot = () => {
    const date = new Date();
    const offsetInHours = date.getTimezoneOffset() / 60;
    return offsetInHours == 3
}

export const getValidDate = (date = null, format = 'YYYY-MM-DD') => {
    const customisedFormat = format;
    if (moment(date).isValid()) return moment(date).format(customisedFormat)
    return null
}
