import {
    MFPaymentRequest,
    MFCustomerAddress,
    MFExecutePaymentRequest,
    MFLanguage,
    MFMobileCountryCodeISO,
    MFCurrencyISO,
    MFInitiatePayment,
    MFTheme,
    MFSettings,
} from 'myfatoorah-reactnative';
import colors from '../constants/colors';
import { MY_FATOORAH_API_BASE_URL, MY_FATOORAH_API_TOKEN } from '../constants/constants';

const MyFatoorah = {
    confiure: () => {
        const baseURL = MY_FATOORAH_API_BASE_URL;
        const token = MY_FATOORAH_API_TOKEN;
        const theme = new MFTheme(colors.mirageBlack, colors.primary_3, 'Payment', 'Cancel')
        MFSettings.sharedInstance.setTheme(theme)
        MFSettings.sharedInstance.configure(baseURL, token);
    },
    getPaymentMethods: (amount) => new Promise((resolve, reject) => {
        const initiateRequest = new MFInitiatePayment(amount, MFCurrencyISO.KUWAIT_KWD)
        MFPaymentRequest.sharedInstance.initiatePayment(initiateRequest, MFLanguage.ENGLISH, (response) => {
            if (response.getError()) reject(response.getError())
            else resolve(response.getPaymentMethods())
        });
    }),
    executeResquestJson: (amount, paymentMethodId, customerData) => {
        const request = new MFExecutePaymentRequest(parseFloat(amount), paymentMethodId);
        const address = new MFCustomerAddress(customerData?.block ?? '', customerData?.street_name ?? '', customerData?.building ?? '', customerData?.avenue ?? '', customerData?.additional_directions ?? '');
        request.customerName = customerData?.first_name ?? ''; // must be email
        request.customerEmail = customerData?.email ?? 'abc@gmail.com'; // must be email
        request.customerMobile = customerData?.phone ?? '';
        request.customerCivilId = '';
        request.customerAddress = address;
        request.customerReference = '';
        request.language = 'en';
        request.mobileCountryCode = MFMobileCountryCodeISO.KUWAIT;
        request.displayCurrencyIso = MFCurrencyISO.KUWAIT_KWD;
        return request;
    },
    executePayment: (navigation, amount, paymentMethodId, customerData) => new Promise((resolve, reject) => {
        const request = MyFatoorah.executeResquestJson(amount, paymentMethodId, customerData);
        MFPaymentRequest.sharedInstance.executePayment(navigation, request, MFLanguage.ENGLISH, (response) => {
            if (response.getError()) reject(response)
            else resolve(response)
        });
    }),
}
export default MyFatoorah;
