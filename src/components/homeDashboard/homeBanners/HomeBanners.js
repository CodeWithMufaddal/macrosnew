import React, { memo, useMemo, useState } from 'react';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import {
    View, StyleSheet,
} from 'react-native';
import CustomImage from '../../custom/CustomImage';
import { wp } from '../../../helpers/screenHelper'
import { getRandomImagePath } from '../../../services/Services'

const HomeBanners = ({ bannersImage = new Array(2).fill(getRandomImagePath(`${wp(100)}x210`)) }) => {
    const [currentIndex, setCurrentIndex] = useState(0);
    const renderItem = ({ item }) => (
        <View style={{
            height: wp(50),
            width: wp(100),
        }}>
            <CustomImage resizeMode={'cover'} source={{ uri: item }} style={{ height: '100%', width: '100%' }} />
        </View>
    )

    const renderPagination = useMemo(() => (
        <Pagination
            dotsLength={bannersImage?.length ?? 0}
            activeDotIndex={currentIndex}
            containerStyle={{
                position: 'absolute',
                bottom: -20,
                left: 0,
                right: 0,
                alignItems: 'center',
                justifyContent: 'center',
            }}
            dotStyle={{
                width: 10,
                height: 10,
                borderRadius: 5,
                backgroundColor: 'rgba(255, 255, 255, 0.92)',
            }}
            inactiveDotOpacity={0.4}
            inactiveDotScale={0.6}
        />
    ), [bannersImage?.length, currentIndex])
    return (
        <View style={styles.mainContainer}>
            <Carousel
                data={bannersImage}
                renderItem={renderItem}
                sliderWidth={wp(100)}
                itemWidth={wp(100)}
                onSnapToItem={setCurrentIndex}
            />
            {renderPagination}
        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        overflow: 'hidden',
    },
})
export default memo(HomeBanners);
