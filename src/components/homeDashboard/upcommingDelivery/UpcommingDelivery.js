import React, { memo, useCallback } from 'react';
import {
    View, StyleSheet, FlatList, TouchableOpacity,
} from 'react-native';
import CustomText from '../../custom/CustomText';
import i18n from '../../../i18n/i18n';
import colors from '../../../constants/colors';
import { calenderDayType, globalPaddingMargin } from '../../../constants/constants';
import CustomProductSqureCard from '../../custom/CustomProductSqureCard';
import { wp } from '../../../helpers/screenHelper';
import images from '../../../assets/images/images';
import CustomIcon from '../../custom/CustomIcon';
import CustomImage from '../../custom/CustomImage';
import { getValidDate } from '../../../helpers/CommonHelpers';

const UpcommingDelivery = ({
    upcommingDate,
    upcommingDeliveryData,
    selectedDateDayType,
    onCalenderPress = () => { },
}) => {
    const renderUpcommingDelivery = useCallback(({ item, index }) => (
        <View style={{
            width: wp(80),
            marginLeft: index === 0 ? 25 : 0,
        }}>
            <CustomProductSqureCard productData={item} />
        </View>
    ), [])

    const renderEmptyProductList = () => (
        <View style={styles.emptyProductListContainer}>
            <CustomImage SvgSource={images.forkSpoonPlate} />
            {selectedDateDayType === calenderDayType.offDay?.key ? (
                <View style={styles.clickHereContainer}>
                    <CustomText fontProps={['fs-small', 'ff-roboto-medium']} color={colors.primary_3}>{i18n.t('off_day')}</CustomText>
                </View>
            ) : (
                <>
                    {[calenderDayType.noPlanExistDay?.key].includes(selectedDateDayType) ? (
                        <View style={styles.clickHereContainer}>
                            <CustomText fontProps={['fs-small', 'ff-roboto-medium']} color={colors.primary_3}>{i18n.t('no_upcomming_delivery')}</CustomText>
                        </View>
                    ) : (
                        <TouchableOpacity onPress={onCalenderPress} style={styles.clickHereContainer}>
                            <CustomIcon disabled={true} size={27} name={'calendar'} color={colors.primary_2} customStyle={styles.calenderIcon} />
                            <View style={styles.clickHereTextContainer}>
                                <CustomText fontProps={['fs-small', 'ff-roboto-medium']} color={colors.primary_3}>{i18n.t('click_here')}</CustomText>
                                <CustomText fontProps={['fs-small', 'ff-roboto-light']}>{i18n.t('setup_your_meal_schedule')}</CustomText>
                            </View>
                        </TouchableOpacity>
                    )}
                </>
            )}
        </View>
    )
    return (
        <View activeOpacity={0.8} style={styles.mainContainer}>
            {/* Title */}
            <CustomText
                fontProps={['fs-medium', 'ff-roboto-medium']}
                color={colors.primary_2}
                customStyle={styles.title}>
                {i18n.t('upcomming_delivery')}
            </CustomText>

            <CustomText
                fontProps={['fs-small', 'ff-roboto-regular']}
                color={colors.primary_3}
                customStyle={styles.upcommingDate}>
                {getValidDate(upcommingDate, 'DD MMMM YYYY')}
            </CustomText>

            {/* Data with Horizontal FlatList */}
            <FlatList
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                data={upcommingDeliveryData ?? []}
                renderItem={renderUpcommingDelivery}
                style={styles.productFlatList}
                contentContainerStyle={styles.productContentContainerFlatList}
                ListEmptyComponent={renderEmptyProductList}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    title: {
        paddingHorizontal: globalPaddingMargin.whole,
    },
    upcommingDate: {
        marginTop: 5,
        paddingHorizontal: globalPaddingMargin.whole,
    },
    mainContainer: {
        paddingTop: globalPaddingMargin.whole,
        alignItems: 'flex-start',
    },
    productFlatList: {
        paddingTop: 10,
    },
    productContentContainerFlatList: {
    },
    emptyProductListContainer: {
        width: wp(100),
        height: 275,
        alignItems: 'center',
        justifyContent: 'center',
    },
    clickHereContainer: {
        marginTop: globalPaddingMargin.whole,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    clickHereTextContainer: {},
    calenderIcon: {
        marginRight: 10,
    },
})
export default memo(UpcommingDelivery);
