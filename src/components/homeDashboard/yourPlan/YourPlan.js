import React, {
    useCallback, useState, useMemo, memo,
} from 'react';
import {
    View, StyleSheet,
} from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { useDispatch } from 'react-redux';
import CustomText from '../../custom/CustomText';
import i18n from '../../../i18n/i18n';
import colors from '../../../constants/colors';
import { globalPaddingMargin } from '../../../constants/constants';
import CustomPlanCardTwo from '../../custom/CustomPlanCardTwo';
import { wp } from '../../../helpers/screenHelper';
import { generateUniqueId, getValidDate } from '../../../helpers/CommonHelpers';
import CustomButton from '../../custom/CustomButton';
import { changeAppFlow } from '../../../redux/actions/SettingsActions';
import { appFlows } from '../../../navigation/screens';

const YourPlan = ({
    selectedPlanData = [],
}) => {
    const dispatch = useDispatch();
    const [currentIndex, setCurrentIndex] = useState(0);
    const planDuration = useCallback((planDurationData) => i18n.t('fromToDate', { fromDate: getValidDate(planDurationData?.start_time, 'DD MMMM'), toDate: getValidDate(planDurationData?.end_time, 'DD MMMM') }), [])

    const getPlanImages = useCallback((item) => [item?.image_1,
    item?.image_2,
    item?.image_3,
    item?.image_4]?.filter((imageData) => imageData),
        [])

    const renderYourPlans = useCallback(({ item }) => {
        const duration = planDuration(item)
        return (
            <View style={styles.singlePlanContainer}>
                <CustomPlanCardTwo
                    showCurrency={false}
                    imageData={getPlanImages(item)}
                    planData={item}
                    planDuration={duration}
                    planMealType={item?.meal_type_name}
                    planPackageName={item?.goal_name}
                />
            </View>
        )
    }, [getPlanImages, planDuration])

    const renderPagination = useMemo(() => selectedPlanData?.length > 1 && (
        <Pagination
            dotsLength={selectedPlanData?.length ?? 0}
            activeDotIndex={currentIndex}
            containerStyle={{
                width: '100%',
                marginTop: -20,
                alignItems: 'center',
                justifyContent: 'center',
            }}
            dotStyle={{
                width: 7,
                height: 7,
            }}
            inactiveDotColor={colors.primary_3}
            dotColor={colors.primary_2}
            inactiveDotOpacity={0.4}
            inactiveDotScale={0.6}
        />
    ), [currentIndex, selectedPlanData?.length])

    const plansKeyExtractor = useCallback((item, index) => generateUniqueId('homeDashboardYourPlans', index), [])

    const onPurchasePlanPress = useCallback(() => {
        dispatch(changeAppFlow(appFlows.purchaseSubscriptionFlowNavigator))
    }, [dispatch])

    const ListEmptyComponent = useMemo(() => (
        <View style={styles.purchasePlanButtonContainer}>
            <CustomButton
                onPress={onPurchasePlanPress}
                buttonLabel={i18n.t('purchase_plan')}
            />
        </View>
    ), [onPurchasePlanPress])

    return (
        <View activeOpacity={0.8} style={styles.mainContainer}>
            {/* Title */}
            <CustomText
                fontProps={['fs-medium', 'ff-roboto-medium']}
                color={colors.primary_2}
                customStyle={styles.title}>
                {i18n.t('your_plan')}
            </CustomText>

            {/* Data with Horizontal FlatList */}
            <Carousel
                ListEmptyComponent={ListEmptyComponent}
                pagingEnabled={true}
                keyExtractor={plansKeyExtractor}
                data={selectedPlanData ?? []}
                renderItem={renderYourPlans}
                sliderWidth={wp(100)}
                itemWidth={wp(100)}
                onSnapToItem={setCurrentIndex}
            />
            {renderPagination}
        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        paddingTop: globalPaddingMargin.whole,
        alignItems: 'flex-start',
    },
    title: {
        marginBottom: 10,
        paddingHorizontal: globalPaddingMargin.horizontal,
    },
    singlePlanContainer: {
        flex: 1,
        paddingHorizontal: globalPaddingMargin.horizontal,
        width: wp(100),
    },
    purchasePlanButtonContainer: {
        width: wp(100),
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        marginVertical: 15,
    },
})
export default memo(YourPlan);
