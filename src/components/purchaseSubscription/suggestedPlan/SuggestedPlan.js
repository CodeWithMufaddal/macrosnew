import React, { useCallback } from 'react';
import {
    View, StyleSheet,
} from 'react-native';
import CustomText from '../../custom/CustomText';
import i18n from '../../../i18n/i18n';
import colors from '../../../constants/colors';
import { generateUniqueId } from '../../../helpers/CommonHelpers';
import { globalPaddingMargin } from '../../../constants/constants';
import CustomPlanCardOne from '../../custom/CustomPlanCardOne';
import CustomLoader from '../../custom/CustomLoader';

const SuggestedPlan = ({
    suggestedPlanData = [],
    selectedPlan = null,
    loading = false,
    onPlanPress = () => { },
}) => {
    const getPlanImages = useCallback((item) => [item?.image_1,
        item?.image_2,
        item?.image_3,
        item?.image_4]?.filter((imageData) => imageData), [])

    const renderSuggestedPlan = useCallback((item) => (
        <CustomPlanCardOne
            planData={item}
            imageData={getPlanImages(item)}
            selected={selectedPlan?.subscription_id === item?.subscription_id}
            onPress={onPlanPress}
        />
    ), [getPlanImages, onPlanPress, selectedPlan?.subscription_id])

    return (
        <View activeOpacity={0.8} style={styles.mainContainer}>
            {/* Title */}
            <CustomText
                fontProps={['fs-small']}
                color={colors.primary_2}
                customStyle={styles.title}>
                {i18n.t('suggested_plan')}
            </CustomText>

            {/* Data with Horizontal FlatList */}
            <View style={{ width: '100%', marginVertical: globalPaddingMargin.top, paddingHorizontal: globalPaddingMargin.horizontal }}>
                {loading ? (
                    <View style={styles.loaderContainer}>
                        <CustomLoader />
                    </View>
                ) : (
                    <>
                        {suggestedPlanData?.length > 0
                            ? suggestedPlanData?.map((item, index) => (
                                <View key={generateUniqueId('suggestedPlanData', index)}>
                                    {renderSuggestedPlan(item)}
                                </View>
                            )) : (
                                <View style={styles.noDataTextContainer}>
                                    <CustomText>{i18n.t('no_plans_there')}</CustomText>
                                </View>
                            )}
                    </>
                )}

            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        paddingTop: globalPaddingMargin.top,
        alignItems: 'flex-start',
    },
    title: {
        paddingHorizontal: globalPaddingMargin.horizontal,
    },
    loaderContainer: {
        justifyContent: 'flex-start',
        width: '100%',
    },
    noDataTextContainer: {
        marginVertical: 10,
    },
})
export default SuggestedPlan;
