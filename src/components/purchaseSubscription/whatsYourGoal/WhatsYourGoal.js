import React, { useCallback, useState } from 'react';
import {
    View, StyleSheet, FlatList, Alert, TouchableOpacity,
} from 'react-native';
import CustomText from '../../custom/CustomText';
import i18n from '../../../i18n/i18n';
import colors from '../../../constants/colors';
import CustomCardOne from '../../custom/CustomCardOne';
import { CommonHorizontalItemSeparator, generateUniqueId } from '../../../helpers/CommonHelpers';
import { globalPaddingMargin } from '../../../constants/constants';
import { getImagePath, imagePathType } from '../../../assets/images/images';
import MealExplorerModal from '../../mealExplorerModal/MealExplorerModal';

const WhatsYourGoal = ({
    packageData = [],
    onGoalPress = () => { },
    selectedGoal = null,
}) => {
    const [showMealExplorerModal, setShowMealExplorerModal] = useState(false);

    const renderGoalData = ({ item }) => (
        <CustomCardOne
            showInfoButton={false}
            selected={item?.id === selectedGoal?.id}
            title={item?.name}
            data={item}
            showInfo
            image={item?.dummy ? item?.image : { uri: getImagePath(imagePathType.packages, item?.image) }}
            onInfoButtonPress={() => Alert.alert(item?.name, item?.sub_title)}
            onPress={() => onGoalPress(item)}
        />
    )

    const goalDataKeyExtractor = useCallback((item, index) => generateUniqueId('goalData', index), []);

    return (
        <View activeOpacity={0.8} style={styles.mainContainer}>
            <MealExplorerModal showModal={showMealExplorerModal} setShowModal={setShowMealExplorerModal} />
            <View style={{
                flexDirection: 'row', width: '100%', justifyContent: 'space-between', alignItems: 'center',
            }}>
                {/* Title */}
                <CustomText
                    fontProps={['fs-small']}
                    color={colors.primary_2}
                    customStyle={styles.title}>
                    {i18n.t('whats_your_goal')}
                </CustomText>
                <TouchableOpacity
                    style={{
                        backgroundColor: colors.primary_3, marginRight: 25, paddingVertical: 5, borderRadius: 50,
                    }}
                    onPress={() => setShowMealExplorerModal(true)}
                >
                    <CustomText
                        fontProps={['fs-small']}
                        color={colors.white}
                        customStyle={styles.title}>
                        {i18n.t('meal_explorer')}
                    </CustomText>
                </TouchableOpacity>
            </View>
            {/* Data with Horizontal FlatList */}
            <FlatList
                ItemSeparatorComponent={CommonHorizontalItemSeparator}
                style={styles.flatListStyle}
                contentContainerStyle={styles.flatListContentContainerStyle}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                horizontal={true}
                data={packageData}
                renderItem={renderGoalData}
                keyExtractor={goalDataKeyExtractor}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        paddingTop: globalPaddingMargin.whole,
        alignItems: 'flex-start',
    },
    title: {
        paddingHorizontal: globalPaddingMargin.horizontal,
    },
    flatListStyle: {
        marginVertical: globalPaddingMargin.top,
    },
    flatListContentContainerStyle: {
        paddingHorizontal: globalPaddingMargin.horizontal,
    },
})
export default WhatsYourGoal;
