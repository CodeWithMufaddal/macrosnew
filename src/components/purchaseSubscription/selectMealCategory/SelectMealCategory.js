import React, { memo, useCallback, useMemo, useState } from 'react';
import {
    View, StyleSheet, FlatList, TouchableOpacity,
} from 'react-native';
import CustomText from '../../custom/CustomText';
import i18n from '../../../i18n/i18n';
import colors from '../../../constants/colors';
import CustomCardOne from '../../custom/CustomCardOne';
import { CommonHorizontalItemSeparator, generateUniqueId } from '../../../helpers/CommonHelpers';
import { globalPaddingMargin } from '../../../constants/constants';
import CustomLoader from '../../custom/CustomLoader';
import { getImagePath, imagePathType } from '../../../assets/images/images';
import CustomIcon from '../../custom/CustomIcon';
import DiatCitationModal from '../../diatCitationModal/DiatCitationModal';

const SelectMealPlanDuration = ({
    mealCategoryData = [],
    selectedMealCategory = null,
    loading = false,
    setSelectedMealCategory = () => { },
}) => {
    const [showDiatCitationModal, setShowDiatCitationModal] = useState(false);
    const renderGoalData = ({ item }) => (
        <CustomCardOne
            title={item?.name}
            image={{ uri: getImagePath(imagePathType.mealTypes, item?.image) }}
            selected={item?.id === selectedMealCategory?.id}
            selectedCardBackgroundColor={colors.primary_2}
            titleStyle={{ color: item?.id === selectedMealCategory?.id ? colors.white : colors.black }}
            onPress={() => setSelectedMealCategory(item)}
        />
    )
    const ListEmptyComponent = useMemo(() => (
        <View style={styles.noDataTextContainer}>
            <CustomText>{i18n.t('no_meal_category_there')}</CustomText>
        </View>
    ), [])

    const onInfoButtonPress = useCallback(() => {
        setShowDiatCitationModal(true)
    }, [])

    const mealCategoryDataKeyExtractor = useCallback((item, index) => generateUniqueId('mealCategoryData', index), []);
    return (
        <View activeOpacity={0.8} style={styles.mainContainer}>
            <DiatCitationModal
                showDiatCitationModal={showDiatCitationModal}
                setShowDiatCitationModal={setShowDiatCitationModal}
            />
            {/* Title */}
            <View style={{ flexDirection: 'row' }}>
                <CustomText
                    fontProps={['fs-small']}
                    color={colors.primary_2}
                    customStyle={styles.title}>
                    {i18n.t('please_select_meal_category')}
                </CustomText>
                <TouchableOpacity style={styles.iButtonContainer} onPress={onInfoButtonPress}>
                    <CustomIcon name="infocirlce" color={colors.charcolGrey} size={15} />
                </TouchableOpacity>
            </View>
            {loading ? (
                <View style={styles.loaderContainer}>
                    <CustomLoader />
                </View>
            ) : (
                <FlatList
                    ItemSeparatorComponent={CommonHorizontalItemSeparator}
                    style={styles.flatListStyle}
                    contentContainerStyle={styles.flatListContentContainerStyle}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    horizontal={true}
                    ListEmptyComponent={ListEmptyComponent}
                    data={mealCategoryData}
                    renderItem={renderGoalData}
                    keyExtractor={mealCategoryDataKeyExtractor}
                />
            )}
            {/* Data with Horizontal FlatList */}

        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        alignItems: 'flex-start',
        paddingTop: globalPaddingMargin.top,
    },
    title: {
        paddingLeft: globalPaddingMargin.horizontal,
        paddingRight: 10,
    },
    flatListStyle: {
        marginVertical: globalPaddingMargin.top,
    },
    flatListContentContainerStyle: {
        paddingHorizontal: globalPaddingMargin.horizontal,
    },
    loaderContainer: {
        height: 162,
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
    },
    noDataTextContainer: {
        marginVertical: 10,
    },
    iButtonContainer: {},
})
export default memo(SelectMealPlanDuration);
