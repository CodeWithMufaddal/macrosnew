import React, { memo, useCallback, useMemo } from 'react';
import {
    TouchableOpacity, StyleSheet, View,
} from 'react-native';
import colors from '../../../constants/colors';
import { EVENT_OFF_DAY, globalPaddingMargin } from '../../../constants/constants';
import i18n from '../../../i18n/i18n';
import { calenderDayStyle } from '../../../styles/CommonStyles';
import CustomFullCalender from '../../custom/CustomFullCalender';
import CustomIcon from '../../custom/CustomIcon';
import CustomButton from '../../custom/CustomButton';
import CustomModal from '../../custom/CustomModal'
import CustomText from '../../custom/CustomText';
import { getHitSlop, getValidDate } from '../../../helpers/CommonHelpers';

const SelectStartDate = ({
    showSelectDateModal = false,
    startDate = null,
    setStartDate = () => { },
    onCloseModal = () => { },
    onPressCalenderConfirm = () => { },
    minDate = new Date(),
}) => {
    const myOnHide = useCallback(() => {
        onCloseModal();
    }, [onCloseModal])

    const onDayPress = useCallback((dateProps) => setStartDate(dateProps?.dateString), [])
    const markedDates = useMemo(() => ({ [startDate]: calenderDayStyle.selectedDate }), [startDate])

    const renderCalender = useMemo(() => (
        <CustomFullCalender
            minDate={minDate}
            disabledDaysIndexes={[EVENT_OFF_DAY]}
            selectedCurrentDate={startDate ?? minDate}
            onDayPress={onDayPress}
            markedDates={markedDates}
        />
    ), [markedDates, minDate, onDayPress, startDate])

    return (
        <CustomModal
            onHide={myOnHide}
            isVisible={showSelectDateModal}
        >
            <View style={styles.mainContainer}>
                <View style={styles.contentContainer}>
                    <View style={styles.headerContainer}>
                        <TouchableOpacity onPress={myOnHide} hitSlop={getHitSlop(20)}>
                            <CustomText
                                fontProps={['fs-small', 'ff-roboto-medium']}
                                customStyle={styles.cancelButtonText}
                                color={colors.primary_3}>
                                {i18n.t('cancel')}
                            </CustomText>
                        </TouchableOpacity>
                    </View>

                    <View>
                        <View style={styles.selectDateContainer}>
                            <CustomText fontProps={['ff-small']} color={colors.lightGrey}>{i18n.t('start_date')}</CustomText>
                            <View style={styles.selectDateChildContainer}>
                                <CustomText fontProps={['ff-roboto-medium', 'fs-small']} color={colors.lightBlack}>
                                    {startDate ? getValidDate(startDate, 'DD MMMM YYYY') : i18n.t('select_start_date')}
                                </CustomText>
                                <CustomIcon name={'calendar'} color={colors.primary_2} />
                            </View>
                        </View>
                    </View>

                    {/* Calender View */}
                    <View style={styles.calenderContainer}>
                        {renderCalender}
                        <CustomButton
                            disabled={!startDate}
                            customStyle={styles.confirmButton}
                            buttonLabel={i18n.t('confirm')}
                            onPress={onPressCalenderConfirm}
                        />
                    </View>
                </View>
            </View>
        </CustomModal>
    )
}
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    contentContainer: {
        backgroundColor: colors.white,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        justifyContent: 'flex-end',
        padding: globalPaddingMargin.whole,
    },
    headerContainer: {
        alignItems: 'flex-end',
    },
    cancelButtonText: {
        textTransform: 'uppercase',
    },
    selectDateContainer: {
        marginVertical: 10,
    },
    selectDateChildContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderColor: colors.xLightGrey,
    },
    calenderContainer: {

    },
    confirmButton: {
        marginVertical: 10,
    },
})
export default memo(SelectStartDate);
