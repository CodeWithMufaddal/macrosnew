import React, { useCallback, useMemo } from 'react';
import {
    View, StyleSheet, FlatList,
} from 'react-native';
import CustomText from '../../custom/CustomText';
import i18n from '../../../i18n/i18n';
import colors from '../../../constants/colors';
import { CommonHorizontalItemSeparator, generateUniqueId } from '../../../helpers/CommonHelpers';
import { globalPaddingMargin } from '../../../constants/constants';
import CustomTicket from '../../custom/CustomTicket';
import CustomLoader from '../../custom/CustomLoader';

const SelectMealPlanDuration = ({
    mealPlanDurationData = [],
    selectedMealPlanDuration = null,
    loading = false,
    onMealPlanDurationPress = () => { },
}) => {
    // Render Meal Plan Duration
    const renderMealPlanDuration = ({ item }) => (
        <CustomTicket
            title={item?.days}
            selected={item?.days === selectedMealPlanDuration?.days}
            onPress={() => onMealPlanDurationPress(item)}
        />
    )

    // Empty Component For Meal Plan Duration
    const ListEmptyComponent = useMemo(() => (
        <View style={styles.noDataTextContainer}>
            <CustomText>{i18n.t('no_duration_days_there')}</CustomText>
        </View>
    ), [])
    // Key Extractor For Meal Plan Duration FlatList
    const mealPlanDurationDataKeyExtractor = useCallback((item, index) => generateUniqueId('mealPlanDurationData', index), []);

    return (
        <View activeOpacity={0.8} style={styles.mainContainer}>
            {/* Title */}
            <CustomText
                fontProps={['fs-small']}
                color={colors.primary_2}
                customStyle={styles.title}>
                {i18n.t('duration_of_the_meal_plan')}
            </CustomText>

            {/* Data with Horizontal FlatList And Loading */}
            {loading ? (
                <View style={styles.loaderContainer}>
                    <CustomLoader />
                </View>
            ) : (
                <FlatList
                    ItemSeparatorComponent={CommonHorizontalItemSeparator}
                    style={styles.flatListStyle}
                    contentContainerStyle={styles.flatListContentContainerStyle}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    horizontal={true}
                    data={mealPlanDurationData}
                    ListEmptyComponent={ListEmptyComponent}
                    renderItem={renderMealPlanDuration}
                    keyExtractor={mealPlanDurationDataKeyExtractor}
                />
            )}
        </View>
    )
}
const styles = StyleSheet.create({
    mainContainer: {
        alignItems: 'flex-start',
        paddingTop: globalPaddingMargin.top,
    },
    title: {
        paddingHorizontal: globalPaddingMargin.horizontal,
    },
    flatListStyle: {
        marginVertical: globalPaddingMargin.top,
    },
    flatListContentContainerStyle: {
        paddingHorizontal: globalPaddingMargin.horizontal,
    },
    loaderContainer: {
        height: 86,
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
    },
    noDataTextContainer: {
        marginVertical: 10,
    },
})

export default SelectMealPlanDuration;
