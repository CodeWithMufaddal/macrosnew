import React, { memo, useCallback, useMemo, useState } from 'react'
import { TouchableOpacity, View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import RNRestart from 'react-native-restart'
import colors from '../../constants/colors'
import { APP_LANGUAGES } from '../../i18n/i18n'
import { changeAppLanguageAction } from '../../redux/actions/SettingsActions'
import styles from './styles'
import CustomText from '../custom/CustomText'

const ChangeLanguage = ({ onChangeLanguage = () => { }, style }) => {
    const dispatch = useDispatch();
    const appLanguage = useSelector((state) => state?.settings?.appLanguage)
    const isEnglish = useMemo(() => appLanguage === APP_LANGUAGES.EN, [appLanguage])
    const onChangeLanguagePress = useCallback(() => {
        onChangeLanguage()
        const changeToLanguage = appLanguage === APP_LANGUAGES.EN ? APP_LANGUAGES?.AR : APP_LANGUAGES?.EN;
        dispatch(changeAppLanguageAction(changeToLanguage, false)).then(() => {
            setTimeout(() => RNRestart.Restart(), 500);
        });
    }, [appLanguage, dispatch, onChangeLanguage]);

    return (
        <TouchableOpacity onPress={onChangeLanguagePress} style={{ ...styles.mainContainer, ...style }}>
            <View style={{ ...styles.langTextContainer, backgroundColor: isEnglish ? colors.primary_3 : colors.lightGrey }}>
                <CustomText color={colors.white}>En</CustomText>
            </View>
            <View style={{ ...styles.langTextContainer, backgroundColor: !isEnglish ? colors.primary_3 : colors.lightGrey }}>
                <CustomText color={colors.white}>Ar</CustomText>
            </View>
        </TouchableOpacity>
    )
}
export default memo(ChangeLanguage)
