import React, { useMemo, useCallback, useEffect, useState } from 'react';
import {
    View, StyleSheet,
} from 'react-native';
import { connect, useDispatch } from 'react-redux';
import CustomHeader from '../../components/custom/CustomHeader';
import CustomText from '../../components/custom/CustomText';
import colors from '../../constants/colors';
import CustomButton from '../../components/custom/CustomButton';
import i18n from '../../i18n/i18n';
import { commonShadow } from '../../styles/CommonStyles';
import CustomKeyboardAvoidingView from '../../components/custom/CustomKeyboardAvoidingView';
import CustomIcon, { IconType } from '../../components/custom/CustomIcon';
import { navigateGoBack, navigateTo } from '../../navigation/navigation';
import { isRTL } from '../../constants/constants';
import screens from '../../navigation/screens';
import CustomTextInputThree from '../../components/custom/CustomTextInputThree';
import _ from 'lodash'
import { getMyGramsAction, saveMyGramsAction } from '../../redux/actions/SubscriptionActions';
import CustomTextInput from '../../components/custom/CustomTextInput';
import CustomLoader from '../custom/CustomLoader';
import { goBack } from '../../navigation/RootNavigation';

const InputsRow = ({
    title,
    gmsValue, mealTypeValue,
    handleGmsChangeText, handleMealTypChangeText,
    extraGmsProps, extraMealTypeProps,
}) => (
    <View style={styles.inputRowContainer}>
        <CustomText
            fontProps={['fs-medium', 'ff-roboto-medium']}
            customStyle={{ width: '25%' }}
            color={colors.primary_2}>
            {title}
        </CustomText>
        <CustomTextInputThree
            {...extraGmsProps}
            value={gmsValue}
            onChangeText={handleGmsChangeText}
            keyboardType='number-pad'
            placeHolder={i18n.t('enterGrams')}
        />
        <CustomTextInputThree
            {...extraMealTypeProps}
            value={mealTypeValue}
            onChangeText={handleMealTypChangeText}
            placeHolder={i18n.t('mealType')}
        />

    </View>
)

const SelectGramsView = ({ subscriptionData = null, readOnly = false, ...props }) => {
    const [mealsData, setMealsData] = useState();
    const dispatch = useDispatch();
    const [inputRefs, setInputRefs] = useState([]);
    const [notes, setNotes] = useState('');
    const [isLoading, setIsLoading] = useState(true);
    let currentIndex = 0

    useEffect(() => {
        getGramsData()
    }, [subscriptionData])

    const bindMealsData = (data) => {
        let mData = _.cloneDeep(data)
        let totalInputsGroup = 0;
        data.map((item, index) => {
            let qty = Number(item?.meal_data?.length) ?? 0
            totalInputsGroup += qty
            mData[index].value = qty

            const mealGrams = []
            new Array(qty).fill('').map((gramItem, gramIndex) => {
                mealGrams.push({
                    protein: item?.meal_data?.[gramIndex]?.protein,
                    protein_type: item?.meal_data?.[gramIndex]?.protein_type,
                    carbohydrates: item?.meal_data?.[gramIndex]?.carbohydrates,
                    carbohydrates_type: item?.meal_data?.[gramIndex]?.carbohydrates_type,
                    fats: item?.meal_data?.[gramIndex]?.fats,
                    fats_type: item?.meal_data?.[gramIndex]?.fats_type,
                    dish_category_id: item?.id,
                })
            })
            mData[index].meal_data = mealGrams
        })
        setInputRefs(new Array(totalInputsGroup * 6).fill(null).map(() => React.createRef()));
        setMealsData(mData);
    }

    const bindNewMealsData = () => {
        let mData = _.cloneDeep(subscriptionData?.meals_Data)
        let totalInputsGroup = 0;
        mData.map((item, index) => {
            let qty = Number(item?.value) ?? 0
            totalInputsGroup += qty
            mData[index].value = qty
            mData[index].name = item?.title
            delete item?.title
            const mealGrams = []
            new Array(qty).fill('').map(() => mealGrams.push({
                protein: null,
                protein_type: null,
                carbohydrates: null,
                carbohydrates_type: null,
                fats: null,
                fats_type: null,
                dish_category_id: item?.id
            }))
            mData[index].meal_data = mealGrams
        })
        setInputRefs(new Array(totalInputsGroup * 6).fill(null).map(() => React.createRef()));
        setMealsData(mData);
    }

    const getGramsData = () => {
        const selectedGoalID = subscriptionData?.goal_ids;
        const selectedPlanID = subscriptionData?.payment_item_ids;
        const selectedMealCategoryId = subscriptionData?.meal_type_id;
        let data = {
            package_ids: selectedGoalID,
            plan_subscription_ids: selectedPlanID,
            meal_type_id: selectedMealCategoryId,
        }
        dispatch(getMyGramsAction(data)).then((res) => {
            if (res?.data?.length > 0) bindMealsData(res?.data)
            else bindNewMealsData()
            setNotes(res?.notes)
        }).finally(() => {
            setIsLoading(false)
        })
    }



    const onContinuePress = useCallback(() => {
        const selectedGoalID = subscriptionData?.goal_ids;
        const selectedPlanID = subscriptionData?.payment_item_ids;
        const selectedMealCategoryId = subscriptionData?.meal_type_id;
        let gramsData = []
        mealsData.map((mealItem) => {
            gramsData = [...gramsData, ...mealItem.meal_data]
        })
        let data = {
            package_ids: selectedGoalID,
            plan_subscription_ids: selectedPlanID,
            meal_type_id: selectedMealCategoryId,
            data: gramsData,
            notes,
        }
        dispatch(saveMyGramsAction(data)).then(()=>{
            getGramsData()
            goBack()
        })
    }, [subscriptionData, mealsData, notes]);

    const onBackPress = useCallback(() => {
        navigateGoBack();
    }, [])


    const renderContinueButton = useMemo(() => (
        <View style={styles.bottomButtonContainer}>
            <CustomButton
                buttonLabel={props?.saveMyGrams?.loading ? i18n.t('saving') : i18n.t('updateMacrosGrams')}
                onPress={onContinuePress}
                disabled={props?.saveMyGrams?.loading}
            />
        </View>
    ), [onContinuePress, props?.saveMyGrams?.loading])

    const onSubmitEditing = useCallback((currentIndex) => {
        if (inputRefs?.[currentIndex + 1]) inputRefs?.[currentIndex + 1]?.current?.focus()
    }, [inputRefs])

    const changeInputFocus = useCallback((index) => ({
        textInputRef: inputRefs[index],
        onSubmitEditing: () => onSubmitEditing(index),
    }), [inputRefs, onSubmitEditing])

    const onTextChange = useCallback((mealTypeIndex, mealCounterIndex, mealGramsType, value) => {
        const mData = _.cloneDeep(mealsData)
        mData[mealTypeIndex].meal_data[mealCounterIndex][mealGramsType] = value
        setMealsData(mData)
    }, [mealsData])

    const netTotalOfGrams = useMemo(() => {
        let totalCalories = 0
        let totalGrams = 0
        let totalProtein = 0
        let totalCarbs = 0
        let totalFats = 0
        let totalOfEachMealGrams = []
        if (mealsData?.length > 0) {
            mealsData.map((mealItem, mealIndex) => {
                totalOfEachMealGrams[mealIndex] = new Array(Number(mealItem?.value))
                mealItem.meal_data.map((gramItem, gramIndex) => {
                    totalProtein = totalProtein + Number(gramItem?.protein ?? 0)
                    totalCarbs = totalCarbs + Number(gramItem?.carbohydrates ?? 0)
                    totalFats = totalFats + Number(gramItem?.fats ?? 0)
                    let totalGms = Number(gramItem?.protein) + Number(gramItem?.carbohydrates) + Number(gramItem?.fats)
                    let totalCal = (Number(gramItem?.protein) * 4) + (Number(gramItem?.carbohydrates) * 4) + (Number(gramItem?.fats) * 9)
                    totalOfEachMealGrams[mealIndex][gramIndex] = { totalGms, totalCal }
                    totalGrams += totalGms
                    totalCalories += totalCal
                })
            })
        }
        return { totalCalories, totalGrams, totalOfEachMealGrams, totalProtein, totalCarbs, totalFats }
    }, [mealsData])

    const renderInputs = useCallback((item, index) => {
        return (
            <>
                {item?.meal_data?.length > 0 && item?.meal_data.map((gramItem, gramIndex, wholeGramsData) => {
                    const handleProteinChange = (val) => onTextChange(index, gramIndex, 'protein', val)
                    const handleProteinMealTypeChange = (val) => onTextChange(index, gramIndex, 'protein_type', val)
                    const handleCarbsChange = (val) => onTextChange(index, gramIndex, 'carbohydrates', val)
                    const handleCarbsMealTypeChange = (val) => onTextChange(index, gramIndex, 'carbohydrates_type', val)
                    const handleFatsChange = (val) => onTextChange(index, gramIndex, 'fats', val)
                    const handleFatsMealTypeChange = (val) => onTextChange(index, gramIndex, 'fats_type', val)

                    let lastCurrentIndex = currentIndex
                    currentIndex = currentIndex + 6

                    return (
                        <View style={styles.mealContainer} key={`selectGramsInputs-${gramIndex}`}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                <CustomText fontProps={['fs-medium', 'ff-roboto-medium']} color={colors.primary_3}>{item?.name} {gramIndex + 1}</CustomText>
                                <CustomText fontProps={['fs-medium', 'ff-roboto-medium']} color={colors.primary_3}>
                                    {netTotalOfGrams?.totalOfEachMealGrams?.[index]?.[gramIndex]?.totalCal ?? 0} cal
                                </CustomText>
                            </View>
                            {readOnly ? (
                                <>
                                    <View style={{ alignItems: 'flex-start', flexDirection: 'row', paddingTop: 10 }}>
                                        <View>
                                            <CustomText customStyle={styles.gramsText} fontProps={['fs-medium', 'ff-roboto-medium']} color={colors.primary_2}>
                                                {i18n.t('protein')} : {gramItem?.protein ? `${gramItem?.protein}gms` : 'none'} {gramItem?.protein_type ? `| ${gramItem?.protein_type}` : null}
                                            </CustomText>
                                            <CustomText customStyle={styles.gramsText} fontProps={['fs-medium', 'ff-roboto-medium']} color={colors.primary_2}>
                                                {i18n.t('carbs')} : {gramItem?.carbohydrates ? `${gramItem?.carbohydrates}gms` : 'none'} {gramItem?.carbohydrates_type ? `| ${gramItem?.carbohydrates_type}` : null}
                                            </CustomText>
                                            <CustomText customStyle={styles.gramsText} fontProps={['fs-medium', 'ff-roboto-medium']} color={colors.primary_2}>
                                                {i18n.t('fats')} : {gramItem?.fats ? `${gramItem?.fats}gms` : 'none'}  {gramItem?.fats_type ? `| ${gramItem?.fats_type}` : null}
                                            </CustomText>
                                        </View>
                                    </View>
                                </>
                            ) : (
                                <>
                                    {/* Proteins */}
                                    <InputsRow
                                        title={i18n.t('protein')}
                                        extraGmsProps={changeInputFocus(lastCurrentIndex)}
                                        extraMealTypeProps={changeInputFocus(lastCurrentIndex + 1)}
                                        gmsValue={gramItem?.protein}
                                        mealTypeValue={gramItem?.protein_type}
                                        handleGmsChangeText={handleProteinChange}
                                        handleMealTypChangeText={handleProteinMealTypeChange}
                                    />

                                    {/* Carbs */}
                                    <InputsRow
                                        title={i18n.t('carbs')}
                                        extraGmsProps={changeInputFocus(lastCurrentIndex + 2)}
                                        extraMealTypeProps={changeInputFocus(lastCurrentIndex + 3)}
                                        gmsValue={gramItem?.carbohydrates}
                                        mealTypeValue={gramItem?.carbohydrates_type}
                                        handleGmsChangeText={handleCarbsChange}
                                        handleMealTypChangeText={handleCarbsMealTypeChange}
                                    />

                                    {/* Fats */}
                                    <InputsRow
                                        title={i18n.t('fats')}
                                        extraGmsProps={changeInputFocus(lastCurrentIndex + 4)}
                                        extraMealTypeProps={changeInputFocus(lastCurrentIndex + 5)}
                                        gmsValue={gramItem?.fats}
                                        mealTypeValue={gramItem?.fats_type}
                                        handleGmsChangeText={handleFatsChange}
                                        handleMealTypChangeText={handleFatsMealTypeChange}
                                    />
                                </>
                            )}
                        </View>
                    )
                })}
            </>
        )
    }, [onTextChange, changeInputFocus, currentIndex, netTotalOfGrams?.totalOfEachMealGrams])

    const renderMealsData = useMemo(() => (
        <>
            {mealsData?.map((item, index) => (
                <View key={`mealTypeName${index}`}>
                    {renderInputs(item, index)}
                </View>
            ))}
        </>
    ), [mealsData, renderInputs])

    const renderNotesSection = useMemo(() => (
        <View style={styles.notesSectionContainer}>
            <CustomText
                fontProps={['fs-small']}
                color={colors.primary_2}>

                {i18n.t('notes')}
            </CustomText>
            <CustomTextInput
                editable={!readOnly}
                value={readOnly ? notes ?? i18n.t('no_notes') : notes}
                onChangeText={setNotes}
                multiline={true}
                customContainerStyle={{ height: 100, borderWidth: 0, marginTop: 10, borderRadius: 10, padding: 10, paddingHorizontal: 10, ...commonShadow() }}
                placeHolder={i18n.t('notes')}
            />
        </View>
    ), [notes, readOnly])

    const renderTotals = useMemo(() => (
        <>
            <View style={{ flexDirection: 'row', marginBottom: 10, alignItems: 'center' }}>
                <CustomText fontProps={['fs-medium', 'ff-roboto-medium']} color={colors.primary_3}>{i18n.t('yourTotalMacros')} = </CustomText>
                <CustomText fontProps={['fs-medium', 'ff-roboto-medium']} color={colors.primary_2}>{netTotalOfGrams?.totalProtein}P {netTotalOfGrams?.totalCarbs}C {netTotalOfGrams?.totalFats}F</CustomText>
            </View>
            <View style={{ flexDirection: 'row', marginBottom: 10, alignItems: 'center' }}>
                <CustomText fontProps={['fs-medium', 'ff-roboto-medium']} color={colors.primary_3}>{i18n.t('yourTotalCalories')} = </CustomText>
                <CustomText fontProps={['fs-medium', 'ff-roboto-medium']} color={colors.primary_2}>{netTotalOfGrams?.totalCalories} Calories</CustomText>
            </View>
        </>
    ), [netTotalOfGrams])

    return (
        <View style={styles.mainContainer}>
            {isLoading && <CustomLoader wholeScreen={true} />}
            <CustomKeyboardAvoidingView
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                extraHeight={200}
                style={styles.contentContainer}>
                <View style={styles.content}>
                    {renderMealsData}
                    {renderTotals}
                    {renderNotesSection}
                </View>
                {/* Continue Button */}
                {mealsData?.length > 0 && !readOnly && renderContinueButton}
            </CustomKeyboardAvoidingView>
        </View>
    )
}

const headerStyles = StyleSheet.create({
    headerLeftContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
})
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: colors.appBackground,
    },
    contentContainer: {
        flex: 1,
        backgroundColor: colors.appBackground,
    },
    content: {
        padding: 20,
    },
    bottomButtonContainer: {
        paddingVertical: 15,
        paddingBottom: 20,
        ...commonShadow({ offsetHeight: -2 }),
    },
    backButton: {
        marginRight: 15,
    },
    mealInputWrapperContainer: {
        flex: 1,
        paddingVertical: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    mealContainer: {
        backgroundColor: colors.white,
        padding: 20,
        borderRadius: 10,
        marginBottom: 20,
    },
    inputRowContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 10,
    },
    gramsText: {
        marginVertical: 2,
    }
})

export default connect((state) => ({
    saveMyGrams: state.subscription.saveMyGrams,
}))(SelectGramsView);
