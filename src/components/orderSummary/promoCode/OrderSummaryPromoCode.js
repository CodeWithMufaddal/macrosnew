import React from 'react';
import {
    View, StyleSheet, TextInput, TouchableOpacity,
} from 'react-native';
import CustomText from '../../custom/CustomText';
import i18n from '../../../i18n/i18n';
import colors from '../../../constants/colors';
import { appTextInputType, globalPaddingMargin } from '../../../constants/constants';
import { fontsSize, getFontsFamily, getFontsSize } from '../../../assets/fonts/fonts';
import { commonShadow, getTextInputRTLDirection } from '../../../styles/CommonStyles';
import { getAmountWithCurrency } from '../../../helpers/CommonHelpers';

const OrderSummaryPromoCode = ({
    onApplyPromoPress = () => { },
    onRemovePress = () => { },
    setCoupon = () => { },
    couponDiscount = null,
    coupon = '',
}) => (
    <View style={styles.mainWrapperContainer}>
        {/* Title */}
        <CustomText
            fontProps={['fs-small']}
            color={colors.primary_2}
            customStyle={styles.title}>
            {i18n.t('promo_or_referral_code')}
        </CustomText>
        <View style={styles.mainContainer}>
            <TextInput
                autoCorrect={false}
                autoCapitalize={'none'}
                onChangeText={setCoupon}
                value={coupon}
                placeholder={i18n.t('promo_or_referral_code')}
                placeholderTextColor={colors.xLightGrey}
                style={{
                    ...getTextInputRTLDirection(appTextInputType.TEXT,),
                    ...styles.textInput,
                }}
            />
            <TouchableOpacity style={styles.applyButtonContainer} onPress={onApplyPromoPress}>
                <CustomText
                    fontProps={['ff-roboto-medium', 'fs-small']}
                    color={colors.primary_3}
                    customStyle={styles.applyButton}>
                    {i18n.t('apply')}
                </CustomText>
            </TouchableOpacity>
        </View>
        <View style={styles.promoStatusContainer}>
            <CustomText
                customStyle={styles.promoCodeAppliedFor}
                fontProps={['ff-roboto-light']}>
                {i18n.t('promo_or_referral_code_applied_for')}
                <CustomText color={couponDiscount > 0 ? colors.successGreen : colors.primary_3} fontProps={['ff-roboto-bold']}> {getAmountWithCurrency(couponDiscount)}</CustomText>
            </CustomText>
            {couponDiscount > 0 && (
                <TouchableOpacity onPress={onRemovePress}>
                    <CustomText
                        color={colors.red}
                        customStyle={styles.removeText}
                        fontProps={['ff-roboto-light']}>
                        {i18n.t('remove')}
                    </CustomText>
                </TouchableOpacity>
            )}
        </View>
    </View>
)

const styles = StyleSheet.create({
    mainWrapperContainer: {
        marginHorizontal: globalPaddingMargin.whole,
        marginTop: globalPaddingMargin.vertical,
        marginBottom: 5,
    },
    mainContainer: {
        marginTop: globalPaddingMargin.top,
        alignItems: 'flex-start',
        flexDirection: 'row',
        height: 48,
        backgroundColor: colors.white,
        borderRadius: 12,
        ...commonShadow(),
    },
    textInput: {
        paddingHorizontal: globalPaddingMargin.left,
        fontFamily: getFontsFamily(),
        fontSize: getFontsSize(fontsSize.medium),
        flex: 1,
        color: colors.smokeyGrey,
        height: '100%',
        textTransform: 'uppercase',
    },
    applyButtonContainer: {
        paddingHorizontal: globalPaddingMargin.left,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    applyButton: {
        textTransform: 'uppercase',
    },
    promoCodeAppliedFor: {
        marginVertical: 10,
        alignSelf: 'flex-end',
    },
    removeText: {
        marginLeft: 10,
        textDecorationLine: 'underline',
    },
    promoStatusContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
})
export default OrderSummaryPromoCode;
