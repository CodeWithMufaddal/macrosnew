import React, { useCallback, useMemo } from 'react';
import {
    View, StyleSheet,
} from 'react-native';
import CustomText from '../../custom/CustomText';
import i18n from '../../../i18n/i18n';
import colors from '../../../constants/colors';
import { globalPaddingMargin } from '../../../constants/constants';
import CustomPlanCardTwo from '../../custom/CustomPlanCardTwo';
import { getPlanDurationFromStartDate, getValidDate } from '../../../helpers/CommonHelpers';

const OrderSummarySelectedPlan = ({
    plan = {},
    goal = {},
    mealCategory = {},
    mealPlanDuration = 0,
    startDate,
}) => {
    const getPlanImages = useCallback((item) => [item?.image_1,
    item?.image_2,
    item?.image_3,
    item?.image_4]?.filter((imageData) => imageData),
        [])

    const planDuration = useMemo(() => {
        const planDurationData = getPlanDurationFromStartDate(startDate, mealPlanDuration);
        return `${getValidDate(planDurationData?.startDate, 'DD MMMM')} to ${getValidDate(planDurationData?.endDate, 'DD MMMM')} (${mealPlanDuration} Days)`
    }, [mealPlanDuration, startDate])

    const renderSelectedPlan = useMemo(() => (
        <CustomPlanCardTwo
            imageData={getPlanImages(plan)}
            planData={plan}
            planDuration={planDuration}
            planMealType={mealCategory?.name}
            planPackageName={goal?.name}
        />
    ), [getPlanImages, goal?.name, mealCategory?.name, plan, planDuration])

    return (
        <View activeOpacity={0.8} style={styles.mainContainer}>
            {/* Title */}
            <CustomText
                fontProps={['fs-small']}
                color={colors.primary_2}
                customStyle={styles.title}>
                {i18n.t('your_selected_plan')}
            </CustomText>

            {/* Data with Horizontal FlatList */}
            {renderSelectedPlan}
        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        paddingTop: globalPaddingMargin.whole,
        alignItems: 'flex-start',
        paddingHorizontal: globalPaddingMargin.horizontal,
    },
})
export default OrderSummarySelectedPlan;
