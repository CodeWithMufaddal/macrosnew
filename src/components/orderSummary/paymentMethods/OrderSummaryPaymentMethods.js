import React from 'react';
import {
    View, StyleSheet,
} from 'react-native';
import { connect } from 'react-redux';
import CustomText from '../../custom/CustomText';
import i18n, { APP_LANGUAGES } from '../../../i18n/i18n';
import colors from '../../../constants/colors';
import { globalPaddingMargin } from '../../../constants/constants';
import CustomCardTwo from '../../custom/CustomCardTwo';
import { generateUniqueId, getAmountWithCurrency } from '../../../helpers/CommonHelpers';
import CustomLoader from '../../custom/CustomLoader';

const OrderSummaryPaymentMethods = ({
    paymentMethodsData = [],
    loading,
    selectedPaymentMethod = null,
    onPaymentMethodPress = () => { },
    ...props
}) => (
    <View activeOpacity={0.8} style={styles.mainContainer}>
        {/* Title */}
        <CustomText
            fontProps={['fs-small']}
            color={colors.primary_2}
            customStyle={styles.title}>
            {i18n.t('payment_methods')} <CustomText color={colors.red}>(*)</CustomText>
        </CustomText>

        {loading ? (
            <CustomLoader />
        ) : (
            <>
                {paymentMethodsData.map((item, index) => (
                    <View key={generateUniqueId('orderSummaryPaymentMethods', index)}>
                        <CustomCardTwo
                            selected={item?.id === selectedPaymentMethod?.id}
                            onPress={() => onPaymentMethodPress(item)}
                            image={item?.imageUrl}
                            title={item?.name}
                            subTitle={getAmountWithCurrency(item?.TotalAmount)}
                        />
                    </View>
                ))}
            </>
        )}
        <CustomText color={colors.primary_2}>
            <CustomText color={colors.red}>*</CustomText> {i18n.t('serviceChargeMayApply')}
        </CustomText>

    </View>
)

const styles = StyleSheet.create({
    mainContainer: {
        marginBottom: 30,
        alignItems: 'flex-start',
        paddingHorizontal: globalPaddingMargin.horizontal,
    },
})
export default connect((state) => ({
    appLanguage: state?.settings?.appLanguage,
}))(OrderSummaryPaymentMethods);
