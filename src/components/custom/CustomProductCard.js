import React, {
    memo, useCallback, useMemo,
} from 'react';
import {
    View, StyleSheet, Pressable, TouchableOpacity,
} from 'react-native';
import colors from '../../constants/colors';

import { commonShadow } from '../../styles/CommonStyles';
import CustomImage from './CustomImage';
import CustomText from './CustomText';
import CustomIcon, { IconType } from './CustomIcon';
import { getServerImagePath } from '../../services/Services';
import { getHitSlop } from '../../helpers/CommonHelpers';
import { imagePathType } from '../../assets/images/images';
import { useNavigation } from '@react-navigation/native';
import FastImage from 'react-native-fast-image';
import i18n from '../../i18n/i18n';

const CustomProductCard = ({
    productData = {},
    key = '',
    onPress = () => { },
    showQtyPicker = false,
    onQtyButtonPress = () => { },
}) => {
    const navigation = useNavigation()
    const onItemPress = useCallback(() => {
        navigation.navigate('productPage', { productData });
    }, [onPress, productData])

    const renderQtyPicker = useMemo(() => showQtyPicker && (
        <View style={styles.qtyContainer}>
            <TouchableOpacity onPress={() => onQtyButtonPress('dec', productData)} hitSlop={getHitSlop(15)} style={styles.minusQtySignContainer}>
                <CustomIcon activeOpacity={0.5} type={IconType.FontAwesome} color={colors.primary_3} name="minus" size={16} />
            </TouchableOpacity>
            <CustomText fontProps={['fs-small']}>{productData?.selected_meal_qty ?? 0}</CustomText>
            <TouchableOpacity onPress={() => onQtyButtonPress('inc', productData)} hitSlop={getHitSlop(15)} style={styles.plusQtySignContainer}>
                <CustomIcon activeOpacity={0.5} type={IconType.FontAwesome} color={colors.primary_3} name="plus" size={16} />
            </TouchableOpacity>
        </View>
    ), [onQtyButtonPress, productData?.selected_meal_qty, showQtyPicker])

    // const itemIncludes = useMemo(() => {
    //     const checkData = ['protein', 'carbohydrates', 'fats', 'calories']
    //     i18n.t('carbohydrates')
    //     const includesData = checkData?.reduce((currentItem, item, index) => {
    //         const chunkIndex = Math.floor(index / 2)
    //         // eslint-disable-next-line no-param-reassign
    //         if (!currentItem[chunkIndex]) currentItem[chunkIndex] = []
    //         currentItem[chunkIndex].push({ name: i18n.t(item), value: productData?.[item] })
    //         return currentItem;
    //     }, [])
    //     return includesData
    // }, [productData])
    return (
        <Pressable
            onPress={onItemPress}
            activeOpacity={0.8}
            key={key}
            style={{ ...styles.mainContainer }}>
            <View style={styles.imageContainer}>
                <CustomImage
                    source={{ uri: getServerImagePath(imagePathType.meals, productData?.image) }}
                    resizeMode={FastImage.resizeMode.stretch}
                    style={styles.cardImage}
                />
            </View>
            <View style={{ ...styles.restContainer }}>
                <View style={{ flex: 1 }}>
                    <CustomText numberOfLines={2} customStyle={styles.productName} fontProps={['fs-small']}>{productData?.name}</CustomText>
                    <CustomText numberOfLines={3} customStyle={styles.productDescription} color={colors.thinGrey} fontProps={['fs-xsmall', 'ff-roboto-light']}>{productData?.description}</CustomText>
                    {renderQtyPicker}
                </View>
                <View style={styles.productMacrosContainer}>
                    <CustomText customStyle={styles.productMacros} fontProps={['fs-small']}>Pro: {productData?.protein || 0} {productData?.protein?.toString().slice(-1) !== 'g' ? 'g' : ''}</CustomText>
                    <CustomText customStyle={styles.productMacros} fontProps={['fs-small']}>Carb: {productData?.carbohydrates || 0} {productData?.carbohydrates?.toString().slice(-1) !== 'g' ? 'g' : ''}</CustomText>
                    <CustomText customStyle={styles.productMacros} fontProps={['fs-small']}>Fat: {productData?.fats || 0} {productData?.fats?.toString().slice(-1) !== 'g' ? 'g' : ''}</CustomText>
                </View>

                <View style={styles.bottomContainer}>
                    <View style={styles.productMacrosContainer}>
                        <CustomText customStyle={styles.productMacros} fontProps={['fs-small']}>
                            <CustomText color={colors.primary_2} fontProps={['fs-small']}>Or in </CustomText>
                            Cal: {productData?.calories || 0} {i18n.t('cals')}</CustomText>
                    </View>
                </View>
            </View>
        </Pressable>
    )
}
const styles = StyleSheet.create({
    mainContainer: {
        marginBottom: 15,
        minHeight: 110,
        width: '100%',
        backgroundColor: colors.white,
        flexDirection: 'row',
        borderRadius: 12,
        overflow: 'hidden',
        ...commonShadow(),
    },
    restContainer: {
        flex: 1,
        padding: 12,
        justifyContent: 'space-between',
    },
    imageContainer: {
        width: 120,
        height: '100%',
        overflow: 'hidden',
    },
    cardImage: {
        flex: 1,
        height: "auto",
        width: "auto",
    },
    productName: {
        lineHeight: 20,
        textAlign: 'left',
    },
    productDescription: {
        textAlign: 'left',
    },
    bottomContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    qtyContainer: {
        flexDirection: 'row',
        alignSelf: 'flex-start',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 5,
        marginTop: 2
    },
    minusQtySignContainer: {
        width: 30,
        height: 20,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    plusQtySignContainer: {
        width: 30,
        height: 20,
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    productMacrosContainer: {
        flex: 1,
        flexDirection: "row",
        marginTop: 3,
        justifyContent: "space-between"
    },
    orText: {
        textAlign: 'center',
    }

})

export default memo(CustomProductCard);
