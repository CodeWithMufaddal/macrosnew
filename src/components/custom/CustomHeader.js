import React, { memo } from 'react';
import { View, StyleSheet } from 'react-native';
import { wp } from '../../helpers/screenHelper';
import { globalPaddingMargin } from '../../constants/constants';
import { commonShadow } from '../../styles/CommonStyles';
import colors from '../../constants/colors';

const CustomHeader = ({
    leftComponent = null,
    centerComponent = null,
    rightComponent = null,
    leftComponentStyle = {},
    rightComponentStyle = {},
    centerComponentStyle = {},

}) => (
    <View style={styles.mainContainer}>
        {leftComponent && (
            <View style={{ ...styles.leftComponent, ...leftComponentStyle }}>
                {leftComponent}
            </View>
        )}

        {centerComponent && (
            <View style={{ ...styles.centerComponent, ...centerComponentStyle }}>
                {centerComponent}
            </View>
        )}

        {rightComponent && (
            <View style={{ ...styles.rightComponent, ...rightComponentStyle }}>
                {rightComponent}
            </View>
        )}

    </View>
)

const styles = StyleSheet.create({
    mainContainer: {
        zIndex: 100,
        flexDirection: 'row',
        alignItems: 'center',
        height: 75,
        backgroundColor: colors.white,
        justifyContent: 'center',
        width: wp(100),
        padding: globalPaddingMargin.whole,
        ...commonShadow(),
    },
    leftComponent: {
        height: '100%',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    centerComponent: {
        flex: 1,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    rightComponent: {
        flex: 1,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
})

export default memo(CustomHeader);
