import React, {
    memo, useCallback, useMemo, useState,
} from 'react';
import { StyleSheet, View } from 'react-native';
import { Calendar } from 'react-native-calendars';
import colors from '../../constants/colors';
import { getDisabledDaysInMonth, getValidDate } from '../../helpers/CommonHelpers';
import CustomLoader from './CustomLoader'

const CustomFullCalender = ({
    selectedCurrentDate = new Date().toDateString(),
    disabledDaysIndexes = [],
    onDayPress = () => { },
    markedDates = {},
    minDate = null,
    maxDate = null,
    onMonthChange = () => { },
}) => {
    const [disabledDays, setDisabledDays] = useState(getDisabledDaysInMonth());
    const [loading, setLoading] = useState(false)
    const theme = useMemo(() => ({
        arrowColor: colors.primary_3,
        textSectionTitleDisabledColor: '#d9e1e8',
        todayTextColor: colors.primary_3,
    }), []);

    const myOnMonthChange = useCallback(async (date) => {
        setLoading(true);
        const disableDays = await getDisabledDaysInMonth(date);
        setDisabledDays(disableDays)
        onMonthChange(date)
        setTimeout(() => setLoading(false), 1000);
    }, [onMonthChange])

    const myMarkedDates = useMemo(() => ({
        ...markedDates,
        ...disabledDays,
    }), [disabledDays, markedDates])

    const customisedCurrentDate = useMemo(() => (selectedCurrentDate ? getValidDate(selectedCurrentDate, 'YYYY-MM-DD') : null), [selectedCurrentDate]);
    const customisedMinDate = useMemo(() => (minDate ? getValidDate(minDate, 'YYYY-MM-DD') : null), [minDate]);
    const customisedMaxDate = useMemo(() => (maxDate ? getValidDate(maxDate, 'YYYY-MM-DD') : null), [maxDate])
    const renderCalender = () => (
        <Calendar
            current={customisedCurrentDate || new Date().toDateString()}
            minDate={customisedMinDate}
            maxDate={customisedMaxDate}
            markingType={'custom'}
            markedDates={myMarkedDates}
            hideExtraDays={true}
            onMonthChange={myOnMonthChange}
            theme={theme}
            monthFormat={'MMM yyyy'}
            disabledDaysIndexes={disabledDaysIndexes}
            disableAllTouchEventsForDisabledDays={true}
            enableSwipeMonths={true}
            onDayPress={onDayPress}
        />
    )

    return (
        <View>
            {loading && (
                <View style={styles.loadingContainer}>
                    <CustomLoader />
                </View>
            )}
            {renderCalender()}
        </View>
    )
}

const styles = StyleSheet.create({
    loadingContainer: {
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.overlayWhite,
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
        zIndex: 100,
    },
})
export default memo(CustomFullCalender);
