import React, { memo } from 'react';
import {
    StyleSheet, TextInput,
} from 'react-native';
import colors from '../../constants/colors';
import { appTextInputType, globalPaddingMargin } from '../../constants/constants';
import { fontsSize, getFontsFamily, getFontsSize } from '../../assets/fonts/fonts';
import { getTextInputRTLDirection } from '../../styles/CommonStyles';

const CustomTextInputThree = ({
    showBorder = true,
    textInputType = appTextInputType.TEXT,
    value = '',
    textInputRef = null,
    placeHolder = '',
    customTextInputStyle = {},
    multiline = false,
    autoFocus = false,
    keyboardType = 'default',
    maxLength = null,
    onChangeText = () => { },
    onSubmitEditing = () => { },
    clearButtonMode = 'while-editing',
    returnKeyType = 'done',
    autoCapitalize = 'none',
    caretHidden = false,
    editable = true,
}) => (
    <TextInput
        editable={editable}
        autoCorrect={false}
        autoCapitalize={autoCapitalize}
        value={value}
        ref={textInputRef}
        maxLength={maxLength}
        clearButtonMode={'never'}
        placeholder={placeHolder}
        placeholderTextColor={colors.xLightGrey}
        style={{
            ...getTextInputRTLDirection(textInputType),
            ...styles.textInput,
            borderBottomWidth: showBorder ? 1 : 0,
            ...customTextInputStyle,
        }}
        multiline={multiline}
        autoFocus={autoFocus}
        keyboardType={keyboardType}
        onChangeText={onChangeText}
        onSubmitEditing={onSubmitEditing}
        returnKeyType={returnKeyType}
        caretHidden={caretHidden}
    />
)

const styles = StyleSheet.create({
    textInput: {
        flex: 1,
        color: colors.mirageBlack,
        height: '100%',
        marginHorizontal: globalPaddingMargin.left,
        fontSize: getFontsSize(fontsSize.medium),
        fontFamily: getFontsFamily(),
        paddingVertical: 0,
        borderBottomColor: colors.primary_2,
        textAlign: 'center',
        textAlignVertical: 'center',
    },
})

export default memo(CustomTextInputThree);
