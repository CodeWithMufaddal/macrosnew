import React, { memo, useCallback, useState } from 'react';
import FastImage from 'react-native-fast-image';
import { StyleSheet, View } from 'react-native'
import images from '../../assets/images/images';
import colors from '../../constants/colors';

const CustomImage = ({
    source = '',
    style = { height: 20, width: 20 },
    resizeMode = 'cover',
    tintColor = null,
    SvgSource = null,
    svgProps,
    showPlaceholder = true,
    ...props
}) => {
    const [loaded, setLoaded] = useState(false);
    const onLoad = useCallback(() => {
        setLoaded(true);
    }, [])
    return (
        <>
            {SvgSource || source ? (
                <>
                    {SvgSource ? (
                        <SvgSource style={style} {...svgProps} />
                    ) : (
                        <>
                            <FastImage
                                tintColor={tintColor}
                                resizeMode={FastImage.resizeMode[resizeMode]}
                                source={source}
                                style={style}
                                onLoad={onLoad}
                                {...props}
                            />
                            {(!loaded) && (
                                <View style={{ ...style, ...(showPlaceholder && { ...styles.imagePlaceHolder }) }} />
                            )}
                        </>
                    )}
                </>
            ) : (
                <FastImage
                    tintColor={tintColor}
                    resizeMode={'contain'}
                    source={images.appSymbol}
                    style={style}
                    {...props}
                />
            )}

        </>
    )
}

const styles = StyleSheet.create({
    imagePlaceHolder: {
        backgroundColor: colors.skeletonGrey,
    },
})
export default memo(CustomImage);
