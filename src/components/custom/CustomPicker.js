import React, { memo, useMemo } from 'react';
import {
    StyleSheet, View,
} from 'react-native';
import ModalSelector from 'react-native-modal-selector'
import colors from '../../constants/colors';
import {
    fontsFamily, fontsSize, getFontsFamily, getFontsSize,
} from '../../assets/fonts/fonts';
import CustomText from './CustomText';
import i18n from '../../i18n/i18n';
import { generateUniqueId } from '../../helpers/CommonHelpers';
import { hp } from '../../helpers/screenHelper';
import CustomIcon, { IconType } from './CustomIcon';

const CustomPicker = ({
    label = '',
    keyName = '',
    selectedValue = '',
    onValueChange = () => { },
    data = [],
    placeHolder = '',
    customContainerStyle = {},
    isRequired = false
}) => {
    const selectedData = useMemo(() => data?.find((item) => item?.key == selectedValue)?.label, [data, selectedValue])
    const renderHeader = useMemo(() => (
        <View style={styles.headerContainer}>
            <CustomText fontProps={['ff-roboto-medium', 'fs-medium']} color={colors.primary_3}>{placeHolder}</CustomText>
        </View>
    ), [])
    return (
        <View style={{ ...styles.pickerContainer, ...customContainerStyle }}>
            <CustomText
                customStyle={styles.title}
                fontProps={['ff-roboto-medium']}
                color={colors.lightGrey}>
                {label} {isRequired && "*"}
            </CustomText>
            <ModalSelector
                animationType={'fade'}
                style={styles.pickerStyle}
                data={data}
                accessible={true}
                cancelText={i18n.t('cancel')}
                initValue={placeHolder}
                header={renderHeader}
                optionContainerStyle={styles.optionContainerStyle}
                cancelStyle={styles.cancelStyle}
                cancelTextStyle={styles.cancelTextStyle}
                optionTextStyle={styles.optionTextStyle}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                optionStyle={styles.optionStyle}
                keyExtractor={(item) => generateUniqueId(keyName, item?.key)}
                onChange={onValueChange}>
                <View style={styles.contentContainerStyle}>
                    <CustomText fontProps={['ff-roboto-medium', 'fs-small']} color={selectedValue ? colors.black : colors.xLightGrey}>
                        {selectedData ?? placeHolder}
                    </CustomText>
                    <CustomIcon name="keyboard-arrow-down" type={IconType.MaterialIcons} color={colors.primary_2} />
                </View>
            </ModalSelector>
        </View>
    )
}

const styles = StyleSheet.create({
    pickerContainer: {
        alignItems: 'flex-start',
        height: 55,
        width: '100%',
        borderBottomColor: colors.mercury,
        borderWidth: 0,
        borderBottomWidth: 1,
        marginBottom: 10,
    },
    contentContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    cancelStyle: {
        height: 50,
        backgroundColor: colors.primary_3,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
    },
    cancelTextStyle: {
        color: colors.white,
        fontFamily: getFontsFamily(fontsFamily.roboto.medium),
        fontSize: getFontsSize(fontsSize.medium),
    },
    optionContainerStyle: {
        height: hp(80),
        borderRadius: 20,
        backgroundColor: colors.white,
    },
    optionStyle: {
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
    optionTextStyle: {
        color: colors.primary_2,
        fontFamily: getFontsFamily(fontsFamily.roboto.medium),
        fontSize: getFontsSize(fontsSize.medium),
    },

    pickerStyle: {
        flex: 1,
        height: '100%',
        width: '100%',
        justifyContent: 'center',
    },
    headerContainer: {
        height: 50,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomColor: colors.borderColor,
        borderBottomWidth: 1,
    },
    title: {
        paddingBottom: 5,
    },
})

export default memo(CustomPicker);
