import React, { useRef, memo } from 'react';
import {
    StyleSheet, TextInput, TouchableOpacity,
} from 'react-native';
import colors from '../../constants/colors';
import { appTextInputType, globalPaddingMargin, isRTL } from '../../constants/constants';
import { fontsSize, getFontsFamily, getFontsSize } from '../../assets/fonts/fonts';
import { getTextInputRTLDirection } from '../../styles/CommonStyles';

const CustomTextInput = ({
    textInputType = appTextInputType.TEXT,
    value = '',
    textInputRef = null,
    placeHolder = '',
    customContainerStyle = {},
    customTextInputStyle = {},
    leftComponent = <></>,
    rightComponent = <></>,
    multiline = false,
    autoFocus = false,
    keyboardType = 'default',
    maxLength = null,
    onChangeText = () => { },
    onSubmitEditing = () => { },
    clearButtonMode = 'while-editing',
    returnKeyType = 'done',
    autoCapitalize = 'none',
    caretHidden = false,
    editable = true,
}) => {
    const inputRef = useRef(null);
    const onTextInputContainerPress = () => {
        const dynamicRef = textInputRef ?? inputRef
        dynamicRef?.current?.focus();
    }

    return (
        <TouchableOpacity
            activeOpacity={1}
            onPress={onTextInputContainerPress}
            style={{ ...styles.textInputContainer, ...customContainerStyle }}>
            {textInputType === 'number' && isRTL ? rightComponent : leftComponent}
            <TextInput
                editable={editable}
                autoCorrect={false}
                autoCapitalize={autoCapitalize}
                value={value}
                ref={textInputRef ?? inputRef}
                maxLength={maxLength}
                clearButtonMode={clearButtonMode}
                placeholder={placeHolder}
                placeholderTextColor={colors.xLightGrey}
                style={{
                    ...getTextInputRTLDirection(textInputType),
                    ...styles.textInput,
                    ...customTextInputStyle,
                }}
                multiline={multiline}
                autoFocus={autoFocus}
                keyboardType={keyboardType}
                onChangeText={onChangeText}
                onSubmitEditing={onSubmitEditing}
                returnKeyType={returnKeyType}
                caretHidden={caretHidden}
            />
            {textInputType === 'number' && isRTL ? leftComponent : rightComponent}
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    textInputContainer: {
        paddingHorizontal: 20,
        flexDirection: 'row',
        backgroundColor: colors.white,
        width: '100%',
        height: 46,
        borderColor: colors.borderColor,
        borderWidth: 0.5,
        justifyContent: 'center',
        borderRadius: 30,
        alignItems: 'center',
        overflow: 'hidden',
    },
    textInput: {
        flex: 1,
        color: colors.mirageBlack,
        height: '100%',
        paddingHorizontal: globalPaddingMargin.left,
        fontSize: getFontsSize(fontsSize.xmedium),
        fontFamily: getFontsFamily(),
        paddingVertical: 0,
    },
})
CustomTextInput.propTypes = {

}

export default memo(CustomTextInput);
