import React, { memo } from 'react';
import { View, StyleSheet } from 'react-native';
import CustomText from './CustomText';

const CustomFieldValueTable = ({
  field = {},
  value = false,
  keyStyle = {},
  valueStyle = {},
  containerStyle = {},
}) => (
  <View style={{ ...styles.mainContainer, ...containerStyle }}>
    <CustomText fontProps={['ff-roboto-bold']} customStyle={keyStyle}>{field} : </CustomText>
    <CustomText fontProps={['ff-roboto-light']} customStyle={valueStyle}>{value}</CustomText>
  </View>
)
const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: 'row',
  },
})

export default memo(CustomFieldValueTable);
