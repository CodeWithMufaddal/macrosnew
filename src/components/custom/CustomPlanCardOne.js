import React, { Fragment, memo, useCallback, useMemo } from 'react';
import {
    View, StyleSheet, Pressable, FlatList,
} from 'react-native';
import colors from '../../constants/colors';
import { imagePathType, imagesMode, getImagePath } from '../../assets/images/images'
import { generateUniqueId, getAmountWithCurrency } from '../../helpers/CommonHelpers';
import { commonShadow } from '../../styles/CommonStyles';
import CustomImage from './CustomImage';
import CustomText from './CustomText';
import i18n from '../../i18n/i18n';

const CustomPlanCardOne = ({
    planData = {},
    selected = false,
    key = '',
    imageData = [],
    onPress = () => { },
}) => {
    const imageResult = imageData.reduce((resultArray, item, index) => {
        const chunkIndex = Math.floor(index / 2)
        if (!resultArray[chunkIndex]) resultArray[chunkIndex] = [] // start a new chunk
        resultArray[chunkIndex].push(getImagePath(imagePathType.subscriptions, item))
        return resultArray
    }, []);

    const onItemPress = useCallback(() => {
        onPress(planData);
    }, [])
    const renderImages = (item) => (
        <CustomImage
            source={{ uri: item }}
            resizeMode={imagesMode.cover}
            style={{ height: 32, width: 32, ...styles.cardImage }}
        />
    )
    const mealCategories = useMemo(() => planData?.meals_Data.reduce((resultArray, item, index) => {
        const chunkIndex = Math.floor(index / 2)
        if (!resultArray[chunkIndex]) resultArray[chunkIndex] = [] // start a new chunk
        resultArray[chunkIndex].push({ name: item?.title, value: item?.value })
        return resultArray
    }, []), []);

    return (
        <Pressable
            onPress={onItemPress}
            activeOpacity={0.8}
            key={key}
            style={{ ...styles.mainContainer, borderColor: selected ? colors.primary_1 : colors.white }}>
            <View style={{ width: 100, padding: 15, height: 110, }}>
                {imageData?.length > 1 ? (
                    <View style={{ alignItems: 'center', width: '100%', height: '100%' }}>
                        {imageResult?.map((item, index) => (
                            <View style={{
                                width: '100%', height: '50%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',
                            }} key={generateUniqueId('planImages', index)}>
                                {renderImages(item?.[0])}
                                {renderImages(item?.[1])}
                            </View>
                        ))}
                    </View>
                ) : (
                    <CustomImage
                        source={imageData?.[0] ? { uri: getImagePath(imagePathType.mealTypes, imageData?.[0] ?? '') } : null}
                        resizeMode={imagesMode.cover}
                        style={{ height: 68, width: 68, ...styles.cardImage }}
                    />
                )}
            </View>
            <View style={{ ...styles.restContainer }}>
                <CustomText fontProps={['ff-roboto-medium', 'fs-small']} numberOfLines={2}>{planData?.subscription_name}</CustomText>
                {mealCategories?.map((mealItem, mealIndex) => (
                    <View key={`plansMainCatArr-${mealIndex}`} style={{ flexDirection: 'row', alignItems: 'center' }}>
                        {mealItem?.map((mealCatItem, mealCatIndex) => (
                            <Fragment key={`plansMainCatArr-${mealCatIndex}`}>
                                <CustomText fontProps={['ff-roboto-light', 'fs-xsmall']} customStyle={{ ...styles.subTitleStyle, flex: 1 }}>{mealCatItem?.value} {mealCatItem?.name}</CustomText>
                                {mealItem?.length - 1 !== mealCatIndex && <CustomText fontProps={['ff-roboto-light', 'fs-xsmall']} color={colors.primary_1} customStyle={{ ...styles.subTitleStyle }}>{' | '}</CustomText>}
                            </Fragment>
                        ))}
                    </View>
                ))}
                <CustomText
                    fontProps={['ff-roboto-medium', 'fs-small']}
                    customStyle={styles.planPrice}
                    color={colors.primary_3}>
                    {getAmountWithCurrency(planData?.price)}
                </CustomText>
            </View>
        </Pressable>
    )
}
const styles = StyleSheet.create({
    mainContainer: {
        marginVertical: 6,
        width: '100%',
        flex: 1,
        backgroundColor: colors.white,
        borderRadius: 12,
        flexDirection: 'row',
        borderWidth: 2,
        ...commonShadow(),
    },
    restContainer: {
        flex: 1,
        paddingVertical: 15,
        paddingRight: 15,
    },
    cardImage: {
        borderRadius: 8,
    },
    subTitleStyle: {
        marginTop: 5,
    },
    planPrice: {
        marginTop: 10,
        textAlign: 'right',
    },
})

export default memo(CustomPlanCardOne);
