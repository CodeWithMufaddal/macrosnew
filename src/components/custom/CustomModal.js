import React, { memo } from 'react';
import { View, StyleSheet, SafeAreaView } from 'react-native';
import Modal from 'react-native-modal';

const CustomHeader = ({
    isVisible = false,
    onHide,
    customStyle = {},
    animationIn = 'slideInUp',
    animationOut = 'slideOutDown',
    animationTiming = 200,
    backdropOpacity = 1,

    ...props
}) => (
    <Modal
        backdropOpacity={backdropOpacity}
        hideModalContentWhileAnimating={true}
        useNativeDriver={true}
        animationIn={animationIn}
        animationOut={animationOut}
        animationInTiming={animationTiming}
        animationOutTiming={animationTiming}
        isVisible={isVisible}
        backdropColor={'rgba(0,0,0,0.52)'}
        onBackdropPress={onHide}
        style={{ ...styles.modalStyle, ...customStyle }}
    >
        <SafeAreaView style={{ flex: 1 }}>
            <View style={styles.mainContainer}>
                {props.children}
            </View>
        </SafeAreaView>
    </Modal>
)

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    modalStyle: {
        margin: 0,
    },
})

export default memo(CustomHeader);
