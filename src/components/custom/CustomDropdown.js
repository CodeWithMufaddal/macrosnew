import React, { memo, useState } from 'react';
import DropDownPicker from 'react-native-dropdown-picker';
import { StyleSheet } from 'react-native';
import colors from '../../constants/colors';
import { globalPaddingMargin, isRTL } from '../../constants/constants';

const CustomDropdown = ({
  placeholder = '',
  open = false,
  searchable = false,
  value = null,
  setValue = () => {},
  data = [],
  setOpen = () => {},
  dropDownContainerStyle = {},
  dropDownContentContainerStyle = {},
  zIndex = 1000,
  zIndexInverse = 1000,
}) => {
  const [items, setItems] = useState(data);
  return (
    <DropDownPicker
      placeholder={placeholder}
      searchable={searchable}
      style={{ ...styles.dropDown }}
      containerStyle={{ ...dropDownContainerStyle }}
      dropDownContainerStyle={{ ...styles.dropDownContentContainer, ...dropDownContentContainerStyle }}
      open={open}
      value={value}
      zIndex={zIndex}
      zIndexInverse={zIndexInverse}
      items={items}
      rtl={isRTL}
      setOpen={setOpen}
      setValue={setValue}
      setItems={setItems}
    />
  )
}

const styles = StyleSheet.create({
  dropDown: {
    paddingLeft: globalPaddingMargin.horizontal,
    borderColor: colors.borderColor,
    borderRadius: 30,
    borderWidth: 0.5,
    height: 46,
  },
  dropDownContentContainer: {
    borderColor: colors.borderColor,
    borderRadius: 30,
    borderWidth: 0.5,
    padding: 10,
  },
})

export default memo(CustomDropdown);
