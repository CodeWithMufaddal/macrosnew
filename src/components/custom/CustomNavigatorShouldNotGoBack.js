import { useEffect } from 'react';

const CustomNavigatorShouldNotGoBack = ({ navigation, canNotGoBack }) => {
    useEffect(() => navigation.addListener('beforeRemove', (e) => {
        if (canNotGoBack) e.preventDefault();
    }), [canNotGoBack]);
    return null;
}

export default CustomNavigatorShouldNotGoBack;
