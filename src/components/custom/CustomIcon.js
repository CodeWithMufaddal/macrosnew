import React, { memo } from 'react';
import Icon from 'react-native-dynamic-vector-icons';
import { TouchableOpacity } from 'react-native';
import colors from '../../constants/colors';
import { getHitSlop } from '../../helpers/CommonHelpers';

export const IconType = {
    AntDesign: 'AntDesign',
    Entypo: 'Entypo',
    EvilIcons: 'EvilIcons',
    Feather: 'Feather',
    FontAwesome: 'FontAwesome',
    FontAwesome5: 'FontAwesome5',
    FontAwesome6: 'FontAwesome6',
    Fontisto: 'Fontisto',
    Foundation: 'Foundation',
    Ionicons: 'Ionicons',
    MaterialIcons: 'MaterialIcons',
    MaterialCommunityIcons: 'MaterialCommunityIcons',
    Octicons: 'Octicons',
    Zocial: 'Zocial',
    SimpleLineIcons: 'SimpleLineIcons',
}
const CustomIcon = ({
    name = 'github',
    type = IconType.AntDesign,
    color = colors.mirageBlack,
    size = 20,
    customStyle = {},
    activeOpacity = 1,
    onPress = null,
}) => (
    // <TouchableOpacity activeOpacity={activeOpacity} onPress={onPress} hitSlop={getHitSlop(10)}>
    <Icon
        name={name}
        type={type}
        size={size}
        color={color}
        style={customStyle}
        onPress={onPress}
    />
    // </TouchableOpacity>
)

export default memo(CustomIcon);
