import React, {
  forwardRef,
  memo,
  useCallback,
  useImperativeHandle,
  useState,
} from "react";
import { StyleSheet, View } from "react-native";
import { SCLAlert, SCLAlertButton } from "react-native-scl-alert";
import CustomIcon, { IconType } from "./CustomIcon";
import i18n from "../../i18n/i18n";
import colors from "../../constants/colors";
import { fontsSize, getFontsSize } from './../../assets/fonts/fonts';

export const alertBoxType = {
  default: "default",
  info: "info",
  inverse: "inverse",
  success: "success",
  danger: "danger",
  warning: "warning",
};

export const iconName = {
  default: { name: "info-outline", family: "MaterialIcons" },
  info: { name: "ios-thumbs-up thumbs-o-up", family: "FontAwesome" },
  inverse: { name: "select-inverse", family: "MaterialCommunityIcons" },
  success: { name: "checkcircleo", family: "AntDesign" },
  danger: { name: "closecircleo", family: "AntDesign" },
  warning: { name: "info-outline", family: "MaterialIcons" },
};
const CustomAlertBox = ({ showCancelButton = true }, ref) => {
  const [show, setShow] = useState(false);
  const [alertData, setAlertData] = useState(null);
  useImperativeHandle(ref, () => ({
    show: data => callAlert(data),
    hide: () => setShow(false),
  }));

  const callAlert = useCallback(
    (
      alertProps = {
        type: alertBoxType.info,
        title: "Info",
        subTitle: "Subtitle Info",
        onPress: () => { },
        buttonLabel: i18n.t("done"),
        contentContainerStyle: {}
      },
    ) => {
      setShow(true);
      setAlertData({
        type: alertProps?.type ?? alertBoxType?.info,
        title: alertProps?.title ?? "Info",
        subTitle: alertProps?.subTitle ?? "Info Subtitle",
        onPress: alertProps?.onPress ?? (() => { console.log('press') }),
        buttonLabel: alertProps?.buttonLabel ?? i18n.t("done"),
        contentComponent: alertProps?.contentComponent ?? <></>,
        titleStyle: alertProps?.titleStyle ?? styles.titleStyle,
        subtitleStyle: alertProps?.subtitleStyle ?? styles.subtitleStyle,
        contentContainerStyle: alertProps?.contentContainerStyle,

      });
    },
    [],
  );

  const onCancelPress = useCallback(() => {
    setShow(false);
  }, []);

  return (
    <SCLAlert
      headerContainerStyles={{ flex: 1 }}
      show={show}
      onRequestClose={onCancelPress}
      theme={alertData?.type}
      title={alertData?.title || "title"}
      subtitle={alertData?.subTitle || "subtitle"}
      titleStyle={alertData?.titleStyle ?? styles.title}
      subtitleStyle={alertData?.subtitleStyle ?? styles.subTitle}
      numberOfLines={0}
      titleNumberOfLines={1}
      contentContainerStyle={alertData?.contentContainerStyle}
      // useNativeDriver={true}
      cancellable={true}
      headerIconComponent={
        <CustomIcon
          type={iconName[alertData?.type]?.family}
          name={iconName[alertData?.type]?.name}
          size={32}
          color="white"
        />
      }>
      <View style={{ width: "100%" }}>{alertData?.contentComponent}</View>

      <View style={{ width: "100%", flexDirection: 'row' }}>
        <SCLAlertButton
          containerStyle={{ ...styles.alertButton, backgroundColor: colors.primary_1 }}
          theme={alertData?.type}
          onPress={alertData?.onPress ? alertData?.onPress : () => { console.log("pressed") }}>
          {alertData?.buttonLabel ?? ""}
        </SCLAlertButton>
        {
          showCancelButton && (
            <SCLAlertButton theme={alertBoxType.default}
              containerStyle={{ ...styles.alertButton }}
              onPress={onCancelPress}>
              {i18n.t("cancel")}
            </SCLAlertButton>
          )
        }
      </View>
    </SCLAlert>
  );
};

const styles = StyleSheet.create({
  alertButton: {
    width: "80%",
    margin: 3,
    display: 'flex',
    flexDirection: "row"
  },
  title: {
    fontSize: getFontsSize(fontsSize.extraLarge),
  },
  subTitle: {
    fontSize: getFontsSize(fontsSize.large),
  },
})


export default memo(forwardRef(CustomAlertBox));
