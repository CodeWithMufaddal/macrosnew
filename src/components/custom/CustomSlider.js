import React, {useCallback, memo, useState, useMemo, useEffect} from 'react';
import {StyleSheet, View} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import {wp} from '../../helpers/screenHelper';
import colors from '../../constants/colors';
import {commonShadow} from '../../styles/CommonStyles';
import CustomText from './CustomText';
import {generateUniqueId} from '../../helpers/CommonHelpers';

const CustomSlider = ({
  value = 0,
  measurementInterval = 40,
  keyName = '',
  min = 0,
  max = 300,
  onValueChangeStart = () => {},
  onValueChangeFinish = () => {},
  onValueChange = () => {},
}) => {
  const [labelVisible, setLabelVisible] = useState(false);
  const [measurementData, setMeasurementData] = useState([]);
  const renderThumb = useCallback(
    () => (
      <View style={styles.outerThumbCircle}>
        <View style={styles.innerThumbCircle} />
      </View>
    ),
    [],
  );

  useEffect(() => {
    let measurements = [min];
    let currentVal = min;
    while (currentVal !== max) {
      currentVal += measurementInterval;
      measurements.push(currentVal);
    }
    setMeasurementData(measurements);
  }, [measurementInterval]);
  const onValuesChangeStart = useCallback(() => {
    setLabelVisible(true);
    onValueChangeStart();
  }, []);

  const onValuesChangeFinish = useCallback(values => {
    onValueChange(values[0]);
    setLabelVisible(false);
    onValueChangeFinish();
  }, []);

  const renderLabel = useCallback(
    values => (
      <View style={{backgroundColor: 'red', width: 20}}>
        <CustomText>{values?.oneMarkerValue}</CustomText>
      </View>
    ),
    [],
  );

  const renderMesurement = useMemo(
    () => (
      <View style={styles.mesurementContainer}>
        {measurementData?.length > 0 &&
          measurementData?.map((item, index) => (
            <View
              key={generateUniqueId(`slider-${keyName}`, index)}
              style={styles.singleMesurementContainer}>
              <View style={styles.mesurementVerticalLine} />
              <CustomText
                customStyle={styles.measurementText}
                fontProps={['fs-xxsmall']}
                color={colors.thinGrey}>
                {item}
              </CustomText>
            </View>
          ))}
      </View>
    ),
    [measurementData],
  );

  const renderSlider = useMemo(
    () => (
      <MultiSlider
        smoothSnapped={true}
        markerOffsetX={3}
        max={max}
        min={min}
        enabledTwo={true}
        containerStyle={styles.multiSliderContainer}
        isMarkersSeparated={true}
        enableLabel={labelVisible}
        customMarkerLeft={renderThumb}
        values={[Number(value)]}
        sliderLength={wp(80)}
        selectedStyle={styles.selectedStyle}
        trackStyle={styles.trackStyle}
        onValuesChangeStart={onValuesChangeStart}
        onValuesChangeFinish={onValuesChangeFinish}
      />
    ),
    [
      max,
      min,
      labelVisible,
      renderThumb,
      renderLabel,
      value,
      onValuesChangeFinish,
    ],
  );

  return (
    <>
      {renderSlider}
      {renderMesurement}
    </>
  );
};

const styles = StyleSheet.create({
  multiSliderContainer: {
    alignSelf: 'center',
  },
  outerThumbCircle: {
    ...commonShadow(),
    borderRadius: 50,
    backgroundColor: colors.white,
    height: 32,
    width: 32,
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerThumbCircle: {
    borderRadius: 50,
    backgroundColor: colors.primary_1,
    height: 25,
    width: 25,
  },
  selectedStyle: {
    backgroundColor: colors.primary_3,
  },
  trackStyle: {
    height: 4,
    backgroundColor: colors.mercury,
  },
  mesurementContainer: {
    width: wp(83),
    alignSelf: 'center',
    height: 20,
    alignItems: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  singleMesurementContainer: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mesurementVerticalLine: {
    backgroundColor: colors.mercury,
    height: 8,
    width: 1,
    borderRadius: 50,
    marginBottom: 5,
  },
  measurementText: {},
});
export default CustomSlider;
