import React, { useRef, memo, useMemo } from 'react';
import {
    StyleSheet, TextInput, View, TouchableOpacity,
} from 'react-native';
import colors from '../../constants/colors';
import { isRTL, appTextInputType } from '../../constants/constants';
import {
    fontsFamily, fontsSize, getFontsFamily, getFontsSize,
} from '../../assets/fonts/fonts';
import CustomText from './CustomText';
import { getTextInputRTLDirection } from '../../styles/CommonStyles';

const CustomTextInputTwo = ({
    value = '',
    label = '',
    textInputType = appTextInputType.TEXT,
    textInputRef = null,
    placeHolder = '',
    customContainerStyle = {},
    customTextInputStyle = {},
    multiline = false,
    autoFocus = false,
    keyboardType = 'default',
    leftComponent = null,
    rightComponent = null,
    maxLength = null,
    onChangeText = () => { },
    onSubmitEditing = () => { },
    clearButtonMode = 'while-editing',
    returnKeyType = 'done',
    caretHidden = false,
    autoCapitalize = 'none',
    isRequired = false,
}) => {
    const inputRef = useRef(null);
    const onTextInputContainerPress = () => {
        const dynamicRef = textInputRef ?? inputRef
        dynamicRef?.current?.focus();
    }

    const renderLeftComponent = useMemo(() => leftComponent && (<View style={textInputType === 'number' && isRTL ? styles.rightComponentContainer : styles.leftComponentContainer}>{leftComponent}</View>), [textInputType, leftComponent]);
    const renderRightComponent = useMemo(() => rightComponent && (<View style={textInputType === 'number' && isRTL ? styles.leftComponentContainer : styles.rightComponentContainer}>{rightComponent}</View>), [textInputType, rightComponent]);
    return (
        <TouchableOpacity
            activeOpacity={1}
            onPress={onTextInputContainerPress}
            style={{ ...styles.textInputContainer, ...customContainerStyle }}>
            <CustomText
                customStyle={styles.title}
                fontProps={['ff-roboto-medium']}
                color={colors.lightGrey}>
                {label} {isRequired && "*"}
            </CustomText>
            <View style={styles.textInputMainContainer}>

                {textInputType === 'number' && isRTL ? renderRightComponent : renderLeftComponent}
                <TextInput
                    autoCorrect={false}
                    autoCapitalize={autoCapitalize}
                    value={value}
                    ref={textInputRef ?? inputRef}
                    maxLength={maxLength}
                    clearButtonMode={clearButtonMode}
                    placeholder={placeHolder}
                    placeholderTextColor={colors.xLightGrey}
                    style={{
                        ...getTextInputRTLDirection(textInputType),
                        ...styles.textInput,
                        ...customTextInputStyle,
                    }}
                    multiline={multiline}
                    autoFocus={autoFocus}
                    keyboardType={keyboardType}
                    onChangeText={onChangeText}
                    onSubmitEditing={onSubmitEditing}
                    returnKeyType={returnKeyType}
                    caretHidden={caretHidden}
                />
                {textInputType === 'number' && isRTL ? renderLeftComponent : renderRightComponent}
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    textInputContainer: {
        alignItems: 'flex-start',
        height: 55,
        width: '100%',
        borderBottomColor: colors.mercury,
        borderBottomWidth: 1,
    },
    title: {
        paddingBottom: 5,
    },
    textInput: {
        padding: 0,
        margin: 0,
        color: colors.mirageBlack,
        flex: 1,
        fontSize: getFontsSize(fontsSize.small),
        fontFamily: getFontsFamily(fontsFamily.roboto.medium),
        height: '100%',
        width: '100%',
    },
    textInputMainContainer: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        flexDirection: 'row',
    },
    leftComponentContainer: {
        marginRight: 10,
    },
    rightComponentContainer: {
        marginHorizontal: 10,
    },
})

export default memo(CustomTextInputTwo);
