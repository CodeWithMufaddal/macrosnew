import React, { Fragment, memo, useMemo } from 'react';
import {
    View, StyleSheet, Pressable, FlatList,
} from 'react-native';
import colors from '../../constants/colors';
import { generateUniqueId, getAmountWithCurrency } from '../../helpers/CommonHelpers';
import { commonShadow } from '../../styles/CommonStyles';
import CustomImage from './CustomImage';
import CustomFieldValueTable from './CustomFieldValueTable';
import CustomText from './CustomText';
import i18n from '../../i18n/i18n';
import { getImagePath, imagePathType, imagesMode } from '../../assets/images/images';

const CustomPlanCardTwo = ({
    planData = {},
    selected = false,
    key = '',
    onPress = () => { },
    planDuration = null,
    planMealType = null,
    planPackageName = null,
    imageData = [],
    showCurrency = true,
}) => {
    let imageResult = imageData.reduce((resultArray, item, index) => {
        const chunkIndex = Math.floor(index / 2)
        if (!resultArray[chunkIndex]) resultArray[chunkIndex] = [] // start a new chunk
        resultArray[chunkIndex].push(getImagePath(imagePathType.subscriptions, item))
        return resultArray
    }, []);



    const onItemPress = () => {
        onPress(planData);
    }
    const renderImages = (item) => (
        <CustomImage
            source={{ uri: item }}
            resizeMode={imagesMode.cover}
            style={{ height: 32, width: 32, ...styles.cardImage }}
        />
    )

    const mealCategories = useMemo(() => planData?.meals_Data.reduce((resultArray, item, index) => {
        const chunkIndex = Math.floor(index / 2)
        if (!resultArray[chunkIndex]) resultArray[chunkIndex] = [] // start a new chunk
        resultArray[chunkIndex].push({ name: item?.title, value: item?.value })
        return resultArray
    }, []), []);

    return (
        <Pressable
            onPress={onItemPress}
            activeOpacity={0.8}
            style={{ ...styles.mainWrapperContainer, backgroundColor: selected ? colors.primary_1 : colors.white }}
            key={key}>
            <View style={{ ...styles.mainContainer }}>
                <View style={{ width: 100, padding: 15, height: 110, }}>
                    {imageData?.length > 1 ? (
                        <View style={{ alignItems: 'center', width: '100%', height: '100%' }}>
                            {imageResult?.map((item, index) => {
                                return (
                                    <View style={{ width: '100%', height: '50%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} key={generateUniqueId('planImagesCard2', index)}>
                                        {renderImages(item?.[0])}
                                        {renderImages(item?.[1])}
                                    </View>
                                )
                            })}
                        </View>
                    ) : (
                        <CustomImage
                            source={imageData?.[0] ? { uri: getImagePath(imagePathType.mealTypes, imageData?.[0] ?? '') } : null}
                            resizeMode={imagesMode.cover}
                            style={{ height: 68, width: 68, ...styles.cardImage }}
                        />
                    )}
                </View>
                <View style={{ ...styles.restContainer }}>
                    <CustomText fontProps={['ff-roboto-medium', 'fs-small']}>{planData?.subscription_name}</CustomText>
                    {mealCategories?.map((mealItem, mealIndex) => (
                        <View key={`orderSummaryMainCatArr-${mealIndex}`} style={{ flexDirection: 'row', alignItems: 'center', width: '100%', }}>
                            {mealItem?.map((mealCatItem, mealCatIndex) => (
                                <Fragment key={`orderSummarySubCatArr-${mealCatIndex}`}>
                                    <CustomText fontProps={['ff-roboto-light', 'fs-xsmall']} customStyle={{ ...styles.subTitleStyle, flex: 1, textAlign: 'left' }}>{mealCatItem?.value} {mealCatItem?.name}</CustomText>
                                    {mealItem?.length - 1 !== mealCatIndex && <CustomText fontProps={['ff-roboto-light', 'fs-xsmall']} color={colors.primary_1} customStyle={{ ...styles.subTitleStyle }}>{' | '}</CustomText>}
                                </Fragment>
                            ))}
                        </View>
                    ))}
                    {showCurrency && <CustomText
                        fontProps={['ff-roboto-medium', 'fs-small']}
                        customStyle={styles.planPrice}
                        color={colors.primary_3}>
                        {getAmountWithCurrency(planData?.price)}
                    </CustomText>}
                </View>
            </View>
            <View style={{ marginBottom: 10 }}>

                {/* Extra Details */}
                {planDuration ? (
                    <CustomFieldValueTable
                        field={i18n.t('plan_duration')}
                        containerStyle={styles.fieldValueContainer}
                        value={planDuration}
                    />) : null}

                {planMealType ? (
                    <CustomFieldValueTable
                        containerStyle={styles.fieldValueContainer}
                        field={i18n.t('meal_type')}
                        value={planMealType}
                    />) : null}

                {planPackageName ? (
                    <CustomFieldValueTable
                        containerStyle={styles.fieldValueContainer}
                        field={i18n.t('goal')}
                        value={planPackageName}
                    />) : null}
            </View>
        </Pressable>
    )
}
const styles = StyleSheet.create({
    mainWrapperContainer: {
        marginVertical: 6,
        width: '100%',
        backgroundColor: colors.white,
        borderRadius: 12,
        ...commonShadow(),
    },
    mainContainer: {
        flexDirection: 'row',
        
    },
    restContainer: {
        flex: 1,
        paddingVertical: 15,
        paddingRight: 15,
    },
    cardImage: {
        borderRadius: 8,
    },
    subTitleStyle: {
        marginTop: 5,
    },
    fieldValueContainer: {
        paddingHorizontal: 15,
        marginVertical: 3,
    },
    planPrice: {
        marginTop: 10,
        textAlign: 'right',
    }
})

export default memo(CustomPlanCardTwo);
