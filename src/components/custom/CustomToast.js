import Toast from 'react-native-toast-message';
import i18n from '../../i18n/i18n';

export const toastType = {
    success: 'success',
    error: 'error',
    info: 'info',
}

const toastCommonProps = {
    position: 'top',
    topOffset: 50,
    visibilityTime: 2000,
    onPress: () => Toast.hide(),
}
const CustomToast = {
    success: (title = null, subTitle = null) => {
        Toast.show({
            type: toastType.success,
            text1: title ?? i18n.t('success'),
            text2: subTitle,
            ...toastCommonProps,
        })
    },
    error: (title = null, subTitle = null) => {
        Toast.show({
            type: toastType.error,
            text1: title ?? i18n.t('error'),
            text2: subTitle,
            ...toastCommonProps,
        })
    },
    info: (title = null, subTitle = null) => {
        Toast.show({
            type: toastType.info,
            text1: title ?? i18n.t('info'),
            text2: subTitle,
            ...toastCommonProps,
        })
    },
}

export default CustomToast;
