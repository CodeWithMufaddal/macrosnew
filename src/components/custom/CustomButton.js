import React, { memo } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import colors from '../../constants/colors';
import CustomText from './CustomText';
import CustomImage from './CustomImage';
import { wp } from '../../helpers/screenHelper';

const CustomButton = ({
    customStyle = {},
    backgroundColor = colors.primary_3,
    disabledBackgroundColor = colors.lightGrey,
    buttonLabel = 'Button Label',
    icon = null,
    onPress = () => {},
    disabled = false,
    activeOpacity = 0.2,
}) => (
    <TouchableOpacity
        activeOpacity={activeOpacity}
        disabled={disabled}
        onPress={onPress}
        style={{
            backgroundColor: disabled ? disabledBackgroundColor : backgroundColor,
            ...styles.buttonMainContainer,
            ...customStyle,
        }}
    >
        {icon && <CustomImage SvgSource={icon} style={styles.icon} />}
        <CustomText fontProps={['fs-medium']} color={colors.white}>
            {buttonLabel}
        </CustomText>
    </TouchableOpacity>
)

const styles = StyleSheet.create({
    buttonMainContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 40,
        width: wp(80),
        alignSelf: 'center',
        height: 45,
    },
    icon: {
        marginRight: 14,
    },
})
export default memo(CustomButton);
