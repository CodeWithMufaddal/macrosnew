import React, { memo } from 'react';
import { View, Pressable, StyleSheet } from 'react-native';
import colors from '../../constants/colors';
import { imagesMode } from '../../assets/images/images';
import { commonShadow } from '../../styles/CommonStyles';
import CustomImage from './CustomImage';
import CustomText from './CustomText';

const CustomCardTwo = ({
    title = '',
    subTitle = '',
    image = null,
    selected = false,
    keyName = '',
    onPress = () => { },
}) => (
    <Pressable
        onPress={onPress}
        activeOpacity={0.8}
        key={keyName}
        style={{ ...styles.mainContainer, borderColor: selected ? colors.primary_1 : colors.white }}>
        <View style={{ width: 100, padding: 15 }}>

            <CustomImage
                source={image}
                resizeMode={imagesMode.contain}
                style={{ height: 55, width: 55, ...styles.cardImage }}
            />
        </View>
        <View style={{ ...styles.restContainer }}>
            <CustomText fontProps={['ff-roboto-light', 'fs-medium']}>{title}</CustomText>
            <CustomText fontProps={['ff-roboto-bold', 'fs-xsmall']} color={colors.successGreen} customStyle={{ ...styles.subTitleStyle }}>{subTitle}</CustomText>
        </View>
    </Pressable>
)
const styles = StyleSheet.create({
    mainContainer: {
        marginVertical: 6,
        height: 70,
        width: '100%',
        borderWidth: 2,
        backgroundColor: colors.white,
        borderRadius: 12,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        ...commonShadow(),
    },
    restContainer: {
        flex: 1,
        paddingVertical: 15,
        justifyContent: 'center',
    },
    cardImage: {
        borderRadius: 8,
    },
    subTitleStyle: {
        marginTop: 5,
    },
})

export default memo(CustomCardTwo);
