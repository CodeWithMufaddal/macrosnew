import React, { memo } from 'react';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

const CustomKeyboardAvoidingView = ({ ...props }) => (
  <KeyboardAwareScrollView
    enableOnAndroid={true}
    showsHorizontalScrollIndicator={false}
    showsVerticalScrollIndicator={false}
    bounces={false}
    {...props}
  >
    {props.children}
  </KeyboardAwareScrollView>
)
CustomKeyboardAvoidingView.propTypes = {}

export default memo(CustomKeyboardAvoidingView);
