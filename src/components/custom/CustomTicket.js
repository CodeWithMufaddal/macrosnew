import React, { Fragment, memo } from 'react';
import { StyleSheet, View, Pressable } from 'react-native';
import colors from '../../constants/colors';
import CustomText from './CustomText';

const CustomTicket = ({
    title = '',
    subTitle = '',
    key = '',
    selected = false,
    selectedTicketBackgroundColor = colors.primary_3,
    onPress = () => { },
}) => (
    <Pressable
        onPress={() => requestAnimationFrame(onPress)}
        activeOpacity={0.8}
        key={key}
        style={{
            ...styles.mainContainer,
            backgroundColor: colors.white,
            borderWidth: selected ? 2 : 1,
            borderColor: selected ? selectedTicketBackgroundColor : colors.thinGrey,
        }}>
        <CustomText fontProps={['fs-medium']}>{title}</CustomText>
        {subTitle ? (
            <Fragment>
                <View style={{ height: 2 }} />
                <CustomText fontProps={['fs-xxxsmall']} color={colors.thinGrey}>{subTitle}</CustomText>
            </Fragment>
        ) : null}
    </Pressable>
)

const styles = StyleSheet.create({
    mainContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 60,
        width: 60,
        borderRadius: 8,

        borderStyle: 'dashed',
        marginVertical: 6,
    },
})

export default memo(CustomTicket);
