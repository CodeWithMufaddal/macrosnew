import React, { memo } from 'react';
import { Text } from 'react-native';
import colors from '../../constants/colors';
import { getFontsFamilyAndSize } from '../../helpers/CommonHelpers';

const CustomText = ({
    customStyle = {},
    color = colors.mirageBlack,
    fontProps = null,
    numberOfLines = undefined,
    ...props
}) => {
    const { fontSize, fontFamily } = getFontsFamilyAndSize(fontProps);
    return (
        <Text
            numberOfLines={numberOfLines}
            style={{
                textAlign: 'left',
                fontSize,
                fontFamily,
                color,
                ...customStyle,
            }}>
            {props.children}
        </Text>
    )
}

export default memo(CustomText);
