import React, { memo } from 'react';
import { StyleSheet, Pressable, View } from 'react-native';
import { imagesMode } from '../../assets/images/images';
import colors from '../../constants/colors';

import { commonShadow } from '../../styles/CommonStyles';
import CustomIcon from './CustomIcon';
import CustomImage from './CustomImage';
import CustomText from './CustomText';
import i18n from '../../i18n/i18n';
// import CustomText from './CustomText';

const CustomCardOne = ({
    // title = '',
    image = '',
    key = '',
    selected = false,
    data = null,
    selectedCardBackgroundColor = colors.primary_1,
    onInfoButtonPress = () => {},
    showInfoButton = false,
    showInfo = false,
    // titleStyle = {},
    onPress = () => {},
}) => {
    const protiens = data?.id == '3'
        ? 200
        : data?.id == '1'
            ? 150
            : data?.id == '13'
                ? 120
                : 200;
    const carbs = data?.id == '3'
        ? 300
        : data?.id == '1'
            ? 150
            : data?.id == '13'
                ? 120
                : 200;

    return (
        <View style={{ flex: 1 }}>
            <Pressable
                onPress={() => requestAnimationFrame(onPress)}
                activeOpacity={0.8}
                key={key}
                style={{
                    ...styles.mainContainer,
                    borderColor: selected ? selectedCardBackgroundColor : colors.white,
                    borderWidth: 2,
                }}>
                <CustomImage
                    source={image}
                    resizeMode={imagesMode.contain}
                    style={styles.cardImage}
                />
                {/* <View style={{ ...styles.titleContainer, backgroundColor: selected ? selectedCardBackgroundColor : colors.white }}>
            <CustomText fontProps={['fs-small']} customStyle={{ textAlign: 'center', ...titleStyle }}>{title}</CustomText>
        </View> */}
                {showInfoButton && (
                    <View style={styles.iButtonContainer}>
                        <CustomIcon
                            onPress={onInfoButtonPress}
                            name="infocirlce"
                            color={colors.charcolGrey}
                            size={15}
                        />
                    </View>
                )}
            </Pressable>
            {showInfo && protiens && carbs && (
                <View style={{ marginLeft: 5, backgroundColor: colors.transparent }}>
                    {data?.id == '2' ? (
                        <CustomText
                            fontProps={['ff-roboto-medium', 'fs-small']}
                            color={colors.charcolGrey}>
                            {i18n.t('build_your_own')}
                        </CustomText>
                    ) : null}
                    <CustomText
                        fontProps={['ff-roboto-medium', 'fs-small']}
                        color={colors.charcolGrey}>
                        {i18n.t('upto_proties', { protien: protiens })}
                    </CustomText>
                    <CustomText
                        fontProps={['ff-roboto-medium', 'fs-small']}
                        color={colors.charcolGrey}>
                        {i18n.t('upto_carbs', { carb: carbs })}
                    </CustomText>
                </View>
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    mainContainer: {
        marginVertical: 6,
        padding: 5,
        height: 130,
        width: 130,
        backgroundColor: colors.white,
        borderRadius: 12,
        ...commonShadow(),
    },
    iButtonContainer: {
        position: 'absolute',
        right: 3,
        top: 3,
    },
    // titleContainer: {
    //     position: 'absolute',
    //     bottom: 0,
    //     alignItems: 'center',
    //     justifyContent: 'center',
    //     width: '100%',
    //     height: 35,
    //     borderBottomLeftRadius: 12,
    //     borderBottomRightRadius: 12,
    // },
    cardImage: {
        height: '100%',
        width: '100%',
        borderRadius: 12,
    },
});

export default memo(CustomCardOne);
