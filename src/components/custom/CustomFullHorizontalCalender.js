import React, { memo, useMemo } from 'react';
import {  StyleSheet, View } from 'react-native';
import CalendarStrip from 'react-native-calendar-strip'
import colors from '../../constants/colors';
import { EVENT_OFF_DAY, isRTL } from '../../constants/constants';

const CustomFullHorizontalCalender = ({
    selectedCurrentDate = null,
    onDayPress = () => { },
    markedDates = {},
    minDate = null,
    maxDate = null,
}) => {
    const datesBlacklistFunc = (date) => date.isoWeekday() === EVENT_OFF_DAY // disable Friday
    const daySelectionAnimation = useMemo(() => ({
        type: 'border', duration: 0, borderWidth: 1, borderHighlightColor: colors.primary_3,
    }), []);

    return (
        <View style={styles.mainContainer}>
            <CalendarStrip
                iconLeftStyle={{ transform: [{ rotateZ: isRTL ? '180deg' : '0deg' }] }}
                iconRightStyle={{ transform: [{ rotateZ: isRTL ? '180deg' : '0deg' }] }}
                markedDates={markedDates}
                selectedDate={selectedCurrentDate}
                scrollable={true}
                datesBlacklist={datesBlacklistFunc}
                minDate={minDate}
                maxDate={maxDate}
                onDateSelected={onDayPress}
                daySelectionAnimation={daySelectionAnimation}
                style={styles.calenderMainContainer}
                calendarHeaderStyle={styles.calenderHeaderStyle}
                dayContainerStyle={styles.dayContainerStyle}
                markedDatesStyle={styles.markedDatesStyle}
                dateNumberStyle={styles.dateNumberStyle}
                highlightDateNameStyle={styles.highlightDateNameStyle}
                highlightDateNumberStyle={styles.highlightDateNumberStyle}
                iconStyle={styles.iconStyle}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        height: 100,
        backgroundColor: colors.white,
    },
    calenderMainContainer: {
        height: 110,
        paddingVertical: 15,
        paddingHorizontal: 5,
    },
    calenderHeaderStyle: {
        color: colors.primary_3,
    },
    dayContainerStyle: {
        paddingVertical: 10,
    },
    markedDatesStyle: {
    },
    dateNumberStyle: {
        color: colors.primary_2,
    },
    highlightDateNameStyle: {
        color: colors.primary_3,
    },
    highlightDateNumberStyle: {
        color: colors.primary_3,
    },
    iconStyle: {},
})

export default memo(CustomFullHorizontalCalender);
