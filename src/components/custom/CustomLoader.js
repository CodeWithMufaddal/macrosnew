import React, {memo, useEffect, useMemo, useState} from "react";
import {View, Image} from "react-native";
import images from "../../assets/images/images";
import colors from "../../constants/colors";
import CustomText from "./CustomText";

const CustomLoader = ({size = 30, wholeScreen = false, title = ""}) => {
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const IMAGES = [images.loader1, images.loader2, images.loader3];
  const [index, setIndex] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setIndex(prev => (prev === IMAGES.length - 1 ? 0 : prev + 1));
    }, 500);
    return () => {
      clearInterval(interval);
    };
  }, [IMAGES.length]);

  const loader = useMemo(
    () => (
      <Image
        source={IMAGES[index]}
        style={{width: 40, height: 40, alignSelf: "center"}}
      />
    ),
    [IMAGES, index],
  );

  return wholeScreen ? (
    <View
      style={{
        position: "absolute",
        top: 0,
        right: 0,
        left: 0,
        bottom: 0,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "rgba(255,255,255,0.7)",
        zIndex: 100,
      }}>
      {loader}
      <CustomText
        customStyle={{textAlign: "center"}}
        color={colors.mirageBlack}>
        {title}
      </CustomText>
    </View>
  ) : (
    loader
  );
};

export default memo(CustomLoader);
