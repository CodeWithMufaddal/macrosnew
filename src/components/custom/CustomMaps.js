import React from 'react';
import { StyleSheet, View } from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import _ from 'lodash';
import { wp } from '../../helpers/screenHelper';
import { DEFAULT_COORDINATE } from '../../constants/constants';
import { getRegionForCoordinates } from '../../helpers/CommonHelpers';
import CustomImage from './CustomImage';
import images from '../../assets/images/images';

const CustomMaps = ({
    onMapReady = () => { },
    pinCoordinates = DEFAULT_COORDINATE,
    mapRef,
    onLocationChange = () => { },
    ...props
}) => {
    const onRegionChangeComplete = _.debounce((region) => {
        const regionCoords = getRegionForCoordinates({
            latitude: region?.latitude ?? 0.0,
            longitude: region?.longitude ?? 0.0,
        });
        onLocationChange(regionCoords, true)
    }, 500);
    return (
        <View style={styles.mapContainer}>
            <MapView.Animated
                onMapReady={onMapReady}
                provider={PROVIDER_GOOGLE}
                ref={mapRef}
                initialRegion={pinCoordinates}
                style={styles.map}
                loadingEnabled
                showsMyLocationButton={true}
                showsUserLocation={true}
                onRegionChangeComplete={onRegionChangeComplete}>
                <Marker.Animated
                    draggable
                    coordinate={pinCoordinates}
                    onDragEnd={(e) => {
                        const regionCoords = getRegionForCoordinates({
                            latitude: e?.nativeEvent?.coordinate?.latitude ?? 0.0,
                            longitude: e?.nativeEvent?.coordinate?.longitude ?? 0.0,
                        });
                        mapRef?.current?.animateToRegion(regionCoords);
                        onLocationChange(regionCoords, true)
                    }}>
                    <CustomImage
                        style={{
                            height: 50,
                            width: 50,
                            alignSelf: 'center',
                        }}
                        source={images.mapMarker}
                    />
                </Marker.Animated>
            </MapView.Animated>
            {props.children}
        </View>
    )
}

const styles = StyleSheet.create({
    mapContainer: {
        ...StyleSheet.absoluteFillObject,
        height: 250,
        width: wp(100),
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
        height: '100%',
        width: '100%',
    },
})
export default CustomMaps;
