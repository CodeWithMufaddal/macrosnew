import React, { memo, useCallback } from 'react';
import {
    View, StyleSheet, Pressable,
} from 'react-native';
import colors from '../../constants/colors';
import { commonShadow } from '../../styles/CommonStyles';
import CustomImage from './CustomImage';
import CustomText from './CustomText';
// import CustomIcon, { IconType } from './CustomIcon';
import { getImagePath, imagePathType, imagesMode } from '../../assets/images/images';
// import i18n from '../../i18n/i18n';

const CustomProductSqureCard = ({
    productData = {},
    key = '',
    onPress = () => { },
}) => {
    const onItemPress = useCallback(() => {
        onPress(productData);
    }, [onPress, productData])

    return (
        <Pressable
            onPress={onItemPress}
            activeOpacity={0.8}
            key={key}
            style={{ ...styles.mainContainer }}>
            <View style={styles.imageContainer}>
                <CustomImage
                    source={{ uri: getImagePath(imagePathType.meals, productData?.image) }}
                    resizeMode={imagesMode.cover}
                    style={{ flex: 1, ...styles.cardImage }}
                />
            </View>
            <View style={{ ...styles.restContainer }}>
                <CustomText numberOfLines={1} customStyle={styles.mealCategory} color={colors.primary_3} fontProps={['fs-small', 'ff-roboto-medium']}>{productData?.mealCategory}</CustomText>
                <CustomText numberOfLines={1} customStyle={styles.productName} fontProps={['fs-small', 'ff-roboto-medium']}>{productData?.name}</CustomText>
                <CustomText numberOfLines={2} color={colors.thinGrey} fontProps={['fs-xsmall', 'ff-roboto-light']}>{productData?.description}</CustomText>
                {/* <View style={styles.bottomContainer}>
                    <View style={styles.caleriesContainer}>
                        <CustomIcon size={16} name="local-fire-department" type={IconType.MaterialIcons} color={colors.thinGrey} />
                        <CustomText color={colors.borderColor} fontProps={['fs-xxsmall']} customStyle={{ ...styles.subTitleStyle }}>{`${productData?.protein} ${i18n.t('protein')} | ${productData?.carbohydrates} ${i18n.t('carbs')} | ${productData?.calories} ${i18n.t('calories')} | ${productData?.fats} ${i18n.t('fats')}`} </CustomText>
                    </View>
                </View> */}
            </View>
        </Pressable>
    )
}
const styles = StyleSheet.create({
    mainContainer: {
        marginBottom: 15,
        minHeight: 280,
        width: 280,
        backgroundColor: colors.white,
        borderRadius: 12,
        overflow: 'hidden',
        ...commonShadow(),
    },
    restContainer: {
        position: 'absolute',
        backgroundColor: colors.white,
        bottom: 0,
        padding: 10,
        width: '100%',
        justifyContent: 'space-between',
    },
    imageContainer: {
        width: '100%',
        minHeight: 280,

    },
    cardImage: {},
    // subTitleStyle: {
    //     marginLeft: 5,
    // },
    mealCategory: {
        marginBottom: 5,
    },
    productName: {
        lineHeight: 15,
        marginBottom: 5,
    },
    // caleriesContainer: {
    //     flexDirection: 'row',
    //     flex: 1,
    //     alignItems: 'center',
    // },
    // bottomContainer: {
    //     flexDirection: 'row',
    //     justifyContent: 'space-between',
    //     alignItems: 'center',
    // },
})

export default memo(CustomProductSqureCard);
