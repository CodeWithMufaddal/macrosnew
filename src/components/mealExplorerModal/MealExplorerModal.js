import { useIsFocused } from '@react-navigation/native';
import React, {
    Fragment,
    useCallback, useEffect, useMemo, useRef, useState,
} from 'react';
import {
    View, FlatList, TouchableOpacity,
} from 'react-native';
import { connect, useDispatch } from 'react-redux';
import CustomLoader from '../custom/CustomLoader';
import CustomProductCard from '../custom/CustomProductCard';
import CustomText from '../custom/CustomText'
import colors from '../../constants/colors';
import { generateUniqueId } from '../../helpers/CommonHelpers';
import i18n from '../../i18n/i18n';
import * as SubscriptionActions from '../../redux/actions/SubscriptionActions';
import styles from './styles'
import CustomModal from '../custom/CustomModal';
import CustomIcon from '../custom/CustomIcon';

const MealExplorerModal = ({ showModal, setShowModal, ...props }) => {
    const dispatch = useDispatch();
    const isFocused = useIsFocused();
    const mealsRef = useRef();
    const [selectedMealType, setSelectedMealType] = useState()
    const [selectedMealCategory, setSelectedMealCategory] = useState();

    useEffect(() => {
        if (isFocused) {
            dispatch(SubscriptionActions.getAllMealTypesAction()).then((mealType) => {
                setSelectedMealType(mealType?.data?.[0])
            });
        }
    }, [isFocused])

    useEffect(() => {
        if (selectedMealType?.id) {
            dispatch(SubscriptionActions.getAllMealsByMealTypeIdAction(selectedMealType?.id)).then((mealCategory) => {
                setSelectedMealCategory(mealCategory[0])
            });
        }
    }, [selectedMealType]);

    useEffect(() => {
        if (selectedMealCategory) {
            mealsRef?.current?.scrollToOffset({ animated: false, offset: 0 });
        }
    }, [selectedMealCategory])

    const onMealTypePress = useCallback((plan) => {
        setSelectedMealType(plan)
    }, [])

    const renderMealTypes = useCallback(({ item }) => (
        <TouchableOpacity
            onPress={() => onMealTypePress(item)}
            style={{ ...styles.singlePlanContainer, borderBottomColor: selectedMealType?.id === item?.id ? colors.primary_3 : colors.ghostWhite }}>
            <CustomText
                fontProps={['fs-small', 'ff-roboto-medium']}
                color={selectedMealType?.id === item?.id ? colors.primary_3 : colors.borderColor}
                customStyle={styles.singlePlanText}>{item?.name?.toUpperCase()}</CustomText>
        </TouchableOpacity>
    ), [onMealTypePress, selectedMealType])

    const onFoodTypePress = useCallback((mealCategory) => {
        setSelectedMealCategory(mealCategory)
    }, [])

    const renderMealCategories = useCallback(({ item }) => (
        <TouchableOpacity
            onPress={() => onFoodTypePress(item)}
            style={{
                ...styles.singleFoodTypeContainer,
                borderBottomColor: selectedMealCategory?.meal_name === item?.meal_name ? colors.primary_2 : colors.ghostWhite,
            }}>
            <CustomText
                fontProps={['fs-small', 'ff-roboto-medium']}
                color={selectedMealCategory?.meal_name === item?.meal_name ? colors.primary_2 : colors.borderColor}
                customStyle={{ ...styles.singleFoodTypeText }}>{item?.meal_name}</CustomText>
        </TouchableOpacity>
    ), [onFoodTypePress, selectedMealCategory])

    const renderMeals = useCallback(({ item }) => (
        <CustomProductCard productData={item} />
    ), [])

    const mealTyoesKeyExtractor = useCallback((item, index) => generateUniqueId('MealsDashboardPlansData', index), [])
    const mealCategoryKeyExtractor = useCallback((item, index) => generateUniqueId('MealsDashboardFoodTypesData', index), [])
    const mealsKeyExtractor = useCallback((item) => generateUniqueId('MealsDashboardMeals', item?.id), [])

    const MealsEmptyContainerComponent = useMemo(() => (
        <View style={styles.noDataTextContainer}>
            <CustomText>{i18n.t('no_meals_are_there')}</CustomText>
        </View>
    ), [])

    const onClosePress = useCallback(() => {
        setShowModal(false)
    }, [setShowModal])

    return (
        <CustomModal isVisible={showModal}>
            <View style={styles.mainContainer}>
                <View style={styles.topBar}>
                    <CustomText fontProps={['fs-medium']} color={colors.white} style={styles.titleText}>{i18n.t('meal_explorer')}</CustomText>
                    <CustomIcon name='close' onPress={onClosePress} color={colors.white} />
                </View>

                {/* Meal Types */}
                <View style={styles.plansViewContainer}>
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={styles.planFlatListContentContainer}
                        showsHorizontalScrollIndicator={false}
                        data={props?.allMealTypes?.data}
                        horizontal={true}
                        keyExtractor={mealTyoesKeyExtractor}
                        style={styles.planFlatList}
                        renderItem={renderMealTypes}
                    />
                </View>

                {props?.allMealsByMealTypeId?.loading ? (
                    <CustomLoader />
                ) : (
                    <Fragment>
                        {/* Meal Categories */}
                        <View style={styles.foodTypesViewContainer}>
                            <FlatList
                                showsVerticalScrollIndicator={false}
                                contentContainerStyle={styles.foodTypesFlatListContentContainer}
                                showsHorizontalScrollIndicator={false}
                                data={props?.allMealsByMealTypeId?.data}
                                horizontal={true}
                                keyExtractor={mealCategoryKeyExtractor}
                                style={styles.foodTypesFlatList}
                                renderItem={renderMealCategories}
                            />
                        </View>

                        {/* Meals  */}
                        <FlatList
                            ref={mealsRef}
                            showsHorizontalScrollIndicator={false}
                            showsVerticalScrollIndicator={false}
                            removeClippedSubviews={true}
                            initialNumToRender={5}
                            maxToRenderPerBatch={2}
                            style={styles.mealsFlatList}
                            windowSize={10}
                            contentContainerStyle={styles.mealsContentContainerStyle}
                            data={selectedMealCategory?.meals_data?.sort((a, b) => a?.name.localeCompare(b?.name, 'es', {
                                sensitivity: 'base',
                            }))}
                            ListEmptyComponent={MealsEmptyContainerComponent}
                            keyExtractor={mealsKeyExtractor}
                            renderItem={renderMeals}
                        />
                    </Fragment>
                )}
            </View>
        </CustomModal>
    )
}

export default connect((state) => ({
    allMealTypes: state.subscription.allMealTypes,
    allMealsByMealTypeId: state.subscription.allMealsByMealTypeId,
}))(MealExplorerModal);
