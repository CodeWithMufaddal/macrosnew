import { StyleSheet } from 'react-native';
import colors from '../../constants/colors';
import { hp } from '../../helpers/screenHelper';

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: colors.white,
        height: hp(90),
        position: 'absolute',
        bottom: 0,
        width: '100%',
        borderRadius: 10,
    },
    titleText: {
        textTransform: 'uppercase',
    },
    parentTitleText: {
        marginBottom: 10,
    },
    topBar: {
        padding: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: colors.primary_3,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomWidth: 1,
        borderBottomColor: colors.desertWhite,
    },
    scrollView: {
        padding: 20,
    },
    sizeChartTopContainer: {
        padding: 20,
        borderBottomColor: colors.desertWhite,
        borderBottomWidth: 4,
    },
    sizeChartHeaderContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        paddingBottom: 10,
    },
    sizeChartItemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        paddingVertical: 10,
    },
    chartHeaderText: {
        textAlign: 'center',
    },
    sizeChartImage: {
        height: 300,
        width: 280,
        alignSelf: 'center',
        marginBottom: 20,
    },
    linkText: {
        textDecorationLine: 'underline',
    },
    parentLinkTextContainer: {
        marginBottom: 10,
    },
    mealsFlatList: {
        width: '90%',
        alignSelf: 'center',
    },
    mealsContentContainerStyle: {
        paddingVertical: 20,
    },
    singlePlanContainer: {
        height: 40,
        borderBottomWidth: 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    singlePlanText: {
        paddingHorizontal: 20,
    },
    singleFoodTypeContainer: {
        height: 40,
        borderBottomWidth: 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    singleFoodTypeText: {
        paddingHorizontal: 20,
    },
    plansViewContainer: {
        height: 40,
        backgroundColor: colors.white,
    },
    foodTypesViewContainer: {
        height: 40,
        backgroundColor: colors.white,
    },
    planFlatListContentContainer: {
        alignItems: 'center',
    },
    planFlatList: {
        alignSelf: 'flex-start',
        height: 40,
        backgroundColor: colors.white,
        borderBottomWidth: 2,
        borderBottomColor: colors.ghostWhite,
    },
    foodTypesFlatListContentContainer: {
        alignItems: 'center',
    },
    foodTypesFlatList: {
        alignSelf: 'flex-start',
        backgroundColor: colors.white,
        borderBottomWidth: 2,
        borderBottomColor: colors.ghostWhite,
    },
    noDataTextContainer: {
        marginVertical: 10,
        alignItems: 'center',
    },
})

export default styles
