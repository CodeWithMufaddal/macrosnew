import React, { memo, useCallback } from 'react';
import { Linking, ScrollView, TouchableOpacity, View } from 'react-native';
import colors from '../../constants/colors';
import i18n from '../../i18n/i18n';
import CustomIcon from '../custom/CustomIcon';
import CustomModal from '../custom/CustomModal';
import styles from './styles';
import CustomText from '../custom/CustomText'
import { diatCitationLinks } from '../../constants/constants';

const DiatCitationModal = ({ showDiatCitationModal, setShowDiatCitationModal }) => {
    const onClosePress = useCallback(() => {
        setShowDiatCitationModal(false)
    }, [setShowDiatCitationModal])

    return (
        <CustomModal
            isVisible={showDiatCitationModal}
        >
            <View style={styles.mainContainer}>
                <View style={styles.topBar}>
                    <CustomText fontProps={['fs-medium']} color={colors.white} style={styles.titleText}>{i18n.t('references')}</CustomText>
                    <CustomIcon name='close' onPress={onClosePress} color={colors.white} />
                </View>
                <ScrollView style={styles.scrollView} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                    <CustomText customStyle={styles.parentTitleText} color={colors.primary_2} fontProps={['fs-medium']}>{i18n.t('diatCitationTitle')}</CustomText>
                    {diatCitationLinks?.map((item, index) => {
                        const onItemPress = () => Linking.openURL(item?.link)
                        return (
                            <TouchableOpacity onPress={onItemPress} key={`diatCitation-${index}`} style={styles.parentLinkTextContainer}>
                                <CustomText>
                                    ⚫{'  '}<CustomText fontProps={['fs-medium']} color={colors.navyBlue} customStyle={styles.linkText}>{item?.title}</CustomText>
                                </CustomText>
                            </TouchableOpacity>
                        )
                    })}
                </ScrollView>
            </View>
        </CustomModal>
    )
}
export default memo(DiatCitationModal);
