import { StyleSheet } from 'react-native';
import colors from '../../constants/colors';
import { hp } from '../../helpers/screenHelper';

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: colors.white,
        height: hp(80),
        position: 'absolute',
        bottom: 0,
        width: '100%',
        borderRadius: 10,
    },
    titleText: {
        textTransform: 'uppercase',
    },
    parentTitleText: {
        marginBottom: 10,
    },
    topBar: {
        padding: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: colors.primary_3,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomWidth: 1,
        borderBottomColor: colors.desertWhite,
    },
    scrollView: {
        padding: 20,
    },
    sizeChartTopContainer: {
        padding: 20,
        borderBottomColor: colors.desertWhite,
        borderBottomWidth: 4,
    },
    sizeChartHeaderContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        paddingBottom: 10,
    },
    sizeChartItemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        paddingVertical: 10,
    },
    chartHeaderText: {
        textAlign: 'center',
    },
    sizeChartImage: {
        height: 300,
        width: 280,
        alignSelf: 'center',
        marginBottom: 20,
    },
    linkText: {
        textDecorationLine: 'underline',
    },
    parentLinkTextContainer: {
        marginBottom: 10,
    },
})

export default styles
