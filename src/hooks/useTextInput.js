import { useCallback, useState } from 'react';

const useTextInput = (initialValue = '') => {
    const [value, setValue] = useState(initialValue?.toString())
    const onChangeText = useCallback((text) => setValue(text), [])

    const bind = {
        value,
        onChangeText,
    }

    return [value, bind, setValue]
}

export default useTextInput;
