import { useCallback, useState } from 'react';

const usePicker = (initialValue = '') => {
    const [value, setValue] = useState(initialValue?.toString())
    const onValueChange = useCallback((item, index) => {
        setValue(item?.key)
    }, [])

    const bind = {
        selectedValue: value,
        onValueChange,
    }

    return [value, bind, setValue]
}

export default usePicker;
