import { atom, useAtom } from 'react-atomic-state';
import colors from '../constants/colors';

const statusBarBackgroundColor = atom(colors.white);
export const setStatusBarBackgroundColor = (val) => statusBarBackgroundColor.set(val);
export const useStatusBar = () => useAtom(statusBarBackgroundColor);
