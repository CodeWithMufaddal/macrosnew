import { useCallback, useState } from 'react';

const useSlider = (initialValue = 0) => {
    const [value, setValue] = useState(initialValue)
    const [scrollEnabled, setScrollEnabled] = useState(true)

    const onValueChange = useCallback((item) => {
        setValue(item)
    }, [])

    const onValueChangeStart = useCallback(() => {
        setScrollEnabled(false)
    }, [])

    const onValueChangeFinish = useCallback(() => {
        setScrollEnabled(true)
    }, [])
    const bind = {
        value,
        onValueChange,
        onValueChangeStart,
        onValueChangeFinish,
    }

    return [value, scrollEnabled, bind, setValue]
}

export default useSlider;
