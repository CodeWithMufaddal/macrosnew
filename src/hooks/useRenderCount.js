import { useRef } from 'react';
import { ENABLE_CONSOLE_LOGS } from '../constants/constants';

const useRenderCount = (title = '') => {
  const render = useRef(1);
  // eslint-disable-next-line no-plusplus
  return ENABLE_CONSOLE_LOGS && console.log(`${title} Render Count : `, (render.current++)) || 0;
}

export default useRenderCount;
