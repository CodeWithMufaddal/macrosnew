import React, {
    useCallback, useState, memo, useEffect,
} from 'react'
import { View } from 'react-native';
import CodePush from 'react-native-code-push';
import CustomText from '../components/custom/CustomText';
import colors from '../constants/colors';
import { AppCenterConfig, ENABLE_CODE_PUSH } from '../constants/constants';
import { isIOS } from '../helpers/screenHelper';

export const CodePushVersionCode = memo(({ appVersion }) => (
    <View style={{
        marginVertical: 5,
        alignItems: 'center',
        justifyContent: 'center',
    }}>
        <CustomText color={colors.thinGrey}>{`${isIOS ? 'iOS' : 'Android'} App Version: ${appVersion?.appVersion ?? 0} / ${appVersion?.label ?? 0}`}</CustomText>
    </View>
))

const useCodePush = () => {
    const [codePushStatus, setCodePushStatus] = useState('')
    const [isUpToDate, setIsUpToDate] = useState('no');
    const [version, setVersion] = useState(null);

    useEffect(() => {
        getAppVersion()
    }, [])

    const getAppVersion = async () => {
        const v = await CodePush.getUpdateMetadata();
        setVersion(v);
    }

    const setUpdated = useCallback(() => {
        setTimeout(() => setIsUpToDate('yes'), 1000)
    }, [])

    const getDownloadingProgress = useCallback((receivedBytes, totalBytes) => Math.ceil((receivedBytes / totalBytes) * 100), [])

    const checkForUpdate = useCallback(async () => {
        let v = await CodePush.getUpdateMetadata();
        setVersion(v);
        setCodePushStatus('Downloading Updates...');
        CodePush.sync(
            {
                deploymentKey: AppCenterConfig.CODEPUSH_DEPLOYMENT_KEY,
                installMode: CodePush.InstallMode.IMMEDIATE,
            },
            async (status) => {
                console.log('rk', status)
                switch (status) {
                    case CodePush.SyncStatus.CHECKING_FOR_UPDATE: return setCodePushStatus('Checking For Updates...');
                    case CodePush.SyncStatus.SYNC_IN_PROGRESS: return setCodePushStatus('Syncing Updates...');
                    case CodePush.SyncStatus.DOWNLOADING_PACKAGE: return setCodePushStatus('Downloading Updates...');
                    case CodePush.SyncStatus.INSTALLING_UPDATE: return setCodePushStatus('Installing Updates...');
                    case CodePush.SyncStatus.UPDATE_INSTALLED: {
                        v = await CodePush.getUpdateMetadata();
                        setVersion(v);
                        setCodePushStatus('App Updated. Restarting App');
                        setUpdated()
                        break;
                    }
                    case CodePush.SyncStatus.UP_TO_DATE:
                    default: {
                        setCodePushStatus('App is up to date');
                        setUpdated()
                        break;
                    }
                }
                return null;
            },
            ({ receivedBytes, totalBytes }) => {
                setCodePushStatus(`Downloading: updates...\n${getDownloadingProgress(receivedBytes, totalBytes)} / 100`)
            },
        ).catch((error) => {
            console.log('rk e', error)
        });
    }, [getDownloadingProgress, setUpdated])

    const codepushSync = useCallback(() => {
        if (ENABLE_CODE_PUSH && !__DEV__) {
            checkForUpdate()
        } else {
            setCodePushStatus('App is up to date');
            setUpdated()
        }
    }, [checkForUpdate, setUpdated])

    return {
        codepushSync,
        isUpToDate,
        codePushStatus,
        version,
    }
}

export default useCodePush
