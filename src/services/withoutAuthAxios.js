/* eslint-disable import/no-unresolved */
import axios from 'axios';
import { BASE_URL } from '@env'
import { ENABLE_CONSOLE_LOGS } from '../constants/constants';
import CustomCrashlytics from '../../crashlytics/CustomCrashlytics';

const withoutAuthAxios = axios.create({
    baseURL: BASE_URL,
});

withoutAuthAxios.interceptors.request.use((config) => {
    ENABLE_CONSOLE_LOGS && console.log(`%c API REQUEST FOR : ${config?.url} `, 'background: #222; color: #FFFF00', config)
    return config
});

withoutAuthAxios.interceptors.response.use((res) => {
    ENABLE_CONSOLE_LOGS && console.log(`%c API SUCCESS RESPONSE FOR : ${res?.config?.url} `, 'background: #222; color: #00FF00', res)
    return res?.data;
},
(error) => {
    CustomCrashlytics.recordError(error);
    ENABLE_CONSOLE_LOGS && console.log(`%c API ERROR RESPONSE FOR : ${error?.config?.url} `, 'background: #222; color: #FF0000', error?.response)
    return Promise.reject(error?.response)
});

export default withoutAuthAxios;
