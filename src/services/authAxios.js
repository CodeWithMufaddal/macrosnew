/* eslint-disable import/no-unresolved */
import axios from 'axios';
import { BASE_URL } from '@env'
import store from '../redux/store';
import { logoutAction } from '../redux/actions/AuthActions';
import { ENABLE_CONSOLE_LOGS } from '../constants/constants';
import CustomCrashlytics from '../../crashlytics/CustomCrashlytics';

const authAxios = axios.create({
    baseURL: BASE_URL,
});

authAxios.interceptors.request.use((config) => {
    const state = store.getState()
    const token = state?.auth?.tokenData?.token ?? '';
    const language = state?.settings?.appLanguage;
    config.headers.Token = token;
    config.headers.Language = language;
    ENABLE_CONSOLE_LOGS && console.log(`%c API REQUEST FOR : ${config?.url} `, 'background: #222; color: #FFFF00', config)
    return config;
});

authAxios.interceptors.response.use((res) => {
    ENABLE_CONSOLE_LOGS && console.log(`%c API SUCCESS RESPONSE FOR : ${res?.config?.url} `, 'background: #222; color: #00FF00', res)
    return res?.data;
},
(error) => {
    CustomCrashlytics.recordError(error);
    if (error?.response?.status === 401) {
        store.dispatch(logoutAction());
    }
    ENABLE_CONSOLE_LOGS && console.log(`%c API ERROR RESPONSE FOR : ${error?.config?.url} `, 'background: #222; color: #FF0000', error?.response)
    return Promise.reject(error?.response)
});

export default authAxios;
