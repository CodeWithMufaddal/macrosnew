/* eslint-disable no-param-reassign */
/* eslint-disable import/no-unresolved */
import {
    BASE_URL,
    GOOGLE_MAPS_API_KEY,
} from '@env'
import qs from 'qs';
import EndPoints from './EndPoints';
import withoutAuthAxios from './withoutAuthAxios';
import authAxios from './authAxios';
import normalAxios from './normalAxios';

export const getServerImagePath = (type, imageName) => `${BASE_URL}/upload/${type}/${imageName}`
export const getRandomImagePath = (size = '400x400') => `https://source.unsplash.com/random/${size}`;

// Get Areas
export function getAreas() {
    return withoutAuthAxios.get(EndPoints.getAreas);
}
// Get Goals
export function getGoals() {
    return authAxios.get(EndPoints.getGoals);
}

// Get All Meal Types
export function getAllMealTypes() {
    return authAxios.get(EndPoints.getAllMealTypes);
}
// Get All Meals By Meal Type ID
export function getAllMealsByMealTypeId(mealTypeId) {
    return authAxios.get(`${EndPoints.getAllMealsByMealTypeId}/${mealTypeId}`);
}

// Get Meal By MealTypeId By Date
export function getMealByMealTypeIdByDate(data) {
    const postData = qs.stringify(data);
    return authAxios.post(EndPoints.getMealsByMealTypeIdAndByDate, postData);
}

// Get Meal Types By Goal ID
export function getMealTypesByGoalId(goalId) {
    return authAxios.get(`${EndPoints.getMealTypesByGoalId}/${goalId}`);
}

// Get Number Of Days
export function getNumberOfDays(userId) {
    return authAxios.get(`${EndPoints.getNumberOfDays}/${userId}`);
}

// Get Plan List By Days
export function getPlanListByDays(days) {
    return authAxios.get(`${EndPoints.getPlanListByDays}/${days}`);
}

// Check Mobile Number is Registered or not
export function checkMobileNumberIsRegisteredOrNot(mobile_number) {
    const postData = qs.stringify({ mobile_number });
    return withoutAuthAxios.post(EndPoints.checkMobileNumberIsRegisteredOrNot, postData);
}

// Login
export function login(data) {
    const postData = qs.stringify(data);
    return withoutAuthAxios.post(EndPoints.login, postData);
}
// Send OTP
export function sendOTP(data) {
    const postData = qs.stringify(data);
    return withoutAuthAxios.post(EndPoints.sendOTP, postData);
}
// verify OTP
export function verifyOTP(data) {
    const postData = qs.stringify(data);
    return withoutAuthAxios.post(EndPoints.verifyOTP, postData);
}

// Delete Account
export function deleteMyAccount(data) {
    const postData = qs.stringify(data);
    return authAxios.post(EndPoints.deleteMyAccount, postData);
}

// Register
export function register(data) {
    const postData = qs.stringify(data);
    return withoutAuthAxios.post(EndPoints.register, postData);
}

// Apply Coupon Code
export function applyCoupon(data) {
    const postData = qs.stringify(data);
    return authAxios.post(EndPoints.applyCoupon, postData);
}

// Purchase Subscription Payment
export function purchaseSubscriptionPayment(data) {
    const postData = qs.stringify(data);
    return authAxios.post(EndPoints.purchaseSubscriptionPayment, postData);
}
// Subscription Payment
export function subscriptionPayment(data) {
    const postData = qs.stringify(data);
    return authAxios.post(EndPoints.subscriptionPayment, postData);
}

// Save Inbody Sheet
export async function  saveInbodySheet(data) {
    const postData = qs.stringify(data);
    let res = await authAxios.post(EndPoints.saveInbodySheet, postData);
    return res 
}

// Save My Grams
export function saveMyGrams(data) {
    return authAxios.post(EndPoints.saveGrams, data);
}

// Get My Grams
export function getMyGrams(data) {
    const postData = qs.stringify(data);
    return authAxios.post(EndPoints.getGrams, postData);
}

// Save My Health Data
export function saveMyHealthData(data) {
    const postData = qs.stringify(data);
    return authAxios.post(EndPoints.saveMyHealthData, postData);
}

// Save Address
export function saveAddress(data) {
    const postData = qs.stringify(data);
    return authAxios.post(EndPoints.saveAddress, postData);
}

// Get Referral Code
export function getReferralCodeAndBalance(user_id) {
    return authAxios.get(`${EndPoints.getReferralCode}/${user_id}`);
}

// Get Coupon Code
export function generateCouponCode(data) {
    const postData = qs.stringify(data);
    return authAxios.post(EndPoints.generateCouponCode, postData);
}

// Save Address
export function saveMyProfile(data) {
    const postData = qs.stringify(data);
    return authAxios.post(EndPoints.saveMyProfile, postData);
}

// Get Referral History
export function getReferralHistory(user_id) {
    return authAxios.get(`${EndPoints.getReferralHistory}/${user_id}`);
}

// Get Address From Lat Long
export const getAddressFromLatLong = (pinCoordinates) => {
    const latlng = `${pinCoordinates?.latitude},${pinCoordinates?.longitude}`
    return normalAxios.get(EndPoints.googleMapGeoCode, { params: { key: GOOGLE_MAPS_API_KEY, latlng } });
}

// Get My Calender Data
export const getMyCalenderData = (data) => {
    const postData = qs.stringify(data);
    return authAxios.post(EndPoints.getMyCalenderData, postData);
}

// Send Package Target
export const sendPackageTarget = (data) => {
    const postData = qs.stringify(data);
    return authAxios.post(EndPoints.sendPackageTarget, postData);
}

// Get Subscription Details
export const getSubscriptionDetails = (data) => {
    const postData = qs.stringify(data);
    return authAxios.post(EndPoints.getSubscriptionDetails, postData);
}
// Paused Package
export const pausedPackage = (data) => {
    const postData = qs.stringify(data);
    return authAxios.post(EndPoints.pausedPackage, postData);
}

// Resume Package
export const resumePackage = (data) => {
    const postData = qs.stringify(data);
    return authAxios.post(EndPoints.resumePackage, postData);
}

// Review Rating
export const reviewRating = (data) => {
    const postData = qs.stringify(data);
    return authAxios.post(EndPoints.reviewRating, postData);
}

// Select Package Meals
export const selectPackageMeals = (data) => authAxios.post(EndPoints.selectPackageMeals, data)

// Get Selected Meals By Date
export const getSelectedMealsByDate = (data) => {
    const postData = qs.stringify(data);
    return authAxios.post(EndPoints.getSelectedMealsByDate, postData);
}

// Get User Profile
export function getUserProfile(user_id) {
    return authAxios.get(`${EndPoints.getUserProfile}/${user_id}`);
}

// Get Inbody Sheet
export function getUserInbodySheet(user_id) {
    return authAxios.get(`${EndPoints.getUserInbodySheet}/${user_id}`);
}

// Get Banners
export function getBanners() {
    return authAxios.get(EndPoints.getBanners);
}
