import { bottomTabsStacks } from './bottomTabsScreens';
import { MFWebView } from 'myfatoorah-reactnative';

/*** All Screens ***/
const screens = {
    getStarted: { key: 'getStarted', name: 'GetStarted', get component() { return require('../screens/introFlow/getStarted/GetStarted').default; } },
    intro: { key: 'intro', name: 'Intro', get component() { return require('../screens/introFlow/intro/Intro').default; } },
    selectLanguage: { key: 'selectLanguage', name: 'SelectLanguage', get component() { return require('../screens/introFlow/selectLanguage/SelectLanguage').default; } },

    signin: { key: 'SignIn', name: 'SignIn', get component() { return require('../screens/loginFlow/login/SignIn').default; } },
    signup: { key: 'SignUp', name: 'SignUp', get component() { return require('../screens/loginFlow/login/SignUp').default; } },
    verifyOtp: { key: 'verifyOtp', name: 'VerifyOtp', get component() { return require('../screens/loginFlow/verifyOtp/VerifyOtp').default; } },

    purchaseSubscriptionOne: { key: 'purchaseSubscriptionOne', name: 'PurchaseSubscriptionOne', get component() { return require('../screens/purchaseSubscriptionFlow/purchaseSubscriptionDashboard/PurchaseSubscriptionOne').default; } },
    orderSummary: { key: 'orderSummary', name: 'OrderSummary', get component() { return require('../screens/purchaseSubscriptionFlow/orderSummary/OrderSummary').default; } },
    selectGrams: { key: 'selectGrams', name: 'SelectGrams', get component() { return require('../screens/purchaseSubscriptionFlow/selectGrams/SelectGrams').default; } },
    personalDetailsOne: { key: 'personalDetailsOne', name: 'PersonalDetailsOne', get component() { return require('../screens/purchaseSubscriptionFlow/personalDetails/PersonalDetailsOne').default; } },
    personalDetailsTwo: { key: 'personalDetailsTwo', name: 'PersonalDetailsTwo', get component() { return require('../screens/purchaseSubscriptionFlow/personalDetails/PersonalDetailsTwo').default; } },
    inviteFriends: { key: 'inviteFriends', name: 'InviteFriends', get component() { return require('../screens/purchaseSubscriptionFlow/inviteFriends/InviteFriends').default; } },
    
    calenderDashboard: { key: 'CalenderDashboard', name: 'CalenderDashboard', get component() { return require('../screens/afterLoginFlow/calenderStack/CalenderDashboard').default; } },
    macrosDashboard: { key: 'MacrosDashboard', name: 'MacrosDashboard', get component() { return require('../screens/afterLoginFlow/macrosStack/MacrosDashboard').default; } },
    homeDashboard: { key: 'HomeDashboard', name: 'HomeDashboard', get component() { return require('../screens/afterLoginFlow/homeStack/HomeDashboard').default; } },
    supportDashboard: { key: 'SupportDashboard', name: 'SupportDashboard', get component() { return require('../screens/afterLoginFlow/supportStack/SupportDashboard').default; } },
    profileDashboard: { key: 'ProfileDashboard', name: 'ProfileDashboard', get component() { return require('../screens/afterLoginFlow/profileStack/ProfileDashboard').default; } },
    inBodySheet: { key: 'InbodySheet', name: 'InbodySheet', get component() { return require('../screens/afterLoginFlow/profileStack/inbodySheet/InbodySheet').default; } },
    myProfile: { key: 'MyProfile', name: 'MyProfile', get component() { return require('../screens/afterLoginFlow/profileStack/myProfile/MyProfile').default; } },
    referAndEarn: { key: 'ReferAndEarn', name: 'ReferAndEarn', get component() { return require('../screens/afterLoginFlow/profileStack/referAndEarn/ReferAndEarn').default; } },
    referralHistory: { key: 'ReferralHistory', name: 'ReferralHistory', get component() { return require('../screens/afterLoginFlow/profileStack/referralHistory/ReferralHistory').default; } },
    singleDateManegeMeal: { key: 'SingleDateManegeMeal', name: 'SingleDateManegeMeal', get component() { return require('../screens/afterLoginFlow/calenderStack/SingleDateManegeMeal').default; } },
    singleDateManegeGram: { key: 'SingleDateManegeGram', name: 'SingleDateManegeGram', get component() { return require('../screens/afterLoginFlow/calenderStack/SingleDateManegeGram').default; } },
    productPage: { key: 'productPage', name: 'productPage', get component() { return require('../screens/afterLoginFlow/calenderStack/ProductPage').default; } },
    notifications: { key: 'Notifications', name: 'Notifications', get component() { return require('../screens/afterLoginFlow/profileStack/notifications/Notifications').default; } },
    MFWebView: { key: 'MFWebView', name: 'MFWebView', component: MFWebView, options: MFWebView.navigationOptions },
    paymentGatewayWebview: { key: 'PaymentWebView', name: 'PaymentWebView', get component() { return require('../screens/afterLoginFlow/homeStack/paymentView/PaymentWebView').default; } },
    ...bottomTabsStacks,
}

/**
 * Intro Flow Screens
 */
export const introFlowScreens = [
    screens.intro,
    screens.getStarted,
    screens.selectLanguage,
]

/**
 * Login Flow Screens
 */
export const loginFlowScreens = [
    screens.signup,
    screens.signin,
    screens.verifyOtp,
    screens.notifications,
]

/**
 * Purchase Package Flow Screens
 */
export const purchasePackageFlowScreens = [
    screens.purchaseSubscriptionOne,
    screens.orderSummary,
    screens.selectGrams,
    screens.notifications,
    screens.paymentGatewayWebview,
]

/**
 * Personal Detail Purchase Subscription Flow Screens
 */
export const personalDetailWhilePurchaseSubscriptionFlowScreens = [
    screens.personalDetailsOne,
    screens.personalDetailsTwo,
    screens.inviteFriends,
    screens.notifications,
]

/**
 * After Login Flow Screens
 */

export const afterLoginStackScreens = [
    screens.inBodySheet,
    screens.myProfile,
    screens.referAndEarn,
    screens.referralHistory,
    screens.singleDateManegeMeal,
    screens.singleDateManegeGram,
    screens.productPage,
    screens.notifications,
]

// Tabs Screens
export const calenderStackScreens = [screens.calenderDashboard, ...afterLoginStackScreens]
export const macrosStackScreens = [screens.macrosDashboard, ...afterLoginStackScreens]
export const homeStackScreens = [screens.homeDashboard, ...afterLoginStackScreens]
export const supportStackScreens = [screens.supportDashboard, ...afterLoginStackScreens]
export const profileStackScreens = [screens.profileDashboard, ...afterLoginStackScreens]

export const afterLoginFlowScreens = [
    screens.calenderStacks,
    screens.macrosStack,
    screens.homeStack,
    screens.supportStack,
    screens.profileStack,
]

export const appFlows = {
    introFlowNavigator: {
        flow: 0, name: 'IntroFlowNavigator', get flowComponent() { return require('./flows/introFlow/IntroFlowNavigator').default; }, screens: introFlowScreens,
    },
    loginFlowNavigator: {
        flow: 1, name: 'LoginFlowNavigator', get flowComponent() { return require('./flows/loginFlow/LoginFlowNavigator').default; }, screens: loginFlowScreens,
    },
    purchaseSubscriptionFlowNavigator: {
        flow: 2, name: 'PurchaseSubscriptionFlowNavigator', get flowComponent() { return require('./flows/purchaseSubscriptionFlow/PurchaseSubscriptionFlowNavigator').default; }, screens: purchasePackageFlowScreens,
    },
    personalDetailWhilePurchaseSubscriptionFlowNavigator: {
        flow: 3, name: 'PersonalDetailWhilePurchaseSubscriptionFlowNavigator', get flowComponent() { return require('./flows/personalDetailWhilePurchaseSubscriptionFlow/PersonalDetailWhilePurchaseSubscriptionFlowNavigator').default; }, screens: personalDetailWhilePurchaseSubscriptionFlowScreens,
    },
    afterLoginFlowNavigator: {
        flow: 4, name: 'AfterLoginFlowNavigator', get flowComponent() { return require('./flows/afterLoginFlow/AfterLoginFlowNavigator').default; }, screens: afterLoginFlowScreens,
    },
}

export default screens;
