/* eslint-disable max-len */
/* eslint-disable import/no-cycle */
import * as navigation from './RootNavigation'

export const navigationType = {
    default: 'push',
    navigate: 'navigate',
    push: 'push',
    replace: 'replace',
    reset: 'reset',
    jumpTo: 'jumpTo',
}
export const navigateGoBack = () => navigation.goBack()
export const navigateTo = (screen, params, navigationsType = navigationType.default) => navigation[navigationsType](screen.name, params)
