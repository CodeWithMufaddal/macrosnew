import React, { Fragment } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import screens, { purchasePackageFlowScreens } from '../../screens';
import GlobalStackNavigator from '../../navigators/stackNavigator/GlobalStackNavigator';

const Stack = createStackNavigator();

const PurchaseSubscriptionFlowNavigator = () => (
    <GlobalStackNavigator initialRouteName={purchasePackageFlowScreens?.[0]?.name}>
        {purchasePackageFlowScreens.map((item) => (
            <Stack.Screen key={item?.key} name={item?.name} component={item?.component} options={item?.options} />
        ))}
        <Stack.Group screenOptions={{ presentation: 'modal', headerShown: true, headerMode: 'screen' }}>
            <Stack.Screen name={screens.MFWebView.name} component={screens.MFWebView.component} options={screens.MFWebView.options} />
        </Stack.Group>
    </GlobalStackNavigator>
)

export default PurchaseSubscriptionFlowNavigator;
