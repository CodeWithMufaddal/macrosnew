import React, { useCallback } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import GlobalBottomTabNavigator from '../../navigators/bottomTabNavigator/GlobalBottomTabNavigator';
import CustomTabBarIcon from '../../navigators/bottomTabNavigator/CustomTabBarIcon';
import screens, { afterLoginFlowScreens } from '../../screens';

const Tab = createBottomTabNavigator();

const AfterLoginFlowNavigator = () => {
    const renderTabBarIcon = useCallback((tabBarIconProps, item) => (
        <CustomTabBarIcon
            label={item?.title}
            isHome={item?.isHome}
            isProfile={item?.isProfile}
            focused={tabBarIconProps?.focused}
            icon={tabBarIconProps?.focused ? item?.focusedIcon : item.normalIcon}
        />
    ), [])
    return (
        <GlobalBottomTabNavigator initialRouteName={screens.homeStack.name}>
            {afterLoginFlowScreens.map((item) => (
                <Tab.Screen key={item?.key} name={item?.name} component={item?.component} options={{
                    tabBarIcon: (tabBarIconProps) => renderTabBarIcon(tabBarIconProps, item),
                }}/>
            ))}
        </GlobalBottomTabNavigator>
    )
}

export default AfterLoginFlowNavigator;
