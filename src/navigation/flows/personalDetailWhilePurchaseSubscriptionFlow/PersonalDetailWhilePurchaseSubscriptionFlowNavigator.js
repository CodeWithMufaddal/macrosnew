import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { personalDetailWhilePurchaseSubscriptionFlowScreens } from '../../screens';
import GlobalStackNavigator from '../../navigators/stackNavigator/GlobalStackNavigator';

const Stack = createStackNavigator();

const PersonalDetailWhilePurchaseSubscriptionFlowNavigator = () => (
    <GlobalStackNavigator initialRouteName={personalDetailWhilePurchaseSubscriptionFlowScreens?.[0]?.name}>
        {personalDetailWhilePurchaseSubscriptionFlowScreens.map((item) => (
            <Stack.Screen key={item?.key} name={item?.name} component={item?.component}/>
        ))}
    </GlobalStackNavigator>
)

export default PersonalDetailWhilePurchaseSubscriptionFlowNavigator;
