import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { loginFlowScreens } from '../../screens';
import GlobalStackNavigator from '../../navigators/stackNavigator/GlobalStackNavigator';

const Stack = createStackNavigator();

const LoginFlowNavigator = () => (
  <GlobalStackNavigator initialRouteName={loginFlowScreens?.[0]?.name}>
    {loginFlowScreens.map((item) => (
      <Stack.Screen key={item?.key} name={item?.name} component={item?.component}/>
    ))}
  </GlobalStackNavigator>
)

export default LoginFlowNavigator;
