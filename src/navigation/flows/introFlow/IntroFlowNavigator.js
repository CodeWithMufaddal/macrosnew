import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { introFlowScreens } from '../../screens';
import GlobalStackNavigator from '../../navigators/stackNavigator/GlobalStackNavigator';

const Stack = createStackNavigator();

const IntroFlowNavigator = () => (
    <GlobalStackNavigator initialRouteName={introFlowScreens?.[0]?.name}>
        {introFlowScreens.map((item) => (
            <Stack.Screen key={item?.key} name={item?.name} component={item?.component} />
        ))}
    </GlobalStackNavigator>
)

export default IntroFlowNavigator;
