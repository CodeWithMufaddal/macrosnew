import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

const GlobalStackNavigator = ({ initialRouteName = '', screenOptions, ...props }) => (
    <Stack.Navigator
        initialRouteName={initialRouteName}
        screenOptions={{
            headerShown: false,
            headerMode: 'screen',
            presentation: 'card',
            ...screenOptions,
        }}
    >
        {props.children}
    </Stack.Navigator>
);

export default GlobalStackNavigator;
