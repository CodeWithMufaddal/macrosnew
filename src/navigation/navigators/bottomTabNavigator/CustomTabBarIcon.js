import React, { useMemo, memo } from 'react';
import { View, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import images from '../../../assets/images/images';
import CustomImage from '../../../components/custom/CustomImage';
import CustomText from '../../../components/custom/CustomText';
import colors from '../../../constants/colors';

const CustomTabBarIcon = ({
    isHome, label, focused, icon, isProfile, ...props
}) => {
    const renderBottomTabCircle = useMemo(() => isHome && (
        <View style={styles.bottomCircleContainer}>
            <CustomImage SvgSource={images.bottomTabRoundShapeSvg} />
        </View>
    ), [isHome])

    const renderBadgeIcon = useMemo(() => props?.notificationUnreadCount > 0 && (
        <View style={styles.badgeContainer}>
            <CustomText fontProps={['ff-roboto-medium']} color={colors.white}>{props?.notificationUnreadCount}</CustomText>
        </View>
    ), [props?.notificationUnreadCount])
    return (
        <>
            {renderBottomTabCircle}
            <View style={{ ...styles.mainContainer }}>
                {isHome ? <CustomImage source={icon} tintColor={focused ? colors.primary_2 : colors.borderColor} style={{ height: 30, width: 40, top: -10 }} /> : <CustomImage SvgSource={icon} />}
                <CustomText
                    fontProps={['ff-roboto-medium', 'fs-xxsmall']}
                    color={focused ? colors.primary_2 : colors.borderColor}
                    customStyle={{ ...styles.title, top: isHome ? -2 : 0 }}>
                    {label}
                </CustomText>
                {isProfile && renderBadgeIcon}
            </View>
        </>
    )
}

export const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        width: '100%',
    },
    title: {
        marginTop: 5,
    },
    bottomCircleContainer: {
        position: 'absolute',
        top: -9,
        zIndex: -1,
        alignSelf: 'center',
        width: '100%',
    },
    badgeContainer: {
        position: 'absolute',
        backgroundColor: colors.primary_3,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        top: 10,
        right: 12,
        height: 20,
        width: 20,
    },
})

export default memo(connect((state) => ({
    notificationUnreadCount: state?.notifications?.notificationUnreadCount,
}))(CustomTabBarIcon))
