import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import colors from '../../../constants/colors';
import { commonShadow } from '../../../styles/CommonStyles';

const Tab = createBottomTabNavigator();

const GlobalBottomTabNavigator = ({ initialRouteName, ...props }) => (
    <Tab.Navigator
        initialRouteName={initialRouteName}
        screenOptions={{
            // tabBarHideOnKeyboard: true,
            lazy: true,
            headerShown: false,
            tabBarShowLabel: false,
            tabBarStyle: {
                height: 75,
                backgroundColor: colors.white,
                borderTopWidth: 0,
                ...commonShadow({ radius: 10 }),
            },
        }}
    >
        {props.children}
    </Tab.Navigator>
);

export default GlobalBottomTabNavigator;
