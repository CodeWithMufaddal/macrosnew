import React, { Fragment } from 'react';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { connect } from 'react-redux';
import { createStackNavigator } from '@react-navigation/stack';
import colors from '../constants/colors';
import { navigationRef } from './RootNavigation';
import { appFlows } from './screens';
import NoInternetModal from '../screens/common/NoInternetModal';
import GlobalStackNavigator from './navigators/stackNavigator/GlobalStackNavigator';

const Stack = createStackNavigator();

const myTheme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        background: colors.white,
    },
};

const AppNavigator = (props) => {
    const { appFlow: { flow: currentFlow } } = props;
    return (
        <NavigationContainer theme={myTheme} ref={navigationRef}>
            <GlobalStackNavigator>
                {Object.keys(appFlows).map((item) => {
                    const { flow, name, flowComponent } = appFlows?.[item];
                    return (
                        <Fragment key={`appFlow-${flow}`}>
                            {currentFlow === flow && (
                                <Stack.Screen key={name} name={name} component={flowComponent} />
                            )}
                        </Fragment>
                    )
                })}
            </GlobalStackNavigator>
        </NavigationContainer>
    )
}

export default connect((state) => ({
    appFlow: state.settings.appFlow,
}))(AppNavigator);
