import { createNavigationContainerRef, StackActions } from '@react-navigation/native';

// Navigation Ref
export const navigationRef = createNavigationContainerRef()

// Navigate
export const navigate = (name, params) => {
    if (navigationRef.isReady()) {
        navigationRef.navigate(name, params);
    }
}

// Push
export const push = (name, params) => {
    if (navigationRef.isReady()) {
        navigationRef.dispatch(StackActions.push(name, params));
    }
}

// Replace
export const replace = (name, params) => {
    if (navigationRef.isReady()) {
        navigationRef.dispatch(StackActions.replace(name, params));
    }
}

// Can Go back
export const canGoBack = () => navigationRef.canGoBack();

// Go back
export const goBack = () => {
    if (navigationRef.isReady() && canGoBack()) {
        navigationRef?.dispatch(StackActions?.pop(1));
    }
}
