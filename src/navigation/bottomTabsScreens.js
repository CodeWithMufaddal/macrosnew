import React from 'react';
import { createStackNavigator } from '@react-navigation/stack'
import GlobalStackNavigator from './navigators/stackNavigator/GlobalStackNavigator'
import {
    calenderStackScreens, homeStackScreens, macrosStackScreens, profileStackScreens, supportStackScreens,
} from './screens'
import images from '../assets/images/images';

const Stack = createStackNavigator();

export const CalenderStack = () => (
    <GlobalStackNavigator>
        {calenderStackScreens.map((item) => (
            <Stack.Screen key={item.key} name={item.name} component={item.component} options={item?.options} />
        ))}
    </GlobalStackNavigator>
)

export const MacrosStack = () => (
    <GlobalStackNavigator>
        {macrosStackScreens.map((item) => (
            <Stack.Screen key={item.key} name={item.name} component={item.component} options={item?.options} />
        ))}
    </GlobalStackNavigator>
)

export const HomeStack = () => (
    <GlobalStackNavigator>
        {homeStackScreens.map((item) => (
            <Stack.Screen key={item.key} name={item.name} component={item.component} options={item?.options} />
        ))}
    </GlobalStackNavigator>
)

export const SupportStack = () => (
    <GlobalStackNavigator>
        {supportStackScreens.map((item) => (
            <Stack.Screen key={item.key} name={item.name} component={item.component} options={item?.options} />
        ))}
    </GlobalStackNavigator>
)

export const ProfileStack = () => (
    <GlobalStackNavigator>
        {profileStackScreens.map((item) => (
            <Stack.Screen key={item.key} name={item.name} component={item.component} options={item?.options} />
        ))}
    </GlobalStackNavigator>
)

export const bottomTabsStacks = {
    calenderStacks: {
        key: 'Calender',
        name: 'CalenderTab',
        title: 'Calender',
        // component: CalenderStack,
        get component() { return CalenderStack },
        normalIcon: images
            .calenderSvg,
        focusedIcon: images.selectedCalenderSvg,
    },
    macrosStack: {
        key: 'Macros',
        name: 'MacrosTab',
        title: 'Macros',
        // component: MacrosStack,
        get component() { return MacrosStack },
        normalIcon: images.mealsSvg,
        focusedIcon: images.selectedMealsSvg,
    },
    homeStack: {
        key: 'Home',
        name: 'HomeTab',
        title: 'HOME',
        // component: HomeStack,
        get component() { return HomeStack },
        normalIcon: images.appSymbol,
        focusedIcon: images.appSymbol,
        isHome: true,
    },
    supportStack: {
        key: 'Support',
        name: 'SupportTab',
        title: 'Support',
        // component: SupportStack,
        get component() { return SupportStack },
        normalIcon: images.supportSvg,
        focusedIcon: images.selectedSupportSvg,
    },
    profileStack: {
        key: 'Profile',
        name: 'ProfileTab',
        title: 'Profile',
        isProfile: true,
        // component: ProfileStack,
        get component() { return ProfileStack },
        normalIcon: images.profileSvg,
        focusedIcon: images.selectedProfileSvg,
    },
}
