import { OneSignal } from 'react-native-onesignal';
import { getOS, getValidDate } from '../helpers/CommonHelpers';

const CustomOneSignal = {
    setAppId: (APP_ID) => OneSignal.initialize(APP_ID),
    setLogLevel: (nsLogLevel) => OneSignal.Debug.setLogLevel(nsLogLevel),
    getNotificationId: () => OneSignal.User.pushSubscription.getIdAsync(),
    setExternalUserId: (userIds) => OneSignal.login(userIds),
    removeExternalUserId: () => OneSignal.logout(),
    userIsRegistered: (isRegistered) => OneSignal.User.addTags({ is_Registered: isRegistered ? 'Yes' : 'No', deviceType: getOS() }),
    userIsSubscribed: (isSubscribed) => OneSignal.User.addTag('is_Subscribed', isSubscribed ? 'Yes' : 'No'),
    userMealSelected: (date) => OneSignal.User.addTag('meal_selected', getValidDate(new Date(date), 'YYYY-MM-DD')),
}
export default CustomOneSignal;
