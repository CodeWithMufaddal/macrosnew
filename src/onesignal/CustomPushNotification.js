import PushNotification from 'react-native-push-notification';
import { checkNotifications, RESULTS } from 'react-native-permissions';
import AsyncStorage from '@react-native-async-storage/async-storage';
import store from '../redux/store';
import * as NotificationsActions from '../redux/actions/NotificationsActions';
import { ANDROID_CHANNEL_ID } from '../constants/constants';
import AsyncKeys from '../constants/AsyncKeys';

const CustomPushNotification = {
    configure: async () => {
        CustomPushNotification.getInitialNotification();
        const notificationData = await AsyncStorage.getItem(AsyncKeys.NOTIFICATION_LIST);
        const notiData = notificationData ? JSON.parse(notificationData) : [];
        if (notiData?.length > 0) store.dispatch(NotificationsActions.setNotificationListAction(notiData));
        PushNotification.configure({
            onNotification: CustomPushNotification.handleNotification,
            permissions: {
                alert: true,
                badge: true,
                sound: true,
            },
            popInitialNotification: true,
            requestPermissions: true,
        });
        CustomPushNotification.checkAndGetPermissionIfAlreadyGiven('configure');
        CustomPushNotification.initAndroidLocalNotifications();
        await new Promise((resolve) => setTimeout(resolve, 1000));
    },
    handleAppStateChange: (newState) => {
        if (newState === 'active') CustomPushNotification.checkAndGetPermissionIfAlreadyGiven('appstate change');
    },
    checkPermission: async () => {
        const authStatus = await checkNotifications().then(({ status }) => status);
        // …'unavailable' | 'denied' | 'limited' | 'granted' | 'blocked'
        let permission = { granted: false, canAsk: false };
        switch (authStatus) {
            case RESULTS.UNAVAILABLE:
                permission = { granted: false, canAsk: false };
                break;
            case RESULTS.DENIED:
                permission = { granted: false, canAsk: true };
                break;
            case RESULTS.LIMITED:
                permission = { granted: true };
                break;
            case RESULTS.GRANTED:
                permission = { granted: true };
                break;
            case RESULTS.BLOCKED:
                permission = { granted: false, canAsk: false };
                break;
            default: break;
        }
        return permission;
    },
    checkAndGetPermissionIfAlreadyGiven: async () => {
        const { granted } = await CustomPushNotification.checkPermission();
        if (!granted) return true;
        const permission = await PushNotification.requestPermissions();
        // const permission = await OneSignal.Notifications.requestPermission()
        return permission;
    },
    channelId: ANDROID_CHANNEL_ID,
    initAndroidLocalNotifications: () => {
        PushNotification.createChannel(
            {
                channelId: CustomPushNotification.channelId,
                channelName: 'Push local notifications',
                soundName: 'default',
                importance: 4,
                vibrate: true,
            },
        );
    },
    localNotification: ({
        title, message, playSound = true, soundName = 'default',
    } = {}) => {
        PushNotification.localNotification({
            title,
            message,
            playSound,
            soundName,
            channelId: CustomPushNotification.channelId,
        });
    },
    cancelAll: () => {
        PushNotification.cancelAllLocalNotifications();
    },
    getInitialNotification: () => {
        PushNotification.popInitialNotification(async (notification) => {
            setTimeout(() => {
                if (notification) CustomPushNotification.handleNotification(notification);
                
            }, 3000);
        })
    },
    getDeliveredNotifications: () => new Promise((resolve) => {
        PushNotification.getDeliveredNotifications((notifications) => {
            resolve(notifications);
        });
    }),
    removeAllDeliveredNotifications: () => {
        PushNotification.removeAllDeliveredNotifications();
    },
    handleNotification: (notification) => {
        const notiData = JSON.parse(JSON.stringify(notification));
        store.dispatch(NotificationsActions.addNotificationListAction(notiData));
    },
}

export default CustomPushNotification;
