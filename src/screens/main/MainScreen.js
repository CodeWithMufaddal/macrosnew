import React, { useState, useEffect, useMemo, useCallback } from "react";
import { I18nManager, View } from "react-native";
import { connect } from "react-redux";
import VersionCheck from "react-native-version-check";
import CustomLoader from "../../components/custom/CustomLoader";
import i18n from "../../i18n/i18n";
import AppNavigator from "../../navigation/AppNavigator";
import { navigateTo, navigationType } from "../../navigation/navigation";
import screens from "../../navigation/screens";
import { navigationRef } from "../../navigation/RootNavigation";
import NeedUpdateScreen from "../common/AppNeedUpdateScreen";
import { STORE_LINK } from "../../constants/constants";
import useCodePush from "../../codepush/useCodePush";
import { OneSignal } from "react-native-onesignal";
import CustomPushNotification from "../../onesignal/CustomPushNotification";

const MainScreen = props => {
  const [visible, setVisible] = useState(false);
  const [appNeedUpdate, setAppNeedUpdate] = useState("MAYBE");
  const [storeLink, setStoreLink] = useState(STORE_LINK);
  const [title, setTitle] = useState();
  const { codepushSync, codePushStatus, isUpToDate } = useCodePush();

  const checkUpdateNeeded = useCallback(async () => {
    VersionCheck.needUpdate()
      .then(updateNeeded => {
        setTimeout(() => setStoreLink(updateNeeded?.storeUrl), 1000);
        setAppNeedUpdate(updateNeeded?.isNeeded ? "YES" : "NO");
        if (!updateNeeded.isNeeded) codepushSync();
        else setTimeout(() => setVisible(true), 2000);
      })
      .catch(() => {
        setAppNeedUpdate("NO");
        codepushSync();
      });
  }, [codepushSync]);

  const initApp = useCallback(async () => {
    if (props.appLanguage) {
      const appLanguage = props?.appLanguage;
      await i18n.changeLanguage(appLanguage);
      await I18nManager.forceRTL(["ar", "he", "fa"].includes(appLanguage));
    }
  }, [props.appLanguage]);

  useEffect(() => {
    checkUpdateNeeded();
    CustomPushNotification.configure();
    oneSignalInit();
    oneSignalNotificationInit()
    initApp();
  }, [checkUpdateNeeded, initApp]);

  useEffect(() => {
    if (isUpToDate === "yes") {
      setTitle("Loading...");
      setVisible(true);
    }
  }, [isUpToDate]);
  const oneSignalNotificationInit = async () => {
    OneSignal.Notifications.addEventListener('foregroundWillDisplay', (event) => {
      event.preventDefault();
      CustomPushNotification.handleNotification(event)
      event.getNotification().display();
    });
  }
  
  const oneSignalInit = async () => {
    await OneSignal.Notifications.requestPermission(true);
    OneSignal.Notifications.addEventListener('click', (data) => {
      CustomPushNotification.handleNotification(data)
      const route = navigationRef?.current?.getCurrentRoute();
      if (route?.name !== screens.notifications.name)
        navigateTo(screens.notifications, navigationType.navigate);
    });
  };

  const renderMainScreen = useMemo(() => {
    if (appNeedUpdate === "YES")
      return <NeedUpdateScreen storeLink={storeLink} />;
    if (!visible && appNeedUpdate === "MAYBE")
      return <CustomLoader wholeScreen={true} title={"Loading..."} />;
    if (appNeedUpdate === "NO" && visible) return <AppNavigator />;
    return (<CustomLoader title={title ?? codePushStatus ?? "Loading..."} wholeScreen={true} />);
  }, [appNeedUpdate, codePushStatus, storeLink, title, visible]);

  return <View style={{ flex: 1 }}>{renderMainScreen}</View>;
};

export default connect(state => ({ appLanguage: state.settings.appLanguage }))(
  MainScreen,
);
