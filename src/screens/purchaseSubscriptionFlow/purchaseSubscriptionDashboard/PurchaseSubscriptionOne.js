import React, {
    useState, useMemo, useCallback, useEffect, Fragment,
} from 'react';
import {
    View, StyleSheet, TouchableOpacity, ScrollView, Linking, RefreshControl,
} from 'react-native';
import { connect, useDispatch } from 'react-redux';
import images from '../../../assets/images/images';
import CustomHeader from '../../../components/custom/CustomHeader';
import CustomImage from '../../../components/custom/CustomImage';
import CustomText from '../../../components/custom/CustomText';
import WhatsYourGoal from '../../../components/purchaseSubscription/whatsYourGoal/WhatsYourGoal';
import SelectMealCategory from '../../../components/purchaseSubscription/selectMealCategory/SelectMealCategory';
import colors from '../../../constants/colors';
import SelectMealPlanDuration from '../../../components/purchaseSubscription/selectMealPlanDuration/SelectMealPlanDuration';
import CustomButton from '../../../components/custom/CustomButton';
import i18n from '../../../i18n/i18n';
import { commonShadow } from '../../../styles/CommonStyles';
import SuggestedPlan from '../../../components/purchaseSubscription/suggestedPlan/SuggestedPlan';
import * as Validation from '../../../validation/Validation';
import SelectStartDate from '../../../components/purchaseSubscription/selectStartDate/SelectStartDate';
import { navigateTo } from '../../../navigation/navigation';
import screens, { appFlows } from '../../../navigation/screens';
import {
    getExpireInDays, getHitSlop, getMinimumStartDate, getWholePlanDuration,
} from '../../../helpers/CommonHelpers';
import { logoutAction } from '../../../redux/actions/AuthActions';
import { MOBILE_NUMBER_PREFIX, PLAN_MINIMUM_EXPIRE_DAYS, WHATSAPP_CHAT_LINK } from '../../../constants/constants';
import * as SubscriptionActions from '../../../redux/actions/SubscriptionActions';
import { changeAppFlow } from '../../../redux/actions/SettingsActions';

const PurchaseSubscriptionOne = (props) => {
    const dispatch = useDispatch();
    const [isRenew, setIsRenew] = useState(false);
    const [showSelectDateModal, setShowSelectDateModal] = useState(false);
    const [startDate, setStartDate] = useState(null);
    const [minStartDate, setMinStartDate] = useState(new Date());
    const [selectedGoal, setSelectedGoal] = useState();
    const [selectedMealCategory, setSelectedMealCategory] = useState(null);
    const [selectedMealPlanDuration, setSelectedMealPlanDuration] = useState(null);
    const [selectedPlan, setSelectedPlan] = useState(null);
    const [refreshing, setRefreshing] = useState(false);

    useEffect(() => {
        const wholePlanDurationData = getWholePlanDuration(props?.subscriptionDetails?.data);
        const expiresIn = wholePlanDurationData ? getExpireInDays(wholePlanDurationData?.end_time) : -1;
        if (wholePlanDurationData && expiresIn >= 0 && expiresIn <= PLAN_MINIMUM_EXPIRE_DAYS) {
            setIsRenew(true);
        }
    }, [props?.subscriptionDetails?.data])

    useEffect(() => {
        dispatch(SubscriptionActions.getGoalsAction());
        dispatch(SubscriptionActions.getAllMealTypesAction());
    }, [])

    useEffect(() => {
        if (selectedMealCategory?.id && selectedGoal?.id) {
            setSelectedMealPlanDuration(null);
            dispatch(SubscriptionActions.sendPackageTargetAction(selectedGoal?.id, selectedMealCategory?.id))
            setSelectedPlan(null);
        }
    }, [selectedMealCategory])

    const onContinuePress = useCallback(() => {
        if (Validation.purchaseSubscriptionOneValidation({
            selectedGoal, selectedMealCategory, selectedMealPlanDuration, selectedPlan,
        })) {
            setShowSelectDateModal(true);
        }
    }, [selectedMealCategory, selectedMealPlanDuration, selectedGoal, selectedPlan]);

    const onPressCalenderConfirm = useCallback(() => {
        if (Validation.purchaseSubscriptionOneStartDateValidation({ startDate })) {
            setShowSelectDateModal(false);
            const isCameFromHomeDashboard = !props?.isNewUser;
            const data = {
                selectedGoal, isCameFromHomeDashboard, selectedMealCategory, selectedMealPlanDuration, selectedPlan, startDate, isRenew,
            }
            if (selectedGoal?.is_custom_grams == '1') navigateTo(screens.selectGrams, data)
            else navigateTo(screens.orderSummary, data)
        }
    }, [isRenew, props?.isNewUser, selectedGoal, selectedMealCategory, selectedMealPlanDuration, selectedPlan, startDate])

    // Header
    const headerLeftComponent = useMemo(() => (
        <View style={headerStyles.headerLeftContainer}>
            <CustomImage source={images.appSymbol} style={headerStyles.appLeftSymbol} />
            <View>
                <CustomText fontProps={['ff-roboto-thin', 'fs-large']} numberOfLines={1}>Hi, {props?.userData?.first_name}</CustomText>
                <CustomText
                    fontProps={['ff-roboto-medium', 'fs-xsmall']}
                    customStyle={styles.headerMobileNumber}>
                    {MOBILE_NUMBER_PREFIX} {props?.userData?.phone}
                </CustomText>
            </View>
        </View>
    ), [props?.userData?.first_name, props?.userData?.phone])

    const onLogoutPress = useCallback(() => {
        if (isRenew || !props?.isNewUser) dispatch(changeAppFlow(appFlows.afterLoginFlowNavigator))
        else dispatch(logoutAction());
    }, [dispatch, isRenew, props?.isNewUser])

    const renderGoToDashboard = useMemo(() => (
        <View style={styles.gotoDashbordContainer}>
            <CustomText
                color={colors.primary_3}
                fontProps={['ff-roboto-bold']}
                customStyle={styles.gotoText}>
                {i18n.t('go_to')}
            </CustomText>
            <CustomText
                customStyle={styles.dashboardText}
                color={colors.primary_3}
                fontProps={['ff-roboto-bold']}
            >{i18n.t('dashboard')}</CustomText>
        </View>
    ), [])

    const onChatPress = useCallback(() => {
        Linking.openURL(WHATSAPP_CHAT_LINK)
    }, [])

    const headerRightComponent = useMemo(() => (
        <View style={styles.headerRightContainer}>
            <TouchableOpacity onPress={onChatPress}><CustomImage svgProps={{ height: 50, width: 50 }} SvgSource={images.chatIcon} /></TouchableOpacity>
            <TouchableOpacity hitSlop={getHitSlop({ top: 20, bottom: 20, right: 20 })} onPress={onLogoutPress}>
                {isRenew
                    ? renderGoToDashboard
                    : (
                        <Fragment>
                            {!props?.isNewUser ? renderGoToDashboard : (
                                <CustomText
                                    color={colors.primary_3}
                                    fontProps={['ff-roboto-bold']}
                                    customStyle={styles.logoutText}>
                                    {i18n.t('logout')}
                                </CustomText>
                            )}
                        </Fragment>
                    )}

            </TouchableOpacity>
        </View >
    ), [isRenew, onChatPress, onLogoutPress, props?.isNewUser, renderGoToDashboard]);

    const renderHeader = useMemo(() => (
        <CustomHeader
            leftComponent={headerLeftComponent}
            rightComponent={headerRightComponent}
        />
    ), [headerLeftComponent, headerRightComponent])

    const onGoalPress = useCallback((goal) => {
        setSelectedGoal(goal);
        if (goal?.id !== 0) {
            dispatch(SubscriptionActions.getMealTypesByGoalIdAction(goal?.id));
            setSelectedMealCategory(null);
        } else {
            setSelectedMealCategory(props?.allMealTypes?.data?.[0])
        }
    }, [dispatch, props?.allMealTypes?.data])

    const renderWhatsYourGoal = useMemo(() => (
        <WhatsYourGoal
            packageData={props?.goals?.data}
            selectedGoal={selectedGoal}
            onGoalPress={onGoalPress}
        />
    ), [onGoalPress, props?.goals?.data, selectedGoal])

    const onMealPlanDurationPress = useCallback((duration) => {
        setSelectedMealPlanDuration(duration)
        dispatch(SubscriptionActions.getPlanListByDaysAction(duration?.days))
        setSelectedPlan(null);
    }, [dispatch])

    const renderSelectMealPlanDuration = useMemo(() => (
        <SelectMealPlanDuration
            loading={props?.numberOfDays?.loading}
            mealPlanDurationData={selectedMealCategory ? props?.numberOfDays?.data : []}
            selectedMealPlanDuration={selectedMealPlanDuration}
            onMealPlanDurationPress={onMealPlanDurationPress}
        />
    ), [onMealPlanDurationPress, props?.numberOfDays?.data, props?.numberOfDays?.loading, selectedMealCategory, selectedMealPlanDuration])

    const renderSelectMealCategory = useMemo(() => (
        <SelectMealCategory
            loading={props?.allMealTypes?.loading || props?.mealTypesByGoalId?.loading}
            mealCategoryData={selectedGoal ? selectedGoal?.id === 0 ? props?.allMealTypes?.data : props?.mealTypesByGoalId?.data : []}
            selectedMealCategory={selectedMealCategory}
            setSelectedMealCategory={setSelectedMealCategory}
        />
    ), [props?.allMealTypes?.loading, props?.allMealTypes?.data, props?.mealTypesByGoalId?.loading, props?.mealTypesByGoalId?.data, selectedGoal, selectedMealCategory])

    const onPlanPress = useCallback((plan) => {
        setSelectedPlan(plan);
        const wholeSubscriptionData = getWholePlanDuration(props?.subscriptionDetails?.data)
        if (wholeSubscriptionData) {
            getMinimumStartDate(plan?.lead_time, wholeSubscriptionData?.start_time ?? null, wholeSubscriptionData?.end_time ?? null).then(async (minStartDt) => {
                setMinStartDate(minStartDt);
                setTimeout(() => setStartDate(minStartDt), 1000);
            });
        } else {
            getMinimumStartDate(plan?.lead_time).then(async (minStartDt) => {
                setMinStartDate(minStartDt);
                setTimeout(() => setStartDate(minStartDt), 1000);
            });
        }
    }, [props?.subscriptionDetails?.data])

    const renderSuggestedPlan = useMemo(() => (
        <SuggestedPlan
            loading={props?.plansByDays?.loading}
            suggestedPlanData={(selectedGoal && selectedMealCategory && selectedMealPlanDuration) ? props?.plansByDays?.data : []}
            selectedPlan={selectedPlan}
            onPlanPress={onPlanPress}
        />
    ), [onPlanPress, props?.plansByDays?.data, props?.plansByDays?.loading, selectedGoal, selectedMealCategory, selectedMealPlanDuration, selectedPlan])

    const onCloseModal = useCallback(() => {
        setShowSelectDateModal(false);
    }, [])

    const renderSelectStartDate = useMemo(() => minStartDate && (
        <SelectStartDate
            minDate={minStartDate}
            onCloseModal={onCloseModal}
            showSelectDateModal={showSelectDateModal}
            startDate={startDate}
            setStartDate={setStartDate}
            onPressCalenderConfirm={onPressCalenderConfirm}
        />
    ), [minStartDate, onCloseModal, onPressCalenderConfirm, showSelectDateModal, startDate])

    const renderContinueButton = useMemo(() => (
        <View style={styles.bottomButtonContainer}>
            <CustomButton
                disabled={!(selectedGoal && selectedPlan && selectedMealCategory && selectedMealPlanDuration)}
                buttonLabel={i18n.t('continue')}
                onPress={onContinuePress}
            />
        </View>
    ), [selectedGoal, selectedPlan, selectedMealCategory, selectedMealPlanDuration, onContinuePress])

    const onRefresh = useCallback(() => {
        setRefreshing(true);
        const promiseData = [dispatch(SubscriptionActions.getGoalsAction()), dispatch(SubscriptionActions.getAllMealTypesAction())];
        Promise.all(promiseData).finally(() => {
            setRefreshing(false);
        })
    }, [dispatch]);

    const renderRefreshControl = useMemo(() => (
        <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
        />
    ), [onRefresh, refreshing])
    return (
        <View style={styles.mainContainer}>
            {/* Header  */}
            {renderHeader}
            <ScrollView
                nestedScrollEnabled={false}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                refreshControl={renderRefreshControl}
                style={styles.contentContainer}>

                {/* Whats your goal */}
                {renderWhatsYourGoal}

                {/* Select Meal Category */}
                {renderSelectMealCategory}

                {/* Duration of Meal Plan */}
                {renderSelectMealPlanDuration}

                {/* Suggested Plan */}
                {renderSuggestedPlan}

                {/* Start Date */}
                {renderSelectStartDate}

            </ScrollView>

            {/* Continue Button */}
            {renderContinueButton}
        </View>
    )
}

const headerStyles = StyleSheet.create({
    appLeftSymbol: {
        marginRight: 10,
        height: 40,
        width: 50,
    },
    headerLeftContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
})
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    contentContainer: {
        flex: 1,
        backgroundColor: colors.appBackground,
    },
    bottomButtonContainer: {
        paddingVertical: 15,
        backgroundColor: colors.white,
        ...commonShadow({ offsetHeight: -2 }),
    },
    headerRightContainer: {
        marginLeft: 5,
        flexDirection: 'row',
        alignItems: 'center',
    },
    logoutText: {
        paddingLeft: 20,
        textTransform: 'uppercase',
    },
    headerMobileNumber: {
        marginTop: 5,
    },
    gotoDashbordContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 15,
    },
    gotoText: {
        textAlign: 'center',
        textTransform: 'uppercase',
    },
    dashboardText: {
        textTransform: 'uppercase',
    },
})

export default connect((state) => ({
    userData: state.auth.userData,
    isNewUser: state.auth.isNewUser,
    goals: state.subscription.goals,
    subscriptionDetails: state.subscription.subscriptionDetails,
    allMealTypes: state.subscription.allMealTypes,
    mealTypesByGoalId: state.subscription.mealTypesByGoalId,
    numberOfDays: state.subscription.numberOfDays,
    plansByDays: state.subscription.plansByDays,
}), null)(PurchaseSubscriptionOne)
