import React, { useMemo, useCallback, useState } from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import Clipboard from '@react-native-clipboard/clipboard';
import { connect, useDispatch } from 'react-redux';
import CustomHeader from '../../../components/custom/CustomHeader';
import CustomText from '../../../components/custom/CustomText';
import colors from '../../../constants/colors';
import i18n from '../../../i18n/i18n';
import { commonShadow } from '../../../styles/CommonStyles';
import { globalPaddingMargin, isRTL } from '../../../constants/constants';
import { navigateGoBack } from '../../../navigation/navigation';
import CustomIcon, { IconType } from '../../../components/custom/CustomIcon';
import { changeAppFlow } from '../../../redux/actions/SettingsActions';
import { appFlows } from '../../../navigation/screens';
import * as AuthActions from '../../../redux/actions/AuthActions';

const InviteFriends = (props) => {
    const dispatch = useDispatch();
    const [copiedTimer, setCopiedTimer] = useState(false);
    const [referralCode] = useState(props?.userData?.referral);

    const onCopy = useCallback(() => {
        setCopiedTimer(true);
        Clipboard.setString(referralCode)
        setTimeout(() => setCopiedTimer(false), 2000);
    }, [referralCode])

    const onSkipPress = useCallback(() => {
        dispatch(AuthActions.setIsNewUserAction(false)).then(() => {
            dispatch(changeAppFlow(appFlows.afterLoginFlowNavigator))
        })
    }, [dispatch])

    const onBackPress = useCallback(() => {
        navigateGoBack();
    }, [])

    // Header
    const headerLeftComponent = useMemo(() => (
        <View style={headerStyles.headerLeftContainer}>
            <CustomIcon
                onPress={onBackPress}
                type={IconType.MaterialIcons}
                name={isRTL ? 'arrow-forward-ios' : 'arrow-back-ios'}
                size={16}
                color={colors.primary_2}
                customStyle={styles.backButton}
            />
            <CustomText fontProps={['ff-roboto-thin', 'fs-large']}>{i18n.t('earn_rewards')}</CustomText>
        </View>
    ), [onBackPress])

    const headerRightComponent = useMemo(() => (
        <TouchableOpacity onPress={onSkipPress} style={styles.skipButtonContainer}>
            <CustomText fontProps={['fs-small']} color={colors.primary_2}>{i18n.t('skip')}</CustomText>
        </TouchableOpacity>
    ), []);

    const renderHeader = useMemo(() => (
        <CustomHeader
            leftComponent={headerLeftComponent}
            rightComponent={headerRightComponent}
        />
    ), [headerLeftComponent])

    return (
        <View style={styles.mainContainer}>
            {/* Header  */}
            {renderHeader}
            <View style={styles.contentContainer}>
                <View style={styles.dollarContainer}>
                    <CustomIcon type={IconType.FontAwesome} name="dollar" size={60} color={colors.primary_1} />
                </View>

                {/* Title */}
                <View style={styles.titleContainer}>
                    {/* Title 1 */}
                    <CustomText customStyle={styles.title} fontProps={['fs-small']}>{i18n.t('invite_your_friends_and_family')}</CustomText>
                    {/* Title 2 */}
                    <CustomText customStyle={styles.title} fontProps={['fs-small']}>{i18n.t('to_earn_rewards_on_each_signup')}</CustomText>
                </View>
            </View>

            {/* ReferralCode */}
            <View style={styles.mainWrapperContainer}>
                <View style={styles.referralCodeMainContainer}>
                    <View style={styles.referCodeContainer}>
                        <CustomText
                            customStyle={styles.referralCode}
                            fontProps={['ff-roboto-medium', 'fs-small']}
                        >{referralCode}</CustomText>
                    </View>
                    <TouchableOpacity disabled={copiedTimer} style={styles.copyButtonContainer} onPress={onCopy}>
                        <CustomText
                            fontProps={['ff-roboto-medium', 'fs-small']}
                            color={copiedTimer ? colors.lightGrey : colors.primary_3}
                            customStyle={styles.copyButton}>
                            {copiedTimer ? i18n.t('copied') : i18n.t('copy')}
                        </CustomText>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

const headerStyles = StyleSheet.create({
    headerLeftContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
})
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: colors.appBackground,
    },
    skipButtonContainer: {
        height: '100%',
        justifyContent: 'center',
    },
    contentContainer: {
        padding: globalPaddingMargin.whole,
        alignItems: 'center',
    },
    backButton: {
        marginRight: 15,
    },
    dollarContainer: {
        height: 85,
        width: 85,
        borderColor: colors.primary_1,
        borderWidth: 10,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
    titleContainer: {
        marginVertical: globalPaddingMargin.vertical,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        marginVertical: 5,
    },
    mainWrapperContainer: {
        marginHorizontal: globalPaddingMargin.whole,
    },
    referralCodeMainContainer: {
        alignItems: 'flex-start',
        flexDirection: 'row',
        height: 48,
        backgroundColor: colors.white,
        borderRadius: 12,
        ...commonShadow(),
    },
    referCodeContainer: {
        flex: 1,
        justifyContent: 'center',
        height: '100%',
    },
    referralCode: {
        paddingHorizontal: globalPaddingMargin.left,
        color: colors.smokeyGrey,
    },
    copyButtonContainer: {
        paddingHorizontal: globalPaddingMargin.left,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    copyButton: {
        textTransform: 'uppercase',
    },
})

export default connect((state) => ({
    userData: state.auth.userData,
}))(InviteFriends);
