import React, {
    useMemo, useCallback, useState, Fragment,
} from 'react';
import {
    View, StyleSheet, TouchableOpacity,
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import { connect, useDispatch } from 'react-redux';
import images from '../../../assets/images/images';
import CustomHeader from '../../../components/custom/CustomHeader';
import CustomImage from '../../../components/custom/CustomImage';
import CustomText from '../../../components/custom/CustomText';
import colors from '../../../constants/colors';
import CustomButton from '../../../components/custom/CustomButton';
import i18n from '../../../i18n/i18n';
import { commonShadow } from '../../../styles/CommonStyles';
import useTextInput from '../../../hooks/useTextInput';
import CustomKeyboardAvoidingView from '../../../components/custom/CustomKeyboardAvoidingView';
import CustomTextInputTwo from '../../../components/custom/CustomTextInputTwo';
import CustomPicker from '../../../components/custom/CustomPicker';
import usePicker from '../../../hooks/usePicker';
import { dailyActivityDummyData, workoutIntensityDummyData, workoutPerWeekDummyData } from '../../../constants/dummyData';
import CustomIcon, { IconType } from '../../../components/custom/CustomIcon';
import { navigateTo } from '../../../navigation/navigation';
import screens from '../../../navigation/screens';
import useSlider from '../../../hooks/useSlider';
import CustomSlider from '../../../components/custom/CustomSlider';
import { checkPermissionForGallery, getHitSlop, getServerImageUploadFormat } from '../../../helpers/CommonHelpers';
import * as Validation from '../../../validation/Validation';
import * as AuthActions from '../../../redux/actions/AuthActions';
import { appTextInputType } from '../../../constants/constants';

const PersonalDetailsOne = (props) => {
    const dispatch = useDispatch();
    const inputRefs = useMemo(() => Array(7).fill(0).map(() => React.createRef()), []);
    const [height, bindHeight] = useTextInput();
    const [weight, bindWeight] = useTextInput();
    const [target_weight, bindTargetWeight] = useTextInput();
    const [age, bindAge] = useTextInput();
    const [daily_activity_lifestyle, bindDailyActivity] = usePicker();
    const [workout_per_week, bindWorkoutPerWeek] = usePicker();
    const [workout_intensity, bindWorkoutIntensity] = usePicker();
    const [food_allergies, bindFoodAlergies] = useTextInput();
    const [health_concerns, bindHealthConcerns] = useTextInput();
    const [avatar] = useState(null)
    const [your_inbody, setInBodyImage] = useState(null)
    const [proteins, proteinsScrollEnabled, bindProteins] = useSlider(props?.userData?.proteins ?? 0);
    const [carbs, carbsScrollEnabled, bindCarbs] = useSlider(props?.userData?.carbs ?? 0);
    const [fats, fatsScrollEnabled, bindFats] = useSlider(props?.userData?.fats ?? 0);

    const goal_name = props?.subscriptionDetails?.data?.[0]?.goal_name

    const maxProtiens = goal_name == 'Lose Weight' ? 120 : goal_name == 'Maintain Weight' ? 150 : 200
    const maxCarbs = goal_name == 'Lose Weight' ? 120 : goal_name == 'Maintain Weight' ? 150 : 300
    const measurementIntervals = goal_name == 'Lose Weight' ? 20 : 50

    const onContinuePress = useCallback(() => {
        const data = {
            height, weight, target_weight, age, daily_activity_lifestyle, workout_per_week, workout_intensity, food_allergies, health_concerns, proteins, carbs, fats, your_inbody, avatar,
        }
        if (Validation.saveInbodySheetValidation({
            ...data,
            is_custom_macros: props?.subscriptionDetails?.data?.[0]?.is_custom_macros,
            is_custom_grams: props?.subscriptionDetails?.data?.[0]?.is_custom_grams,
        })) {
            dispatch(AuthActions.saveInbodySheetAction(data)).then(() => {
                navigateTo(screens.personalDetailsTwo);
            })
        }
    }, [height, weight, target_weight, age, daily_activity_lifestyle, workout_per_week, workout_intensity, food_allergies, health_concerns, proteins, carbs, fats, your_inbody, avatar, props?.subscriptionDetails?.data, dispatch]);

    const headerLeftComponent = useMemo(() => (
        <View style={headerStyles.headerLeftContainer}>
            <CustomImage source={images.appSymbol} style={headerStyles.appLeftSymbol} />
            <CustomText fontProps={['ff-roboto-thin', 'fs-large']}>{i18n.t('tell_us_about_yourself')}</CustomText>
        </View>
    ), [])

    const onLogoutPress = useCallback(() => {
        dispatch(AuthActions.logoutAction());
    }, [dispatch])

    const headerRightComponent = useMemo(() => (
        <TouchableOpacity hitSlop={getHitSlop(20)} onPress={onLogoutPress}>
            <CustomText
                color={colors.primary_3}
                fontProps={['ff-roboto-bold']}
                customStyle={styles.logoutText}>
                {i18n.t('logout')}
            </CustomText>
        </TouchableOpacity>
    ), [onLogoutPress]);

    const renderHeader = useMemo(() => (
        <CustomHeader
            leftComponent={headerLeftComponent}
            rightComponent={headerRightComponent}
            rightComponentStyle={{ flex: 0 }}
        />
    ), [headerLeftComponent, headerRightComponent])

    const renderContinueButton = useMemo(() => (
        <View style={styles.bottomButtonContainer}>
            <CustomButton
                buttonLabel={props?.saveInbodySheet?.loading ? i18n.t('saving') : i18n.t('next')}
                onPress={onContinuePress}
                disabled={props?.saveInbodySheet?.loading}
            />
        </View>
    ), [onContinuePress, props?.saveInbodySheet?.loading])

    // const onLookUploadPress = useCallback(() => {
    //     ImagePicker.openPicker({
    //         width: 300,
    //         height: 300,
    //         mediaType: 'photo',
    //     }).then((image) => {
    //         setAvatar(getServerImageUploadFormat(image));
    //     }).catch((error) => {
    //         if (error?.message?.includes('User did not grant library permission')) {
    //             checkPermissionForGallery()
    //         }
    //     });
    // }, [])

    const onInBodyUploadPress = useCallback(() => {
        ImagePicker.openPicker({
            width: 300,
            height: 300,
            mediaType: 'photo',
        }).then((image) => {
            setInBodyImage(getServerImageUploadFormat(image));
        }).catch((error) => {
            if (error?.message?.includes('User did not grant library permission')) {
                checkPermissionForGallery()
            }
        });
    }, [])

    const onSubmitEditing = useCallback((currentIndex) => {
        if (inputRefs?.[currentIndex + 1]) inputRefs?.[currentIndex + 1]?.current?.focus()
    }, [inputRefs])

    const changeInputFocus = useCallback((index) => ({
        textInputRef: inputRefs[index],
        onSubmitEditing: () => onSubmitEditing(index),
    }), [inputRefs, onSubmitEditing])

    return (
        <View style={styles.mainContainer}>
            {/* Header  */}
            {renderHeader}
            <CustomKeyboardAvoidingView
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                scrollEnabled={(proteinsScrollEnabled === carbsScrollEnabled === fatsScrollEnabled)}
                extraHeight={200}
                style={styles.contentContainer}>
                <View style={styles.content}>
                    {/* Height */}
                    <CustomTextInputTwo
                        {...bindHeight}
                        {...changeInputFocus(0)}
                        label={i18n.t('label_height')}
                        placeHolder={i18n.t('enter_height')}
                        textInputType={appTextInputType.NUMBER}
                        keyboardType={'numeric'}
                        returnKeyType={'done'}
                        customContainerStyle={styles.textInputContainerStyle}
                    />

                    {/* Weight */}
                    <CustomTextInputTwo
                        {...bindWeight}
                        {...changeInputFocus(1)}
                        label={i18n.t('label_weight')}
                        placeHolder={i18n.t('enter_weight')}
                        textInputType={appTextInputType.NUMBER}
                        keyboardType={'numeric'}
                        returnKeyType={'done'}
                        customContainerStyle={styles.textInputContainerStyle}
                    />

                    {/* Target Weight */}
                    <CustomTextInputTwo
                        {...bindTargetWeight}
                        {...changeInputFocus(2)}
                        label={i18n.t('label_target_weight')}
                        textInputType={appTextInputType.NUMBER}
                        placeHolder={i18n.t('enter_target_weight')}
                        keyboardType={'numeric'}
                        customContainerStyle={styles.textInputContainerStyle}
                    />

                    {/* Age */}
                    <CustomTextInputTwo
                        {...bindAge}
                        {...changeInputFocus(3)}
                        label={i18n.t('label_age')}
                        placeHolder={i18n.t('enter_age')}
                        textInputType={appTextInputType.NUMBER}
                        keyboardType={'numeric'}
                        customContainerStyle={styles.textInputContainerStyle}
                    />

                    {/* Daily Activity / Lifestyle */}
                    <CustomPicker
                        {...bindDailyActivity}
                        label={i18n.t('label_daily_activity')}
                        data={dailyActivityDummyData}
                        placeHolder={i18n.t('select_daily_activity')}
                        keyName={'daily_activity_lifestyle'}
                    />

                    {/* Workout per week */}
                    <CustomPicker
                        {...bindWorkoutPerWeek}
                        data={workoutPerWeekDummyData}
                        label={i18n.t('label_workout_per_week')}
                        placeHolder={i18n.t('select_workout_per_week')}
                        keyName={'workout_per_week'}
                    />

                    {/* Workout intensity */}
                    <CustomPicker
                        {...bindWorkoutIntensity}
                        label={i18n.t('label_workout_intensity')}
                        data={workoutIntensityDummyData}
                        placeHolder={i18n.t('select_workout_intensity')}
                        keyName={'daily_activity_lifestyle'}
                    />
                    {/* Food Alergies */}
                    <CustomTextInputTwo
                        {...bindFoodAlergies}
                        {...changeInputFocus(4)}
                        label={i18n.t('label_food_alergies')}
                        placeHolder={i18n.t('enter_food_allergies')}
                        customContainerStyle={styles.textInputContainerStyle}
                    />
                    {/* Health concerns */}
                    <CustomTextInputTwo
                        {...bindHealthConcerns}
                        {...changeInputFocus(5)}
                        label={i18n.t('label_health_concerns')}
                        placeHolder={i18n.t('enter_health_concerns')}
                        customContainerStyle={styles.textInputContainerStyle}
                    />

                    {/* How Do you look */}
                    {/* <TouchableOpacity onPress={onLookUploadPress} style={styles.uploadImageMainContainer}>
                        <CustomText
                            fontProps={['ff-roboto-medium', 'fs-medium']}
                            color={colors.primary_2}>
                            {i18n.t('how_do_you_look')}
                        </CustomText>
                        <View onPress={onLookUploadPress} style={styles.uploadImageContainer}>
                            <CustomIcon type={IconType.Feather} name="camera" size={20} color={colors.primary_3} />
                        </View>
                    </TouchableOpacity> */}

                    {avatar && (
                        <CustomImage
                            source={{ uri: avatar?.uri }}
                            style={styles.uploadImage}
                        />
                    )}

                    {/* Upload your inbody */}
                    <TouchableOpacity onPress={onInBodyUploadPress} style={styles.uploadImageMainContainer}>
                        <CustomText
                            fontProps={['ff-roboto-medium', 'fs-medium']}
                            color={colors.primary_2}>
                            {i18n.t('upload_your_inbody')}
                        </CustomText>
                        <View style={styles.uploadImageContainer}>
                            <CustomIcon type={IconType.Feather} name="upload" size={20} color={colors.primary_3} />
                        </View>
                    </TouchableOpacity>
                    {your_inbody && (
                        <CustomImage
                            source={{ uri: your_inbody?.uri }}
                            style={styles.uploadImage}
                        />
                    )}

                    <Fragment>
                        {/* Proteins */}
                        <View style={styles.sliderContainer}>
                            <CustomText
                                fontProps={['ff-roboto-medium', 'fs-medium']}
                                color={colors.primary_2}>
                                {i18n.t('proteins')} &nbsp; : &nbsp;
                                <CustomText
                                    fontProps={['ff-roboto-medium', 'fs-medium']}
                                    color={colors.primary_3}>
                                    {proteins}
                                </CustomText>
                            </CustomText>
                            <CustomSlider measurementInterval={measurementIntervals} max={maxProtiens} {...bindProteins} />
                        </View>

                        {/* Carbs */}
                        <View style={styles.sliderContainer}>
                            <CustomText
                                fontProps={['ff-roboto-medium', 'fs-medium']}
                                color={colors.primary_2}>
                                {i18n.t('carbs')} &nbsp; : &nbsp;
                                <CustomText
                                    fontProps={['ff-roboto-medium', 'fs-medium']}
                                    color={colors.primary_3}>
                                    {carbs}
                                </CustomText>
                            </CustomText>
                            <CustomSlider measurementInterval={measurementIntervals} max={maxCarbs} {...bindCarbs} />
                        </View>

                        {/* Fats */}

                    </Fragment>

                    <View style={styles.sliderContainer}>
                        <CustomText
                            fontProps={['ff-roboto-medium', 'fs-medium']}
                            color={colors.primary_2}>
                            {i18n.t('fats')} &nbsp; : &nbsp;
                            <CustomText
                                fontProps={['ff-roboto-medium', 'fs-medium']}
                                color={colors.primary_3}>
                                {fats}
                            </CustomText>
                        </CustomText>
                        <CustomSlider measurementInterval={40} max={160} {...bindFats} />
                    </View>

                </View>

            </CustomKeyboardAvoidingView>

            {/* Continue Button */}
            {renderContinueButton}
        </View>
    )
}

const headerStyles = StyleSheet.create({
    appLeftSymbol: {
        marginRight: 10,
        height: 40,
        width: 50,
    },
    headerLeftContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
})
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: colors.white,
    },
    contentContainer: {
        flex: 1,
        backgroundColor: colors.white,
    },
    content: {
        padding: 30,
    },
    bottomButtonContainer: {
        paddingVertical: 15,
        backgroundColor: colors.white,
        ...commonShadow({ offsetHeight: -2 }),
    },
    textInputContainerStyle: {
        marginBottom: 10,
    },
    uploadImageMainContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 20,
    },
    uploadImageContainer: {
        marginLeft: 20,
        height: 36,
        width: 36,
        borderRadius: 50,
        backgroundColor: colors.white,
        borderWidth: 1,
        borderColor: colors.smokeyGrey,
        alignItems: 'center',
        justifyContent: 'center',
    },
    uploadImage: {
        height: 100,
        width: 100,
        borderRadius: 10,
        marginBottom: 10,
    },
    sliderContainer: {
        marginBottom: 15,
    },
    logoutText: {
        textTransform: 'uppercase',
    },
})
export default connect((state) => ({
    userData: state.auth.userData,
    saveInbodySheet: state.auth.saveInbodySheet,
    subscriptionDetails: state.subscription.subscriptionDetails,
}))(PersonalDetailsOne);
