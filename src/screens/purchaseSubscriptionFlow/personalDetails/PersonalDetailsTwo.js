import React, {
    useMemo, useCallback, useRef, useState, useEffect,
} from 'react';
import {
    View, StyleSheet,
} from 'react-native';
import { connect, useDispatch } from 'react-redux';
import CustomHeader from '../../../components/custom/CustomHeader';
import CustomText from '../../../components/custom/CustomText';
import colors from '../../../constants/colors';
import CustomButton from '../../../components/custom/CustomButton';
import i18n from '../../../i18n/i18n';
import { commonShadow } from '../../../styles/CommonStyles';
import {
    DEFAULT_COORDINATE, globalPaddingMargin, isRTL, MOBILE_NUMBER_LENGTH, MOBILE_NUMBER_PREFIX, appTextInputType,
} from '../../../constants/constants';
import useTextInput from '../../../hooks/useTextInput';
import CustomKeyboardAvoidingView from '../../../components/custom/CustomKeyboardAvoidingView';
import CustomTextInputTwo from '../../../components/custom/CustomTextInputTwo';
import CustomPicker from '../../../components/custom/CustomPicker';
import usePicker from '../../../hooks/usePicker';
import CustomIcon, { IconType } from '../../../components/custom/CustomIcon';
import { navigateGoBack, navigateTo } from '../../../navigation/navigation';
import CustomMaps from '../../../components/custom/CustomMaps';
import { wp } from '../../../helpers/screenHelper';
import { getCurrentLocation, getRegionForCoordinates, getFormattedAddress } from '../../../helpers/CommonHelpers';
import screens from '../../../navigation/screens';
import * as Validation from '../../../validation/Validation';
import * as AuthActions from '../../../redux/actions/AuthActions';
import { getAreasAction } from '../../../redux/actions/SettingsActions';
import * as Services from '../../../services/Services';

const PersonalDetailsTwo = (props) => {
    const dispatch = useDispatch();
    const mapRef = useRef(null);
    const inputRefs = useMemo(() => Array(10).fill(0).map(() => React.createRef()), []);
    const [pinCoordinates, setPinCoordinates] = useState(getRegionForCoordinates(DEFAULT_COORDINATE));
    const [address_nickname, bindAddressNickName] = useTextInput();
    const [area_id, bindArea] = usePicker(props?.userData?.area_id);
    const [block, bindBlock, setBlock] = useTextInput();
    const [street_name, bindStreet, setStreet] = useTextInput();
    const [house_number, bindHouseNumber] = useTextInput();
    const [avenue, bindAvenue] = useTextInput();
    const [building, bindBuilding] = useTextInput();
    const [floor, bindFloor] = useTextInput();
    const [office, bindOffice] = useTextInput();
    const [additional_directions, bindAdditionalDirection] = useTextInput();
    const [pickup_phone_number, bindPickupPhoneNumber] = useTextInput();

    useEffect(() => {
        dispatch(getAreasAction());
    }, [dispatch])

    const onTakeMeHomePress = useCallback(() => {
        const data = {
            address_nickname, area_id, block, street_name, avenue, building, house_number, floor, office, additional_directions, pickup_phone_number,
        }
        if (Validation.saveAddressValidation(data)) {
            dispatch(AuthActions.saveAddressAction({ ...data, is_address: 'yes' })).then(() => {
                navigateTo(screens.inviteFriends)
            })
        }
    }, [additional_directions, address_nickname, area_id, avenue, block, building, dispatch, floor, house_number, office, pickup_phone_number, street_name]);

    const onBackPress = useCallback(() => {
        navigateGoBack();
    }, [])

    // Header
    const headerLeftComponent = useMemo(() => (
        <View style={headerStyles.headerLeftContainer}>
            <CustomIcon
                onPress={onBackPress}
                type={IconType.MaterialIcons}
                name={isRTL ? 'arrow-forward-ios' : 'arrow-back-ios'}
                size={16}
                color={colors.primary_2}
                customStyle={styles.backButton}
            />
            <CustomText fontProps={['ff-roboto-thin', 'fs-large']}>{i18n.t('tell_us_about_yourself')}</CustomText>
        </View>
    ), [onBackPress])

    const renderHeader = useMemo(() => (
        <CustomHeader
            leftComponent={headerLeftComponent}
        />
    ), [headerLeftComponent])

    const renderContinueButton = useMemo(() => (
        <View style={styles.bottomButtonContainer}>
            <CustomButton
                buttonLabel={props?.saveAddress?.loading ? i18n.t('saving') : i18n.t('take_me_home')}
                onPress={onTakeMeHomePress}
                disabled={props?.saveAddress?.loading}
            />
        </View>
    ), [onTakeMeHomePress, props?.saveAddress?.loading])

    const onSubmitEditing = useCallback((currentIndex) => {
        if (inputRefs[currentIndex + 1]) inputRefs[currentIndex + 1]?.current?.focus()
    }, [inputRefs])

    const changeInputFocus = useCallback((index) => ({
        textInputRef: inputRefs[index],
        onSubmitEditing: () => onSubmitEditing(index),
    }), [inputRefs, onSubmitEditing])

    // Map Code
    const getFromGMap = useCallback((coordinates) => {
        Services.getAddressFromLatLong(coordinates).then((res) => {
            // const add = res?.results?.[0] ? getFormattedAddress(res?.results?.[0] ?? null) : null;
            const add = res?.results ? getFormattedAddress(res?.results ?? null) : null;
            setBlock(add?.block_number);
            setStreet(add?.street_name)
        })
    }, [setBlock, setStreet])

    const getMyCurrentLocation = useCallback(() => {
        getCurrentLocation().then((coords) => {
            const region = getRegionForCoordinates({
                latitude: coords?.latitude ?? 0.0,
                longitude: coords?.longitude ?? 0.0,
            });
            // const region = getRegionForCoordinates({
            //     latitude: 29.1286327,
            //     longitude: 48.1174048,
            // });
            if (region) {
                onLocationChange(region);
            }
        })
    }, [onLocationChange])

    const onLocationChange = (coords, userInteraction = false) => {
        setPinCoordinates(coords)
        getFromGMap(coords);
        if (!userInteraction) {
            mapRef?.current?.animateToRegion(coords);
        }
    }

    const renderMaps = useMemo(() => (
        <View style={styles.mapContainer}>
            <CustomMaps
                pinCoordinates={pinCoordinates}
                showsMyLocationButton={true}
                mapRef={mapRef}
                onMapReady={getMyCurrentLocation}
                onLocationChange={onLocationChange}
            >
                <View style={styles.refineButtonContainer}>
                    <CustomButton
                        onPress={getMyCurrentLocation}
                        customStyle={styles.refineButton}
                        buttonLabel={i18n.t('use_current_location')}
                    />
                </View>
            </CustomMaps>
        </View>
    ), [getMyCurrentLocation, pinCoordinates])
    const renderPhoneLeftComponent = useMemo(() => (
        <CustomText fontProps={['ff-roboto-medium', 'fs-small']} color={colors.borderColor}>
            {MOBILE_NUMBER_PREFIX}
        </CustomText>
    ), [])

    const areasData = useMemo(() => (props.areas?.data?.length > 0 ? props.areas?.data?.map((areaItem) => ({ key: areaItem?.id, label: areaItem?.name })) : []), [props.areas?.data])
    return (
        <View style={styles.mainContainer}>
            {/* Header  */}
            {renderHeader}
            <CustomKeyboardAvoidingView
                extraHeight={200}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                style={styles.contentContainer}>
                <>
                    {/* Maps */}
                    {renderMaps}

                    <View style={styles.content}>
                        <CustomText
                            fontProps={['ff-roboto-medium', 'fs-medium']}
                            customStyle={{ marginBottom: 10 }}
                            color={colors.primary_2}>
                            {i18n.t('deliverry_address')} {'*'}
                        </CustomText>

                        {/* Address Nick Name */}
                        <CustomTextInputTwo
                            {...bindAddressNickName}
                            {...changeInputFocus(0)}
                            label={i18n.t('label_address_nickname')}
                            placeHolder={i18n.t('enter_address_nickname')}
                            customContainerStyle={styles.textInputContainerStyle}
                        />

                        {/* Area */}
                        <CustomPicker
                            {...bindArea}
                            label={i18n.t('label_area')}
                            data={areasData}
                            placeHolder={i18n.t('select_area')}
                            keyName={'areas'}
                            isRequired
                        />

                        {/* Block */}
                        <CustomTextInputTwo
                            {...bindBlock}
                            {...changeInputFocus(1)}
                            label={i18n.t('label_block')}
                            placeHolder={i18n.t('enter_block')}
                            customContainerStyle={styles.textInputContainerStyle}
                            isRequired
                        />

                        {/* Street */}
                        <CustomTextInputTwo
                            {...bindStreet}
                            {...changeInputFocus(2)}
                            label={i18n.t('label_street')}
                            placeHolder={i18n.t('enter_street')}
                            customContainerStyle={styles.textInputContainerStyle}
                            isRequired
                        />

                        {/* Avenue */}
                        <CustomTextInputTwo
                            {...bindAvenue}
                            {...changeInputFocus(3)}
                            label={i18n.t('label_avenue')}
                            placeHolder={i18n.t('enter_avenue')}
                            customContainerStyle={styles.textInputContainerStyle}
                        />

                        {/* House Number */}
                        <CustomTextInputTwo
                            {...bindHouseNumber}
                            {...changeInputFocus(4)}
                            label={`${i18n.t('label_house_number')} / ${i18n.t('label_building')}`}
                            placeHolder={`${i18n.t('enter_house_number')} / ${i18n.t('enter_building')}`}
                            customContainerStyle={styles.textInputContainerStyle}
                            isRequired
                        />


                        {/* Floor */}
                        <CustomTextInputTwo
                            {...bindFloor}
                            {...changeInputFocus(6)}
                            label={i18n.t('label_floor')}
                            placeHolder={i18n.t('enter_floor')}
                            customContainerStyle={styles.textInputContainerStyle}
                        />

                        {/* Office */}
                        <CustomTextInputTwo
                            {...bindOffice}
                            {...changeInputFocus(7)}
                            label={i18n.t('label_office')}
                            placeHolder={i18n.t('enter_office')}
                            customContainerStyle={styles.textInputContainerStyle}
                        />

                        {/* Additional Direction */}
                        <CustomTextInputTwo
                            {...bindAdditionalDirection}
                            {...changeInputFocus(8)}
                            label={i18n.t('label_additional_direction')}
                            placeHolder={i18n.t('enter_additional_direction')}
                            customContainerStyle={styles.textInputContainerStyle}
                        />

                        {/* Alternative Phone Number */}
                        <CustomTextInputTwo
                            {...bindPickupPhoneNumber}
                            {...changeInputFocus(9)}
                            maxLength={MOBILE_NUMBER_LENGTH}
                            leftComponent={renderPhoneLeftComponent}
                            textInputType={appTextInputType.NUMBER}
                            keyboardType={'numeric'}
                            label={i18n.t('label_alternative_phone_number')}
                            placeHolder={i18n.t('enter_alternative_phone_number')}
                            customContainerStyle={styles.textInputContainerStyle}
                        />

                    </View>
                </>
            </CustomKeyboardAvoidingView>

            {/* Continue Button */}
            {renderContinueButton}
        </View>
    )
}

const headerStyles = StyleSheet.create({
    headerLeftContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
})
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: colors.white,
    },
    contentContainer: {
        flex: 1,
        backgroundColor: colors.white,
    },
    content: {
        padding: globalPaddingMargin.whole,
    },
    bottomButtonContainer: {
        paddingVertical: 15,
        backgroundColor: colors.white,
        ...commonShadow({ offsetHeight: -2 }),
    },
    textInputContainerStyle: {
        marginBottom: 10,
    },
    backButton: {
        marginRight: 15,
    },
    mapContainer: {
        height: 250,
        width: wp(100),
    },
    refineButtonContainer: {
        position: 'absolute',
        alignSelf: 'center',
        bottom: 10,
    },
    refineButton: {
        width: wp(40),
    },
})
export default connect((state) => ({
    areas: state.settings.areas,
    saveAddress: state.auth.saveAddress,
    userData: state.auth.userData,
}))(PersonalDetailsTwo);