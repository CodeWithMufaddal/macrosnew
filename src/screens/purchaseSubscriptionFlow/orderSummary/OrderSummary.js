import React, {
    useState, useMemo, useCallback,
} from 'react';
import {
    View, StyleSheet, TouchableOpacity, ScrollView, Linking,
} from 'react-native';
import { connect, useDispatch } from 'react-redux';
import { useNavigation, useRoute } from '@react-navigation/native';
import images from '../../../assets/images/images';
import CustomHeader from '../../../components/custom/CustomHeader';
import CustomImage from '../../../components/custom/CustomImage';
import CustomText from '../../../components/custom/CustomText';
import colors from '../../../constants/colors';
import { commonShadow } from '../../../styles/CommonStyles';
import CustomIcon, { IconType } from '../../../components/custom/CustomIcon';
import CustomToast from '../../../components/custom/CustomToast';
import { navigateGoBack, navigateTo } from '../../../navigation/navigation';
import i18n from '../../../i18n/i18n';
import CustomButton from '../../../components/custom/CustomButton';
import OrderSummarySelectedPlan from '../../../components/orderSummary/orderSummarySelectedPlan/OrderSummarySelectedPlan';
import OrderSummaryPaymentMethods from '../../../components/orderSummary/paymentMethods/OrderSummaryPaymentMethods';
import OrderSummaryPromoCode from '../../../components/orderSummary/promoCode/OrderSummaryPromoCode';
import CustomKeyboardAvoidingView from '../../../components/custom/CustomKeyboardAvoidingView'
import * as Validation from '../../../validation/Validation';
import { getAmountWithCurrency, getValidDate } from '../../../helpers/CommonHelpers';
import { ALLOW_ONLINE_PAYMENT, isRTL, WHATSAPP_CHAT_LINK } from '../../../constants/constants';
import * as SubscriptionActions from '../../../redux/actions/SubscriptionActions';
import CustomTextInput from '../../../components/custom/CustomTextInput';
import screens from '../../../navigation/screens';

const OrderSummary = ({ navigation, ...props }) => {
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(false);
    const [isRenew] = useState(props?.route?.params?.isRenew ?? false)
    const [isCameFromHomeDashboard] = useState(props?.route?.params?.isCameFromHomeDashboard ?? false)
    const [selectedGoal] = useState(props?.route?.params?.selectedGoal)
    const [selectedMealCategory] = useState(props?.route?.params?.selectedMealCategory)
    const [selectedMealPlanDuration] = useState(props?.route?.params?.selectedMealPlanDuration)
    const [selectedPlan] = useState(props?.route?.params?.selectedPlan)
    const [startDate] = useState(props?.route?.params?.startDate)
    const [selectedPaymentMethod, setSelectedPaymentMethod] = useState(null);
    const [coupon, setCoupon] = useState('');
    const [couponDiscount, setCouponDiscount] = useState(0);
    const [notes, setNotes] = useState('')

    const netTotal = useMemo(() => {
        const price = Number(selectedPlan?.price ?? 0);
        const discountAmount = Number(couponDiscount ?? 0);
        const finalAmount = price - discountAmount
        return finalAmount > 0 ? finalAmount : 0;
    }, [couponDiscount, selectedPlan?.price])

    const displayedNetTotal = useMemo(() => (netTotal > 0 ? selectedPaymentMethod?.TotalAmount ?? netTotal : netTotal), [netTotal, selectedPaymentMethod?.TotalAmount])
    // eslint-disable-next-line react-hooks/exhaustive-deps
    const paymentMethodData = [
        {
            id: 1, imageUrl: images.knetIcon, name: 'Knet', label: 'knet', TotalAmount: displayedNetTotal,
        },
        {
            id: 2, imageUrl: images.creditCardIcon, name: 'Card', label: 'card', TotalAmount: displayedNetTotal,
        },
    ]

    const onBackPress = useCallback(() => {
        navigateGoBack();
    }, [])

    // Header
    const headerLeftComponent = useMemo(() => (
        <View style={headerStyles.headerLeftContainer}>
            <CustomIcon
                onPress={onBackPress}
                type={IconType.MaterialIcons}
                name={isRTL ? 'arrow-forward-ios' : 'arrow-back-ios'}
                size={16}
                color={colors.primary_2}
                customStyle={styles.backButton}
            />
            <CustomText fontProps={['ff-roboto-thin', 'fs-large']}>Hi, {props?.userData?.first_name}</CustomText>
        </View>
    ), [onBackPress, props?.userData?.first_name])

    const onChatPress = useCallback(() => {
        Linking.openURL(WHATSAPP_CHAT_LINK)
    }, [])

    const headerRightComponent = useMemo(() => (
        <TouchableOpacity onPress={onChatPress}><CustomImage SvgSource={images.chatIcon} /></TouchableOpacity>
    ), [onChatPress]);

    const renderHeader = useMemo(() => (
        <CustomHeader
            leftComponent={headerLeftComponent}
            rightComponent={headerRightComponent}
        />
    ), [headerLeftComponent, headerRightComponent])

    const onPayPress = useCallback(() => {
        const subscriptionData = {
            is_custom_macros: selectedGoal?.is_custom_macros,
            is_custom_grams: selectedGoal?.is_custom_grams,
            price: Number(selectedPlan?.price),
            amount: displayedNetTotal,
            subscription_ids: selectedPlan?.subscription_ids,
            coupon,
            coupon_discount: couponDiscount,
            start_date: getValidDate(startDate, 'YYYY-MM-DD'),
            renew_plan: isRenew ? 'Yes' : 'No',
            isCameFromHomeDashboard,
            notes,
            paymentMethod: selectedPaymentMethod?.label,
        }

        if (ALLOW_ONLINE_PAYMENT && Math.floor(netTotal) > 0) {
            setLoading(true);
            dispatch(SubscriptionActions.subscriptionPaymentAction({ orderData: subscriptionData })).then((res) => {
                navigateTo(screens.paymentGatewayWebview, { url: res?.data?.payment_data?.paymentUrl, orderData: subscriptionData })
                setLoading(false)
            }).catch((error) => {
                if (error?.body) CustomToast.error(i18n.t('payment_failed'))
                else CustomToast.error(i18n.t('payment_cancelled'))
                setLoading(false)
            })
        } else {
            const orderPaymentStatusData = { paymentStatus: 'success' }
            dispatch(SubscriptionActions.purchaseSubscriptionPaymentAction({ orderData: subscriptionData, orderPaymentStatusData })).finally(() => {
                setLoading(false);
            })
        }
    }, [coupon, couponDiscount, dispatch, displayedNetTotal, isCameFromHomeDashboard, isRenew, netTotal, notes, selectedGoal?.is_custom_grams, selectedGoal?.is_custom_macros, selectedPaymentMethod?.label, selectedPlan?.price, selectedPlan?.subscription_ids, startDate])

    const renderPayButton = useMemo(() => (
        <View style={styles.bottomButtonContainer}>
            <CustomButton
                disabled={loading || (!selectedPaymentMethod && Math.floor(displayedNetTotal) > 0)}
                buttonLabel={loading
                    ? i18n.t('processing')
                    : i18n.t('pay_amount', { amount: getAmountWithCurrency(displayedNetTotal) })}
                onPress={onPayPress}
            />
        </View>
    ), [displayedNetTotal, loading, onPayPress, selectedPaymentMethod])

    const renderSelectedPlan = useMemo(() => (
        <OrderSummarySelectedPlan
            plan={selectedPlan}
            goal={selectedGoal}
            mealCategory={selectedMealCategory}
            mealPlanDuration={selectedMealPlanDuration?.days ?? 0}
            startDate={startDate}
        />
    ), [selectedGoal, selectedMealCategory, selectedMealPlanDuration, selectedPlan, startDate])

    const onPaymentMethodPress = useCallback((paymentMethod) => {
        setSelectedPaymentMethod(paymentMethod)
    }, [])

    const renderPaymentMethods = useMemo(() => Math.floor(netTotal) > 0 && (
        <OrderSummaryPaymentMethods
            paymentMethodsData={paymentMethodData}
            onPaymentMethodPress={onPaymentMethodPress}
            selectedPaymentMethod={selectedPaymentMethod}
        />
    ), [netTotal, onPaymentMethodPress, paymentMethodData, selectedPaymentMethod]);

    const callCouponCodeAPI = useCallback((code) => {
        const price = Number(selectedPlan?.price ?? 0)
        dispatch(SubscriptionActions.applyCouponAction(selectedPlan?.subscription_id, code?.toUpperCase())).then((res) => {
            if (res?.data?.discount_amount <= price) {
                CustomToast.success(res?.message);
                setCouponDiscount(res?.data?.discount_amount)
            } else {
                CustomToast.error(i18n.t('invalidPromoCode'));
                setCoupon('')
                setCouponDiscount(0)
            }
        }).catch(() => {
            setCouponDiscount(0)
            setCoupon('')
        });
    }, [dispatch, selectedPlan?.price, selectedPlan?.subscription_id])

    const onRemovePromoPress = useCallback(() => {
        setCoupon('')
        setCouponDiscount(0)
    }, []);

    const onApplyPromoPress = useCallback(() => {
        if (Validation.orderSummaryPromoCodeValidation({ coupon })) {
            callCouponCodeAPI(coupon)
        }
    }, [callCouponCodeAPI, coupon]);

    const renderNotesSection = useMemo(() => (
        <View style={styles.notesSectionContainer}>
            <CustomText
                fontProps={['fs-small']}
                color={colors.primary_2}
                customStyle={styles.title}>
                {i18n.t('notes')}
            </CustomText>
            <CustomTextInput
                value={notes}
                onChangeText={setNotes}
                multiline={true}
                customContainerStyle={{
                    height: 100, borderWidth: 0, marginTop: 10, borderRadius: 10, padding: 10, paddingHorizontal: 10, ...commonShadow(),
                }}
                placeHolder={i18n.t('notes')}
            />
        </View>
    ), [notes])

    return (
        <View style={styles.mainContainer}>
            {/* Header  */}
            {renderHeader}

            <ScrollView
                nestedScrollEnabled={false}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                style={styles.contentContainer}>
                <CustomKeyboardAvoidingView>
                    {/* Your Selected Plan */}
                    {renderSelectedPlan}

                    {/* Promo Code */}
                    <OrderSummaryPromoCode
                        onRemovePress={onRemovePromoPress}
                        couponDiscount={couponDiscount}
                        setCoupon={(text) => setCoupon(text)}
                        coupon={coupon}
                        onApplyPromoPress={onApplyPromoPress}
                    />

                    {/* Notes */}
                    {renderNotesSection}

                    {/* Payment Methods */}
                    {renderPaymentMethods}
                </CustomKeyboardAvoidingView>
            </ScrollView>

            {/* Pay Button */}
            {renderPayButton}

        </View>
    )
}

const headerStyles = StyleSheet.create({
    headerLeftContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
})
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    backButton: {
        marginRight: 15,
    },
    contentContainer: {
        flex: 1,
        backgroundColor: colors.appBackground,
    },
    bottomButtonContainer: {
        paddingVertical: 15,
        backgroundColor: colors.white,
        ...commonShadow({ offsetHeight: -2 }),
    },
    notesSectionContainer: {
        padding: 20,
    },

})
export default connect((state) => ({
    userData: state.auth.userData,

}))(OrderSummary);
