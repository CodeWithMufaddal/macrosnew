import React, { useCallback } from 'react'
import { Linking, StyleSheet, View } from 'react-native'
import images from '../../assets/images/images'
import CustomButton from '../../components/custom/CustomButton'
import CustomIcon, { IconType } from '../../components/custom/CustomIcon'
import CustomImage from '../../components/custom/CustomImage'
import CustomText from '../../components/custom/CustomText'
import colors from '../../constants/colors'
import { hp } from '../../helpers/screenHelper'
import i18n from '../../i18n/i18n'

const NeedUpdateScreen = ({ storeLink }) => {
    const onUpdatePress = useCallback(() => {
        if (storeLink) Linking.openURL(storeLink)
    }, [storeLink])

    return (
        <View style={styles.mainContainer}>
            <CustomImage source={images.macrosLogo} style={styles.appLogo} />
            <CustomIcon type={IconType.MaterialIcons} name="system-update" size={hp(10)} color={colors.primary_3} />
            <CustomText customStyle={styles.appNeedUpdateText} fontProps={['fs-large']}>{i18n.t('appNeedUpdate')}</CustomText>
            <CustomButton customStyle={styles.updateButton} onPress={onUpdatePress} buttonLabel={i18n.t('updateAnApp')} />
        </View>
    )
}

export const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    appLogo: {
        marginBottom: hp(10),
    },
    appNeedUpdateText: {
        marginVertical: 20,
        marginBottom: 30,
    },
    updateButton: {
        marginVertical: 20,
    },
})

export default NeedUpdateScreen
