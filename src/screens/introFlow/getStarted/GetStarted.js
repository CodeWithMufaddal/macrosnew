/* eslint-disable import/no-cycle */
import React, { useCallback } from 'react'
import { StyleSheet, View } from 'react-native'
import CustomImage from '../../../components/custom/CustomImage'
import images, { imagesMode } from '../../../assets/images/images'
import { hp, wp } from '../../../helpers/screenHelper'

import CustomButton from '../../../components/custom/CustomButton'
import i18n from '../../../i18n/i18n'
import { navigateTo } from '../../../navigation/navigation'
import screens from '../../../navigation/screens'
import colors from '../../../constants/colors'

const GetStarted = () => {
    const onGetStartedPress = useCallback(() => {
        navigateTo(screens.intro)
    }, [])

    return (
        <View style={styles.mainContainer}>
            {/* Logo Container  */}
            <View style={styles.logoContainer}>
                <CustomImage style={{ height: 150, width: 150, backgroundColor: 'red' }} source={images.appSymbol} />
            </View>
            {/* Button Container */}
            <View style={styles.buttonContainer}>
                <CustomButton onPress={onGetStartedPress} buttonLabel={i18n.t('get_started')} customStyle={styles.button} />
                <CustomImage source={images.getStartedBackground} style={styles.bottomImage} resizeMode={imagesMode.cover} />
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: colors.white,
    },
    logoContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonContainer: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    bottomImage: {
        position: 'absolute',
        bottom: 0,
        height: hp(25),
        width: wp(100),
    },
    button: {
        width: wp(40),
        marginTop: hp(10),
    },
})
export default GetStarted
