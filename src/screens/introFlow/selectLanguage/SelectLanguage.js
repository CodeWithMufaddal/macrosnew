import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import RNRestart from 'react-native-restart'
import { connect, useDispatch } from 'react-redux';
import CustomImage from '../../../components/custom/CustomImage';
import images from '../../../assets/images/images';
import { globalPaddingMargin } from '../../../constants/constants';
import i18n, { APP_LANGUAGES } from '../../../i18n/i18n';
import CustomText from '../../../components/custom/CustomText';
import CustomButton from '../../../components/custom/CustomButton';
import CustomDropdown from '../../../components/custom/CustomDropdown';
import { changeAppFlow, changeAppLanguageAction } from '../../../redux/actions/SettingsActions';
import CustomToast from '../../../components/custom/CustomToast';
import { appFlows } from '../../../navigation/screens';
import CustomLoader from '../../../components/custom/CustomLoader';

const SelectLanguage = (props) => {
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(false);
    const [openLanguageModal, setOpenLanguageModal] = useState(false);
    const [selectedLanguage, setSelectedLanguage] = useState(null);
    const [items] = useState([
        { label: 'English', value: APP_LANGUAGES.EN },
        { label: 'Arabic', value: APP_LANGUAGES.AR },
    ]);

    const onNextPress = () => {
        if (!selectedLanguage) return CustomToast.error(i18n.t('select_language'));
        setLoading(true);
        dispatch(changeAppLanguageAction(selectedLanguage, false)).then((lang) => {
            if (lang === props?.appLanguage) {
                dispatch(changeAppFlow(appFlows.loginFlowNavigator))
            } else {
                dispatch(changeAppFlow(appFlows.loginFlowNavigator)).then(() => {
                    setTimeout(() => RNRestart.Restart(), 500)
                });
            }
        })
        return null;
    }
    return (
        <View style={styles.superMainContainer}>
            {loading ? (
                <CustomLoader />
            ) : (
                <View style={styles.mainContainer}>
                    <View style={styles.wrapperContainer}>
                        {/* Logo */}
                        <View style={styles.logoContainer}>
                            <CustomImage style={{ height: 150, width: 190 }} source={images.appSymbol} />
                        </View>

                        {/* Pick Your Language */}
                        <View style={styles.pickPackageContainer}>
                            <CustomText fontProps={['ff-roboto-thin', 'fs-xlarge']}>{i18n.t('pick_your_language')}</CustomText>
                            <CustomDropdown
                                dropDownContainerStyle={styles.dropDownContainer}
                                placeholder={i18n.t('select_language')}
                                open={openLanguageModal}
                                value={selectedLanguage}
                                data={items}
                                setOpen={setOpenLanguageModal}
                                setValue={setSelectedLanguage}
                            />
                        </View>
                    </View>

                    <CustomButton customStyle={styles.bottomButton} onPress={onNextPress} buttonLabel={i18n.t('next')} />
                </View>
            )}

        </View>
    )
}
const styles = StyleSheet.create({
    superMainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: globalPaddingMargin.whole,
    },
    wrapperContainer: {
        flex: 1,
        // justifyContent: 'center',
        marginTop: globalPaddingMargin.vertical * 2,
        alignItems: 'center',
    },
    logoContainer: {
        alignItems: 'center',
        padding: globalPaddingMargin.whole,
        marginTop: globalPaddingMargin.vertical,
    },
    pickPackageContainer: {
        marginTop: globalPaddingMargin.vertical * 2,
        alignItems: 'center',
    },
    bottomButton: {
        marginVertical: globalPaddingMargin.vertical,
    },
    dropDownContainer: {
        marginTop: globalPaddingMargin.vertical * 2,
    },

})
export default connect((state) => ({
    appLanguage: state.settings.appLanguage,
}))(SelectLanguage);
