import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { useDispatch } from 'react-redux';
import CustomImage from '../../../components/custom/CustomImage';
import images, { imagesMode } from '../../../assets/images/images';
import { hp, wp } from '../../../helpers/screenHelper';
import { globalPaddingMargin } from '../../../constants/constants';
import i18n from '../../../i18n/i18n';
import CustomText from '../../../components/custom/CustomText';
import colors from '../../../constants/colors';
import screens from '../../../navigation/screens';
import { navigateTo } from '../../../navigation/navigation';
import { resetStore } from '../../../redux/actions/SettingsActions';

const Intro = () => {
    const dispatch = useDispatch();
    const onSkipPress = () => {
        navigateTo(screens.selectLanguage, null);
        dispatch(resetStore())
    }
    return (
        <View style={styles.mainContainer}>
            {/* Skip Container */}
            <View style={{ backgroundColor: 'red ', alignItems: 'flex-end', padding: globalPaddingMargin.vertical }}>
                <TouchableOpacity onPress={onSkipPress}>
                    <CustomText fontProps={['fs-small']} color={colors.primary_2}>{i18n.t('skip_intro')}</CustomText>
                </TouchableOpacity>
            </View>

            {/* Logo Container  */}
            <View style={styles.introTitleContainer}>
                <CustomText fontProps={['ff-roboto-thin', 'fs-xlarge']}>{i18n.t('join_macros_end_enjoy_meals')}</CustomText>
                <CustomText
                    fontProps={['ff-roboto-thin', 'fs-small']}
                    customStyle={styles.descriptionText}
                >
                    {i18n.t('intro_screen_description')}
                </CustomText>
            </View>

            {/* Button Container */}
            <CustomImage showPlaceholder={false} source={images.macrosBoy} style={styles.bottomImage} resizeMode={imagesMode.contain} />
        </View>
    )
}
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        marginTop: globalPaddingMargin.top,
    },
    introTitleContainer: {
        flex: 1,
        alignItems: 'center',
        padding: 10,
    },
    bottomImage: {
        position: 'absolute',
        bottom: 0,
        height: hp(60),
        width: wp(100),
    },
    descriptionText: {
        lineHeight: 20,
        width: wp(80),
        marginTop: globalPaddingMargin.top,
        textAlign: 'center',
    },
})
export default Intro;
