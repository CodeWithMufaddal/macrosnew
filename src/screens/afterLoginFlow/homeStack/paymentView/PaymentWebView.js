import React, { useEffect, useState } from 'react';
import { BackHandler, View } from 'react-native';
import WebView from 'react-native-webview';
import { useRoute } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
import i18n from '../../../../i18n/i18n';
import { changeAppFlow } from '../../../../redux/actions/SettingsActions';
import screens, { appFlows } from '../../../../navigation/screens';
// import CustomOneSignal from '../../../../onesignal/CustomOneSignal';
import CustomToast from '../../../../components/custom/CustomToast';
import { getSubscriptionDetailsAction } from '../../../../redux/actions/SubscriptionActions';
import { navigateTo } from '../../../../navigation/navigation';
import CustomLoader from '../../../../components/custom/CustomLoader';

// create a component
const PaymentWebView = () => {
    const route = useRoute();
    const dispatch = useDispatch()
    const { orderData, url } = route?.params;
    const [urlData, setURLData] = useState('');
    const [isLoading, setIsLoading] = useState(true);
    const [paymentIsProcessing, setPaymentIsProcessing] = useState(false);

    // eslint-disable-next-line consistent-return
    const changeSuccessFlow = () => {
        CustomToast.success(i18n.t('subscribed'));
        CustomOneSignal.userIsSubscribed(true);
        dispatch(getSubscriptionDetailsAction())
        if (orderData?.renew_plan === 'Yes' || orderData?.isCameFromHomeDashboard) {
            return dispatch(changeAppFlow(appFlows.afterLoginFlowNavigator))
        }
        return dispatch(changeAppFlow(appFlows.personalDetailWhilePurchaseSubscriptionFlowNavigator))
    }

    const changeFailFlow = () => {
        CustomToast.error(i18n.t('payment_failed'));
        navigateTo(screens.purchaseSubscriptionOne)
    }

    const handleBackButton = () => {
        if (paymentIsProcessing) {
            return true;
        }
        return false;
    };

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);

        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
        };
    }, [paymentIsProcessing]);

    useEffect(() => {
        if (urlData.includes('paymentsuccess')) {
            changeSuccessFlow()
            setPaymentIsProcessing(false)
        }
        if (urlData.includes('paymentfail')) {
            changeFailFlow()
            setPaymentIsProcessing(false)
        }
    }, [urlData]);

    return (
        <View style={{ flex: 1 }}>
            <WebView
                onNavigationStateChange={(state) => {
                    setPaymentIsProcessing(true)
                    setURLData(state?.url);
                }}
                onLoad={() => setIsLoading(false)}
                source={{ uri: url }}
            />
            {isLoading
                && <CustomLoader wholeScreen/>
            }
        </View>
    );
};

export default PaymentWebView;
