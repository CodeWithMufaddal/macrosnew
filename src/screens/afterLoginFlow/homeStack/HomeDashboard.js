import React, {
    useCallback, useEffect, useMemo, useState,
} from 'react';
import {
    View, StyleSheet, ScrollView, TouchableOpacity, RefreshControl, Alert,
} from 'react-native';
import { connect, useDispatch } from 'react-redux';
import { useIsFocused } from '@react-navigation/native';
import images, { getImagePath, imagePathType } from '../../../assets/images/images';
import CustomHeader from '../../../components/custom/CustomHeader';
import CustomImage from '../../../components/custom/CustomImage';
import CustomText from '../../../components/custom/CustomText';
import colors from '../../../constants/colors';
import {
    calenderDayType, globalPaddingMargin, isRTL, PLAN_MINIMUM_EXPIRE_DAYS,
} from '../../../constants/constants';
import i18n from '../../../i18n/i18n';
import YourPlan from '../../../components/homeDashboard/yourPlan/YourPlan';
import HomeBanners from '../../../components/homeDashboard/homeBanners/HomeBanners';
import UpcommingDelivery from '../../../components/homeDashboard/upcommingDelivery/UpcommingDelivery';
import CustomIcon, { IconType } from '../../../components/custom/CustomIcon';
import * as CommonHelpers from '../../../helpers/CommonHelpers';
import { navigateTo } from '../../../navigation/navigation';
import * as SubscriptionActions from '../../../redux/actions/SubscriptionActions';
import * as SettingsActions from '../../../redux/actions/SettingsActions';
import * as AuthActions from '../../../redux/actions/AuthActions';
import screens, { appFlows } from '../../../navigation/screens';
import { getSingleSubscriptionData } from '../../../helpers/CommonHelpers'
import moment from 'moment';

const HomeDashboard = (props) => {
    const isFocused = useIsFocused();
    const dispatch = useDispatch();
    const [upcommingDate] = useState(new Date(new Date().setDate(new Date().getDate() + 1)));
    const [refreshing, setRefreshing] = useState(false);
    const [upcommingDeliveryData, setUpcommingDeliveryData] = useState([]);
    useEffect(() => {
        dispatch(SettingsActions.getBannersAction());
        dispatch(AuthActions.getUserInbodySheetAction())
    }, [dispatch])

    useEffect(() => {
        if (isFocused) {
            dispatch(SubscriptionActions.getSubscriptionDetailsAction()).then(() => {
                dispatch(SubscriptionActions.getMyCalenderDataAction()).then(() => {
                    dispatch(SubscriptionActions.getSelectedMealsByDateAction(upcommingDate)).then((res) => {
                        const uData = [];
                        res?.map((mealData) => {
                            mealData?.meals_data.map((subMealData) => {
                                uData.push({ ...subMealData, mealCategory: mealData?.meal_name })
                            })
                        })
                        setUpcommingDeliveryData([...uData]);
                    })
                });
            });
        }
    }, [isFocused])

    // Header
    const headerLeftComponent = useMemo(() => (
        <View style={headerStyles.headerLeftContainer}>
            <CustomImage source={images.appSymbol} style={{ height: 30, width: 40, marginRight: 10 }} />
            <CustomText fontProps={['ff-roboto-thin', 'fs-large']} numberOfLines={1} customStyle={{ maxWidth: '40%' }}>Hi, {props.userData?.first_name}</CustomText>
            <CustomText fontProps={['fs-medium']}>&nbsp;&nbsp;{i18n.t('welcome_to_macros')}</CustomText>
        </View>
    ), [props.userData?.first_name])

    const renderHeader = useMemo(() => (
        <CustomHeader
            leftComponent={headerLeftComponent}
        />
    ), [headerLeftComponent])

    const renderYourPlan = useMemo(() => (
        <YourPlan
            selectedPlanData={props?.subscriptionDetails?.data}
        />
    ), [props?.subscriptionDetails?.data])

    const bannersImages = useMemo(() => props?.banners?.data?.map((item) => getImagePath(imagePathType.banners, item?.image)), [props?.banners])
    const renderBanners = useMemo(() => (
        <HomeBanners bannersImage={bannersImages ?? []} />
    ), [bannersImages])

    const selectedDateDayType = useMemo(() => CommonHelpers.getCalenderDayType(
        upcommingDate,
        props?.subscriptionDetails?.data,
        props?.myCalender?.data,
    ), [props?.myCalender?.data, props?.subscriptionDetails?.data, upcommingDate]);

    const onCalenderPress = useCallback(() => {
        dispatch(SubscriptionActions.setCalenderSelectedDateAction(moment (upcommingDate).add(1, 'd')))
        navigateTo(screens.singleDateManegeMeal)
    }, [dispatch, navigateTo, upcommingDate])

    const renderUpCommingDelivery = useMemo(() => {
        const currentSubscriptionData = getSingleSubscriptionData(props?.subscriptionDetails?.data, upcommingDate);
        const isCustomPlan = currentSubscriptionData?.is_custom_grams == 1
        if (![calenderDayType.noPlanExistDay.key, calenderDayType.offDay.key, calenderDayType].includes(selectedDateDayType) && !isCustomPlan) {
            return (
                <UpcommingDelivery
                    upcommingDate={upcommingDate}
                    selectedDateDayType={selectedDateDayType}
                    onCalenderPress={onCalenderPress}
                    upcommingDeliveryData={upcommingDeliveryData ?? []} />
            )
        }
        return <></>
    }, [onCalenderPress, props?.subscriptionDetails?.data, selectedDateDayType, upcommingDate, upcommingDeliveryData])

    const onRenewPress = useCallback(() => {
        dispatch(SettingsActions.changeAppFlow(appFlows.purchaseSubscriptionFlowNavigator))
    }, [dispatch])

    useEffect(() => {
        if (isFocused) {
            const wholePlanDurationData = CommonHelpers.getWholePlanDuration(props?.subscriptionDetails?.data);
            const expiresIn = wholePlanDurationData ? CommonHelpers.getExpireInDays(wholePlanDurationData?.end_time) : -1;
            if (expiresIn <= PLAN_MINIMUM_EXPIRE_DAYS && props?.subscriptionDetails?.data?.length <= 1) {
                Alert.alert(
                    i18n.t('your_subscription'),
                    '',
                    [
                        { text: i18n.t('cancel'), style: 'cancel' },
                        { text: i18n.t('renew'), style: 'destructive', onPress: onRenewPress },
                    ],
                )
            }
        }
    }, [isFocused])

    const renderRenewPlan = useMemo(() => {
        const wholePlanDurationData = CommonHelpers.getWholePlanDuration(props?.subscriptionDetails?.data);
        const expiresIn = wholePlanDurationData ? CommonHelpers.getExpireInDays(wholePlanDurationData?.end_time) : -1;
        return wholePlanDurationData && expiresIn >= 0 && (
            <>
                {expiresIn <= PLAN_MINIMUM_EXPIRE_DAYS && props?.subscriptionDetails?.data?.length <= 1 && (
                    <View style={styles.renewContainer}>
                        <CustomText fontProps={['fs-small']}>{i18n.t('you_have_days_remaining', { days: expiresIn })}</CustomText>
                        <TouchableOpacity onPress={onRenewPress} style={styles.renewButtonContainer} hitSlop={CommonHelpers.getHitSlop(20)}>
                            <CustomText
                                customStyle={styles.renewText}
                                color={colors.primary_3}
                                fontProps={['fs-small', 'ff-roboto-bold']}>
                                {i18n.t('renew')}
                            </CustomText>
                            <CustomIcon size={12} name={isRTL ? 'arrow-back-ios' : 'arrow-forward-ios'} color={colors.thinGrey} type={IconType.MaterialIcons} />
                        </TouchableOpacity>
                    </View>
                )}
            </>
        )
    }, [onRenewPress, props?.subscriptionDetails?.data])

    const onRefresh = useCallback(() => {
        setRefreshing(true);
        dispatch(SettingsActions.getBannersAction());
        dispatch(SubscriptionActions.getMyCalenderDataAction());
        dispatch(SubscriptionActions.getSelectedMealsByDateAction(upcommingDate)).then((res) => {
            const uData = [];
            res.map((mealData) => {
                mealData?.meals_data.map((subMealData) => {
                    uData.push({ ...subMealData, mealCategory: mealData?.meal_name })
                })
            })
            setUpcommingDeliveryData([...uData]);
        })
        dispatch(SubscriptionActions.getSubscriptionDetailsAction()).finally(() => {
            setRefreshing(false);
        });
    }, [dispatch, upcommingDate])

    const refreshControl = useMemo(() => (
        <RefreshControl
            onRefresh={onRefresh}
            refreshing={refreshing}
        />
    ), [onRefresh, refreshing])
    return (
        <View style={styles.mainContainer}>
            {/* Header  */}
            {renderHeader}
            <ScrollView
                refreshControl={refreshControl}
                nestedScrollEnabled={false}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={styles.contentContainer}
                style={styles.scrollView}>

                {/* Banner */}
                {renderBanners}

                {/* Upcomming Delivery */}
                {renderUpCommingDelivery}

                {/* Your Plan */}
                {renderYourPlan}

                {/* Renew Plan */}
                {renderRenewPlan}
            </ScrollView>

        </View>
    )
}

const headerStyles = StyleSheet.create({
    appLeftSymbol: {
        marginRight: 10,
        height: 40,
        width: 50,
        backgroundColor: 'red',
        alignSelf: 'center',
    },
    headerLeftContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
})
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    contentContainer: {
        paddingBottom: 20,
    },
    scrollView: {
        flex: 1,
        backgroundColor: colors.appBackground,
    },
    renewContainer: {
        paddingHorizontal: globalPaddingMargin.whole,
        marginVertical: 20,
        marginBottom: 30,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    renewButtonContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        height: '100%',
        justifyContent: 'center',
    },
    renewText: {
        marginRight: 5,
        textTransform: 'uppercase',
    },

})
export default connect((state) => ({
    banners: state.settings.banners,
    userData: state.auth.userData,
    subscriptionDetails: state.subscription.subscriptionDetails,
    myCalender: state.subscription.myCalender,
}), null)(HomeDashboard);
