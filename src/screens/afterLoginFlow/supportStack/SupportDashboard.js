import React, {
    useCallback, useEffect, useMemo, useState,
} from 'react';
import { TAWK_TO_CHAT_DIRECT_LINK } from '@env';
import {
    View, StyleSheet, SafeAreaView, StatusBar, TouchableOpacity,
} from 'react-native';
import { WebView } from 'react-native-webview';
import { useIsFocused } from '@react-navigation/native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import CustomLoader from '../../../components/custom/CustomLoader';
import colors from '../../../constants/colors';
import { setStatusBarBackgroundColor } from '../../../hooks/useStatusBar';
import { isAndroid } from '../../../constants/constants';
import CustomIcon, { IconType } from '../../../components/custom/CustomIcon'
import CustomText from '../../../components/custom/CustomText'
import i18n from '../../../i18n/i18n'
import { connect } from 'react-redux';

const SupportDashboard = (props) => {
    const isFocused = useIsFocused();
    const [renderFlag, setRenderFlag] = useState(false)
    const [showWebView, setShowWebView] = useState(true)

    const onRefreshPress = useCallback(() => {
        setShowWebView(false)
        setTimeout(() => setShowWebView(true), 500)
    }, [])

    useEffect(() => {
        setRenderFlag((flag) => !flag)
        if (isFocused) {
            setStatusBarBackgroundColor(colors.primary_2)
            StatusBar.setBarStyle('light-content')
        } else {
            setStatusBarBackgroundColor(colors.white)
            StatusBar.setBarStyle('dark-content')
        }
    }, [isFocused])

    const renderLoader = useCallback(() => (
        <View style={styles.loaderContainer}>
            <CustomLoader />
        </View>
    ), [])
    const jsCode = 'function hideHeaderToggle() {var headerToggle = document.getElementsByClassName("tawk-footer"); var i;for (i = 0; i < headerToggle.length; i += 1) {headerToggle[i].style.display = "none";};}; hideHeaderToggle();';
    const renderWebView = useMemo(() => (
        <View style={{ flex: 1, top: -1 }}>
            {showWebView && <WebView
                renderFlag={renderFlag}
                useWebKit
                originWhitelist={['*']}
                automaticallyAdjustContentInsets={false}
                javaScriptEnabled
                javaScriptEnabledAndroid
                scalesPageToFit
                style={{ flexGrow: 1 }}
                injectedJavaScript={jsCode}
                renderLoading={renderLoader}
                startInLoadingState
                source={{ uri: `${TAWK_TO_CHAT_DIRECT_LINK}${props.userData.phone}1fkkf` }}
                allowsInlineMediaPlayback={true}
            />}
            <TouchableOpacity style={styles.refreshButton} onPress={onRefreshPress}>
                <CustomIcon customStyle={styles.refreshIcon} type={IconType.MaterialCommunityIcons} name="refresh" size={25} color={colors.white} />
            </TouchableOpacity>

        </View>
    ), [showWebView, renderFlag, renderLoader, onRefreshPress])

    return (
        <View style={styles.mainContainer}>
            <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
                {renderWebView}
            </SafeAreaView>
            {isAndroid && <KeyboardSpacer />}
        </View>
    );
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    loaderContainer: {
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        bottom: 0,
        backgroundColor: 'rgba(255,255,255,0.8)',
        zIndex: 100,
        height: '100%',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    refreshIcon: {
        alignSelf: 'center',
        marginTop: 5,
        backgroundColor: colors.primary_2,
        padding: 2,
        borderRadius: 100,
    },
    refreshButton: {
        overflow: 'hidden',
        position: 'absolute',
        top: 15,
        right: 110,
        alignContent: 'center',
        justifyContent: 'center',
    },
})
export default connect((state) => ({
    userData: state.auth.userData,
}))(SupportDashboard);