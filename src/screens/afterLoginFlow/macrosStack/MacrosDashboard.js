import React, {
    useMemo, useCallback, useEffect, useState,
} from 'react';
import {
    View, StyleSheet, TouchableOpacity,
} from 'react-native';
import { connect, useDispatch } from 'react-redux';
import CustomHeader from '../../../components/custom/CustomHeader';
import CustomText from '../../../components/custom/CustomText';
import colors from '../../../constants/colors';
import i18n from '../../../i18n/i18n';
import CustomKeyboardAvoidingView from '../../../components/custom/CustomKeyboardAvoidingView';
import useSlider from '../../../hooks/useSlider';
import CustomSlider from '../../../components/custom/CustomSlider';
import { getHitSlop, getPlanType } from '../../../helpers/CommonHelpers';
import * as Validation from '../../../validation/Validation';
import * as AuthActions from '../../../redux/actions/AuthActions';
import CustomNavigatorShouldNotGoBack from '../../../components/custom/CustomNavigatorShouldNotGoBack';
import { PLAN_TYPE } from '../../../constants/constants';
import DiatCitationModal from '../../../components/diatCitationModal/DiatCitationModal';
import CustomIcon from '../../../components/custom/CustomIcon';

const MacrosDashboard = (props) => {
    const dispatch = useDispatch();
    const [proteins, proteinsScrollEnabled, bindProteins, setProteins] = useSlider(props?.userData?.proteins ?? 0);
    const [carbs, carbsScrollEnabled, bindCarbs, setCarbs] = useSlider(props?.userData?.carbs ?? 0);
    const [fats, fatsScrollEnabled, bindFats, setFats] = useSlider(props?.userData?.fats ?? 0);
    const [calories, caloriesScrollEnabled, bindCalories, setCalories] = useSlider(props?.userData?.calories ?? 0);
    const [showDiatCitationModal, setShowDiatCitationModal] = useState(false);

    const goal_name = props?.subscriptionDetails?.data?.length && props?.subscriptionDetails?.data[0]?.goal_name

    useEffect(() => {
        dispatch(AuthActions.getUserInbodySheetAction())
    }, [])

    useEffect(() => {
        if (calories > 0) {
            if (proteins > 0) setProteins(0);
            if (carbs > 0) setCarbs(0);
            if (fats > 0) setFats(0);
        }
    }, [calories])

    useEffect(() => {
        if (proteins > 0 || carbs > 0 || fats > 0) if (calories > 0) setCalories(0);
    }, [proteins, carbs, fats])

    const currentPlanType = useMemo(() => {
        const planType = getPlanType(new Date(), props?.subscriptionDetails?.data);
        return planType;
    }, [props?.subscriptionDetails?.data])

    const onSavePress = useCallback(() => {
        const data = {
            weight: props?.userData?.weight, proteins, carbs, fats, calories,
        }
        if (Validation.saveMyHealthDataValidation(data)) {
            dispatch(AuthActions.saveMyHealthDataAction(data))
        }
    }, [props?.userData?.weight, proteins, carbs, fats, calories, dispatch]);

    const headerRightComponent = useMemo(() => (
        <TouchableOpacity disabled={props?.saveMyHealthData?.loading} onPress={onSavePress} hitSlop={getHitSlop(20)}>
            <CustomText color={colors.primary_2} fontProps={['fs-medium']}>{props?.saveMyHealthData?.loading ? i18n.t('saving') : i18n.t('save')}</CustomText>
        </TouchableOpacity>
    ), [onSavePress, props?.saveMyHealthData?.loading]);

    const headerCenterComponent = useMemo(() => (
        <CustomText fontProps={['ff-roboto-bold', 'fs-medium']} color={colors.primary_2}>{i18n.t('adjustMacros')}</CustomText>
    ), [])

    const onIButtonPress = useCallback(() => {
        setShowDiatCitationModal(true);
    }, [])

    const headerLeftComponent = useMemo(() => (
        <>
            <DiatCitationModal
                showDiatCitationModal={showDiatCitationModal}
                setShowDiatCitationModal={setShowDiatCitationModal}
            />
            <TouchableOpacity onPress={onIButtonPress} hitSlop={getHitSlop(20)}>
                <CustomIcon name="infocirlce" color={colors.charcolGrey} size={20} />
            </TouchableOpacity>
        </>
    ), [onIButtonPress, showDiatCitationModal])
    const renderHeader = useMemo(() => (
        <CustomHeader
            leftComponent={headerLeftComponent}
            centerComponent={headerCenterComponent}
            rightComponent={headerRightComponent}
        />
    ), [headerCenterComponent, headerLeftComponent, headerRightComponent])

    const maxProtiens = goal_name == 'Lose Weight' ? 120 : goal_name == 'Maintain Weight' ? 150 : 200
    const maxCarbs = goal_name == 'Lose Weight' ? 120 : goal_name == 'Maintain Weight' ? 150 : 300
    const measurementIntervals = goal_name == 'Lose Weight' ? 20 : 50

    return (
        <View style={styles.mainContainer}>
            <CustomNavigatorShouldNotGoBack navigation={props?.navigation} canNotGoBack={props?.saveMyHealthData?.loading} />
            {/* Header  */}
            {renderHeader}
            <CustomKeyboardAvoidingView
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                scrollEnabled={(proteinsScrollEnabled === carbsScrollEnabled === fatsScrollEnabled === caloriesScrollEnabled)}
                extraHeight={200}
                style={styles.contentContainer}>
                <View style={styles.content}>
                    {/* calories */}
                    <View style={styles.sliderContainer}>
                        <CustomText
                            fontProps={['ff-roboto-medium', 'fs-medium']}
                            color={colors.primary_2}>
                            {i18n.t('calories')} &nbsp; : &nbsp;
                            <CustomText
                                fontProps={['ff-roboto-medium', 'fs-medium']}
                                color={colors.primary_3}>
                                {calories}
                            </CustomText>
                        </CustomText>
                        <CustomSlider {...bindCalories} min={0} max={4000} measurementInterval={800} />
                    </View>

                    <CustomText
                        color={colors.smokeyGrey}
                        fontProps={['ff-roboto-bold']}
                        customStyle={styles.orText}>
                        - OR -
                    </CustomText>
                    {/* Proteins */}
                    <View style={styles.sliderContainer}>
                        <CustomText
                            fontProps={['ff-roboto-medium', 'fs-medium']}
                            color={colors.primary_2}>
                            {i18n.t('proteins')} &nbsp; : &nbsp;
                            <CustomText
                                fontProps={['ff-roboto-medium', 'fs-medium']}
                                color={colors.primary_3}>
                                {proteins}
                            </CustomText>
                        </CustomText>
                        <CustomSlider measurementInterval={measurementIntervals} max={maxProtiens} {...bindProteins} />
                    </View>

                    {/* Carbs */}
                    <View style={styles.sliderContainer}>
                        <CustomText
                            fontProps={['ff-roboto-medium', 'fs-medium']}
                            color={colors.primary_2}>
                            {i18n.t('carbs')} &nbsp; : &nbsp;
                            <CustomText
                                fontProps={['ff-roboto-medium', 'fs-medium']}
                                color={colors.primary_3}>
                                {carbs}
                            </CustomText>
                        </CustomText>
                        <CustomSlider measurementInterval={measurementIntervals} max={maxCarbs} {...bindCarbs} />
                    </View>

                    {/* Fats */}
                    {currentPlanType != PLAN_TYPE.CUSTOM_GRAMS && (
                        <View style={styles.sliderContainer}>
                            <CustomText
                                fontProps={['ff-roboto-medium', 'fs-medium']}
                                color={colors.primary_2}>
                                {i18n.t('fats')} &nbsp; : &nbsp;
                                <CustomText
                                    fontProps={['ff-roboto-medium', 'fs-medium']}
                                    color={colors.primary_3}>
                                    {fats}
                                </CustomText>
                            </CustomText>
                            <CustomSlider measurementInterval={40} max={160} {...bindFats} />
                        </View>)}
                </View>

            </CustomKeyboardAvoidingView>

        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: colors.white,
    },
    contentContainer: {
        flex: 1,
        backgroundColor: colors.white,
    },
    content: {
        padding: 30,
    },
    sliderContainer: {
        marginBottom: 15,
    },
    orText: {
        textAlign: 'center',
        marginVertical: 10,
    },
})
export default connect((state) => ({
    userData: state?.auth?.userData,
    saveMyHealthData: state?.auth?.saveMyHealthData,
    subscriptionDetails: state?.subscription?.subscriptionDetails,
}))(MacrosDashboard);
