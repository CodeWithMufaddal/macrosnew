import React, {
    useMemo, useCallback, useState,
} from 'react';
import {
    View, StyleSheet, TouchableOpacity, ScrollView, RefreshControl, Linking, Alert, Pressable,
} from 'react-native';
import RNRestart from 'react-native-restart';
import { connect, useDispatch } from 'react-redux';
import ToggleSwitch from 'toggle-switch-react-native'
import CustomText from '../../../components/custom/CustomText';
import colors from '../../../constants/colors';
import CustomHeader from '../../../components/custom/CustomHeader';
import {
    globalPaddingMargin,
    INSTAGRAM_LINK,
    isRTL,
    MOBILE_NUMBER_PREFIX,
    WHATSAPP_CHAT_LINK,
} from '../../../constants/constants';
import CustomIcon, { IconType } from '../../../components/custom/CustomIcon';
import { getAmountWithCurrency, getHitSlop, getSingleSubscriptionData } from '../../../helpers/CommonHelpers';
import { commonShadow } from '../../../styles/CommonStyles';
import i18n, { APP_LANGUAGES } from '../../../i18n/i18n';
import screens from '../../../navigation/screens';
import { navigateTo, navigationType } from '../../../navigation/navigation';
import CustomButton from '../../../components/custom/CustomButton';
import { logoutAction } from '../../../redux/actions/AuthActions';
import { changeAppLanguageAction } from '../../../redux/actions/SettingsActions';
import * as AuthActions from '../../../redux/actions/AuthActions';
import CustomLoader from '../../../components/custom/CustomLoader';
import { navigationRef } from '../../../navigation/RootNavigation';
import CustomImage from '../../../components/custom/CustomImage';
import images from '../../../assets/images/images';
import useCodePush, { CodePushVersionCode } from '../../../codepush/useCodePush';

const ProfileDashboard = (props) => {
    const dispatch = useDispatch();
    const [refreshing, setRefreshing] = useState(false);
    const [loading, setLoading] = useState(false);
    const [isProfileScreenOpen, setIsProfileScreenOpen] = useState(false);

    const onNotificationPress = useCallback(() => {
        const route = navigationRef?.current?.getCurrentRoute();
        if (route?.name !== screens.notifications.name) navigateTo(screens.notifications, navigationType.navigate);
    }, []);

    const { version } = useCodePush()

    const initLoad = useCallback(() => {
        Promise.all([
            dispatch(AuthActions.getUserProfileAction()),
            dispatch(AuthActions.getUserInbodySheetAction()),
        ]).then(() => {
            setRefreshing(false);
        })
    }, [dispatch])



    const onMyProfilePress = useCallback(() => {
        if (!isProfileScreenOpen) {
            setIsProfileScreenOpen(true);
            dispatch(AuthActions.getUserProfileAction()).then(() => {
                navigateTo(screens.myProfile);
            })
            setTimeout(() => {
                setIsProfileScreenOpen(false);
            }, 5000); // Adjust the delay as needed
        }
    }, [dispatch, isProfileScreenOpen]);

    const onMyInbodySheetPress = useCallback(() => {
        if (!isProfileScreenOpen) {
            setIsProfileScreenOpen(true);
            dispatch(AuthActions.getUserInbodySheetAction()).then(() => {
                navigateTo(screens.inBodySheet);
            })
            setTimeout(() => {
                setIsProfileScreenOpen(false);
            }, 5000); // Adjust the delay as needed
        }
    }, [dispatch, isProfileScreenOpen]);

    const renderHeaderLeftComponent = useMemo(
        () => (
            <View style={styles.headerLeftContainer}>
                {/* <CustomImage
                    source={props?.userData?.avatar ? { uri: getImagePath(imagePathType.avatar, props?.userData?.avatar) } : images.profilePicPlaceHolder}
                    style={styles.profileImage}
                /> */}
                <View style={styles.descriptionContainer}>
                    <CustomText
                        fontProps={['ff-roboto-medium', 'fs-medium']}
                        customStyle={styles.fullNameText}
                        color={colors.primary_2}>
                        {props?.userData?.first_name}
                    </CustomText>
                    <CustomText fontProps={['fs-small']} color={colors.primary_2}>
                        {MOBILE_NUMBER_PREFIX} {props?.userData?.phone}
                    </CustomText>
                </View>
            </View>
        ),
        [props?.userData?.first_name, props?.userData?.phone],
    );

    const renderHeaderRightComponent = useMemo(
        () => (
            <TouchableOpacity onPress={onNotificationPress} hitSlop={getHitSlop(35)}>
                <CustomIcon
                    size={20}
                    type={IconType.Fontisto}
                    name="bell"
                    color={colors.primary_3}
                />
                {/* Badge */}
                {props?.notificationUnreadCount > 0 && <View style={styles.badgeContainer}>
                    <CustomText fontProps={['ff-roboto-medium']} color={colors.white}>{props?.notificationUnreadCount}</CustomText>
                </View>}
            </TouchableOpacity>
        ),
        [onNotificationPress, props?.notificationUnreadCount],
    );

    const onReferAndEarnPress = useCallback(() => {
        navigateTo(screens.referAndEarn);
    }, []);

    const renderReferAndEarnSection = useMemo(
        () => (
            <View style={styles.commonCardContainer}>
                {/* Referral Balance */}
                <View style={styles.referralBalanceContainer}>
                    <CustomText fontProps={['fs-small']}>
                        {i18n.t('referral_balance')}
                        <CustomText fontProps={['fs-small', 'ff-roboto-bold']}>
                            {' '}{getAmountWithCurrency(props?.userData?.balance)}
                        </CustomText>
                    </CustomText>

                    {/* Redeem Button */}
                    <TouchableOpacity
                        onPress={onReferAndEarnPress}
                        style={styles.redeemButtonContainer}
                        hitSlop={getHitSlop(20)}>
                        <CustomText
                            customStyle={styles.redeemText}
                            color={colors.primary_3}>
                            {i18n.t('redeem')}
                        </CustomText>
                        <CustomIcon
                            size={12}
                            name={isRTL ? 'arrow-back-ios' : 'arrow-forward-ios'}
                            color={colors.thinGrey}
                            type={IconType.MaterialIcons}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.seperator} />

                {/* Refer & Earn Button */}
                <TouchableOpacity
                    onPress={onReferAndEarnPress}
                    hitSlop={getHitSlop(20)}
                    style={styles.referAndEarnButtonContainer}>
                    <CustomIcon
                        name="share-google"
                        type={IconType.EvilIcons}
                        size={25}
                        color={colors.primary_3}
                    />
                    <CustomText
                        fontProps={['fs-medium', 'ff-roboto-medium']}
                        color={colors.mirageBlack}
                        customStyle={styles.referAndEarnText}>
                        {i18n.t('refer_and_earn')}
                    </CustomText>
                </TouchableOpacity>
            </View>
        ),
        [onReferAndEarnPress, props?.userData?.balance],
    );

    const renderMyProfileButton = useMemo(
        () => (
            <TouchableOpacity
                onPress={onMyProfilePress}
                hitSlop={getHitSlop(20)}
                style={styles.myProfileButtonContainer}>
                <CustomIcon
                    name="user-circle-o"
                    type={IconType.FontAwesome}
                    size={25}
                    color={colors.primary_3}
                />
                <CustomText
                    fontProps={['fs-medium', 'ff-roboto-medium']}
                    color={colors.mirageBlack}
                    customStyle={styles.referAndEarnText}>
                    {i18n.t('my_profile')}
                </CustomText>
            </TouchableOpacity>
        ),
        [onMyProfilePress],
    );

    const renderMyInBodyButton = useMemo(
        () => (
            <TouchableOpacity
                onPress={onMyInbodySheetPress}
                hitSlop={getHitSlop(20)}
                style={styles.myInBodyButtonContainer}>
                <CustomIcon
                    name="head-dots-horizontal"
                    type={IconType.MaterialCommunityIcons}
                    size={25}
                    color={colors.primary_3}
                />
                <CustomText
                    fontProps={['fs-medium', 'ff-roboto-medium']}
                    color={colors.mirageBlack}
                    customStyle={styles.referAndEarnText}>
                    {i18n.t('my_additional_details')}
                </CustomText>
            </TouchableOpacity>
        ),
        [onMyInbodySheetPress],
    );

    const onDeleteAccountPress = useCallback(() => {
        const onConfirmDeleteAccountPress = () => {
            setLoading(true);
            dispatch(AuthActions.deleteAccountAction())
                .catch(() => setLoading(false))
        }
        if (props?.subscriptionDetails?.data?.length > 0) {
            Alert.alert(
                'Delete Account',
                'You Can\'t Delete Account, because you have already active plan in your account',
            )
        } else {
            Alert.alert(
                'Are you sure want to delete account ?',
                'It will remove everything associated with this account. For ex.  plan, subscription personal details etc.',
                [
                    { text: 'Delete', style: 'destructive', onPress: onConfirmDeleteAccountPress },
                    { text: 'Cacnel', style: 'cancel' },
                ],
            )
        }
    }, [dispatch, props?.subscriptionDetails?.data?.length]);

    const onLogoutPress = useCallback(() => {
        dispatch(logoutAction());
    }, [dispatch]);

    const onChangeLanguagePress = useCallback(() => {
        const changeToLanguage = props?.appLanguage === APP_LANGUAGES.EN
            ? APP_LANGUAGES?.AR
            : APP_LANGUAGES?.EN;
        dispatch(changeAppLanguageAction(changeToLanguage, false)).then(() => {
            setTimeout(() => RNRestart.Restart(), 500);
        });
    }, [dispatch, props?.appLanguage]);

    const onRefresh = useCallback(() => {
        setRefreshing(true);
        initLoad();
    }, [initLoad]);

    const refreshControl = useMemo(() => (
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
    ), [onRefresh, refreshing])

    const renderLogoutButton = useMemo(() => (
        <CustomButton
            customStyle={styles.logoutButton}
            buttonLabel={i18n.t('logout')}
            backgroundColor={colors.primary_2}
            onPress={onLogoutPress}
        />
    ), [onLogoutPress])

    const renderDeleteButton = useMemo(() => (
        <CustomButton
            customStyle={styles.deleteAccountButton}
            buttonLabel={i18n.t('delete_account')}
            backgroundColor={colors.beanRed}
            onPress={onDeleteAccountPress}
        />
    ), [onDeleteAccountPress])

    const renderLanguageSwitchButton = useMemo(() => (
        <View style={styles.languageSwitchContainer}>
            <CustomText fontProps={['ff-roboto-bold', 'fs-medium']}>English</CustomText>
            <View style={{ marginHorizontal: 10 }}>
                <ToggleSwitch
                    isOn={props?.appLanguage === APP_LANGUAGES.AR}
                    onColor={colors.primary_3}
                    offColor={colors.primary_1}
                    size="large"
                    onToggle={onChangeLanguagePress}
                />
            </View>
            <CustomText fontProps={['ff-roboto-bold', 'fs-medium']}>عربي</CustomText>

        </View>
    ), [onChangeLanguagePress, props?.appLanguage])

    const onWhatsappPress = useCallback(() => {
        Linking.openURL(WHATSAPP_CHAT_LINK)
    }, [])

    const onInstagtamPress = useCallback(() => {
        Linking.openURL(INSTAGRAM_LINK)
    }, [])

    const renderSocialLinks = useMemo(() => (
        <View style={styles.socialLinksContainer}>
            <CustomText color={colors.thinGrey}>{i18n.t('contactUs')}</CustomText>
            <View style={styles.socialLinksButtonContainer}>
                <TouchableOpacity style={styles.socialLinkIconContainer} onPress={onWhatsappPress}>
                    <CustomImage SvgSource={images.whatsappSVG} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.socialLinkIconContainer} onPress={onInstagtamPress}>
                    <CustomImage SvgSource={images.instagramSVG} />
                </TouchableOpacity>
            </View>
        </View>
    ), [onInstagtamPress, onWhatsappPress])

    return (
        <View style={styles.mainContainer}>
            {/* Header */}
            <CustomHeader
                leftComponent={renderHeaderLeftComponent}
                rightComponent={renderHeaderRightComponent}
            />
            {props?.getUserData?.loading && !refreshing || loading && <CustomLoader wholeScreen={true} />}
            <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} refreshControl={refreshControl} style={styles.scrollViewStyle} contentContainerStyle={styles.contentContainer}>
                {/* Refer and Earn */}
                {renderReferAndEarnSection}

                {/* My Profile */}
                {renderMyProfileButton}

                {/* My Inbody Sheet */}
                {renderMyInBodyButton}

                {/* Language Switch Button */}
                {renderLanguageSwitchButton}

                {/* Logout Button */}
                {renderLogoutButton}

                {/* Delete Account */}
                {renderDeleteButton}

                {/* Social Links */}
                {renderSocialLinks}

                {/* App Version */}
                {/* <CodePushVersionCode appVersion={version} /> */}
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: colors.appBackground,
    },
    // profileImage: {
    //     height: 60,
    //     width: 60,
    //     marginRight: 20,
    //     borderRadius: 50,
    //     borderWidth: 1,
    //     borderColor: colors.primary_3,
    // },
    headerLeftContainer: {
        flexDirection: 'row',
    },
    descriptionContainer: {
        alignSelf: 'center',
    },
    badgeContainer: {
        position: 'absolute',
        backgroundColor: colors.primary_1,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        top: -5,
        right: -5,
        height: 18,
        width: 18,
    },
    scrollViewStyle: {
        flex: 1,
    },
    contentContainer: {
        padding: globalPaddingMargin.whole,
    },
    commonCardContainer: {
        backgroundColor: colors.white,
        borderRadius: 12,
        ...commonShadow(),
    },
    referralBalanceContainer: {
        paddingVertical: 10,
        paddingHorizontal: globalPaddingMargin.whole,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    redeemButtonContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        height: '100%',
        justifyContent: 'center',
    },
    redeemText: {
        marginRight: 5,
    },
    seperator: {
        width: '100%',
        height: 2,
        backgroundColor: colors.ghostWhite,
    },
    fullNameText: {
        marginBottom: 5,
    },
    referAndEarnButtonContainer: {
        paddingVertical: 10,
        paddingHorizontal: 40,
        flexDirection: 'row',
        alignItems: 'center',
    },
    referAndEarnText: {
        marginLeft: 15,
    },
    myProfileButtonContainer: {
        height: 55,
        paddingHorizontal: 40,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: colors.white,
        borderRadius: 12,
        marginTop: 20,
        ...commonShadow(),
    },
    myInBodyButtonContainer: {
        height: 55,
        paddingHorizontal: 40,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: colors.white,
        borderRadius: 12,
        marginTop: 20,
        ...commonShadow(),
    },
    logoutButton: {
        marginBottom: 10,
    },
    deleteAccountButton: {
        height: 30,
    },
    languageSwitchContainer: {
        marginVertical: 15,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    developedByContainer: {
        marginVertical: 10,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    socialLinksContainer: {
        marginVertical: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    socialLinksButtonContainer: {
        marginVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    socialLinkIconContainer: {
        marginHorizontal: 10,
    },
});

export default connect(
    (state) => ({
        userData: state.auth.userData,
        getUserData: state.auth.getUserData,
        subscriptionDetails: state.subscription.subscriptionDetails,
        appLanguage: state.settings.appLanguage,
        notificationUnreadCount: state?.notifications?.notificationUnreadCount,
    }),
    null,
)(ProfileDashboard);
