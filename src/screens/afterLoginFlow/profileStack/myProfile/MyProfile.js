import React, {
    useMemo, useCallback, useState, useEffect, Fragment, useRef,
} from 'react';
import {
    View, StyleSheet, TouchableOpacity,
} from 'react-native';
import { connect, useDispatch } from 'react-redux';
import CustomHeader from '../../../../components/custom/CustomHeader';
import CustomText from '../../../../components/custom/CustomText';
import colors from '../../../../constants/colors';
import i18n from '../../../../i18n/i18n';
import {
    globalPaddingMargin, isRTL, MOBILE_NUMBER_LENGTH, MOBILE_NUMBER_PREFIX, appTextInputType, DEFAULT_COORDINATE,
} from '../../../../constants/constants';
import useTextInput from '../../../../hooks/useTextInput';
import CustomKeyboardAvoidingView from '../../../../components/custom/CustomKeyboardAvoidingView';
import CustomTextInputTwo from '../../../../components/custom/CustomTextInputTwo';
import CustomPicker from '../../../../components/custom/CustomPicker';
import usePicker from '../../../../hooks/usePicker';
import CustomIcon, { IconType } from '../../../../components/custom/CustomIcon';
import { navigateGoBack } from '../../../../navigation/navigation';
import * as Validation from '../../../../validation/Validation';
import * as AuthActions from '../../../../redux/actions/AuthActions';
import { getAreasAction } from '../../../../redux/actions/SettingsActions';
import { checkPermissionForGallery, getCurrentLocation, getFormattedAddress, getHitSlop, getRegionForCoordinates, getServerImageUploadFormat } from '../../../../helpers/CommonHelpers';
import CustomImage from '../../../../components/custom/CustomImage';
// import images, { getImagePath, imagePathType } from '../../../../assets/images/images';
import VerifyOtpModal from './VerifyOtpModal';
import CustomNavigatorShouldNotGoBack from '../../../../components/custom/CustomNavigatorShouldNotGoBack';
import { wp } from '../../../../helpers/screenHelper';
import CustomMaps from '../../../../components/custom/CustomMaps';
import CustomButton from '../../../../components/custom/CustomButton';
import * as Services from '../../../../services/Services';


const MyProfile = (props) => {
    const dispatch = useDispatch();
    const mapRef = useRef(null);

    const inputRefs = useMemo(() => Array(10).fill(0).map(() => React.createRef()), []);
    const [avatar] = useState('');
    const [pinCoordinates, setPinCoordinates] = useState(getRegionForCoordinates(DEFAULT_COORDINATE));
    const [first_name, bindFirstName] = useTextInput(props?.userData?.first_name);
    const [mobile_number, bindMobileNumber] = useTextInput(props?.userData?.phone);
    const [address_nickname, bindAddressNickName] = useTextInput(props?.userData?.address_nickname);
    const [area_id, bindArea] = usePicker(props?.userData?.area_id);
    const [block, bindBlock, setBlock] = useTextInput(props?.userData?.block);
    const [street_name, bindStreet, setStreet] = useTextInput(props?.userData?.street_name);
    const [avenue, bindAvenue] = useTextInput(props?.userData?.avenue);
    const [building, bindBuilding] = useTextInput(props?.userData?.building);
    const [floor, bindFloor] = useTextInput(props?.userData?.floor);
    const [house_number, bindHouseNumber] = useTextInput(props?.userData?.house_number);
    const [office, bindOffice] = useTextInput(props?.userData?.office);
    const [additional_directions, bindAdditionalDirection] = useTextInput(props?.userData?.additional_directions);
    const [pickup_phone_number, bindPickupPhoneNumber] = useTextInput(props?.userData?.pickup_phone_number);

    const [phoneVerificationData, setPhoneVerificationData] = useState({ oldMobileNumber: props?.userData?.phone, newMobileNumber: mobile_number })
    const [showVerifyOtpModal, setVerifyOtpModal] = useState(false);
    const [authConfirmation, setAuthConfirmation] = useState(null);

    useEffect(() => {
        setPhoneVerificationData((pData) => ({ ...pData, newMobileNumber: mobile_number }));
    }, [mobile_number]);

    useEffect(() => {
        dispatch(getAreasAction());
    }, [dispatch])

    const onSavePress = useCallback(() => {
        const data = {
            first_name, mobile_number, address_nickname, avatar, area_id, block, street_name, avenue, building, house_number, floor, office, additional_directions, pickup_phone_number,
        }
        if (Validation.saveMyProfileValidation({ ...data, phoneVerificationData })) {
            dispatch(AuthActions.saveMyProfileAction(data)).then(() => {
                navigateGoBack();
            })
        }
    }, [first_name, mobile_number, address_nickname, avatar, area_id, block, street_name, avenue, building, house_number, floor, office, additional_directions, pickup_phone_number, phoneVerificationData, dispatch]);

    const onBackPress = useCallback(() => {
        navigateGoBack();
    }, [])

    // const onProfilePicUploadPress = useCallback(() => {
    //     ImagePicker.openPicker({
    //         width: 300,
    //         height: 300,
    //         mediaType: 'photo',
    //     }).then((image) => {
    //         setAvatar(getServerImageUploadFormat(image));
    //     }).catch((error) => {
    //         if (error?.message?.includes('User did not grant library permission')) {
    //             checkPermissionForGallery()
    //         }
    //     });
    // }, [])

    // Header
    const headerLeftComponent = useMemo(() => (
        <View style={headerStyles.headerLeftContainer}>
            <CustomIcon
                onPress={onBackPress}
                type={IconType.MaterialIcons}
                name={isRTL ? 'arrow-forward-ios' : 'arrow-back-ios'}
                size={16}
                color={colors.primary_2}
                customStyle={styles.backButton}
            />
            <CustomText fontProps={['ff-roboto-thin', 'fs-large']}>{i18n.t('my_profile')}</CustomText>
        </View>
    ), [onBackPress])

    const headerRightComponent = useMemo(() => (
        <TouchableOpacity disabled={props?.saveMyProfile?.loading} onPress={onSavePress} hitSlop={getHitSlop(20)}>
            <CustomText color={colors.primary_2} fontProps={['fs-medium']}>{props?.saveMyProfile?.loading ? i18n.t('saving') : i18n.t('save')}</CustomText>
        </TouchableOpacity>
    ), [onSavePress, props?.saveMyProfile?.loading])

    const renderHeader = useMemo(() => (
        <CustomHeader
            leftComponent={headerLeftComponent}
            rightComponent={headerRightComponent}
        />
    ), [headerLeftComponent, headerRightComponent])

    const onSubmitEditing = useCallback((currentIndex) => {
        if (inputRefs[currentIndex + 1]) inputRefs[currentIndex + 1]?.current?.focus()
    }, [inputRefs])

    const changeInputFocus = useCallback((index) => ({
        textInputRef: inputRefs[index],
        onSubmitEditing: () => onSubmitEditing(index),
    }), [inputRefs, onSubmitEditing])

    const areasData = useMemo(() => (props.areas?.data?.length > 0 ? props.areas?.data?.map((areaItem) => ({ key: areaItem?.id, label: areaItem?.name })) : []), [props.areas?.data])
    const renderPhoneLeftComponent = useMemo(() => (
        <CustomText fontProps={['ff-roboto-medium', 'fs-small']} color={colors.borderColor}>
            {MOBILE_NUMBER_PREFIX}
        </CustomText>
    ), [])

    const onVerifyPress = useCallback(() => {
        dispatch(AuthActions.sendOtpToMobileNumberAction(phoneVerificationData.newMobileNumber)).then((res) => {
            if (res?.authConfirmation) {
                setAuthConfirmation(res?.authConfirmation)
                setVerifyOtpModal(true);
            }
        })
    }, [dispatch, phoneVerificationData.newMobileNumber])

    const phoneVerifiedComponent = useMemo(() => (
        <Fragment>
            {phoneVerificationData.oldMobileNumber === phoneVerificationData.newMobileNumber ? (
                <CustomIcon size={17} name='checkcircle' color={colors.successGreen} />
            ) : (
                <TouchableOpacity onPress={onVerifyPress}>
                    <CustomText color={colors.red} fontProps={['fs-small', 'ff-roboto-bold']}>{i18n.t('verify')}</CustomText>
                </TouchableOpacity>
            )}
        </Fragment>

    ), [onVerifyPress, phoneVerificationData.newMobileNumber, phoneVerificationData.oldMobileNumber])

    const renderVerifyOtpModal = useMemo(() => (
        <VerifyOtpModal
            mobile_number={mobile_number}
            showVerifyOtpModal={showVerifyOtpModal}
            onHide={(type) => {
                if (type === 'success') setPhoneVerificationData((pData) => ({ ...pData, oldMobileNumber: pData.newMobileNumber }))
                setVerifyOtpModal(false)
            }}
            authConfirmation={authConfirmation}
        />
    ), [authConfirmation, mobile_number, showVerifyOtpModal])

    // Google Map 

    const getFromGMap = useCallback((coordinates) => {
        Services.getAddressFromLatLong(coordinates).then((res) => {
            const add = res?.results ? getFormattedAddress(res?.results ?? null) : null;
            setBlock(add?.block_number);
            setStreet(add?.street_name)
        })
    }, [])

    const getMyCurrentLocation = useCallback(() => {
        getCurrentLocation().then((coords) => {
            const region = getRegionForCoordinates({
                latitude: coords?.latitude ?? 0.0,
                longitude: coords?.longitude ?? 0.0,
            });
            // const region = getRegionForCoordinates({
            //     latitude: 29.1286327,
            //     longitude: 48.1174048,
            // });
            if (region) {
                onLocationChange(region);
            }
        })
    }, [onLocationChange])

    const onLocationChange = (coords, userInteraction = false) => {
        setPinCoordinates(coords)
        getFromGMap(coords);
        if (!userInteraction) {
            mapRef?.current?.animateToRegion(coords);
        }
    }

    const renderMaps = useMemo(() => (
        <View style={styles.mapContainer}>
            <CustomMaps
                pinCoordinates={pinCoordinates}
                showsMyLocationButton={true}
                mapRef={mapRef}
                // onMapReady={getMyCurrentLocation}
                onLocationChange={onLocationChange}
            >
                <View style={styles.refineButtonContainer}>
                    <CustomButton
                        onPress={getMyCurrentLocation}
                        customStyle={styles.refineButton}
                        buttonLabel={i18n.t('use_current_location')}
                    />
                </View>
            </CustomMaps>
        </View>
    ), [getMyCurrentLocation, pinCoordinates])


    return (
        <View style={styles.mainContainer}>
            <CustomNavigatorShouldNotGoBack navigation={props.navigation} canNotGoBack={props?.saveMyProfile?.loading} />
            {/* Header  */}
            {renderHeader}

            {/* Verify Otp Modal */}
            {renderVerifyOtpModal}
            <CustomKeyboardAvoidingView
                extraHeight={200}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                style={styles.contentContainer}>
                <>
                    {/* Maps */}
                    {renderMaps}


                    <View style={styles.content}>
                        <View style={styles.personalDetailsContainer}>
                            <View style={{ flex: 1 }}>
                                <CustomTextInputTwo
                                    {...bindFirstName}
                                    {...changeInputFocus(0)}
                                    label={i18n.t('label_name')}
                                    placeHolder={i18n.t('enter_name')}
                                    customContainerStyle={styles.textInputContainerStyle}
                                />

                                <CustomTextInputTwo
                                    {...bindMobileNumber}
                                    {...changeInputFocus(1)}
                                    textInputType={appTextInputType.NUMBER}
                                    keyboardType={'numeric'}
                                    leftComponent={renderPhoneLeftComponent}
                                    maxLength={MOBILE_NUMBER_LENGTH}
                                    rightComponent={phoneVerifiedComponent}
                                    label={i18n.t('label_phone')}
                                    placeHolder={i18n.t('enter_phone')}
                                    customContainerStyle={styles.textInputContainerStyle}
                                />
                            </View>
                            {/* Profile Pic Container */}
                            {/* <View style={styles.profilePicContainer}>
                                <TouchableOpacity activeOpacity={0.8} onPress={onProfilePicUploadPress} style={styles.profilePicTouchableContainer}>
                                    <CustomImage
                                        source={avatar ? { uri: avatar?.uri ?? avatar } : images.profilePicPlaceHolder}
                                        style={styles.profilePic}
                                    />
                                </TouchableOpacity>
                            </View> */}
                        </View>
                        <CustomText
                            fontProps={['ff-roboto-medium', 'fs-medium']}
                            customStyle={{ marginBottom: 10 }}
                            color={colors.primary_2}>
                            {i18n.t('deliverry_address')} {'*'}
                        </CustomText>

                        {/* Address Nick Name */}
                        <CustomTextInputTwo
                            {...bindAddressNickName}
                            {...changeInputFocus(0)}
                            label={i18n.t('label_address_nickname')}
                            placeHolder={i18n.t('enter_address_nickname')}
                            customContainerStyle={styles.textInputContainerStyle}
                        />

                        {/* Area */}
                        <CustomPicker
                            {...bindArea}
                            label={i18n.t('label_area')}
                            data={areasData}
                            placeHolder={i18n.t('select_area')}
                            keyName={'areas'}
                            isRequired
                        />

                        {/* Block */}
                        <CustomTextInputTwo
                            {...bindBlock}
                            {...changeInputFocus(1)}
                            label={i18n.t('label_block')}
                            placeHolder={i18n.t('enter_block')}
                            customContainerStyle={styles.textInputContainerStyle}
                            isRequired
                        />

                        {/* Street */}
                        <CustomTextInputTwo
                            {...bindStreet}
                            {...changeInputFocus(2)}
                            label={i18n.t('label_street')}
                            placeHolder={i18n.t('enter_street')}
                            customContainerStyle={styles.textInputContainerStyle}
                            isRequired
                        />

                        {/* Avenue */}
                        <CustomTextInputTwo
                            {...bindAvenue}
                            {...changeInputFocus(3)}
                            label={i18n.t('label_avenue')}
                            placeHolder={i18n.t('enter_avenue')}
                            customContainerStyle={styles.textInputContainerStyle}
                        />

                        {/* House number / Building number */}
                        <CustomTextInputTwo
                            {...bindHouseNumber}
                            {...changeInputFocus(4)}
                            label={`${i18n.t('label_house_number')} / ${i18n.t('label_building')}`}
                            placeHolder={`${i18n.t('enter_house_number')} / ${i18n.t('enter_building')}`}
                            customContainerStyle={styles.textInputContainerStyle}
                            isRequired
                        />


                        {/* Floor */}
                        <CustomTextInputTwo
                            {...bindFloor}
                            {...changeInputFocus(5)}
                            label={i18n.t('label_floor')}
                            placeHolder={i18n.t('enter_floor')}
                            customContainerStyle={styles.textInputContainerStyle}
                        />

                        {/* Office */}
                        <CustomTextInputTwo
                            {...bindOffice}
                            {...changeInputFocus(6)}
                            label={i18n.t('label_office')}
                            placeHolder={i18n.t('enter_office')}
                            customContainerStyle={styles.textInputContainerStyle}
                        />

                        {/* Additional Direction */}
                        <CustomTextInputTwo
                            {...bindAdditionalDirection}
                            {...changeInputFocus(7)}
                            label={i18n.t('label_additional_direction')}
                            placeHolder={i18n.t('enter_additional_direction')}
                            customContainerStyle={styles.textInputContainerStyle}
                        />

                        {/* Alternative Phone Number */}
                        <CustomTextInputTwo
                            {...bindPickupPhoneNumber}
                            {...changeInputFocus(8)}
                            textInputType={appTextInputType.NUMBER}
                            leftComponent={renderPhoneLeftComponent}
                            maxLength={MOBILE_NUMBER_LENGTH}
                            keyboardType={'numeric'}
                            label={i18n.t('label_alternative_phone_number')}
                            placeHolder={i18n.t('enter_alternative_phone_number')}
                            customContainerStyle={styles.textInputContainerStyle}
                        />

                    </View>
                </>
            </CustomKeyboardAvoidingView>

        </View>
    )
}

const headerStyles = StyleSheet.create({
    headerLeftContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
})
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: colors.white,
    },
    contentContainer: {
        flex: 1,
        backgroundColor: colors.white,
    },
    content: {
        padding: globalPaddingMargin.whole,
    },
    textInputContainerStyle: {
        marginBottom: 10,
    },
    backButton: {
        marginRight: 15,
    },
    personalDetailsContainer: {
        flexDirection: 'row',
    },

    // Location
    mapContainer: {
        height: 250,
        width: wp(100),
    },
    refineButtonContainer: {
        position: 'absolute',
        alignSelf: 'center',
        bottom: 10,
    },
    refineButton: {
        width: wp(40),
    },
})
export default connect((state) => ({
    areas: state.settings.areas,
    saveMyProfile: state.auth.saveMyProfile,
    userData: state.auth.userData,
}))(MyProfile);
