/* eslint-disable no-underscore-dangle */
import React, {
    useCallback, useState,
} from 'react';
import {
    View, StyleSheet,
} from 'react-native'
import OTPInputView from '@twotalltotems/react-native-otp-input';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {
    fontsSize, getFontsFamily, getFontsSize,
} from '../../../../assets/fonts/fonts';
import CustomButton from '../../../../components/custom/CustomButton';
import CustomKeyboardAvoidingView from '../../../../components/custom/CustomKeyboardAvoidingView';
import CustomModal from '../../../../components/custom/CustomModal'
import CustomText from '../../../../components/custom/CustomText';
import colors from '../../../../constants/colors';
import {
    FIREBASE_SEND_OTP_ENABLED, globalPaddingMargin, OTP_LENGTH,
} from '../../../../constants/constants';
import i18n from '../../../../i18n/i18n';
import CustomIcon from '../../../../components/custom/CustomIcon';
import { getHitSlop } from '../../../../helpers/CommonHelpers';

const VerifyOtpModal = ({
    showVerifyOtpModal, mobile_number, authConfirmation, onHide
}) => {
    const [otp, setOtp] = useState('');
    const [errorMessage, setErrorMessage] = useState(null);
    const [loading, setLoading] = useState(false);

    const onVerifyPress = useCallback(() => {
        setLoading(true);
        if (FIREBASE_SEND_OTP_ENABLED) {
            authConfirmation.confirm(otp).then((res) => {
                if (res?.user?._user?.uid) {
                    onHide('success');
                    setOtp('');
                } else {
                    setErrorMessage(i18n.t('something_went_wrong'))
                }
            }).catch((error) => {
                if (error.code === 'auth/invalid-verification-code') {
                    setErrorMessage(i18n.t('invalid_otp'))
                } else {
                    setErrorMessage(error.code);
                }
            }).finally(() => {
                setLoading(false);
            })
        } else if (authConfirmation == otp) {
            onHide('success');
            setOtp('');
            setLoading(false);
        } else {
            setErrorMessage(i18n.t('invalid_otp'))
            setLoading(false);
        }
    }, [authConfirmation, onHide, otp])

    return (
        <CustomModal
            animationIn={'fadeIn'}
            animationOut={'fadeOut'}
            onHide={onHide}
            isVisible={showVerifyOtpModal}
        >
            <CustomKeyboardAvoidingView contentContainerStyle={styles.container}>
                <View style={styles.mainContainer}>
                    <View>
                        <TouchableOpacity onPress={onHide} style={styles.closeContainer} hitSlop={getHitSlop(30)}>
                            <CustomIcon name="close" color={colors.primary_3} />
                        </TouchableOpacity>
                    </View>
                    <CustomText
                        fontProps={['fs-medium']}
                        color={colors.primary_2}
                        customStyle={styles.generateCouponText}>
                        {i18n.t('verify_otp')}
                    </CustomText>
                    <CustomText
                        fontProps={['fs-medium']}
                        color={colors.black}
                        customStyle={styles.subTitle}>
                        {i18n.t('otp_sent_to', { mobile_number })}
                    </CustomText>

                    {/* Code */}
                    <View style={styles.mainWrapperContainer}>
                        <OTPInputView
                            style={styles.otpContainer}
                            codeInputFieldStyle={styles.underlineStyleBase}
                            codeInputHighlightStyle={styles.underlineStyleHighLighted}
                            pinCount={OTP_LENGTH}
                            code={otp}
                            onCodeChanged={setOtp}
                            autoFocusOnLoad={false}
                        />
                    </View>
                    {errorMessage && (
                        <CustomText
                            customStyle={{ textAlign: 'center', textTransform: 'uppercase', marginVertical: 15 }}
                            fontProps={['ff-roboto-bold', 'fs-small']}
                            color={colors.red}>
                            {errorMessage}
                        </CustomText>
                    )}
                    <CustomButton
                        disabled={loading}
                        customStyle={styles.bottomButton}
                        buttonLabel={loading ? i18n.t('verifying') : i18n.t('verify_otp')}
                        onPress={onVerifyPress} />

                </View>
            </CustomKeyboardAvoidingView>
        </CustomModal>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    mainContainer: {
        backgroundColor: colors.white,
        width: '90%',
        borderRadius: 20,
        alignSelf: 'center',
        paddingVertical: globalPaddingMargin.whole,
        paddingHorizontal: 5,
    },
    closeContainer: {
        zIndex: 100,
        position: 'absolute',
        right: 10,
        top: 0,
    },
    mainWrapperContainer: {
        marginHorizontal: globalPaddingMargin.whole,
    },
    generateCouponText: {
        textAlign: 'center',
        marginBottom: globalPaddingMargin.whole,
        textTransform: 'uppercase',
    },
    subTitle: {
        textAlign: 'center',
        marginBottom: globalPaddingMargin.whole,
    },
    bottomButton: {
        marginTop: globalPaddingMargin.whole,
        width: '85%',
    },
    underlineStyleBase: {
        color: colors.black,
        fontFamily: getFontsFamily(),
        fontSize: getFontsSize(fontsSize.medium),
        borderRadius: 5,
        backgroundColor: colors.white,
        borderColor: colors.primary_1,
        borderWidth: 1,
        width: 40,
        height: 40,
        marginHorizontal: 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    otpContainer: {
        width: '100%',
        height: 80,
    },
    underlineStyleHighLighted: {
        borderColor: colors.primary_2,
    },
})

export default VerifyOtpModal;
