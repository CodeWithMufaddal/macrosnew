import React, { useMemo, useCallback, useState } from 'react';
import {
    View, StyleSheet, TouchableOpacity,
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import { connect, useDispatch } from 'react-redux';
import CustomHeader from '../../../../components/custom/CustomHeader';
import CustomImage from '../../../../components/custom/CustomImage';
import CustomText from '../../../../components/custom/CustomText';
import colors from '../../../../constants/colors';
import i18n from '../../../../i18n/i18n';
import useTextInput from '../../../../hooks/useTextInput';
import CustomKeyboardAvoidingView from '../../../../components/custom/CustomKeyboardAvoidingView';
import CustomTextInputTwo from '../../../../components/custom/CustomTextInputTwo';
import CustomPicker from '../../../../components/custom/CustomPicker';
import usePicker from '../../../../hooks/usePicker';
import { dailyActivityDummyData, workoutIntensityDummyData, workoutPerWeekDummyData } from '../../../../constants/dummyData';
import CustomIcon, { IconType } from '../../../../components/custom/CustomIcon';
import { navigateGoBack } from '../../../../navigation/navigation';
import { checkPermissionForGallery, getHitSlop, getServerImageUploadFormat } from '../../../../helpers/CommonHelpers';
import * as Validation from '../../../../validation/Validation';
import * as AuthActions from '../../../../redux/actions/AuthActions';
import CustomNavigatorShouldNotGoBack from '../../../../components/custom/CustomNavigatorShouldNotGoBack';
import { isRTL, appTextInputType } from '../../../../constants/constants';
import { getImagePath, imagePathType } from '../../../../assets/images/images';

const InbodySheet = (props) => {
    const dispatch = useDispatch();
    const inputRefs = useMemo(() => Array(7).fill(0).map(() => React.createRef()), []);
    const [height, bindHeight] = useTextInput(props?.userData?.height);
    const [weight, bindWeight] = useTextInput(props?.userData?.weight);
    const [target_weight, bindTargetWeight] = useTextInput(props?.userData?.target_weight);
    const [age, bindAge] = useTextInput(props?.userData?.age);
    const [daily_activity_lifestyle, bindDailyActivity] = usePicker(props?.userData?.daily_activity_lifestyle);
    const [workout_per_week, bindWorkoutPerWeek] = usePicker(props?.userData?.workout_per_week);
    const [workout_intensity, bindWorkoutIntensity] = usePicker(props?.userData?.workout_intensity);
    const [food_allergies, bindFoodAlergies] = useTextInput(props?.userData?.food_allergies);
    const [health_concerns, bindHealthConcerns] = useTextInput(props?.userData?.health_concerns);
    const [your_inbody, setInBodyImage] = useState(getImagePath(imagePathType.bodysheet, props?.userData?.your_inbody))

    const onSavePress = useCallback(() => {
        const {
            proteins = 0, carbs = 0, cals = 0, fats = 0,
        } = props?.userData;
        const data = {
            height, weight, target_weight, age, daily_activity_lifestyle, workout_per_week, workout_intensity, food_allergies, health_concerns, proteins, carbs, cals, fats, your_inbody,
        }
        if (Validation.saveInbodySheetValidation(data)) {
            dispatch(AuthActions.saveInbodySheetAction(data)).then(() => {
                navigateGoBack();
            })
        }
    }, [age, daily_activity_lifestyle, dispatch, food_allergies, health_concerns, height, props?.userData, target_weight, weight, workout_intensity, workout_per_week, your_inbody]);

    const onBackPress = useCallback(() => {
        navigateGoBack();
    }, [])

    // Header
    const headerLeftComponent = useMemo(() => (
        <View style={headerStyles.headerLeftContainer}>
            <TouchableOpacity onPress={onBackPress} hitSlop={getHitSlop(20)}>
                <CustomIcon
                    type={IconType.MaterialIcons}
                    name={isRTL ? 'arrow-forward-ios' : 'arrow-back-ios'}
                    size={16}
                    color={colors.primary_2}
                    customStyle={headerStyles.backButton}
                />
            </TouchableOpacity>
            <CustomText fontProps={['ff-roboto-thin', 'fs-large']}>{i18n.t('my_additional_details')}</CustomText>
        </View>
    ), [onBackPress])

    const headerRightComponent = useMemo(() => (
        <TouchableOpacity disabled={props?.saveInbodySheet?.loading} onPress={onSavePress} hitSlop={getHitSlop(20)}>
            <CustomText color={colors.primary_2} fontProps={['fs-medium']}>{props?.saveInbodySheet?.loading ? i18n.t('saving') : i18n.t('save')}</CustomText>
        </TouchableOpacity>
    ), [onSavePress, props?.saveInbodySheet?.loading]);

    const renderHeader = useMemo(() => (
        <CustomHeader
            leftComponentStyle={{ flex: 0 }}
            leftComponent={headerLeftComponent}
            rightComponent={headerRightComponent}
        />
    ), [headerLeftComponent, headerRightComponent])

    const onInBodyUploadPress = useCallback(() => {
        ImagePicker.openPicker({
            width: 300,
            height: 300,
            mediaType: 'photo',
        }).then((image) => {
            setInBodyImage(getServerImageUploadFormat(image));
        }).catch((error) => {
            if (error?.message?.includes('User did not grant library permission')) {
                checkPermissionForGallery()
            }
        });
    }, [])

    const onSubmitEditing = useCallback((currentIndex) => {
        if (inputRefs[currentIndex + 1]) inputRefs[currentIndex + 1]?.current?.focus()
    }, [inputRefs])

    const changeInputFocus = useCallback((index) => ({
        textInputRef: inputRefs[index],
        onSubmitEditing: () => onSubmitEditing(index),
    }), [inputRefs, onSubmitEditing])

    return (
        <View style={styles.mainContainer}>
            <CustomNavigatorShouldNotGoBack navigation={props?.navigation} canNotGoBack={props?.saveInbodySheet?.loading} />
            {/* Header  */}
            {renderHeader}
            <CustomKeyboardAvoidingView
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                extraHeight={200}
                style={styles.contentContainer}>
                <View style={styles.content}>
                    {/* Height */}
                    <CustomTextInputTwo
                        {...bindHeight}
                        {...changeInputFocus(0)}
                        label={i18n.t('label_height')}
                        placeHolder={i18n.t('enter_height')}
                        textInputType={appTextInputType.NUMBER}
                        keyboardType={'numeric'}
                        returnKeyType={'done'}
                        customContainerStyle={styles.textInputContainerStyle}
                    />

                    {/* Weight */}
                    <CustomTextInputTwo
                        {...bindWeight}
                        {...changeInputFocus(1)}
                        label={i18n.t('label_weight')}
                        placeHolder={i18n.t('enter_weight')}
                        textInputType={appTextInputType.NUMBER}
                        keyboardType={'numeric'}
                        returnKeyType={'done'}
                        customContainerStyle={styles.textInputContainerStyle}
                    />

                    {/* Target Weight */}
                    <CustomTextInputTwo
                        {...bindTargetWeight}
                        {...changeInputFocus(2)}
                        label={i18n.t('label_target_weight')}
                        placeHolder={i18n.t('enter_target_weight')}
                        keyboardType={'numeric'}
                        textInputType={appTextInputType.NUMBER}
                        customContainerStyle={styles.textInputContainerStyle}
                    />

                    {/* Age */}
                    <CustomTextInputTwo
                        {...bindAge}
                        {...changeInputFocus(3)}
                        label={i18n.t('label_age')}
                        placeHolder={i18n.t('enter_age')}
                        keyboardType={'numeric'}
                        textInputType={appTextInputType.NUMBER}
                        customContainerStyle={styles.textInputContainerStyle}
                    />

                    {/* Daily Activity / Lifestyle */}
                    <CustomPicker
                        {...bindDailyActivity}
                        label={i18n.t('label_daily_activity')}
                        data={dailyActivityDummyData}
                        placeHolder={i18n.t('select_daily_activity')}
                        keyName={'daily_activity_lifestyle'}
                    />

                    {/* Workout per week */}
                    <CustomPicker
                        {...bindWorkoutPerWeek}
                        data={workoutPerWeekDummyData}
                        label={i18n.t('label_workout_per_week')}
                        placeHolder={i18n.t('select_workout_per_week')}
                        keyName={'workout_per_week'}
                    />

                    {/* Workout intensity */}
                    <CustomPicker
                        {...bindWorkoutIntensity}
                        label={i18n.t('label_workout_intensity')}
                        data={workoutIntensityDummyData}
                        placeHolder={i18n.t('select_workout_intensity')}
                        keyName={'daily_activity_lifestyle'}
                    />
                    {/* Food Alergies */}
                    <CustomTextInputTwo
                        {...bindFoodAlergies}
                        {...changeInputFocus(5)}
                        label={i18n.t('label_food_alergies')}
                        placeHolder={i18n.t('enter_food_allergies')}
                        customContainerStyle={styles.textInputContainerStyle}
                    />
                    {/* Health concerns */}
                    <CustomTextInputTwo
                        {...bindHealthConcerns}
                        {...changeInputFocus(6)}
                        label={i18n.t('label_health_concerns')}
                        placeHolder={i18n.t('enter_health_concerns')}
                        customContainerStyle={styles.textInputContainerStyle}
                    />

                    {/* Upload your inbody */}
                    <TouchableOpacity onPress={onInBodyUploadPress} style={styles.uploadImageMainContainer}>
                        <CustomText
                            fontProps={['ff-roboto-medium', 'fs-medium']}
                            color={colors.primary_2}>
                            {i18n.t('upload_your_inbody')}
                        </CustomText>
                        <View style={styles.uploadImageContainer}>
                            <CustomIcon type={IconType.Feather} name="upload" size={20} color={colors.primary_3} />
                        </View>
                    </TouchableOpacity>
                    {your_inbody ? (
                        <CustomImage
                            source={{ uri: your_inbody?.uri ?? your_inbody }}
                            style={styles.uploadImage}
                        />
                    ) : null}
                </View>

            </CustomKeyboardAvoidingView>

        </View>
    )
}

const headerStyles = StyleSheet.create({
    headerLeftContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    backButton: {
        marginRight: 15,
    },
})
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: colors.white,
    },
    contentContainer: {
        flex: 1,
        backgroundColor: colors.white,
    },
    content: {
        padding: 30,
    },
    textInputContainerStyle: {
        marginBottom: 10,
    },
    uploadImageMainContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 20,
    },
    uploadImageContainer: {
        marginLeft: 20,
        height: 36,
        width: 36,
        borderRadius: 50,
        backgroundColor: colors.white,
        borderWidth: 1,
        borderColor: colors.smokeyGrey,
        alignItems: 'center',
        justifyContent: 'center',
    },
    uploadImage: {
        height: 100,
        width: 100,
        borderRadius: 10,
        marginBottom: 10,
    },
})
export default connect((state) => ({
    userData: state.auth.userData,
    saveInbodySheet: state.auth.saveInbodySheet,
}))(InbodySheet);
