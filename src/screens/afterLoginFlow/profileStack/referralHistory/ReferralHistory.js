import React, { useMemo, useCallback, useEffect } from 'react';
import {
    View, StyleSheet, FlatList,
} from 'react-native';
import { connect, useDispatch } from 'react-redux';
import CustomHeader from '../../../../components/custom/CustomHeader';
import CustomText from '../../../../components/custom/CustomText';
import colors from '../../../../constants/colors';
import i18n from '../../../../i18n/i18n';
import { APP_CURRENCY, globalPaddingMargin, isRTL } from '../../../../constants/constants';
import { navigateGoBack } from '../../../../navigation/navigation';
import CustomIcon, { IconType } from '../../../../components/custom/CustomIcon';
import images from '../../../../assets/images/images';
import CustomImage from '../../../../components/custom/CustomImage';
import { getReferralHistoryAction } from '../../../../redux/actions/UsersActions';
import CustomLoader from '../../../../components/custom/CustomLoader';
import { getValidDate } from '../../../../helpers/CommonHelpers';

const ReferAndEarn = (props) => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getReferralHistoryAction())
    }, [dispatch])
    const onBackPress = useCallback(() => {
        navigateGoBack();
    }, [])

    // Header
    const headerLeftComponent = useMemo(() => (
        <View style={headerStyles.headerLeftContainer}>
            <CustomIcon
                onPress={onBackPress}
                type={IconType.MaterialIcons}
                name={isRTL ? 'arrow-forward-ios' : 'arrow-back-ios'}
                size={16}
                color={colors.primary_2}
                customStyle={styles.backButton}
            />
            <CustomText fontProps={['ff-roboto-thin', 'fs-large']}>{i18n.t('referral_history')}</CustomText>
        </View>
    ), [onBackPress])

    const renderHeader = useMemo(() => <CustomHeader leftComponent={headerLeftComponent} />, [headerLeftComponent])

    const renderReferralHistory = useCallback(({ item }) => (
        <View style={styles.singleReferralCodeItem}>
            <View style={styles.getReferralContainer}>
                <CustomImage source={images.getRefferal} style={{ height: 35, width: 35 }} />
            </View>
            <View style={styles.referralTitleContainer}>
                <CustomText fontProps={['fs-small', 'ff-roboto-light']}>{item?.title} : {item?.code}</CustomText>
                <CustomText fontProps={['fs-xsmall']} customStyle={styles.dateText}>{getValidDate(item?.created_time, 'DD MMM YYYY HH:MM')}</CustomText>
            </View>
            <View style={styles.amountReferralContainer}>
                <CustomText fontProps={['fs-xsmall']} color={colors.primary_2}>{APP_CURRENCY} &nbsp;</CustomText>
                <CustomText fontProps={['fs-large']} color={colors.primary_2}>{item?.amount}</CustomText>
            </View>
        </View>
    ), [])

    const flatListSeparator = useCallback(() => (
        <View style={styles.flatListSeparator} />
    ), [])

    const ListEmptyComponent = useMemo(() => (
        <View style={styles.noDataTextContainer}>
            <CustomText>{i18n.t('no_data_found')}</CustomText>
        </View>
    ), [])

    return (
        <View style={styles.mainContainer}>
            {/* Header  */}
            {renderHeader}
            {props?.referralHistory?.loading ? (
                <CustomLoader />
            ) : (
                <FlatList
                    ListEmptyComponent={ListEmptyComponent}
                    style={styles.flatListContainer}
                    contentContainerStyle={styles.flatListContentContainer}
                    data={props?.referralHistory?.data}
                    renderItem={renderReferralHistory}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    ItemSeparatorComponent={flatListSeparator}
                />
            )}
        </View>
    )
}

const headerStyles = StyleSheet.create({
    headerLeftContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
})
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: colors.appBackground,
    },
    flatListContainer: {
        width: '100%',
    },
    flatListContentContainer: {
        alignItems: 'center',
        padding: globalPaddingMargin.whole,
    },
    backButton: {
        marginRight: 15,
    },
    singleReferralCodeItem: {
        flexDirection: 'row',
    },
    getReferralContainer: {
        width: '20%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    referralTitleContainer: {
        width: '60%',
        justifyContent: 'center',
    },
    amountReferralContainer: {
        width: '20%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    dateText: {
        marginTop: 10,
    },
    flatListSeparator: {
        height: 20,
        width: '100%',
        backgroundColor: 'green',
    },
    noDataTextContainer: {
        marginVertical: 10,
        alignItems: 'center',
    },
})
export default connect((state) => ({
    referralHistory: state.users.referralHistory,
}))(ReferAndEarn);
