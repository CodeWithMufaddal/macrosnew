import React, { useMemo, useCallback, useEffect } from 'react';
import {
    View, StyleSheet, FlatList,
} from 'react-native';
import { connect, useDispatch } from 'react-redux';
import CustomHeader from '../../../../components/custom/CustomHeader';
import CustomText from '../../../../components/custom/CustomText';
import colors from '../../../../constants/colors';
import i18n from '../../../../i18n/i18n';
import { globalPaddingMargin, isRTL } from '../../../../constants/constants';
import { navigateGoBack } from '../../../../navigation/navigation';
import CustomIcon, { IconType } from '../../../../components/custom/CustomIcon';

import { sendDataToReducer } from '../../../../helpers/CommonHelpers';
import * as ReduxTypes from '../../../../redux/types/ReduxTypes';
import { isIOS } from '../../../../helpers/screenHelper';

const Notifications = (props) => {
    const dispatch = useDispatch();
    const onBackPress = useCallback(() => {
        navigateGoBack();
    }, [])

    useEffect(() => {
        sendDataToReducer(dispatch, ReduxTypes.SET_NOTIFICATIONS_UNREAD_COUNT, 0);
    }, [props?.notificationList]);

    // Header
    const headerLeftComponent = useMemo(() => (
        <View style={headerStyles.headerLeftContainer}>
            <CustomIcon
                onPress={onBackPress}
                type={IconType.MaterialIcons}
                name={isRTL ? 'arrow-forward-ios' : 'arrow-back-ios'}
                size={16}
                color={colors.primary_2}
                customStyle={styles.backButton}
            />
            <CustomText fontProps={['ff-roboto-thin', 'fs-large']}>{i18n.t('notifications')}</CustomText>
        </View>
    ), [onBackPress])

    const renderHeader = useMemo(() => <CustomHeader leftComponent={headerLeftComponent} />, [headerLeftComponent])

    const notificationTitle = useCallback((item) => {
        if (isIOS) return item?.title
        else return item?.title;
    }, [])

    const notificationSubtitle = useCallback((item) => {
        if (isIOS) return item?.message
        else return item?.body;
    }, [])
    const renderNotifications = useCallback(({ item }) => (
        <View style={styles.singleNotificationItem}>
            <CustomText fontProps={['fs-medium']} >{notificationTitle(item?.notification)}</CustomText>
            <CustomText fontProps={['fs-medium', 'ff-roboto-light']} customStyle={styles.subTitle}>{notificationSubtitle(item?.notification)}</CustomText>
        </View>
    ), [])

    const flatListSeparator = useCallback(() => (
        <View style={styles.flatListSeparator} />
    ), [])

    const ListEmptyComponent = useMemo(() => (
        <View style={styles.noDataTextContainer}>
            <CustomText>{i18n.t('no_notifications_found')}</CustomText>
        </View>
    ), [])

    return (
        <View style={styles.mainContainer}>
            {/* Header  */}
            {renderHeader}
            <FlatList
                ListEmptyComponent={ListEmptyComponent}
                style={styles.flatListContainer}
                contentContainerStyle={styles.flatListContentContainer}
                data={props?.notificationList ?? []}
                renderItem={renderNotifications}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                ItemSeparatorComponent={flatListSeparator}
            />
        </View>
    )
}

const headerStyles = StyleSheet.create({
    headerLeftContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
})
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: colors.appBackground,
    },
    flatListContainer: {
        flex: 1,
    },
    flatListContentContainer: {
        padding: globalPaddingMargin.whole,
    },
    backButton: {
        marginRight: 15,
    },
    singleNotificationItem: {
        width: '100%',
        alignItems: 'flex-start',
    },
    subTitle: {
        marginTop: 10,
    },
    flatListSeparator: {
        height: 1,
        marginVertical: 10,
        width: '100%',
        backgroundColor: colors.thinGrey,
    },
    noDataTextContainer: {
        marginVertical: 10,
        alignItems: 'center',
    },
})
export default connect((state) => ({
    notificationList: state.notifications.notificationList,
}))(Notifications);
