import React, { useCallback, useMemo, useState } from 'react';
import {
    View, StyleSheet, TouchableOpacity, TextInput,
} from 'react-native'
import { useDispatch, connect } from 'react-redux';
import Clipboard from '@react-native-clipboard/clipboard';
import {
    fontsFamily, fontsSize, getFontsFamily, getFontsSize,
} from '../../../../assets/fonts/fonts';
import CustomButton from '../../../../components/custom/CustomButton';
import CustomIcon, { IconType } from '../../../../components/custom/CustomIcon';
import CustomKeyboardAvoidingView from '../../../../components/custom/CustomKeyboardAvoidingView';
import CustomModal from '../../../../components/custom/CustomModal'
import CustomText from '../../../../components/custom/CustomText';
import colors from '../../../../constants/colors';
import { appTextInputType, APP_CURRENCY, globalPaddingMargin, isRTL } from '../../../../constants/constants';
import { getAmountWithCurrency, getHitSlop } from '../../../../helpers/CommonHelpers';
import i18n from '../../../../i18n/i18n';
import { generateCouponCodeAction } from '../../../../redux/actions/UsersActions';
import { getTextInputRTLDirection } from '../../../../styles/CommonStyles';

const RedeemCode = ({ showRedeemCodeModal, onHide, ...props }) => {
    const dispatch = useDispatch()
    const [screenType, setScreenType] = useState('generate');
    const [amount, setAmount] = useState();
    const [copied, setCopied] = useState(false);
    const [couponData, setCouponData] = useState(null);
    const [errorMessage, setErrorMessage] = useState(null);
    const displayData = useMemo(() => ({
        generate: {
            title: i18n.t('generate_coupon'),
            subTitle: i18n.t('generate_coupon_subtitle'),
            bottomButtonText: i18n.t('cancel'),
        },
        redeem: {
            title: i18n.t('redeem_coupon'),
            subTitle: i18n.t('redeem_coupon_subtitle'),
            bottomButtonText: i18n.t('ok'),
        },
    }), [])

    const onBottomButtonPress = useCallback(() => {
        onHide();
        setAmount()
        setScreenType('generate')
        setErrorMessage(null);
    }, [onHide])

    const onGenerate = useCallback(() => {
        if (!amount) return alert(i18n.t('enter_amount'))
        if (amount <= 0) return alert(i18n.t('amount_should be_greter'))
        dispatch(generateCouponCodeAction(amount)).then((res) => {
            setCouponData(res?.data);
            setScreenType('redeem');
            setErrorMessage(null);
        }).catch((error) => {
            setErrorMessage(error?.data?.message);
        })
        return true;
    }, [amount, dispatch])

    const onCopy = useCallback(() => {
        Clipboard.setString(couponData?.code)
        setCopied(true)
        setTimeout(() => {
            setCopied(false);
        }, 1000)
    }, [])
    return (
        <CustomModal
            animationIn={'fadeIn'}
            animationOut={'fadeOut'}
            onHide={onHide}
            isVisible={showRedeemCodeModal}
        >
            <CustomKeyboardAvoidingView contentContainerStyle={styles.container}>
                <View style={styles.mainContainer}>
                    <CustomText
                        fontProps={['fs-medium']}
                        color={colors.primary_2}
                        customStyle={styles.generateCouponText}>
                        {displayData[screenType].title}
                    </CustomText>
                    <CustomText
                        fontProps={['fs-medium']}
                        color={colors.black}
                        customStyle={styles.subTitle}>
                        {displayData[screenType].subTitle}
                    </CustomText>

                    {/* Code */}
                    <View style={styles.mainWrapperContainer}>
                        <View style={styles.referralCodeMainContainer}>
                            <View style={styles.referCodeContainer}>
                                {screenType === 'generate' && (
                                    <CustomText
                                        fontProps={['ff-roboto-medium', 'fs-medium']}
                                        color={colors.mirageBlack}>
                                        {APP_CURRENCY}
                                    </CustomText>
                                )}
                                <TextInput
                                    autoCorrect={false}
                                    autoCapitalize={'none'}
                                    editable={screenType === 'generate'}
                                    returnKeyType={'done'}
                                    onChangeText={setAmount}
                                    placeholder={'_ _ _'}
                                    keyboardType={'decimal-pad'}
                                    value={screenType === 'generate' ? amount : couponData?.code}
                                    style={{ 
                                        ...styles.referralCodeTextInput, 
                                        ...getTextInputRTLDirection(appTextInputType.NUMBER),
                                    }}
                                />
                            </View>
                            <TouchableOpacity
                                disabled={props?.generateCoupon?.loading}
                                hitSlop={getHitSlop(20)}
                                style={styles.copyButtonContainer}
                                onPress={screenType === 'generate' ? onGenerate : onCopy}>
                                {screenType === 'generate' ? (
                                    <CustomText
                                        fontProps={['ff-roboto-medium', 'fs-small']}
                                        color={colors.primary_3}
                                        customStyle={styles.copyButton}>
                                        {props?.generateCoupon?.loading ? i18n.t('generating') : i18n.t('generate')}
                                    </CustomText>
                                ) : (
                                    <CustomIcon
                                        size={24}
                                        name="content-copy"
                                        color={colors.borderColor}
                                        type={IconType.MaterialIcons}
                                    />
                                )}
                            </TouchableOpacity>
                        </View>
                        {errorMessage && (
                            <CustomText
                                customStyle={{ textAlign: 'center', textTransform: 'uppercase', marginVertical: 15 }}
                                fontProps={['ff-roboto-bold', 'fs-small']}
                                color={colors.red}>
                                {errorMessage}
                            </CustomText>
                        )}
                        {copied && (
                            <CustomText
                                customStyle={{ textAlign: 'center', textTransform: 'uppercase', marginVertical: 15 }}
                                fontProps={['ff-roboto-bold', 'fs-small']}
                                color={colors.primary_3}>
                                {i18n.t('coupon_copied')}
                            </CustomText>
                        )}

                        {screenType === 'redeem' && (
                            <CustomText
                                customStyle={{ textAlign: 'center', textTransform: 'uppercase', marginVertical: 15 }}
                                fontProps={['fs-small']}
                                color={colors.borderColor}>
                                {`VALUE ${getAmountWithCurrency(amount)}`}
                            </CustomText>
                        )}

                    </View>
                    <CustomButton
                        customStyle={styles.bottomButton}
                        buttonLabel={displayData[screenType].bottomButtonText}
                        onPress={onBottomButtonPress} />
                </View>
            </CustomKeyboardAvoidingView>
        </CustomModal>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    mainContainer: {
        backgroundColor: colors.white,
        width: '90%',
        borderRadius: 20,
        alignSelf: 'center',
        padding: globalPaddingMargin.whole,
    },
    mainWrapperContainer: {
        direction: 'ltr',
        marginHorizontal: globalPaddingMargin.whole,
    },
    generateCouponText: {
        textAlign: 'center',
        marginBottom: globalPaddingMargin.whole,
        textTransform: 'uppercase',
    },
    referralCodeMainContainer: {
        alignItems: 'flex-start',
        flexDirection: 'row',
        height: 48,
        backgroundColor: colors.white,
        borderColor: colors.lightBlack,
        borderWidth: 0.5,
        borderRadius: 8,
    },
    referCodeContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 15,
        height: '100%',
    },
    referralCodeTextInput: {
        flex: 1,
        height: '100%',
        paddingHorizontal: globalPaddingMargin.left,
        color: colors.smokeyGrey,
        fontSize: getFontsSize(fontsSize.small),
        fontFamily: getFontsFamily(fontsFamily.roboto.medium),
    },
    copyButtonContainer: {
        paddingHorizontal: globalPaddingMargin.left,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    copyButton: {
        textTransform: 'uppercase',
    },
    subTitle: {
        textAlign: 'center',
        marginBottom: globalPaddingMargin.whole,
    },
    bottomButton: {
        marginTop: globalPaddingMargin.whole,
        width: '85%',
    },
})

export default connect((state) => ({
    generateCoupon: state.users.generateCoupon,
}))(RedeemCode);
