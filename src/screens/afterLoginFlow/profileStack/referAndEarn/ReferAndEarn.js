import React, { useMemo, useCallback, useState } from 'react';
import {
    View, StyleSheet, TouchableOpacity, ScrollView, 
} from 'react-native';
import Clipboard from '@react-native-clipboard/clipboard';
import { connect } from 'react-redux';
import CustomHeader from '../../../../components/custom/CustomHeader';
import CustomText from '../../../../components/custom/CustomText';
import colors from '../../../../constants/colors';
import i18n from '../../../../i18n/i18n';
import { commonShadow } from '../../../../styles/CommonStyles';
import { globalPaddingMargin, isRTL } from '../../../../constants/constants';
import { navigateGoBack, navigateTo } from '../../../../navigation/navigation';
import CustomIcon, { IconType } from '../../../../components/custom/CustomIcon';
import screens from '../../../../navigation/screens';
import CustomButton from '../../../../components/custom/CustomButton';
import images from '../../../../assets/images/images';
import RedeemCode from './RedeemCode';
import { getAmountWithCurrency } from '../../../../helpers/CommonHelpers';
import CustomToast from '../../../../components/custom/CustomToast';

const ReferAndEarn = (props) => {
    const [copiedTimer, setCopiedTimer] = useState(false);
    const [showRedeemCodeModal, setShowRedeemCodeModal] = useState(false);
    const [referralCode] = useState(props?.userData?.referral);
    const onCopy = useCallback(() => {
        setCopiedTimer(true);
        Clipboard.setString(referralCode)
        setTimeout(() => setCopiedTimer(false), 2000);
    }, [referralCode])

    const onBackPress = useCallback(() => {
        navigateGoBack();
    }, [])

    // Header
    const headerLeftComponent = useMemo(() => (
        <View style={headerStyles.headerLeftContainer}>
            <CustomIcon
                onPress={onBackPress}
                type={IconType.MaterialIcons}
                name={isRTL ? 'arrow-forward-ios' : 'arrow-back-ios'}
                size={16}
                color={colors.primary_2}
                customStyle={styles.backButton}
            />
            <CustomText fontProps={['ff-roboto-thin', 'fs-large']}>{i18n.t('earn_rewards')}</CustomText>
        </View>
    ), [onBackPress])

    const renderHeader = useMemo(() => (
        <CustomHeader
            leftComponent={headerLeftComponent}
        />
    ), [headerLeftComponent])

    const onRedeemButtonPress = useCallback(() => {
        if (Number(props?.userData?.balance) > 0) setShowRedeemCodeModal(true);
        else CustomToast.info(i18n.t('you_have_not_sufficient_balance'))
    }, [props?.userData?.balance])

    const onHideGenerateRedeemModal = useCallback(() => {
        setShowRedeemCodeModal(false);
    }, [])
    const renderGenerateRedeemCode = useMemo(() => (
        <RedeemCode showRedeemCodeModal={showRedeemCodeModal} onHide={onHideGenerateRedeemModal} />
    ), [onHideGenerateRedeemModal, showRedeemCodeModal])

    const onRedeemHistoryButtonPress = useCallback(() => {
        navigateTo(screens.referralHistory)
    }, [])
    return (
        <View style={styles.mainContainer}>
            {/* Header  */}
            {renderHeader}
            {renderGenerateRedeemCode}
            <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                <View style={styles.contentContainer}>
                    <View style={styles.dollarContainer}>
                        <CustomIcon type={IconType.FontAwesome} name="dollar" size={60} color={colors.primary_1} />
                    </View>

                    {/* Title */}
                    <View style={styles.titleContainer}>
                        {/* Title 1 */}
                        <CustomText customStyle={styles.title} fontProps={['fs-small']}>{i18n.t('invite_your_friends_and_family')}</CustomText>
                        {/* Title 2 */}
                        <CustomText customStyle={styles.title} fontProps={['fs-small']}>{i18n.t('to_earn_rewards_on_each_signup')}</CustomText>
                    </View>
                </View>

                {/* ReferralCode */}
                <View style={styles.mainWrapperContainer}>
                    <View style={styles.referralCodeMainContainer}>
                        <View style={styles.referCodeContainer}>
                            <CustomText
                                customStyle={styles.referralCode}
                                fontProps={['ff-roboto-medium', 'fs-small']}
                            >{referralCode}</CustomText>
                        </View>
                        <TouchableOpacity disabled={copiedTimer} style={styles.copyButtonContainer} onPress={onCopy}>
                            <CustomText
                                fontProps={['ff-roboto-medium', 'fs-small']}
                                color={copiedTimer ? colors.lightGrey : colors.primary_3}
                                customStyle={styles.copyButton}>
                                {copiedTimer ? i18n.t('copied') : i18n.t('copy')}
                            </CustomText>
                        </TouchableOpacity>
                    </View>
                </View>

                {/* Your Earnings */}
                <View style={styles.mainYourEarningsContainer}>
                    <View style={styles.referralCodeMainContainer}>
                        <View style={styles.referCodeContainer}>
                            <CustomText
                                customStyle={styles.referralCode}
                                fontProps={['ff-roboto-medium', 'fs-small']}
                            >{getAmountWithCurrency(props?.userData?.balance)}</CustomText>
                        </View>
                        <View style={styles.copyButtonContainer}>
                            <CustomText
                                fontProps={['ff-roboto-medium', 'fs-small']}
                                color={colors.primary_3}
                                customStyle={styles.copyButton}>
                                {i18n.t('your_earningns')}
                            </CustomText>
                        </View>
                    </View>
                </View>

                {/* Redeem Button */}
                <CustomButton
                    icon={images.redeemIconSvg}
                    onPress={onRedeemButtonPress}
                    customStyle={styles.bottomButton}
                    buttonLabel={i18n.t('redeem')}
                />

                {/* History Button */}
                <CustomButton
                    onPress={onRedeemHistoryButtonPress}
                    icon={images.historyIconSvg}
                    customStyle={styles.historyBottomButton}
                    buttonLabel={i18n.t('history')}
                />
            </ScrollView>
        </View>
    )
}

const headerStyles = StyleSheet.create({
    headerLeftContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
})
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: colors.appBackground,
    },
    contentContainer: {
        padding: globalPaddingMargin.whole,
        alignItems: 'center',
    },
    backButton: {
        marginRight: 15,
    },
    dollarContainer: {
        height: 85,
        width: 85,
        borderColor: colors.primary_1,
        borderWidth: 10,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
    titleContainer: {
        marginVertical: globalPaddingMargin.vertical,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        marginVertical: 5,
    },
    mainWrapperContainer: {
        marginHorizontal: globalPaddingMargin.whole,
    },
    mainYourEarningsContainer: {
        marginHorizontal: globalPaddingMargin.whole,
        marginTop: globalPaddingMargin.whole,
    },
    referralCodeMainContainer: {
        alignItems: 'flex-start',
        flexDirection: 'row',
        height: 48,
        backgroundColor: colors.white,
        borderRadius: 12,
        ...commonShadow(),
    },
    referCodeContainer: {
        flex: 1,
        justifyContent: 'center',
        height: '100%',
    },
    referralCode: {
        paddingHorizontal: globalPaddingMargin.left,
        color: colors.smokeyGrey,
    },
    copyButtonContainer: {
        paddingHorizontal: globalPaddingMargin.left,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    copyButton: {
        textTransform: 'uppercase',
    },
    bottomButton: {
        backgroundColor: colors.primary_2,
        marginTop: globalPaddingMargin.whole,
    },
    historyBottomButton: {
        backgroundColor: colors.primary_2,
        marginVertical: globalPaddingMargin.whole,
    },
})

export default connect((state) => ({
    userData: state.auth.userData,
}))(ReferAndEarn);
