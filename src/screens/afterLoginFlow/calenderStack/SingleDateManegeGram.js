import React, {
    useCallback,
    useEffect,
    useMemo,
    useRef,
    useState,
} from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    SectionList,
} from 'react-native';
import { connect, useDispatch } from 'react-redux';
import _ from 'lodash';
import images from '../../../assets/images/images';
import CustomButton from '../../../components/custom/CustomButton';
import CustomFullHorizontalCalender from '../../../components/custom/CustomFullHorizontalCalender';
import CustomHeader from '../../../components/custom/CustomHeader';
import CustomIcon, { IconType } from '../../../components/custom/CustomIcon';
import CustomLoader from '../../../components/custom/CustomLoader';
import CustomProductCard from '../../../components/custom/CustomProductCard';
import CustomText from '../../../components/custom/CustomText';
import colors from '../../../constants/colors';
import {
    calenderDayType,
    globalPaddingMargin,
    isRTL,
} from '../../../constants/constants';
import SelectGramsView from '../../../components/selectGramsView/SelectGramsView';
import {
    getHitSlop,
    getHorizontalCalenderMarkedDates,
    getSingleSubscriptionData,
    getValidDate,
} from '../../../helpers/CommonHelpers';
import i18n from '../../../i18n/i18n';
import { navigateGoBack } from '../../../navigation/navigation';
import * as SubscriptionActions from '../../../redux/actions/SubscriptionActions';
import CustomAlertBox from '../../../components/custom/CustomAlertBox';
import { getUserInbodySheetAction } from '../../../redux/actions/AuthActions';
import ChangeMyHealthDataModal from './ChangeMyHealthDataModal';
import { isIOS, hp, wp } from "../../../helpers/screenHelper";
import { fontsFamily } from '../../../assets/fonts/fonts';
import { commonShadow } from './../../../styles/CommonStyles';

const SingleDateManegeGram = props => {
    const dispatch = useDispatch();
    const customAlertBoxRef = useRef(null);
    const [loading, setLoading] = useState(false);
    const [wholeScreenLoading, setWholeScreenLoading] = useState(true);
    const [subscriptionData, setSubscriptionData] = useState(null);
    useEffect(() => {
        dispatch(getUserInbodySheetAction());
    }, [dispatch]);

    useEffect(() => {
        setTimeout(() => setWholeScreenLoading(false), 1500);
    }, [props?.subscriptionDetails?.data]);

    useEffect(() => {
        let singleSubscriptionData = getSingleSubscriptionData(
            props?.subscriptionDetails?.data,
            props?.calenderSelectedDate,
        );
        setSubscriptionData(singleSubscriptionData);
        dispatch(
            SubscriptionActions.getSelectedMealsByDateAction(
                props?.calenderSelectedDate,
            ),
        );
    }, [props?.calenderSelectedDate]);

    const headerCenterComponent = useMemo(
        () => (
            <CustomText
                fontProps={['ff-roboto-bold', 'fs-medium']}
                color={colors.primary_2}>
                {i18n.t('selectGrams')}
            </CustomText>
        ),
        [],
    );

    const onPausedDayPress = useCallback(() => {
        dispatch(
            SubscriptionActions.pausedPackageAction(props?.calenderSelectedDate),
        );
    }, [props?.calenderSelectedDate, dispatch]);

    const onResumeDayPress = useCallback(() => {
        dispatch(
            SubscriptionActions.resumePackageAction(props?.calenderSelectedDate),
        );
    }, [props?.calenderSelectedDate, dispatch]);

    const onDayPress = useCallback(date => {
        dispatch(
            SubscriptionActions.setCalenderSelectedDateAction(
                new Date(getValidDate(date, 'YYYY-MM-DD 00:00:00')),
            ),
        );
    }, []);

    const markedDates = useMemo(() => {
        const markDates = getHorizontalCalenderMarkedDates(props?.myCalender?.data);
        return markDates;
    }, [props?.myCalender?.data]);

    const renderCalender = useMemo(
        () => (
            <CustomFullHorizontalCalender
                minDate={props?.calenderRange?.minDate}
                maxDate={props?.calenderRange?.maxDate}
                selectedCurrentDate={props?.calenderSelectedDate ?? null}
                onDayPress={onDayPress}
                markedDates={markedDates}
            />
        ),
        [
            props?.calenderRange?.minDate,
            props?.calenderRange?.maxDate,
            props?.calenderSelectedDate,
            onDayPress,
            markedDates,
        ],
    );

    // No Meals Selected Day
    const renderNoMealsSelctedDay = useMemo(
        () => (
            <View style={styles.selectDishbottomButtonsContainer}>
                <View style={styles.selectedMealTopContainer}>
                    <TouchableOpacity
                        onPress={onPausedDayPress}
                        hitSlop={getHitSlop(20)}
                        style={styles.selectedDayPausedThisDayContainer}>
                        <CustomText
                            fontProps={['fs-small', 'ff-roboto-medium']}
                            color={colors.smokeyGrey}>
                            {i18n.t('pause_this_day')}
                        </CustomText>
                        <CustomIcon
                            size={20}
                            name="pause-circle"
                            type={IconType.Feather}
                            color={colors.beanRed}
                            customStyle={styles.pausedIcon}
                        />
                    </TouchableOpacity>
                    <CustomText fontProps={['fs-small']} color={colors.smokeyGrey}>
                        {i18n.t('selected_meals_for_day')}
                    </CustomText>
                </View>

                {/* Meals Listing By Type */}
            </View>
        ),
        [onPausedDayPress],
    );

    // No Plan Exist Day
    const renderNoPlanExistDay = useMemo(
        () => (
            <View style={styles.bottomButtonsContainer}>
                <CustomIcon
                    type={IconType.MaterialIcons}
                    name="event-note"
                    size={40}
                    customStyle={{ marginBottom: 10 }}
                    color={colors.primary_3}
                />
                <CustomText fontProps={['fs-medium']}>
                    {i18n.t('no_plan_exist')}
                </CustomText>
            </View>
        ),
        [],
    );

    const renderTimeGoesDayMeals = useCallback(
        ({ item }) => (
            <View style={{ paddingHorizontal: globalPaddingMargin.whole }}>
                <CustomProductCard productData={item} />
            </View>
        ),
        [],
    );

    const selectedMealsByDate = useMemo(() => {
        const mealsDataWithCategory = props?.selectedMealsByDate?.data?.filter(
            item => item?.meals_data?.length > 0,
        );
        return mealsDataWithCategory?.length > 0
            ? mealsDataWithCategory?.map(item => ({
                title: item?.meal_name ?? '',
                data: item?.meals_data ?? [],
            }))
            : [];
    }, [props?.selectedMealsByDate?.data]);

    const timeGoesSectionListHeader = () => (
        <View style={{ alignItems: 'center', marginVertical: 10 }}>
            <CustomIcon
                type={IconType.MaterialCommunityIcons}
                name="clock-time-three-outline"
                size={40}
                customStyle={{ marginBottom: 10 }}
                color={colors.primary_3}
            />
            <CustomText fontProps={['fs-medium']}>
                {i18n.t('time_goes_for_selection_for_this_date')}
            </CustomText>
        </View>
    );

    const renderSectionHeader = useCallback(({ section: { title } }) => {
        return (
            <View style={styles.sectionHeader}>
                <CustomText
                    fontProps={['fs-small', 'ff-roboto-medium']}
                    color={colors.primary_2}>
                    {title}
                </CustomText>
            </View>
        );
    }, []);

    // Time Goes For Selection
    const renderTimeGoesForSelectionDay = useMemo(
        () => (
            <View>
                <SectionList
                    ListHeaderComponent={timeGoesSectionListHeader}
                    bounces={false}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    sections={selectedMealsByDate}
                    keyExtractor={(item, index) => item + index}
                    renderItem={renderTimeGoesDayMeals}
                    renderSectionHeader={renderSectionHeader}
                />
            </View>
        ),
        [
            timeGoesSectionListHeader,
            selectedMealsByDate,
            renderTimeGoesDayMeals,
            renderSectionHeader,
        ],
    );

    // Selected Day
    const renderSelectedDay = useMemo(
        () => (
            <View style={styles.bottomButtonsContainer}>
                <View style={styles.selectedMealTopContainer}>
                    <TouchableOpacity
                        onPress={onPausedDayPress}
                        hitSlop={getHitSlop(20)}
                        style={styles.selectedDayPausedThisDayContainer}>
                        <CustomIcon
                            size={20}
                            name="pause-circle"
                            type={IconType.Feather}
                            color={colors.primary_2}
                            customStyle={styles.pausedIcon}
                        />
                        <CustomText
                            fontProps={['fs-small', 'ff-roboto-medium']}
                            color={colors.smokeyGrey}>
                            {i18n.t('pause_this_day')}
                        </CustomText>
                    </TouchableOpacity>
                    <CustomText fontProps={['fs-small']} color={colors.smokeyGrey}>
                        {i18n.t('selected_meals_for_day')}
                    </CustomText>
                </View>

                {/* Meals Listing By Type */}
            </View>
        ),
        [onPausedDayPress],
    );

    // Paused Day
    const renderPausedDay = useMemo(
        () => (
            <View style={styles.bottomButtonsContainer}>
                <CustomText fontProps={['fs-medium']}>
                    {i18n.t('paused_day')}
                </CustomText>
                <CustomText
                    fontProps={['fs-small', 'ff-roboto-light']}
                    customStyle={styles.bottomSubTitle}>
                    {i18n.t('paused_day_subtitle')}
                </CustomText>
                <CustomButton
                    backgroundColor={colors.primary_2}
                    icon={images.resumeIcon}
                    onPress={onResumeDayPress}
                    buttonLabel={i18n.t('resume')}
                />
            </View>
        ),
        [onResumeDayPress],
    );

    // Off Day
    const renderOffDay = useMemo(
        () => (
            <View style={styles.bottomButtonsContainer}>
                <CustomText fontProps={['fs-medium']}>
                    {i18n.t('this_is_off_day')}
                </CustomText>
            </View>
        ),
        [],
    );

    // Select Date From Above Calender
    const renderSelectDateFromAboveCalender = useMemo(
        () => (
            <View style={styles.bottomButtonsContainer}>
                <CustomText fontProps={['fs-medium']}>
                    {i18n.t('select_date')}
                </CustomText>
            </View>
        ),
        [],
    );

    const renderButtonsAndTitles = useMemo(() => {
        if (!props?.calenderSelectedDate) return renderSelectDateFromAboveCalender;
        if (props?.pausedOrResumePackage?.loading) return <CustomLoader />;
        switch (props?.calenderSelectedDayType) {
            case calenderDayType.noPlanExistDay.key:
                return renderNoPlanExistDay;
            case calenderDayType.noMealSelectedDay.key:
                return renderNoMealsSelctedDay;
            case calenderDayType.timeGoesForSelection.key:
                return renderTimeGoesForSelectionDay;
            case calenderDayType.selectedDay.key:
                return renderSelectedDay;
            case calenderDayType.pausedDay.key:
                return renderPausedDay;
            case calenderDayType.offDay:
                return renderOffDay;
            default:
                return null;
        }
    }, [
        props?.calenderSelectedDate,
        renderSelectDateFromAboveCalender,
        props?.pausedOrResumePackage?.loading,
        props?.calenderSelectedDayType,
        renderNoPlanExistDay,
        renderTimeGoesForSelectionDay,
        renderNoMealsSelctedDay,
        renderSelectedDay,
        renderPausedDay,
        renderOffDay,
    ]);

    const onBackPress = useCallback(() => {
        navigateGoBack();
    }, []);

    const headerLeftComponent = useMemo(
        () => (
            <CustomIcon
                onPress={onBackPress}
                type={IconType.MaterialIcons}
                name={isRTL ? 'arrow-forward-ios' : 'arrow-back-ios'}
                size={16}
                color={colors.primary_2}
                customStyle={headerStyles.backButton}
            />
        ),
        [onBackPress],
    );



    const headerRightComponent = useMemo(
        () => (
            <TouchableOpacity
                disabled
                onPress={() => {}}
                hitSlop={getHitSlop(20)}>
                {[
                    calenderDayType?.noMealSelectedDay?.key,
                    calenderDayType?.selectedDay?.key,
                ]?.includes(props?.calenderSelectedDayType) && (
                        <CustomText color={colors.primary_2} fontProps={['fs-medium']}>
                            {/* {i18n.t('save')} */}
                        </CustomText>
                    )}
            </TouchableOpacity>
        ),
        [
            props?.calenderSelectedDayType,
            subscriptionData?.is_custom_grams,
        ],
    );

    // Render Header
    const renderHeader = useMemo(
        () => (
            <CustomHeader
                leftComponent={headerLeftComponent}
                centerComponent={headerCenterComponent}
                rightComponent={headerRightComponent}
            />
        ),
        [headerCenterComponent, headerLeftComponent, headerRightComponent],
    );

    const renderCustomGrams = useMemo(
        () => <SelectGramsView subscriptionData={subscriptionData} />,
        [subscriptionData],
    );

    return (
        <View style={styles.mainContainer}>
            {/* Header */}
            {renderHeader}

            {/* Render Loader */}
            {wholeScreenLoading && <CustomLoader wholeScreen={true} />}
            <CustomAlertBox ref={customAlertBoxRef} />

            {/* Calender */}
            {renderCalender}
            {loading ? (
                <View style={{ marginTop: 20 }}>
                    <CustomLoader />
                </View>
            ) : (
                <>
                    {renderButtonsAndTitles}
                    {
                        [
                            calenderDayType.noMealSelectedDay.key,
                            calenderDayType.selectedDay.key,
                        ].includes(props?.calenderSelectedDayType)
                        &&
                        renderCustomGrams
                    }
                </>
            )}
        </View>
    );
};

const headerStyles = StyleSheet.create({
    backButton: {
        marginRight: 15,
    },
});

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: colors.appBackground,
    },
    bottomButtonsContainer: {
        padding: globalPaddingMargin.whole,
        alignItems: 'center',
    },
    selectDishbottomButtonsContainer: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        alignItems: 'center',
    },
    bottomSubTitle: {
        lineHeight: 25,
        marginVertical: 20,
        textAlign: 'center',
        width: '80%',
    },
    contentContainer: {
        flex: 1,
    },
    selectedDayPausedThisDayContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    pausedIcon: {
        marginLeft: 10,
    },
    selectedMealTopContainer: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    sectionHeader: {
        backgroundColor: colors.appBackground,
        height: 35,
        justifyContent: 'center',
        paddingHorizontal: globalPaddingMargin.whole,
    },
    mealCategoriesFlatListContentContainer: {
        // alignItems: 'center',
    },
    foodTypesFlatList: {
        backgroundColor: colors.white,
        borderBottomWidth: 2,
        borderBottomColor: colors.ghostWhite,
    },
    singleFoodTypeContainer: {
        height: 65,
        borderBottomWidth: 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    singleFoodTypeText: {
        paddingHorizontal: 20,
    },
    noMealsText: {
        textAlign: 'center',
    },
    caleriesContainer: {
        flex: 1,
        marginTop: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
    },
    subTitleStyle: {
        marginLeft: 5,
    },
    dropDownIconStyle: {
        flex: 1,
        height: wp(5),
        width: wp(3.5),
        marginHorizontal: wp(4),
        alignSelf: 'center',
        tintColor: colors.black,
    },
    dropDownStyle: {
        width: wp(35),
        borderRadius: hp(1),
        position: 'absolute',
        marginTop: isIOS ? 0 : hp(5),
        left: wp(55),
    },
    buttonStyle: {
        width: 0,
        height: 0,
        marginHorizontal: wp(2),
    },
    rowTextStyle: {
        fontFamily: fontsFamily.roboto.regular,
        fontSize: 16,
    },
    dropdown3RowChildStyle: {
        alignItems: 'center',
    },
    dropdown3RowTxt: {
        textAlign: 'center',
        fontFamily: fontsFamily.roboto.regular,
        fontSize: 16,
        color: colors.lightBlack,
    },
    ListHeaderContainer: {
        // textAlign: 'center',
        // fontFamily: fontsFamily.roboto.regular,
        // fontSize: 16,
        marginHorizontal: 24,
        marginVertical: 10,
        backgroundColor: colors.white,
        marginBottom: 15,
        flex: 1,

        // minHeight: 110,
        // width: '100%',
        // flexDirection: 'row',
        borderRadius: 12,
        // overflow: 'hidden',
        ...commonShadow(),
    },
});

export default connect(state => ({
    userData: state.auth.userData,
    myCalender: state.subscription.myCalender,
    subscriptionDetails: state.subscription.subscriptionDetails,
    pausedOrResumePackage: state.subscription.pausedOrResumePackage,
    calenderRange: state.subscription.calenderRange,
    calenderSelectedDate: state.subscription.calenderSelectedDate,
    calenderSelectedDayType: state.subscription.calenderSelectedDayType,
    selectedMealsByDate: state.subscription.selectedMealsByDate,
}))(SingleDateManegeGram);
