import React, {
    useCallback, useEffect, useMemo, useState,
} from 'react';
import {
    View, StyleSheet, TouchableOpacity, SectionList,
} from 'react-native';
import { useIsFocused } from '@react-navigation/native';
import { connect, useDispatch } from 'react-redux';
import images from '../../../assets/images/images';
import CustomButton from '../../../components/custom/CustomButton';
import CustomFullCalender from '../../../components/custom/CustomFullCalender';
import CustomHeader from '../../../components/custom/CustomHeader';
import CustomIcon, { IconType } from '../../../components/custom/CustomIcon';
import CustomProductCard from '../../../components/custom/CustomProductCard';
import CustomText from '../../../components/custom/CustomText'
import colors from '../../../constants/colors';
import {
    calenderDayIndicator, calenderDayType, EVENT_OFF_DAY, globalPaddingMargin,
} from '../../../constants/constants';
import { getCalenderMarkedDates, getHitSlop, getSingleSubscriptionData, getValidDate } from '../../../helpers/CommonHelpers';
import { navigateTo } from '../../../navigation/navigation';
import screens from '../../../navigation/screens';
import i18n from '../../../i18n/i18n';
import * as SubscriptionActions from '../../../redux/actions/SubscriptionActions';
import CustomLoader from '../../../components/custom/CustomLoader';
import SelectGramsView from '../../../components/selectGramsView/SelectGramsView';

const CalenderDashboard = (props) => {
    const isFocused = useIsFocused();
    const dispatch = useDispatch()
    const [subscriptionData, setSubscriptionData] = useState(null)

    useEffect(() => {
        if (isFocused) {
            dispatch(SubscriptionActions.getSubscriptionDetailsAction()).then(() => {
                dispatch(SubscriptionActions.getMyCalenderDataAction()).then(() => {
                    dispatch(SubscriptionActions.getSelectedMealsByDateAction(props?.calenderSelectedDate || new Date()))
                })
            });
        }
    }, [isFocused])

    const headerCenterComponent = useMemo(() => (
        <CustomText fontProps={['ff-roboto-bold', 'fs-medium']} color={colors.primary_2}>{i18n.t('meal_calender')}</CustomText>
    ), [])

    const onPausedDayPress = useCallback(() => {
        dispatch(SubscriptionActions.pausedPackageAction(props?.calenderSelectedDate));
    }, [props?.calenderSelectedDate, dispatch]);

    const onResumeDayPress = useCallback(() => {
        dispatch(SubscriptionActions.resumePackageAction(props?.calenderSelectedDate));
    }, [props?.calenderSelectedDate, dispatch]);

    const onDayPress = useCallback((dateProps) => {
        dispatch(SubscriptionActions.setCalenderSelectedDateAction(dateProps?.dateString))
        const currentSubscriptionData = getSingleSubscriptionData(props?.subscriptionDetails?.data, dateProps?.dateString)
        setSubscriptionData(currentSubscriptionData)
        if (currentSubscriptionData?.is_custom_grams == '0') {
            dispatch(SubscriptionActions.getSelectedMealsByDateAction(dateProps?.dateString))
        }
    }, [dispatch, props?.subscriptionDetails?.data])

    const markedDates = useMemo(() => {
        const markDates = getCalenderMarkedDates(props?.calenderSelectedDate, props?.myCalender?.data)
        return markDates;
    }, [props?.calenderSelectedDate, props?.myCalender?.data])

    const renderCalender = useMemo(() => (
        <CustomFullCalender
            minDate={props?.calenderRange?.minDate}
            maxDate={props?.calenderRange?.maxDate}
            disabledDaysIndexes={[EVENT_OFF_DAY]}
            selectedCurrentDate={props?.calenderSelectedDate ?? null}
            onDayPress={onDayPress}
            markedDates={markedDates}
        />
    ), [props?.calenderSelectedDate, markedDates, onDayPress, props?.calenderRange])

    const onSelectMealButtonPress = useCallback(() => {
        navigateTo(screens.singleDateManegeMeal)
    }, [])
    const onSelectGramButtonPress = useCallback(() => {
        navigateTo(screens.singleDateManegeGram)
    }, [])

    // No Meals Selected Day
    const renderNoMealsSelctedDay = useMemo(() => (
        <View style={styles.bottomButtonsContainer}>
            {subscriptionData?.is_custom_grams == '0' && (
                <>
                    <CustomText fontProps={['fs-medium']}>{i18n.t('no_orders_yet')}</CustomText>
                    <CustomText fontProps={['fs-small', 'ff-roboto-light']} customStyle={styles.bottomSubTitle}>
                        {i18n.t('no_orders_subtitle')}
                    </CustomText>

                </>
            )}
            <CustomButton onPress={onSelectMealButtonPress} buttonLabel={i18n.t('select_meal')} />
            {subscriptionData?.is_custom_grams == 1 && <CustomButton onPress={onSelectGramButtonPress} customStyle={{ marginTop: 10 }} buttonLabel={i18n.t('changeMacrosGrams')} />}
            <TouchableOpacity style={styles.pausedThisDayContainer} onPress={onPausedDayPress}>
                <CustomIcon
                    size={20}
                    name="pause-circle"
                    type={IconType.Feather}
                    color={colors.primary_2}
                    customStyle={styles.pausedIcon}
                />
                <CustomText fontProps={['fs-medium', 'ff-roboto-medium']} color={colors.smokeyGrey}>{i18n.t('pause_this_day')}</CustomText>
            </TouchableOpacity>
        </View>
    ), [onPausedDayPress, onSelectMealButtonPress, onSelectGramButtonPress, subscriptionData])

    // No Plan Exist Day
    const renderNoPlanExistDay = useMemo(() => (
        <View style={styles.bottomButtonsContainer}>
            <CustomIcon type={IconType.MaterialIcons} name="event-note" size={40} customStyle={{ marginBottom: 10 }} color={colors.primary_3} />
            <CustomText fontProps={['fs-medium']}>{i18n.t('no_plan_exist')}</CustomText>
        </View>
    ), [])

    // Time Goes For Selection
    const renderTimeGoesForSelectionDay = useMemo(() => (
        <View style={styles.bottomButtonsContainer}>
            <CustomIcon type={IconType.MaterialCommunityIcons} name="clock-time-three-outline" size={40} customStyle={{ marginBottom: 10 }} color={colors.primary_3} />
            <CustomText fontProps={['fs-medium']}>{i18n.t('time_goes_for_selection_for_this_date')}</CustomText>
        </View>
    ), [])

    // Selected Day
    const renderSelectedDay = useMemo(() => (
        <View style={styles.bottomButtonsContainer}>
            <View style={styles.selectedMealTopContainer}>
                <TouchableOpacity onPress={onPausedDayPress} hitSlop={getHitSlop(20)} style={styles.selectedDayPausedThisDayContainer}>
                    <CustomIcon
                        size={20}
                        name="pause-circle"
                        type={IconType.Feather}
                        color={colors.primary_2}
                        customStyle={styles.pausedIcon}
                    />
                    <CustomText
                        fontProps={['fs-small', 'ff-roboto-medium']}
                        color={colors.smokeyGrey}>
                        {i18n.t('pause_this_day')}
                    </CustomText>
                </TouchableOpacity>
                <CustomText
                    fontProps={['fs-small']}
                    color={colors.smokeyGrey}>
                    {i18n.t('selected_meals_for_day')}
                </CustomText>
            </View>

            <CustomButton customStyle={{ marginVertical: 10 }} onPress={onSelectMealButtonPress} buttonLabel={i18n.t('change_meal')} />

        </View>
    ), [onPausedDayPress, onSelectMealButtonPress])

    // Paused Day
    const renderPausedDay = useMemo(() => (
        <View style={styles.bottomButtonsContainer}>
            <CustomText fontProps={['fs-medium']}>{i18n.t('paused_day')}</CustomText>
            <CustomText fontProps={['fs-small', 'ff-roboto-light']} customStyle={styles.bottomSubTitle}>
                {i18n.t('paused_day_subtitle')}
            </CustomText>
            <CustomButton
                backgroundColor={colors.primary_2}
                icon={images.resumeIcon}
                onPress={onResumeDayPress}
                buttonLabel={i18n.t('resume')}
            />
        </View>
    ), [onResumeDayPress])

    // Off Day
    const renderOffDay = useMemo(() => (
        <View style={styles.bottomButtonsContainer}>
            <CustomText fontProps={['fs-medium']}>{i18n.t('this_is_off_day')}</CustomText>
        </View>
    ), [])

    // Select Date From Above Calender
    const renderSelectDateFromAboveCalender = useMemo(() => (
        <View style={styles.bottomButtonsContainer}>
            <CustomText fontProps={['fs-medium']}>{i18n.t('select_date')}</CustomText>
        </View>
    ), [])

    const renderButtonsAndTitles = useMemo(() => {
        if (!props?.calenderSelectedDate) return renderSelectDateFromAboveCalender;
        if (props?.pausedOrResumePackage?.loading || props?.selectedMealsByDate?.loading) return <CustomLoader />
        switch (props?.calenderSelectedDayType) {
            case calenderDayType.noPlanExistDay.key: return renderNoPlanExistDay
            case calenderDayType.noMealSelectedDay.key: return renderNoMealsSelctedDay
            case calenderDayType.timeGoesForSelection.key: return renderTimeGoesForSelectionDay
            case calenderDayType.selectedDay.key: return renderSelectedDay
            case calenderDayType.pausedDay.key: return renderPausedDay
            case calenderDayType.offDay.key: return renderOffDay
            default: return null
        }
    }, [props?.calenderSelectedDate, props?.pausedOrResumePackage?.loading, props?.selectedMealsByDate?.loading, props?.calenderSelectedDayType, renderSelectDateFromAboveCalender, renderNoPlanExistDay, renderNoMealsSelctedDay, renderTimeGoesForSelectionDay, renderSelectedDay, renderPausedDay, renderOffDay])

    const renderCalenderDayIndicator = useMemo(() => (
        <View style={styles.calenderIndicatorContainer}>
            {calenderDayIndicator.map((item, index) => (
                <View key={`calenderDayIndicator${index}`} style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <View style={{
                        backgroundColor: item?.color, height: 20, width: 20, marginHorizontal: 10, borderRadius: 50,
                    }} />
                    <CustomText>{item.name}</CustomText>
                </View>
            ))}
        </View>
    ), [])

    const renderSelectedDate = useMemo(() => (
        <CustomText
            color={colors.primary_3}
            fontProps={['fs-medium', 'ff-roboto-medium']}
            customStyle={{ textAlign: 'center', marginVertical: 10 }}>
            {props?.calenderSelectedDate
                ? getValidDate(props?.calenderSelectedDate, 'DD MMM YYYY')
                : null}
        </CustomText>
    ), [props?.calenderSelectedDate])

    const renderCustomGrams = useMemo(() => (
        <SelectGramsView subscriptionData={subscriptionData} readOnly={true} />
    ), [subscriptionData])

    const ListHeaderComponent = useMemo(() => (
        <View>
            {/* Calender */}
            {renderCalender}

            {/* Calender Days Indicator */}
            {renderCalenderDayIndicator}

            {/* Render Selected Date */}
            {renderSelectedDate}

            {/* Bottom Titles and Buttons */}
            {renderButtonsAndTitles}

            {subscriptionData?.is_custom_grams == '1' && (
                <>
                    {[calenderDayType.noMealSelectedDay.key].includes(props?.calenderSelectedDayType) && renderCustomGrams}
                </>
            )}
        </View>
    ), [props?.calenderSelectedDayType, renderButtonsAndTitles, renderCalender, renderCalenderDayIndicator, renderCustomGrams, renderSelectedDate, subscriptionData])

    const renderSectionHeader = useCallback(({ section: { title } }) => (
        <View style={styles.sectionHeader}>
            <CustomText
                fontProps={['fs-small', 'ff-roboto-medium']}
                color={colors.primary_2}>
                {title}
            </CustomText>
        </View>
    ), [])

    const renderSelectedDayMeals = useCallback(({ item }) => (
        <View style={{ paddingHorizontal: globalPaddingMargin.whole }}>
            <CustomProductCard productData={item} />
        </View>
    ), [])

    const selectedMealsByDate = useMemo(() => {
        const mealsDataWithCategory = props?.selectedMealsByDate?.data?.filter((item) => item?.meals_data?.length > 0);
        return (mealsDataWithCategory?.length > 0
            ? mealsDataWithCategory?.map((item) => ({ title: item?.meal_name ?? '', data: item?.meals_data ?? [] }))
            : []
        )
    }, [props?.selectedMealsByDate?.data])

    return (
        <View style={styles.mainContainer}>
            {/* Header */}
            <CustomHeader
                centerComponent={headerCenterComponent}
            />
            <SectionList
                ListHeaderComponent={ListHeaderComponent}
                bounces={false}
                style={styles.contentContainer}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                sections={[calenderDayType.selectedDay.key, calenderDayType.timeGoesForSelection.key].includes(props?.calenderSelectedDayType) ? selectedMealsByDate : []}
                keyExtractor={(item, index) => item + index}
                renderItem={renderSelectedDayMeals}
                renderSectionHeader={renderSectionHeader}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: colors.appBackground,
    },
    bottomButtonsContainer: {
        padding: 10,
        alignItems: 'center',
    },
    bottomSubTitle: {
        lineHeight: 25,
        marginVertical: 20,
        textAlign: 'center',
        width: '80%',
    },
    contentContainer: {
        flex: 1,
    },
    pausedThisDayContainer: {
        width: '100%',
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 10,
    },
    selectedDayPausedThisDayContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    pausedIcon: {
        marginRight: 10,
    },
    selectedMealTopContainer: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    sectionHeader: {
        backgroundColor: colors.appBackground,
        height: 35,
        justifyContent: 'center',
        paddingHorizontal: globalPaddingMargin.whole,
    },
    calenderIndicatorContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 10,
        justifyContent: 'center',
    },
})

export default connect((state) => ({
    myCalender: state.subscription.myCalender,
    subscriptionDetails: state.subscription.subscriptionDetails,
    pausedOrResumePackage: state.subscription.pausedOrResumePackage,
    calenderRange: state.subscription.calenderRange,
    calenderSelectedDate: state.subscription.calenderSelectedDate,
    calenderSelectedDayType: state.subscription.calenderSelectedDayType,
    selectedMealsByDate: state.subscription.selectedMealsByDate,
}))(CalenderDashboard);
