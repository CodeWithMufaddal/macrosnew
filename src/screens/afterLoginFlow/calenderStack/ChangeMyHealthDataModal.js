import React, {
    Fragment, useCallback, useEffect, useMemo, useState,
} from 'react';
import {
    View, StyleSheet,
} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useDispatch, connect } from 'react-redux';
import CustomButton from '../../../components/custom/CustomButton';
import CustomIcon from '../../../components/custom/CustomIcon';
import CustomKeyboardAvoidingView from '../../../components/custom/CustomKeyboardAvoidingView';
import CustomModal from '../../../components/custom/CustomModal'
import CustomSlider from '../../../components/custom/CustomSlider';
import CustomText from '../../../components/custom/CustomText';
import DiatCitationModal from '../../../components/diatCitationModal/DiatCitationModal';
import colors from '../../../constants/colors';
import { PLAN_TYPE } from '../../../constants/constants';
import { getHitSlop, getPlanType } from '../../../helpers/CommonHelpers';
import { wp } from '../../../helpers/screenHelper';
import useSlider from '../../../hooks/useSlider';
import i18n from '../../../i18n/i18n';
import * as AuthActions from '../../../redux/actions/AuthActions';
import * as Validation from '../../../validation/Validation';

const ChangeMyHealthDataModal = ({
    showModal, onHide, ...props
}) => {
    const dispatch = useDispatch()
    const [proteins, proteinsScrollEnabled, bindProteins, setProteins] = useSlider(props?.userData?.proteins ?? 0);
    const [carbs, carbsScrollEnabled, bindCarbs, setCarbs] = useSlider(props?.userData?.carbs ?? 0);
    const [fats, fatsScrollEnabled, bindFats, setFats] = useSlider(props?.userData?.fats ?? 0);
    const [calories, caloriesScrollEnabled, bindCalories, setCalories] = useSlider(props?.userData?.calories ?? 0);
    const [showDiatCitationModal, setShowDiatCitationModal] = useState(false);

    useEffect(() => {
        if (calories > 0) {
            if (proteins > 0) setProteins(0);
            if (carbs > 0) setCarbs(0);
            if (fats > 0) setFats(0);
        }
    }, [calories])
    useEffect(() => {
        if (proteins > 0 || carbs > 0 || fats > 0) if (calories > 0) setCalories(0);
    }, [proteins, carbs, fats])

    const resetData = useCallback(() => {
        setProteins(props?.userData?.proteins)
        setCarbs(props?.userData?.carbs)
        setFats(props?.userData?.fats)
        setCalories(props?.userData?.calories)
    }, [props?.userData?.calories, props?.userData?.carbs, props?.userData?.fats, props?.userData?.proteins, setCalories, setCarbs, setFats, setProteins])

    const onCancelPress = useCallback(() => {
        resetData();
        onHide();
    }, [onHide, resetData])

    const onSavePress = useCallback(() => {
        const data = {
            weight: props?.userData?.weight, proteins, carbs, fats, calories,
        }
        if (Validation.saveMyHealthDataValidation(data)) {
            dispatch(AuthActions.saveMyHealthDataAction(data)).then(() => {
                onHide('success');
            })
        }
    }, [calories, carbs, dispatch, fats, onHide, props?.userData?.weight, proteins]);

    const currentPlanType = useMemo(() => {
        const planType = getPlanType(props?.calenderSelectedDate, props?.subscriptionDetails?.data);
        return planType;
    }, [props?.calenderSelectedDate, props?.subscriptionDetails?.data])

    const onIButtonPress = useCallback(() => {
        setShowDiatCitationModal(true);
    }, [])

    const maxProtiens = props?.goal_name == 'Lose Weight' ? 120 : props?.goal_name == 'Maintain Weight' ? 150 : 200
    const maxCarbs = props?.goal_name == 'Lose Weight' ? 120 : props?.goal_name == 'Maintain Weight' ? 150 : 300
    const measurementIntervals = props?.goal_name == 'Lose Weight' ? 20 : 50

    return (
        <CustomModal
            animationIn={'fadeIn'}
            animationOut={'fadeOut'}
            onHide={onHide}
            isVisible={showModal}
        >
            <View style={styles.mainContainer}>
                <DiatCitationModal
                    showDiatCitationModal={showDiatCitationModal}
                    setShowDiatCitationModal={setShowDiatCitationModal}
                />
                <View style={styles.contentContainer}>
                    <View style={styles.restContainer}>
                        {/* Heading */}
                        <View style={styles.headerContainer}>
                            <CustomIcon onPress={onIButtonPress} name="infocirlce" color={colors.charcolGrey} size={20} />
                            <CustomText fontProps={['ff-roboto-bold', 'fs-medium']} color={colors.primary_3}>{i18n.t('adjustMacros')}</CustomText>
                            <CustomIcon name="close" onPress={onCancelPress} />
                        </View>
                        <CustomKeyboardAvoidingView
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false}
                            scrollEnabled={(proteinsScrollEnabled === carbsScrollEnabled === fatsScrollEnabled === caloriesScrollEnabled)}
                            style={styles.scrollView}
                            extraHeight={200}>

                            {/* calories */}
                            <View style={styles.sliderContainer}>
                                <CustomText
                                    fontProps={['ff-roboto-medium', 'fs-medium']}
                                    color={colors.primary_2}>
                                    {i18n.t('calories')} &nbsp; : &nbsp;
                                    <CustomText
                                        fontProps={['ff-roboto-medium', 'fs-medium']}
                                        color={colors.primary_3}>
                                        {calories}
                                    </CustomText>
                                </CustomText>
                                <CustomSlider {...bindCalories} min={0} max={4000} measurementInterval={800} />
                            </View>

                            <CustomText
                                color={colors.smokeyGrey}
                                fontProps={['ff-roboto-bold']}
                                customStyle={styles.orText}>
                                - OR -
                            </CustomText>

                            <View style={styles.sliderContainer}>
                                <CustomText
                                    fontProps={['ff-roboto-medium', 'fs-medium']}
                                    color={colors.primary_2}>
                                    {i18n.t('proteins')} &nbsp; : &nbsp;
                                    <CustomText
                                        fontProps={['ff-roboto-medium', 'fs-medium']}
                                        color={colors.primary_3}>
                                        {proteins}
                                    </CustomText>
                                </CustomText>
                            </View>
                            <CustomSlider measurementInterval={measurementIntervals} max={maxProtiens} {...bindProteins} />

                            {/* Carbs */}
                            <View style={styles.sliderContainer}>
                                <CustomText
                                    fontProps={['ff-roboto-medium', 'fs-medium']}
                                    color={colors.primary_2}>
                                    {i18n.t('carbs')} &nbsp; : &nbsp;
                                    <CustomText
                                        fontProps={['ff-roboto-medium', 'fs-medium']}
                                        color={colors.primary_3}>
                                        {carbs}
                                    </CustomText>
                                </CustomText>
                            </View>
                            <CustomSlider measurementInterval={measurementIntervals} max={maxCarbs} {...bindCarbs} />

                            {/* Fats */}
                            {currentPlanType != PLAN_TYPE.CUSTOM_GRAMS && (
                                <Fragment>
                                    <View style={styles.sliderContainer}>
                                        <CustomText
                                            fontProps={['ff-roboto-medium', 'fs-medium']}
                                            color={colors.primary_2}>
                                            {i18n.t('fats')} &nbsp; : &nbsp;
                                            <CustomText
                                                fontProps={['ff-roboto-medium', 'fs-medium']}
                                                color={colors.primary_3}>
                                                {fats}
                                            </CustomText>
                                        </CustomText>
                                    </View>
                                    <CustomSlider measurementInterval={40} max={160} {...bindFats} />
                                </Fragment>
                            )}
                        </CustomKeyboardAvoidingView>
                    </View>
                </View>
                <View style={styles.bottomButtonContainer}>
                    <CustomButton
                        activeOpacity={0.7}
                        disabled={props?.saveMyHealthData?.loading}
                        backgroundColor={colors.primary_3}
                        customStyle={styles.bottomButton}
                        buttonLabel={props?.saveMyHealthData?.loading ? i18n.t('saving') : i18n.t('save')}
                        onPress={onSavePress} />
                </View>
            </View>
        </CustomModal>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        paddingVertical: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    contentContainer: {
        flex: 1,
        width: wp(90),
        alignSelf: 'center',
        backgroundColor: colors.white,
        borderRadius: 15,
        overflow: 'hidden',
        paddingBottom: 10,
    },
    bottomButton: {
        width: '90%',
        marginTop: 10,
    },
    restContainer: {
        flex: 1,
    },
    bottomButtonContainer: {
        width: '100%',
    },
    headerContainer: {
        height: 50,
        width: wp(90),
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomColor: colors.smokeyGrey,
        borderBottomWidth: 1,
        flexDirection: 'row',
        paddingHorizontal: 20,

    },
    scrollView: {
    },
    sliderContainer: {
        paddingHorizontal: 20,
        paddingVertical: 10,
    },
    orText: {
        textAlign: 'center',
        marginVertical: 5,
    },
})

export default connect((state) => ({
    generateCoupon: state.users.generateCoupon,
    userData: state.auth.userData,
    saveMyHealthData: state.auth.saveMyHealthData,
    subscriptionDetails: state.subscription.subscriptionDetails,
    calenderSelectedDate: state.subscription.calenderSelectedDate,
}))(ChangeMyHealthDataModal);
