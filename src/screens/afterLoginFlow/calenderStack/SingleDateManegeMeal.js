import React, {
  Fragment,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  SectionList,
  Text,
} from 'react-native';
import { connect, useDispatch } from 'react-redux';
import _ from 'lodash';
import images, { imagesMode } from '../../../assets/images/images';
import CustomButton from '../../../components/custom/CustomButton';
import CustomFullHorizontalCalender from '../../../components/custom/CustomFullHorizontalCalender';
import CustomHeader from '../../../components/custom/CustomHeader';
import CustomIcon, { IconType } from '../../../components/custom/CustomIcon';
import CustomLoader from '../../../components/custom/CustomLoader';
import CustomProductCard from '../../../components/custom/CustomProductCard';
import CustomText from '../../../components/custom/CustomText';
import colors from '../../../constants/colors';
import {
  calenderDayType,
  EVENT_OFF_DAY,
  globalPaddingMargin,
  isRTL,
  PLAN_TYPE,
} from '../../../constants/constants';
import SelectGramsView from '../../../components/selectGramsView/SelectGramsView';
import {
  generateUniqueId,
  getHitSlop,
  getHorizontalCalenderMarkedDates,
  getPlanType,
  getSingleSubscriptionData,
  getValidDate,
  getWholePlanDuration,
  sendDataToReducer,
} from '../../../helpers/CommonHelpers';
import i18n from '../../../i18n/i18n';
import { navigateGoBack } from '../../../navigation/navigation';
import * as SubscriptionActions from '../../../redux/actions/SubscriptionActions';
import CustomToast from '../../../components/custom/CustomToast';
import CustomAlertBox from '../../../components/custom/CustomAlertBox';
import { getUserInbodySheetAction } from '../../../redux/actions/AuthActions';
import ChangeMyHealthDataModal from './ChangeMyHealthDataModal';
import { isIOS, hp, wp } from "../../../helpers/screenHelper";
import SelectDropdown from 'react-native-select-dropdown';
import { fontsFamily, fontsSize, getFontsSize } from '../../../assets/fonts/fonts';
import ReplaceCarbsModal from './ReplaceCarbsModal';
import CustomImage from '../../../components/custom/CustomImage';
import { commonShadow } from './../../../styles/CommonStyles';
import { CALCULATE_MACROS } from '../../../redux/types/ReduxTypes';

const SingleDateManegeMeal = props => {
  const dispatch = useDispatch();
  const mealsFlatListRef = useRef();
  const customAlertBoxRef = useRef(null);
  const dropDownRef = useRef();
  const obj = { name: 'All', id: null };
  const [mealsData, setMealsData] = useState();
  const [selectedMealCategory, setSelectedMealCategory] = useState(0);
  const [loading, setLoading] = useState(false);
  const [wholeScreenLoading, setWholeScreenLoading] = useState(true);
  const [showChangeHealthDataModal, setShowChangeHealthDataModal] =
    useState(false);
  const [subscriptionData, setSubscriptionData] = useState(null);
  const [selectedItem, setSelectedItem] = useState(obj);
  const [showMenu, setShownMenu] = useState(false);
  const [mealIndex, setMealIndex] = useState(0);
  const [substituteData, setSubstituteData] = useState([]);
  useEffect(() => {
    dispatch(getUserInbodySheetAction());
  }, [dispatch]);

  useEffect(() => {
    setTimeout(() => setWholeScreenLoading(false), 1500);
  }, [props?.subscriptionDetails?.data]);

  useEffect(() => {
    let singleSubscriptionData = getSingleSubscriptionData(
      props?.subscriptionDetails?.data,
      props?.calenderSelectedDate,
    );
    setSubscriptionData(singleSubscriptionData);
    dispatch(
      SubscriptionActions.getSelectedMealsByDateAction(
        props?.calenderSelectedDate,
      ),
    );
  }, [props?.calenderSelectedDate]);

  useEffect(() => {
    setLoading(true);
    dispatch(
      SubscriptionActions.getMealsByMealTypeIdAndDateAction(
        props?.calenderSelectedDate,
      ),
    ).then(res => {
      const data = res?.map((item, index) => {
        return { ...item, id: index + 1 };
      });

      setMealsData(data);
      sendDataToReducer(dispatch, CALCULATE_MACROS, data)
      setSelectedMealCategory(data?.[0]?.id);
      setLoading(false);
    });
  }, [props?.calenderSelectedDate]);

  useEffect(() => {
    mealsFlatListRef?.current?.scrollToOffset({ animated: true, offset: 0 });
  }, [selectedMealCategory, props?.calenderSelectedDate]);

  const headerCenterComponent = useMemo(
    () => (
      <CustomText
        fontProps={['ff-roboto-bold', 'fs-medium']}
        color={colors.primary_2}>
        {i18n.t('select_dish')}
      </CustomText>
    ),
    [],
  );
  const onPausedDayPress = useCallback(() => {
    dispatch(
      SubscriptionActions.pausedPackageAction(props?.calenderSelectedDate),
    );
  }, [props?.calenderSelectedDate, dispatch]);

  const onResumeDayPress = useCallback(() => {
    dispatch(
      SubscriptionActions.resumePackageAction(props?.calenderSelectedDate),
    );
  }, [props?.calenderSelectedDate, dispatch]);

  const onDayPress = useCallback(date => {
    dispatch(
      SubscriptionActions.setCalenderSelectedDateAction(
        new Date(getValidDate(date, 'YYYY-MM-DD 00:00:00')),
      ),
    );
  }, [dispatch]);

  const markedDates = useMemo(() => {
    const markDates = getHorizontalCalenderMarkedDates(props?.myCalender?.data);
    return markDates;
  }, [props?.myCalender?.data]);

  const renderCalender = useMemo(
    () => (
      <CustomFullHorizontalCalender
        minDate={props?.calenderRange?.minDate}
        maxDate={props?.calenderRange?.maxDate}
        selectedCurrentDate={props?.calenderSelectedDate ?? null}
        onDayPress={onDayPress}
        markedDates={markedDates}
      />
    ),
    [
      props?.calenderRange?.minDate,
      props?.calenderRange?.maxDate,
      props?.calenderSelectedDate,
      onDayPress,
      markedDates,
    ],
  );

  const renderMacros = useMemo(() => {
    let protein = props?.totalMacros?.protein
    let fats = props?.totalMacros?.fats
    let carbs = props?.totalMacros?.carbohydrates
    let calories = props?.totalMacros?.calories
    let text = '';
    let textCal = '';
    text = `${protein} ${i18n.t('protein')} | ${carbs} ${i18n.t('carbs')} | ${fats} ${i18n.t('fats')}`;
    textCal = `${calories} ${i18n.t('calories')}`;
    return (
      <View style={{}}>
        <View style={styles.productMacrosContainer}>
          {/* <CustomText customStyle={styles.productMacros} fontProps={['fs-small']}>Pro:  {protein}g </CustomText>
          <CustomText customStyle={styles.productMacros} fontProps={['fs-small']}>Carb: {carbohydrates}g </CustomText>
          <CustomText customStyle={styles.productMacros} fontProps={['fs-small']}>Fat:  {fats}g </CustomText> */}
          <CustomText
            color={colors.smokeyGrey}
            fontProps={['fs-xxsmall']}
            customStyle={{ ...styles.subTitleStyle }}>
            {text}
          </CustomText>
        </View>

        <View style={{}}>
          <View style={styles.productMacrosContainer}>
            <CustomText
              color={colors.smokeyGrey}
              fontProps={['fs-xxsmall']}
              customStyle={{ ...styles.subTitleStyle }}>
              <CustomText color={colors.beanRed} fontProps={['ff-roboto-bold', 'fs-xsmall']}> Or in </CustomText>
              {textCal}
            </CustomText>
          </View>
        </View>
      </View>
    )
  },
    [props?.totalMacros, dispatch, props?.calenderSelectedDate, onDayPress],
  );

  const renderMacrosAlert = useMemo(() => {
    let protein = props?.totalMacros?.protein
    let fats = props?.totalMacros?.fats
    let carbohydrates = props?.totalMacros?.carbohydrates
    let calories = props?.totalMacros?.calories
    return (
      <View style={{ ...styles.restContainer }}>
        <View style={styles.productMacrosContainer}>
          <CustomText customStyle={styles.productMacrosAlert} fontProps={['fs-small']}>Pro:  {protein}g</CustomText>
          <CustomText customStyle={styles.productMacrosAlert} fontProps={['fs-small']}>Carb: {carbohydrates}g</CustomText>
          <CustomText customStyle={styles.productMacrosAlert} fontProps={['fs-small']}>Fat:  {fats}g</CustomText>
        </View>

        <View style={styles.bottomContainer}>
          <View style={styles.productMacrosContainer}>
            <CustomText customStyle={styles.productMacrosAlert} fontProps={['fs-small']}>
              <CustomText color={colors.primary_2} fontProps={['fs-small']}>Or in </CustomText>
              Cal: {calories} {i18n.t('cals')}</CustomText>
          </View>
        </View>
      </View>
    )
  },
    [props?.totalMacros, dispatch, props?.calenderSelectedDate, onDayPress],
  );

  // No Meals Selected Day
  const renderNoMealsSelctedDay = useMemo(
    () => (
      <View style={styles.selectDishbottomButtonsContainer}>
        <View style={styles.selectedMealTopContainer}>
          {renderMacros}
          <TouchableOpacity
            onPress={onPausedDayPress}
            hitSlop={getHitSlop(20)}
            style={styles.selectedDayPausedThisDayContainer}>
            <CustomText
              fontProps={['fs-small', 'ff-roboto-medium']}
              color={colors.smokeyGrey}>
              {i18n.t('pause_this_day')}
            </CustomText>
            <CustomIcon
              size={20}
              name="pause-circle"
              type={IconType.Feather}
              color={colors.beanRed}
              customStyle={styles.pausedIcon}
            />
          </TouchableOpacity>
        </View>
      </View>
    ),
    [onPausedDayPress, props?.totalMacros],
  );
  // No Plan Exist Day
  const renderNoPlanExistDay = useMemo(
    () => (
      <View style={styles.bottomButtonsContainer}>
        <CustomIcon
          type={IconType.MaterialIcons}
          name="event-note"
          size={40}
          customStyle={{ marginBottom: 10 }}
          color={colors.primary_3}
        />
        <CustomText fontProps={['fs-medium']}>
          {i18n.t('no_plan_exist')}
        </CustomText>
      </View>
    ),
    [],
  );

  const renderTimeGoesDayMeals = useCallback(
    ({ item }) => (
      <View style={{ paddingHorizontal: globalPaddingMargin.whole }}>
        <CustomProductCard productData={item} />
      </View>
    ),
    [],
  );

  const selectedMealsByDate = useMemo(() => {
    const mealsDataWithCategory = props?.selectedMealsByDate?.data?.filter(
      item => item?.meals_data?.length > 0,
    );
    return mealsDataWithCategory?.length > 0
      ? mealsDataWithCategory?.map(item => ({
        title: item?.meal_name ?? '',
        data: item?.meals_data ?? [],
      }))
      : [];
  }, [props?.selectedMealsByDate?.data]);

  const timeGoesSectionListHeader = () => (
    <View style={{ alignItems: 'center', marginVertical: 10 }}>
      <CustomIcon
        type={IconType.MaterialCommunityIcons}
        name="clock-time-three-outline"
        size={40}
        customStyle={{ marginBottom: 10 }}
        color={colors.primary_3}
      />
      <CustomText fontProps={['fs-medium']}>
        {i18n.t('time_goes_for_selection_for_this_date')}
      </CustomText>
    </View>
  );

  const renderSectionHeader = useCallback(({ section: { title } }) => {
    return (
      <View style={styles.sectionHeader}>
        <CustomText
          fontProps={['fs-small', 'ff-roboto-medium']}
          color={colors.primary_2}>
          {title}
        </CustomText>
      </View>
    );
  }, []);

  // Time Goes For Selection
  const renderTimeGoesForSelectionDay = useMemo(
    () => (
      <View>
        <SectionList
          ListHeaderComponent={timeGoesSectionListHeader}
          bounces={false}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          sections={selectedMealsByDate}
          keyExtractor={(item, index) => item + index}
          renderItem={renderTimeGoesDayMeals}
          renderSectionHeader={renderSectionHeader}
        />
      </View>
    ),
    [
      timeGoesSectionListHeader,
      selectedMealsByDate,
      renderTimeGoesDayMeals,
      renderSectionHeader,
    ],
  );

  // Selected Day
  const renderSelectedDay = useMemo(
    () => (
      <View style={styles.selectDishbottomButtonsContainer}>
        <View style={styles.selectedMealTopContainer}>
          {renderMacros}
          <TouchableOpacity
            onPress={onPausedDayPress}
            hitSlop={getHitSlop(20)}
            style={styles.selectedDayPausedThisDayContainer}>
            <CustomText
              fontProps={['fs-small', 'ff-roboto-medium']}
              color={colors.smokeyGrey}>
              {i18n.t('pause_this_day')}
            </CustomText>
            <CustomIcon
              size={20}
              name="pause-circle"
              type={IconType.Feather}
              color={colors.beanRed}
              customStyle={styles.pausedIcon}
            />
          </TouchableOpacity>
        </View>
      </View>
    ),
    [onPausedDayPress, props?.totalMacros],
  );

  // Paused Day
  const renderPausedDay = useMemo(
    () => (
      <View style={styles.bottomButtonsContainer}>
        <CustomText fontProps={['fs-medium']}>
          {i18n.t('paused_day')}
        </CustomText>
        <CustomText
          fontProps={['fs-small', 'ff-roboto-light']}
          customStyle={styles.bottomSubTitle}>
          {i18n.t('paused_day_subtitle')}
        </CustomText>
        <CustomButton
          backgroundColor={colors.primary_2}
          icon={images.resumeIcon}
          onPress={onResumeDayPress}
          buttonLabel={i18n.t('resume')}
        />
      </View>
    ),
    [onResumeDayPress],
  );

  // Off Day
  const renderOffDay = useMemo(
    () => (
      <View style={styles.bottomButtonsContainer}>
        <CustomText fontProps={['fs-medium']}>
          {i18n.t('this_is_off_day')}
        </CustomText>
      </View>
    ),
    [],
  );

  // Select Date From Above Calender
  const renderSelectDateFromAboveCalender = useMemo(
    () => (
      <View style={styles.bottomButtonsContainer}>
        <CustomText fontProps={['fs-medium']}>
          {i18n.t('select_date')}
        </CustomText>
      </View>
    ),
    [],
  );

  const renderButtonsAndTitles = useMemo(() => {
    if (!props?.calenderSelectedDate) return renderSelectDateFromAboveCalender;
    if (props?.pausedOrResumePackage?.loading) return <CustomLoader />;
    switch (props?.calenderSelectedDayType) {
      case calenderDayType.noPlanExistDay.key:
        return renderNoPlanExistDay;
      case calenderDayType.noMealSelectedDay.key:
        return renderNoMealsSelctedDay;
      case calenderDayType.timeGoesForSelection.key:
        return renderTimeGoesForSelectionDay;
      case calenderDayType.selectedDay.key:
        return renderSelectedDay;
      case calenderDayType.pausedDay.key:
        return renderPausedDay;
      case calenderDayType.offDay:
        return renderOffDay;
      default:
        return null;
    }
  }, [
    props?.calenderSelectedDate,
    renderSelectDateFromAboveCalender,
    props?.pausedOrResumePackage?.loading,
    props?.calenderSelectedDayType,
    renderNoPlanExistDay,
    renderTimeGoesForSelectionDay,
    renderNoMealsSelctedDay,
    renderSelectedDay,
    renderPausedDay,
    renderOffDay,
  ]);

  const onMyHealthDataChangePress = useCallback(() => {
    setShowChangeHealthDataModal(true);
  }, []);

  const currentPlanType = useMemo(() => {
    const planType = getPlanType(
      props?.calenderSelectedDate,
      props?.subscriptionDetails?.data,
    );
    return planType;
  }, [props?.calenderSelectedDate, props?.subscriptionDetails?.data]);

  const selectedMealsByCalegory = useMemo(
    () => mealsData?.find(item => item?.id === selectedMealCategory),
    [mealsData, selectedMealCategory],
  );

  const renderCustomPlanContainer = useMemo(() => {
    const { proteins = 0, carbs = 0, calories = 0, fats = 0 } = props?.userData;
    let text = '';
    if (calories > 0) text = `${calories} ${i18n.t('calories')}`;
    else
      text = `${proteins} ${i18n.t('protein')} | ${carbs} ${i18n.t('carbs')}`;
    if (currentPlanType && calories <= 0)
      text += ` | ${fats} ${i18n.t('fats')}`;
    return (
      [
        calenderDayType?.selectedDay?.key,
        calenderDayType?.noMealSelectedDay?.key,
      ].includes(props?.calenderSelectedDayType) && (
        <View style={{ ...styles.selectDishbottomButtonsContainer, borderBottomWidth: 1, borderBottomColor: colors.skeletonGrey, flexDirection: 'row', flex: 1 }}>
          <TouchableOpacity
            onPress={onMyHealthDataChangePress}
            style={styles.caleriesContainer}>
            <CustomIcon
              size={14}
              name="local-fire-department"
              type={IconType.MaterialIcons}
              color={colors.thinGrey}
            />
            <CustomText
              color={colors.smokeyGrey}
              fontProps={['fs-xxsmall']}
              customStyle={{ ...styles.subTitleStyle }}>
              {text}
            </CustomText>
            <CustomText
              numberOfLines={2}
              fontProps={['ff-roboto-bold', 'fs-xxsmall']}
              color={colors.beanRed}>
              {' '}
              ( {i18n.t('change')} )
            </CustomText>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => {
              if (selectedMealsByCalegory?.child_dish_category?.length == 0) {
                CustomToast.error(i18n.t('there_is_no_secondary_meal_type'));
              } else {
                dropDownRef?.current?.openDropdown();
              }
            }}
            style={{ ...styles.caleriesContainer, flex: 0 }}>
            <CustomText
              color={colors.smokeyGrey}
              fontProps={['fs-xsmall']}
              customStyle={{}}>
              {i18n.t('filter_by_meal_type')}

            </CustomText>
            <View
              style={{ borderWidth: 1, flexDirection: 'row', padding: 2, borderColor: colors.beanRed, borderRadius: 3, paddingVertical: 2, justifyContent: 'space-between', alignItems: 'center', display: 'flex' }}
            >
              <CustomText
                color={colors.beanRed}
                fontProps={['ff-roboto-bold', 'fs-xsmall']}
                customStyle={{ paddingRight: 5 }}>
                {selectedItem?.name}
              </CustomText>
              <CustomImage
                source={images.downArrow}
                tintColor={colors.beanRed}
                resizeMode={imagesMode.center}
                style={{ height: 10, width: 10 }}
              />
            </View>
          </TouchableOpacity>
        </View>
      )
    );
  }, [
    currentPlanType,
    selectedItem,
    selectedMealsByCalegory,
    props?.calenderSelectedDayType,
    props?.userData,
    onMyHealthDataChangePress,
  ]);

  const ListHeaderComponent = useMemo(
    () => (
      <View style={styles.ListHeaderContainer}>
        {/* Custom Plan Container */}
        {/* <View style={{ borderWidth: 2 }}> */}
        {renderCustomPlanContainer}
        {/* </View> */}

        {/* <View style={{ borderWidth: 2 }}> */}
        {/* Bottom Titles and Buttons */}
        {renderButtonsAndTitles}
        {/* </View> */}
      </View>
    ),
    [renderCustomPlanContainer, renderButtonsAndTitles],
  );

  const mealCategoryKeyExtractor = useCallback(
    (item, index) => generateUniqueId('SelectDishFoodTypesData', index),
    [],
  );

  const onMealCategoryPress = mealCategory => {
    setSelectedItem(obj);
    setSelectedMealCategory(mealCategory?.id);
  };

  const renderMealCategory = useCallback(
    ({ item }) => {
      let totalQty = 0;
      const selectedMealCategoryIndex = mealsData?.findIndex(
        custMealItem => custMealItem?.id === item?.id,
      );
      if (selectedMealCategoryIndex !== -1) {
        totalQty = mealsData?.[selectedMealCategoryIndex]?.meals_data?.reduce(
          (currentValue, mealItem) =>
            Number(currentValue) + Number(mealItem?.selected_meal_qty ?? 0),
          0,
        );
      }
      const onPress = () => {
        onMealCategoryPress(item);
      };
      return (
        <TouchableOpacity
          delayPressOut={0}
          onPress={onPress}
          style={{
            ...styles.singleFoodTypeContainer,
            borderBottomColor:
              selectedMealCategory === item?.id
                ? colors.primary_2
                : colors.ghostWhite
          }}>
          <CustomImage
            SvgSource={images[item?.meal_name === "Main Course" ? "MainCourse" : item?.meal_name]}
            svgProps={{ height: 25, width: 25 }}
            style={styles.rating}
          />
          <CustomText
            fontProps={['fs-small', 'ff-roboto-medium']}
            color={
              selectedMealCategory === item?.id
                ? colors.primary_2
                : colors.borderColor
            }
            customStyle={{ ...styles.singleFoodTypeText }}>
            {item?.meal_name}
          </CustomText>
          <CustomText
            fontProps={['fs-xxsmall', 'ff-roboto-light']}
            color={
              selectedMealCategory === item?.id
                ? colors.primary_3
                : colors.borderColor
            }
            customStyle={{ ...styles.singleFoodTypeText }}>
            {`Selected ${totalQty ?? 0} of ${item?.no_of_meals ?? 0}`}
          </CustomText>
        </TouchableOpacity>
      );
    },
    [mealsData, onMealCategoryPress, selectedMealCategory],
  );

  const onBackPress = useCallback(() => {
    navigateGoBack();
  }, []);

  const headerLeftComponent = useMemo(
    () => (
      <CustomIcon
        onPress={onBackPress}
        type={IconType.MaterialIcons}
        name={isRTL ? 'arrow-forward-ios' : 'arrow-back-ios'}
        size={16}
        color={colors.primary_2}
        customStyle={headerStyles.backButton}
      />
    ),
    [onBackPress],
  );

  const onDonePress = useCallback(() => {
    customAlertBoxRef?.current?.hide();
    setWholeScreenLoading(true);
    dispatch(
      SubscriptionActions?.selectPackageMealsAction(
        getValidDate(props?.calenderSelectedDate, 'YYYY-MM-DD'),
        mealsData,
      ),
    )
      .then(() => {
        setWholeScreenLoading(false);
        let futureDate = getValidDate(
          new Date(props?.calenderSelectedDate).setDate(
            new Date(props?.calenderSelectedDate).getDate() + 1,
          ),
          'YYYY-MM-DD 00:00:00',
        );
        if (new Date(futureDate)?.getDay() === EVENT_OFF_DAY)
          futureDate = getValidDate(
            new Date(futureDate)?.setDate(new Date(futureDate)?.getDate() + 1),
            'YYYY-MM-DD 00:00:00',
          );
        let wholePlanDurationData = getWholePlanDuration(
          props?.subscriptionDetails?.data,
        );

        if (
          new Date(futureDate) <= new Date(wholePlanDurationData?.end_time) &&
          new Date(futureDate) >= new Date(wholePlanDurationData?.start_time)
        ) {
          dispatch(
            SubscriptionActions.setCalenderSelectedDateAction(
              new Date(futureDate),
            ),
          );
        } else {
          setWholeScreenLoading(false);
          navigateGoBack();
        }
      })
      .catch(() => {
        setWholeScreenLoading(false);
      });
  }, [mealsData]);

  const renderAlertContent = useCallback(
    mealItems => (
      <View>
        {[...mealItems, { name: "Total Macros", subItem: [] }]?.map((mealItem, mealIndex) => (
          <View
            key={`alertContentItem,${mealIndex}`}
            style={{ flexDirection: 'row', marginTop: 5, paddingHorizontal: 15 }}>
            <View style={{ flex: 1 }}>
              <CustomText
                fontProps={['ff-roboto-bold', 'fs-medium']}
                color={colors.primary_3}>
                {mealItem?.name} :{' '}
              </CustomText>
            </View>
            <View style={{ flex: 1 }}>
              {mealItem?.subItems?.map((subItem, subIndex) => (
                <View
                  key={`alertContentSubItem-${subIndex}`}
                  style={{ marginBottom: 5 }}>
                  <CustomText
                    fontProps={['fs-medium']}
                    color={colors.primary_2}
                    numberOfLines={2}>
                    {subItem?.name}
                  </CustomText>
                </View>
              ))}
            </View>
          </View>
        ))}
      </View>
    ),
    [],
  );



  const onSavePress = useCallback(async () => {
    if (
      [
        calenderDayType?.noMealSelectedDay?.key,
        calenderDayType?.selectedDay?.key,
      ]?.includes(props?.calenderSelectedDayType)
    ) {
      let dummyMealItems = [];
      let totalMacrosOnSave = {
        protein: 0,
        carbohydrates: 0,
        calories: 0,
        fats: 0
      }
      mealsData?.map((mealItem, mealIndex) => {
        const subMealItem = mealItem?.meals_data?.filter(
          subItem => {
            if (Number(subItem?.selected_meal_qty) > 0) {
              totalMacrosOnSave.calories += parseFloat(subItem?.calories || 0) * Number(subItem?.selected_meal_qty)
              totalMacrosOnSave.carbohydrates += parseFloat(subItem?.carbohydrates || 0) * Number(subItem?.selected_meal_qty)
              totalMacrosOnSave.protein += parseFloat(subItem?.protein || 0) * Number(subItem?.selected_meal_qty)
              totalMacrosOnSave.fats += parseFloat(subItem?.fats || 0) * Number(subItem?.selected_meal_qty)
            }
            return Number(subItem?.selected_meal_qty) > 0
          },
        );
        if (subMealItem?.length > 0) {
          let subMealItems = [];
          subMealItem?.map(sItem => {
            subMealItems.push({
              name: sItem?.name,
              calories: sItem?.calories,
              carbohydrates: sItem?.carbohydrates,
              fats: sItem?.fats,
              protein: sItem?.protein,
            })
          });
          dummyMealItems.push({
            id: mealIndex,
            name: mealItem?.meal_name,
            subItems: subMealItems,
          });
        }
      });

      const contentComponent = renderAlertContent(dummyMealItems);
      if (dummyMealItems?.length > 0) {
        customAlertBoxRef?.current?.show({
          title: i18n.t('are_you_sure'),
          subTitle: i18n.t('you_have_selected_following_for', {
            selected_date: getValidDate(
              props?.calenderSelectedDate,
              'DD MMM YYYY',
            ),
          }),
          onPress: () => onDonePress(),
          contentComponent: (() => {
            return (
              <>
                {contentComponent}
                {renderMacrosAlert}
              </>
            )
          })(),
        });
      } else {
        onDonePress();
      }

    }
  }, [mealsData, props?.calenderSelectedDayType, props?.totalMacros]);

  const headerRightComponent = useMemo(
    () => (
      <TouchableOpacity
        onPress={onSavePress}
        hitSlop={getHitSlop(20)}>
        {[
          calenderDayType?.noMealSelectedDay?.key,
          calenderDayType?.selectedDay?.key,
        ]?.includes(props?.calenderSelectedDayType) && (
            <CustomText color={colors.primary_2} fontProps={['fs-medium']}>
              {i18n.t('save')}
            </CustomText>
          )}
      </TouchableOpacity>
    ),
    [
      onSavePress,
      props?.calenderSelectedDayType,
      subscriptionData?.is_custom_grams,
    ],
  );

  const ListEmptyComponent = useMemo(
    () =>
      [
        calenderDayType?.noMealSelectedDay?.key,
        calenderDayType?.selectedDay?.key,
      ]?.includes(props?.calenderSelectedDayType) && (
        <CustomText customStyle={styles.noMealsText}>
          {i18n.t('no_meals_are_there')}
        </CustomText>
      ),
    [props?.calenderSelectedDayType],
  );

  const renderSelectedDayMeals = ({ item }) => {
    const onQtyButtonPress = type => {
      if (type === 'inc') {
        const meals = mealsData;
        const selectedMealCategoryIndex = meals?.findIndex(
          item => item?.id === selectedMealCategory,
        );
        if (selectedMealCategoryIndex !== -1) {
          const selectedMealIndex = meals?.[
            selectedMealCategoryIndex
          ]?.meals_data?.findIndex(fitem => fitem?.meal_id === item?.meal_id);
          setMealIndex(selectedMealIndex);
          if (selectedMealIndex !== -1) {
            const selectedMealItem =
              meals?.[selectedMealCategoryIndex]?.meals_data?.[
              selectedMealIndex
              ];

            // selectedMealItem
            const qty = Number(selectedMealItem?.selected_meal_qty) ?? 0;
            const requiredQty =
              Number(meals?.[selectedMealCategoryIndex]?.no_of_meals) ?? 0;
            const totalQty = meals?.[
              selectedMealCategoryIndex
            ]?.meals_data.reduce(
              (currentValue, item) =>
                Number(currentValue) + Number(item?.selected_meal_qty ?? 0),
              0,
            );
            if (Number(totalQty) < Number(requiredQty)) {
              meals[selectedMealCategoryIndex].meals_data[
                selectedMealIndex
              ].selected_meal_qty = qty + 1;
              setMealsData([...meals]);
              sendDataToReducer(dispatch, CALCULATE_MACROS, meals)
              // setTotalMacros(prev => {
              //   prev.calories += parseFloat(selectedMealItem?.calories || 0)
              //   prev.carbohydrates += parseFloat(selectedMealItem?.carbohydrates || 0)
              //   prev.protein += parseFloat(selectedMealItem?.protein || 0)
              //   prev.fats += parseFloat(selectedMealItem?.fats || 0)
              //   return prev
              // })
            } else {
              setShownMenu(false);
              CustomToast.error(
                i18n.t('error'),
                i18n.t('you_cant_add_item_in_this_category', {
                  meal_category: selectedMealsByCalegory?.meal_name,
                }),
              );
            }
          }
        }
        if (item?.substitute?.length > 0) {
          setSubstituteData(item?.substitute);
          setShownMenu(true);
        }
      } else {
        const meals = _.cloneDeep(mealsData);
        const selectedMealCategoryIndex = meals?.findIndex(
          item => item?.id === selectedMealCategory,
        );
        if (selectedMealCategoryIndex !== -1) {
          const selectedMealIndex = meals?.[
            selectedMealCategoryIndex
          ]?.meals_data?.findIndex(fitem => fitem?.meal_id === item?.meal_id);
          if (selectedMealIndex !== -1) {
            const selectedMealItem = meals?.[selectedMealCategoryIndex]?.meals_data?.[selectedMealIndex];
            const qty = Number(selectedMealItem?.selected_meal_qty) ?? 0;
            if (qty > 0) {
              meals[selectedMealCategoryIndex].meals_data[
                selectedMealIndex
              ].selected_meal_qty = qty - 1;
              setMealsData([...meals]);
              sendDataToReducer(dispatch, CALCULATE_MACROS, meals)
              // setTotalMacros(prev => {
              //   prev.calories -= parseFloat(selectedMealItem?.calories || 0)
              //   prev.carbohydrates -= parseFloat(selectedMealItem?.carbohydrates || 0)
              //   prev.protein -= parseFloat(selectedMealItem?.protein || 0)
              //   prev.fats -= parseFloat(selectedMealItem?.fats || 0)
              //   return prev
              // })
            }
          }
        }
      }
    }
    // , [props?.totalMacros, dispatch, renderNoMealsSelctedDay])
    return (
      <View style={{ paddingHorizontal: globalPaddingMargin.whole }}>
        <CustomProductCard
          productData={item}
          showQtyPicker={true}
          onQtyButtonPress={type => onQtyButtonPress(type, item)}
        />
      </View>
    );
  };

  const obj1 = selectedMealsByCalegory?.child_dish_category ?? [];
  const dropdownData = [obj, ...obj1];

  // const renderMeals = useMemo(
  //   () => (
  //     <FlatList
  //       ref={mealsFlatListRef}
  //       ListHeaderComponent={ListHeaderComponent}
  //       bounces={false}
  //       style={styles.contentContainer}
  //       showsVerticalScrollIndicator={false}
  //       showsHorizontalScrollIndicator={false}
  //       data={
  //         selectedItem?.id !== null
  //           ? selectedMealsByCalegory?.meals_data
  //               ?.sort((a, b) =>
  //                 a?.name.localeCompare(b?.name, 'es', {
  //                   sensitivity: 'base',
  //                 }),
  //               )
  //               .filter(item => item?.sub_dish_category_id == selectedItem?.id)
  //           : [
  //               calenderDayType?.noMealSelectedDay?.key,
  //               calenderDayType?.selectedDay?.key,
  //             ]?.includes(props?.calenderSelectedDayType)
  //           ? selectedMealsByCalegory?.meals_data?.sort((a, b) =>
  //               a?.name.localeCompare(b?.name, 'es', {
  //                 sensitivity: 'base',
  //               }),
  //             ) ?? []
  //           : []
  //       }
  //       keyExtractor={(item, index) => item + index}
  //       renderItem={renderSelectedDayMeals}
  //       ListEmptyComponent={ListEmptyComponent}
  //     />
  //   ),
  //   [
  //     selectedItem,
  //     ListEmptyComponent,
  //     ListHeaderComponent,
  //     props?.calenderSelectedDayType,
  //     renderSelectedDayMeals,
  //     selectedMealsByCalegory?.meals_data,
  //   ],
  // );

  // Render Header
  const renderHeader = useMemo(
    () => (
      <CustomHeader
        leftComponent={headerLeftComponent}
        centerComponent={headerCenterComponent}
        rightComponent={headerRightComponent}
      />
    ),
    [headerCenterComponent, headerLeftComponent, headerRightComponent],
  );

  const onHideChangeHealthDataModal = useCallback(() => {
    setShowChangeHealthDataModal(false);
  }, []);


  return (
    <View style={styles.mainContainer}>
      {/* Header */}
      {renderHeader}

      {/* Change My Health Data Modal */}
      <ChangeMyHealthDataModal
        goal_name={selectedMealCategory}
        showModal={showChangeHealthDataModal}
        onHide={onHideChangeHealthDataModal}
      />

      {/* Replace carbs with data Modal */}
      <ReplaceCarbsModal
        data={substituteData}
        onItemPress={item => {
          const meals = _.cloneDeep(mealsData);
          const selectedMealCategoryIndex = meals?.findIndex(
            item => item?.id === selectedMealCategory,
          );
          if (mealIndex !== 1) {
            const updatedObj = {
              ...meals[selectedMealCategoryIndex].meals_data[mealIndex],
            };
            updatedObj.substitute_id = item?.id ?? null;
            meals[selectedMealCategoryIndex].meals_data[mealIndex] = updatedObj;
            setMealsData([...meals]);
          }
        }}
        showModal={showMenu}
        onHide={() => setShownMenu(false)}
      />

      {/* Render Loader */}
      {wholeScreenLoading && <CustomLoader wholeScreen={true} />}
      <CustomAlertBox ref={customAlertBoxRef} />

      {/* Calender */}
      {renderCalender}
      {loading ? (
        <View style={{ marginTop: 20 }}>
          <CustomLoader />
        </View>
      ) : (
        <Fragment>
          {/* Meal Categories */}
          <Fragment>
            {[
              calenderDayType?.selectedDay?.key,
              calenderDayType?.noMealSelectedDay?.key,
            ].includes(props?.calenderSelectedDayType) && (
                <View style={{ flexDirection: 'row', borderTopWidth: 1, borderTopColor: colors.skeletonGrey }}>
                  <FlatList
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={
                      styles.mealCategoriesFlatListContentContainer
                    }
                    showsHorizontalScrollIndicator={false}
                    data={mealsData ?? []}
                    horizontal={true}
                    keyExtractor={mealCategoryKeyExtractor}
                    style={styles.foodTypesFlatList}
                    renderItem={renderMealCategory}
                  />
                  {/* {selectedMealsByCalegory?.child_dish_category?.length >
                      0 && (
                      <TouchableOpacity
                        hitSlop={getHitSlop(10)}
                        onPress={() => dropDownRef?.current?.openDropdown()}
                        activeOpacity={0.1}
                        style={{backgroundColor: 'white'}}>
                        <Image
                          source={images.downArrow}
                          resizeMode="contain"
                          style={styles.dropDownIconStyle}
                        />
                      </TouchableOpacity>
                    )} */}
                </View>
              )}
            {selectedMealsByCalegory?.child_dish_category?.length > 0 && (
              <SelectDropdown
                data={dropdownData}
                onSelect={selectedItem => {
                  setSelectedItem(selectedItem);
                }}
                ref={dropDownRef}
                renderCustomizedRowChild={item => {
                  return (
                    <View style={styles.dropdown3RowChildStyle}>
                      <Text style={styles.dropdown3RowTxt}>
                        {item?.name}
                      </Text>
                    </View>
                  );
                }}
                rowTextStyle={styles.rowTextStyle}
                dropdownStyle={styles.dropDownStyle}
                buttonStyle={styles.buttonStyle}
              />
            )}
          </Fragment>

          {/* Render Meals */}
          {/* {renderMeals} */}
          <FlatList
            ref={mealsFlatListRef}
            ListHeaderComponent={ListHeaderComponent}
            bounces={false}
            style={styles.contentContainer}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            data={
              isRTL
                ? selectedMealsByCalegory?.meals_data
                : selectedItem?.id !== null
                  ? selectedMealsByCalegory?.meals_data
                    ?.sort((a, b) =>
                      a?.name.localeCompare(b?.name, 'es', {
                        sensitivity: 'base',
                      }),
                    )
                    .filter(
                      item =>
                        item?.sub_dish_category_id == selectedItem?.id,
                    )
                  : [
                    calenderDayType?.noMealSelectedDay?.key,
                    calenderDayType?.selectedDay?.key,
                  ]?.includes(props?.calenderSelectedDayType)
                    ? selectedMealsByCalegory?.meals_data?.sort((a, b) =>
                      a?.name.localeCompare(b?.name, 'es', {
                        sensitivity: 'base',
                      }),
                    ) ?? []
                    : []
            }
            keyExtractor={(item, index) => item + index}
            renderItem={renderSelectedDayMeals}
            ListEmptyComponent={ListEmptyComponent}
          />
        </Fragment>
      )}
    </View>
  );
};

const headerStyles = StyleSheet.create({
  backButton: {
    marginRight: 15,
  },
});

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: colors.appBackground,
  },
  bottomButtonsContainer: {
    padding: globalPaddingMargin.whole,
    alignItems: 'center',
  },
  selectDishbottomButtonsContainer: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    alignItems: 'center',
  },
  bottomSubTitle: {
    lineHeight: 25,
    marginVertical: 20,
    textAlign: 'center',
    width: '80%',
  },
  contentContainer: {
    flex: 1,
  },
  selectedDayPausedThisDayContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  pausedIcon: {
    marginLeft: 10,
  },
  selectedMealTopContainer: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  sectionHeader: {
    backgroundColor: colors.appBackground,
    height: 35,
    justifyContent: 'center',
    paddingHorizontal: globalPaddingMargin.whole,
  },
  mealCategoriesFlatListContentContainer: {
    // alignItems: 'center',
  },
  foodTypesFlatList: {
    backgroundColor: colors.white,
    borderBottomWidth: 2,
    borderBottomColor: colors.ghostWhite,
  },
  singleFoodTypeContainer: {
    height: 65,
    borderBottomWidth: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  singleFoodTypeText: {
    paddingHorizontal: 20,
  },
  noMealsText: {
    textAlign: 'center',
  },
  caleriesContainer: {
    flex: 1,
    marginTop: 10,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
  },
  subTitleStyle: {
    marginLeft: 5,
  },
  dropDownIconStyle: {
    flex: 1,
    height: wp(5),
    width: wp(3.5),
    marginHorizontal: wp(4),
    alignSelf: 'center',
    tintColor: colors.black,
  },
  dropDownStyle: {
    width: wp(35),
    borderRadius: hp(1),
    position: 'absolute',
    marginTop: isIOS ? 0 : hp(5),
    left: wp(55),
  },
  buttonStyle: {
    width: 0,
    height: 0,
    marginHorizontal: wp(2),
  },
  rowTextStyle: {
    fontFamily: fontsFamily.roboto.regular,
    fontSize: 16,
  },
  dropdown3RowChildStyle: {
    alignItems: 'center',
  },
  dropdown3RowTxt: {
    textAlign: 'center',
    fontFamily: fontsFamily.roboto.regular,
    fontSize: 16,
    color: colors.lightBlack,
  },
  ListHeaderContainer: {
    // textAlign: 'center',
    // fontFamily: fontsFamily.roboto.regular,
    // fontSize: 16,
    marginHorizontal: 24,
    marginVertical: 10,
    backgroundColor: colors.white,
    marginBottom: 15,
    flex: 1,

    // minHeight: 110,
    // width: '100%',
    // flexDirection: 'row',
    borderRadius: 12,
    // overflow: 'hidden',
    ...commonShadow(),
  },
  restContainer: {
    // padding: 12,
    marginHorizontal: 10,
    // justifyContent: 'space-between',
    justifyContent: 'space-around',
  },
  bottomContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginVertical: 5,
  },
  productMacrosContainer: {
    display: 'flex',
    flexDirection: "row",
    letterSpacing: 1,
    justifyContent: "flex-start"
  },
  productMacros: {
    fontSize: getFontsSize(fontsSize.xsmall)
  },
  productMacrosAlert: {
    marginHorizontal: 5,
    fontSize: getFontsSize(fontsSize.medium)
  },
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: globalPaddingMargin.bottom

  },
  text: {
    textAlign: 'center',
    fontSize: getFontsSize(fontsSize.large),
    fontWeight: '300'
  }

});

export default connect(state => ({
  userData: state.auth.userData,
  myCalender: state.subscription.myCalender,
  subscriptionDetails: state.subscription.subscriptionDetails,
  pausedOrResumePackage: state.subscription.pausedOrResumePackage,
  calenderRange: state.subscription.calenderRange,
  calenderSelectedDate: state.subscription.calenderSelectedDate,
  calenderSelectedDayType: state.subscription.calenderSelectedDayType,
  selectedMealsByDate: state.subscription.selectedMealsByDate,
  totalMacros: state.subscription.totalMacros,
}))(SingleDateManegeMeal);
