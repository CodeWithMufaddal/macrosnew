import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { View, StyleSheet, Pressable } from 'react-native';
import { connect, useDispatch } from 'react-redux';
import { getServerImagePath } from '../../../services/Services';
import images, { imagePathType, imagesMode } from '../../../assets/images/images';
import CustomImage from '../../../components/custom/CustomImage';
import CustomText from '../../../components/custom/CustomText';
import colors from '../../../constants/colors';
import CustomIcon, { IconType } from '../../../components/custom/CustomIcon';
import { navigateGoBack } from '../../../navigation/navigation';
import { isRTL } from '../../../constants/constants';
import { hp, wp } from '../../../helpers/screenHelper';
import { fontsFamily } from '../../../assets/fonts/fonts';
import { commonShadow } from './../../../styles/CommonStyles';
import * as SubscriptionActions from '../../../redux/actions/SubscriptionActions';
import CustomLoader from '../../../components/custom/CustomLoader';
import i18n from '../../../i18n/i18n';

const ProductPage = (props) => {
    const { productData } = props.route.params
    const [wholeScreenLoading, setWholeScreenLoading] = useState(false);
    const [user_rating, setUser_rating] = useState(productData?.user_rating);
    const dispatch = useDispatch();
    const findProductInMeals = useCallback(async (productData, mealCategories) => {
        for (const category of mealCategories) {
            for (const meal of category?.meals_data) {
                if (meal.meal_id === productData.meal_id) {
                    setUser_rating(meal.user_rating)
                }
            }
        }
        return null; // Product not found in the array
    }, [])
    useEffect(() => {
        // if (!productData?.user_rating) {
        setWholeScreenLoading(true);
        dispatch(SubscriptionActions.getMealsByMealTypeIdAndDateAction(props?.calenderSelectedDate))
            .then(res => {
                const data = res?.map((item, index) => {
                    return { ...item, id: index + 1 };
                });
                findProductInMeals(productData, data)
                setWholeScreenLoading(false);
            });
        // }
    }, [props?.calenderSelectedDate]);

    const onRatingPress = useCallback(async (rate) => {
        setWholeScreenLoading(true)
        setUser_rating(() => rate);
        await dispatch(SubscriptionActions.setProductRatingAction({ rating: rate, meal_id: productData.meal_ids }));
        setWholeScreenLoading(false)
    }, [user_rating, dispatch, props?.calenderSelectedDate]);

    const macrosDetailsContainer = [{
        image: "protien",
        customStyle: styles.macrosIcon(colors.primary_3),
        macrosTitle: i18n.t('protein'),
        macrosName: "protein",
    }, {
        image: "carbs",
        customStyle: styles.macrosIcon(colors.primary_2),
        macrosTitle: i18n.t('carbs'),
        macrosName: "carbohydrates",
    }, {
        image: "fats",
        customStyle: styles.macrosIcon(colors.primary_1),
        macrosTitle: i18n.t('fats'),
        macrosName: "fats",
    },
    ]

    const onBackPress = useCallback(() => {
        navigateGoBack();
    }, [])

    const headerLeftComponent = useMemo(
        () => (
            <View style={[styles.backButton,
            isRTL ? {
                padding: 6,
            } : {
                padding: 3,
                paddingVertical: 6,
                paddingLeft: 9,
            }
            ]}>

                <CustomIcon
                    onPress={onBackPress}
                    type={IconType.MaterialIcons}
                    name={isRTL ? 'arrow-forward-ios' : 'arrow-back-ios'}
                    size={16}
                    color={colors.primary_2}
                />
            </View>
        ),
        [onBackPress],
    );

    const renderRating = useMemo(() => (
        <View style={styles.ratingContainer}>
            <CustomText fontProps={['fs-large', fontsFamily.roboto.medium]}>{i18n.t('how_would_you_rate_the_dish')} </CustomText>
            <View style={styles.starContainer}>
                {[...Array(5)].map((_, index) => {
                    return (
                        <Pressable key={index} onPress={() => onRatingPress(index + 1)}>
                            <CustomImage
                                SvgSource={user_rating <= index ? images.star : images.starActive}
                                svgProps={{ height: 30, width: 30 }}
                                style={styles.user_rating}
                            />
                        </Pressable>
                    )
                })}
            </View>
        </View>
    ),
        [onBackPress, user_rating],
    );

    return (
        <>
            <View style={styles.imageContainer}>
                <CustomImage
                    source={{ uri: getServerImagePath(imagePathType.meals, productData?.image) }}
                    resizeMode={imagesMode.cover}
                    style={{ flex: 1, ...styles.cardImage }}
                />
            </View>
            {wholeScreenLoading && <CustomLoader wholeScreen={true} />}

            {headerLeftComponent}

            <View style={{ ...styles.restContainer }}>
                <CustomText customStyle={styles.productName} fontProps={['fs-xlarge', fontsFamily.roboto.bold]}>{productData?.name}</CustomText>
                <View style={styles.kcalContainer}>
                    <View style={styles.kcalContainerB}>
                        <CustomIcon
                            type={IconType.FontAwesome5}
                            name={'gripfire'}
                            size={20}
                            color={colors.shadowBlack}
                        />
                        <CustomText customStyle={{ ...styles.productName, paddingLeft: 7 }} fontProps={['fs-medium', fontsFamily.roboto.medium]}>{productData?.calories || 0} {i18n.t('cals')}</CustomText>
                    </View>
                </View>

            </View>
            <View style={styles.macrosDetailsContainer}>
                {macrosDetailsContainer.map((macrosDetails, index) => {
                    return (
                        <View style={styles.macrosContainer} key={index} >
                            <CustomImage
                                source={images[macrosDetails.image]}
                                style={{ width: 40, height: 40 }}
                            />
                            <View style={styles.macrosDetails}>
                                <CustomText customStyle={styles.productName} fontProps={['fs-medium', fontsFamily.roboto.medium]}>{macrosDetails.macrosTitle}</CustomText>
                                <CustomText customStyle={{ ...styles.productName, color: colors.smokeyGrey }} fontProps={['fs-small', fontsFamily.roboto.medium]}>{productData[macrosDetails.macrosName] || 0} {productData[macrosDetails.macrosName]?.toString().slice(-1) !== 'g' ? 'g' : ''}</CustomText>
                            </View>
                        </View>
                    )
                })
                }
            </View>
            <CustomText customStyle={{ ...styles.productName, ...styles.restContainer }} fontProps={['fs-medium', fontsFamily.roboto.medium]}>{productData?.description}</CustomText>

            {renderRating}
        </>
    );
};

const styles = StyleSheet.create({
    backButton: {
        position: "absolute",
        top: 0,
        left: 0,
        backgroundColor: "white",
        elevation: 5,
        borderRadius: 50,
        // transform: [{ rotate: '0deg' }],
        margin: 15,
    },
    restContainer: {
        paddingLeft: 10 * 2,
        paddingRight: 10,
        paddingTop: 10,
        justifyContent: 'space-between',
    },
    imageContainer: {
        width: "100%",
        height: "30%",
        overflow: 'hidden',
    },
    cardImage: {
    },
    productName: {
        textAlign: 'left',
    },
    kcalContainerB: {
        backgroundColor: colors.appBackground,
        borderRadius: 30,
        width: 'auto',
        height: hp(3.7),
        marginVertical: 7,
        paddingHorizontal: 10,
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
        flexDirection: 'row',
    },
    kcalContainer: {
        display: 'flex',
        alignItems: 'flex-start',
    },
    kcal: {},
    macrosDetailsContainer: {
        flexDirection: "row",
        justifyContent: 'space-around',
        marginVertical: 10,
        padding: 10,
        backgroundColor: colors.appBackground
    },
    macrosContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    macrosIcon: (bgColor) => ({
        // display: "flex",
        // justifyContent: "center",
        // alignSelf: "center",
        // alignItems: 'center',
        // alignContent: 'center',
        // textAlign: 'center',
        // marginHorizontal: 5,

        borderRadius: 50,
        padding: 5,
        width: 38,
        height: 38,
        backgroundColor: bgColor,
    }),
    macrosDetails: { margin: 5 },

    // Rating
    ratingContainer: {
        position: 'absolute',
        bottom: 10,
        right: 0,
        left: 0,
        zIndex: 99,

        marginHorizontal: 20,
        // backgroundColor: colors.appBackground,
        padding: 5,
        borderRadius: 10,

        flex: 1,
        display: 'flex',
        alignItems: 'center',
        // ...commonShadow(),
        // elevation: 0
    },
    ratingText: {
        margin: 20
    },
    starContainer: {
        flex: 1,
        flexDirection: 'row',
        marginHorizontal: 10,
        marginVertical: 5,
        padding: 2,
    },
    user_rating: {
        display: 'flex',
        // flexDirection: 'row',
        flex: 1,
        marginHorizontal: 2,

    }
});

export default connect((state) => ({
    calenderSelectedDate: state.subscription.calenderSelectedDate,
}))(ProductPage);
