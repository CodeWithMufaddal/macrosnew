import React, { useCallback } from 'react';
import {
    View, StyleSheet, FlatList, TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import CustomButton from '../../../components/custom/CustomButton';
import CustomModal from '../../../components/custom/CustomModal';
import CustomText from '../../../components/custom/CustomText';
import colors from '../../../constants/colors';
import { wp } from '../../../helpers/screenHelper';
import i18n from '../../../i18n/i18n';

const ReplaceCarbsModal = ({ showModal, onHide, ...props }) => {
    const onCancelPress = useCallback(() => {
        onHide();
    }, [onHide]);

    return (
        <CustomModal
            animationIn={'fadeIn'}
            animationOut={'fadeOut'}
            onHide={onHide}
            isVisible={showModal}>
            <View style={styles.mainContainer}>
                <View style={styles.contentContainer}>
                    <View style={styles.restContainer}>
                        {/* Heading */}
                        <View style={styles.headerContainer}>
                            <CustomText
                                fontProps={['ff-roboto-bold', 'fs-medium']}
                                color={colors.primary_3}>
                                {i18n.t('replace_your_carbs_with')}
                            </CustomText>
                        </View>
                        <FlatList
                            data={props?.data}
                            keyExtractor={(_, index) => index.toString()}
                            renderItem={({ item, index }) => (
                                <TouchableOpacity
                                    onPress={() => {
                                        onHide();
                                        props?.onItemPress(item);
                                    }}
                                    style={[
                                        styles.headerContainer,
                                        {
                                            marginHorizontal: 20,
                                            borderBottomColor: colors.skeletonGrey,
                                        },
                                    ]}>
                                    <CustomText
                                        fontProps={['ff-roboto-bold', 'fs-medium']}
                                        color={colors.primary_2}>
                                        {item?.name}
                                    </CustomText>
                                </TouchableOpacity>
                            )}
                        />
                    </View>
                </View>
                <View style={styles.bottomButtonContainer}>
                    <CustomButton
                        activeOpacity={0.7}
                        backgroundColor={colors.primary_1}
                        customStyle={styles.bottomButton}
                        buttonLabel={i18n.t('with_original_carbs')}
                        onPress={onCancelPress}
                    />
                </View>
                <View style={styles.bottomButtonContainer}>
                    <CustomButton
                        activeOpacity={0.7}
                        backgroundColor={colors.primary_3}
                        customStyle={styles.bottomButton}
                        buttonLabel={i18n.t('cancel')}
                        onPress={onCancelPress}
                    />
                </View>
            </View>
        </CustomModal>
    );
};

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        paddingVertical: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    contentContainer: {
        flex: 1,
        width: wp(90),
        alignSelf: 'center',
        backgroundColor: colors.white,
        borderRadius: 15,
        overflow: 'hidden',
        paddingBottom: 10,
    },
    bottomButton: {
        width: '90%',
        marginTop: 10,
    },
    restContainer: {
        flex: 1,
    },
    bottomButtonContainer: {
        width: '100%',
    },
    headerContainer: {
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomColor: colors.smokeyGrey,
        borderBottomWidth: 1,
        flexDirection: 'row',
        marginHorizontal: 10,
    },
});

export default connect((state) => ({
    generateCoupon: state.users.generateCoupon,
    userData: state.auth.userData,
    saveMyHealthData: state.auth.saveMyHealthData,
    subscriptionDetails: state.subscription.subscriptionDetails,
    calenderSelectedDate: state.subscription.calenderSelectedDate,
}))(ReplaceCarbsModal);
