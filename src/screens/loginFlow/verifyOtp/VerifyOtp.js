import React, {
    useState, useCallback, useMemo, useEffect,
} from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { connect, useDispatch, useSelector } from 'react-redux';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import auth from '@react-native-firebase/auth'
import CustomImage from '../../../components/custom/CustomImage';
import images from '../../../assets/images/images';
import {
    globalPaddingMargin, isRTL, MOBILE_NUMBER_PREFIX, OTP_LENGTH,
} from '../../../constants/constants';
import CustomText from '../../../components/custom/CustomText';
import CustomButton from '../../../components/custom/CustomButton';
import { wp } from '../../../helpers/screenHelper';
import CustomKeyboardAvoidingView from '../../../components/custom/CustomKeyboardAvoidingView';
import colors from '../../../constants/colors';
import i18n from '../../../i18n/i18n';
import { fontsSize, getFontsFamily, getFontsSize } from '../../../assets/fonts/fonts';
import * as Validation from '../../../validation/Validation';
import * as AuthActions from '../../../redux/actions/AuthActions';
import CustomIcon, { IconType } from '../../../components/custom/CustomIcon';
import { navigateGoBack } from '../../../navigation/navigation';
import { getHitSlop } from '../../../helpers/CommonHelpers';
import { getAuthUserDataSelector } from '../../../redux/selectors/SettingsSelectors';
import CustomLoader from '../../../components/custom/CustomLoader';

const VerifyOtp = (props) => {
    const dispatch = useDispatch();
    const authUserData = useSelector(getAuthUserDataSelector)
    const [mobileNumber] = useState(authUserData?.mobileNumber);
    const [otp, setOtp] = useState('');
    useEffect(() => {
        const unsubscribe = auth().onAuthStateChanged((user) => {
            if (user) dispatch(AuthActions.validatingUserAction())
            else dispatch(AuthActions.logoutAction())
        })
        return () => {
            unsubscribe()
        }
    }, [])

    const onVerifyPress = useCallback(() => {
        if (Validation.verifyOtpValidation(otp)) {
            dispatch(AuthActions.verifyOtpAction(otp))
        }
    }, [dispatch, otp])

    const onResendPress = useCallback(() => {
        dispatch(AuthActions.reSendOtpAction(mobileNumber))
        setOtp('');
    }, [dispatch, mobileNumber])

    const onBackPress = useCallback(() => {
        navigateGoBack();
    }, [])

    const headerLeftComponent = useMemo(() => (
        <TouchableOpacity hitSlop={getHitSlop(25)} style={styles.backButtonContainer}>
            <CustomIcon onPress={onBackPress}
                type={IconType.MaterialIcons}
                name={isRTL ? 'arrow-forward-ios' : 'arrow-back-ios'}
                size={16}
                color={colors.primary_2}
            />
        </TouchableOpacity>
    ), [onBackPress])

    // const onCodeFilled = useCallback((code) => {
    //     if (Validation.verifyOtpValidation(code)) dispatch(AuthActions.verifyOtpAction(code))
    // }, [dispatch])

    return (
        <View style={styles.mainContainer}>
            <CustomKeyboardAvoidingView>
                <View style={styles.wrapperContainer}>
                    {headerLeftComponent}
                    {/* Logo */}
                    <View style={styles.logoContainer}>
                        <CustomImage style={{ height: 150, width: 230 }} source={images.macrosLogo} />
                    </View>

                    {/* OTP Verification */}
                    {!props?.verifyOtp?.loading ? <View style={styles.pickPackageContainer}>
                        <CustomText color={colors.primary_2} fontProps={['fs-medium']}>{i18n.t('otp_verification')}</CustomText>

                        {/* Sub Title */}
                        <CustomText color={colors.lightGrey} customStyle={styles.enterOtpPlaceHolder}>
                            {i18n.t('enter_otp', { prefix_mobile_number: MOBILE_NUMBER_PREFIX, mobile_number: mobileNumber })}
                        </CustomText>

                        {/* OTP Input Box */}
                        <OTPInputView
                            style={styles.otpContainer}
                            codeInputFieldStyle={styles.underlineStyleBase}
                            codeInputHighlightStyle={styles.underlineStyleHighLighted}
                            pinCount={OTP_LENGTH}
                            code={otp}
                            onCodeChanged={setOtp}
                            // onCodeFilled={onCodeFilled}
                            autoFocusOnLoad
                        />

                        {/* Resend */}
                        <TouchableOpacity onPress={onResendPress}>
                            <CustomText color={colors.lightGrey}>
                                {i18n.t('didnt_receive_otp')}
                                <CustomText color={colors.primary_2}> {props.sendOtp?.loading ? i18n.t('resending') : i18n.t('resend')}</CustomText>
                            </CustomText>
                        </TouchableOpacity>
                    </View> : (
                        <View style={styles.pickPackageContainer}>
                            <CustomLoader />
                        </View>
                    )}
                </View>
            </CustomKeyboardAvoidingView>
            <CustomButton
                disabled={otp?.length !== OTP_LENGTH || props?.verifyOtp?.loading}
                customStyle={styles.bottomButton}
                onPress={onVerifyPress}
                buttonLabel={props?.verifyOtp?.loading ? i18n.t('verifying') : i18n.t('verify_and_continue')}
            />
        </View>
    )
}
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    wrapperContainer: {
        flex: 1,
        marginTop: globalPaddingMargin.vertical * 2,
        alignItems: 'center',
    },
    logoContainer: {
        alignItems: 'center',
        padding: globalPaddingMargin.whole,
        marginTop: globalPaddingMargin.vertical,
    },
    pickPackageContainer: {
        marginTop: globalPaddingMargin.vertical * 2,
        alignItems: 'center',
        width: wp(80),
    },
    bottomButton: {
        marginVertical: globalPaddingMargin.vertical,
        marginBottom: globalPaddingMargin.vertical * 2,
    },
    enterOtpPlaceHolder: {
        marginTop: globalPaddingMargin.top,
    },
    underlineStyleBase: {
        color: colors.black,
        padding: 0,
        fontFamily: getFontsFamily(),
        fontSize: getFontsSize(fontsSize.medium),
        borderRadius: 5,
        backgroundColor: colors.white,
        borderColor: colors.primary_1,
        borderWidth: 1,
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    otpContainer: {
        width: '100%',
        height: 80,
    },
    underlineStyleHighLighted: {
        borderColor: colors.primary_2,
    },
    backButtonContainer: {
        alignSelf: 'flex-start',
    },
})
export default connect((state) => ({
    appLanguage: state.settings.appLanguage,
    verifyOtp: state.auth.verifyOtp,
    sendOtp: state.auth.sendOtp,
}))(VerifyOtp);
