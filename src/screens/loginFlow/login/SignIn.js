import React, { useCallback, useMemo, useState } from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { connect, useDispatch } from 'react-redux';
import CustomImage from '../../../components/custom/CustomImage';
import images from '../../../assets/images/images';
import {
    globalPaddingMargin,
    MOBILE_NUMBER_LENGTH,
    MOBILE_NUMBER_PREFIX,
    appTextInputType,
} from '../../../constants/constants';
import CustomText from '../../../components/custom/CustomText';
import CustomButton from '../../../components/custom/CustomButton';
import CustomTextInput from '../../../components/custom/CustomTextInput';
import { wp } from '../../../helpers/screenHelper';
import CustomKeyboardAvoidingView from '../../../components/custom/CustomKeyboardAvoidingView';
import CustomIcon from '../../../components/custom/CustomIcon';
import colors from '../../../constants/colors';
import { navigateGoBack } from '../../../navigation/navigation';
import i18n from '../../../i18n/i18n';
import { sendOtpAction } from '../../../redux/actions/AuthActions';

const SignIn = (props) => {
    const dispatch = useDispatch();
    const [mobileNumber, setMobileNumber] = useState(
        props?.route?.params?.mobileNumber,
    );
    const [loading, setLoading] = useState(false);

    const onSignInPress = useCallback(async () => {
        setLoading(true);
        dispatch(sendOtpAction(mobileNumber)).finally(() => {
            setLoading(false);
        });
    }, [dispatch, mobileNumber]);

    const MobileNumberLeftComponent = useMemo(
        () => (
            <CustomText fontProps={['ff-roboto-bold', 'fs-xmedium']}>
                {MOBILE_NUMBER_PREFIX}
            </CustomText>
        ),
        [],
    );

    const MobileNumberRightComponent = useMemo(
        () => mobileNumber?.length === MOBILE_NUMBER_LENGTH && (
            <CustomIcon size={17} name="checkcircle" color={colors.primary_3} />
        ),
        [mobileNumber?.length],
    );

    const onBottomSubTitlePress = useCallback(() => {
        navigateGoBack();
    }, []);
    return (
        <View style={styles.mainContainer}>
            <CustomKeyboardAvoidingView>
                <View View style={styles.wrapperContainer}>
                    {/* Logo */}
                    <View View style={styles.logoContainer}>
                        <CustomImage
                            style={{ height: 150, width: 230 }}
                            source={images.macrosLogo}
                        />
                    </View>

                    {/* Pick Your Language */}
                    <View style={styles.pickPackageContainer}>
                        <CustomText fontProps={['ff-roboto-thin', 'fs-xlarge']}>
                            {i18n.t('sign_in')}
                        </CustomText>
                        <CustomTextInput
                            textInputType={appTextInputType.NUMBER}
                            value={mobileNumber}
                            onChangeText={setMobileNumber}
                            maxLength={MOBILE_NUMBER_LENGTH}
                            keyboardType={'number-pad'}
                            customContainerStyle={styles.textInputContainer}
                            placeHolder={i18n.t('enter_mobile_number')}
                            returnKeyType={'done'}
                            leftComponent={MobileNumberLeftComponent}
                            rightComponent={MobileNumberRightComponent}
                        />
                    </View>
                    <CustomText color={colors.lightGrey}>
                        {i18n.t('otp_will_send')}
                    </CustomText>
                </View>
            </CustomKeyboardAvoidingView>
            <View style={styles.bottomMainContainer}>
                <CustomButton
                    customStyle={styles.bottomButton}
                    onPress={onSignInPress}
                    buttonLabel={loading ? i18n.t('sending_otp') : i18n.t('sign_in')}
                    disabled={loading}
                />
                <TouchableOpacity onPress={onBottomSubTitlePress}>
                    <CustomText
                        customStyle={styles.bottomSubTitle}
                        color={colors.lightGrey}>
                        {i18n.t('new_user')}
                        <CustomText
                            fontProps={['fs-medium', 'ff-roboto-medium']}
                            color={colors.primary_3}>
                            {' '}
                            {i18n.t('sign_up')}
                        </CustomText>
                    </CustomText>
                </TouchableOpacity>
            </View>
        </View>
    );
};
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    wrapperContainer: {
        flex: 1,
        marginTop: globalPaddingMargin.vertical,
        alignItems: 'center',
    },
    logoContainer: {
        alignItems: 'center',
        padding: globalPaddingMargin.whole,
        marginTop: globalPaddingMargin.vertical,
    },
    pickPackageContainer: {
        marginTop: globalPaddingMargin.vertical,
        alignItems: 'center',
        width: wp(80),
    },
    bottomMainContainer: {
        marginVertical: globalPaddingMargin.vertical,
    },
    bottomButton: {
        marginBottom: 10,
    },
    textInputContainer: {
        marginVertical: globalPaddingMargin.vertical,
    },
    bottomSubTitle: {
        alignSelf: 'flex-end',
    },
});
export default connect((state) => ({
    appLanguage: state.settings.appLanguage,
    sendOtp: state.auth.sendOtp,
}))(SignIn);
