import React, {
    useCallback, useMemo, useState, useEffect,
} from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { connect, useDispatch } from 'react-redux';
import CustomImage from '../../../components/custom/CustomImage';
import images from '../../../assets/images/images';
import {
    globalPaddingMargin,
    MOBILE_NUMBER_LENGTH,
    MOBILE_NUMBER_PREFIX,
    appTextInputType,
} from '../../../constants/constants';
import CustomText from '../../../components/custom/CustomText';
import CustomButton from '../../../components/custom/CustomButton';
import screens from '../../../navigation/screens';
import CustomTextInput from '../../../components/custom/CustomTextInput';
import { wp } from '../../../helpers/screenHelper';
import CustomKeyboardAvoidingView from '../../../components/custom/CustomKeyboardAvoidingView';
import CustomIcon from '../../../components/custom/CustomIcon';
import colors from '../../../constants/colors';
import { navigateTo, navigationType } from '../../../navigation/navigation';
import i18n from '../../../i18n/i18n';
import CustomPickerTwo from '../../../components/custom/CustomPickerTwo';
import usePicker from '../../../hooks/usePicker';
import { signUpAction } from '../../../redux/actions/AuthActions';
import ChangeLanguage from '../../../components/changeLanguage/ChangeLanguage';
import {
    getAreasAction,
    resetStore,
} from '../../../redux/actions/SettingsActions';

const SignUp = (props) => {
    const dispatch = useDispatch();
    const [mobileNumber, setMobileNumber] = useState('');
    const [name, setName] = useState('');
    const [area, bindArea] = usePicker();

    useEffect(() => {
        dispatch(resetStore());
        dispatch(getAreasAction());
    }, []);

    const onSignUpPress = () => {
        dispatch(signUpAction({ mobileNumber, name, area }));

        return null;
    };
    const MobileNumberLeftComponent = useMemo(
        () => (
            <CustomText fontProps={['ff-roboto-bold', 'fs-xmedium']}>
                {MOBILE_NUMBER_PREFIX}
            </CustomText>
        ),
        [],
    );

    const MobileNumberRightComponent = useMemo(
        () => mobileNumber?.length === MOBILE_NUMBER_LENGTH && (
            <CustomIcon size={17} name="checkcircle" color={colors.primary_3} />
        ),
        [mobileNumber?.length],
    );

    const onBottomSubTitlePress = useCallback(() => {
        navigateTo(screens.signin, navigationType.replace);
    }, []);

    const areasData = useMemo(
        () => (props.areas?.data?.length > 0
            ? props.areas?.data?.map((areaItem) => ({
                key: areaItem?.id,
                label: areaItem?.name,
            }))
            : []),
        [props.areas?.data],
    );

    const renderLanguageSwitchButton = useMemo(
        () => <ChangeLanguage style={styles.languageSelectorContainer} />,
        [],
    );

    return (
        <View style={styles.mainContainer}>
            <CustomKeyboardAvoidingView>
                <View style={styles.wrapperContainer}>
                    {/* Logo */}
                    <View style={styles.logoContainer}>
                        <CustomImage
                            style={{ height: 150, width: 230 }}
                            source={images.macrosLogo}
                        />
                    </View>

                    {/* Pick Your Language */}
                    <View style={styles.pickPackageContainer}>
                        <CustomText fontProps={['ff-roboto-thin', 'fs-xlarge']}>
                            {i18n.t('sign_up')}
                        </CustomText>
                        <CustomTextInput
                            value={name}
                            onChangeText={setName}
                            customContainerStyle={styles.textInputContainer}
                            placeHolder={i18n.t('enter_name')}
                            returnKeyType={'done'}
                        />
                        <CustomPickerTwo
                            {...bindArea}
                            data={areasData}
                            customContainerStyle={styles.textInputContainer}
                            placeHolder={i18n.t('select_area')}
                        />
                        <CustomTextInput
                            textInputType={appTextInputType.NUMBER}
                            value={mobileNumber}
                            onChangeText={setMobileNumber}
                            maxLength={MOBILE_NUMBER_LENGTH}
                            keyboardType={'number-pad'}
                            customContainerStyle={styles.textInputContainer}
                            placeHolder={i18n.t('enter_mobile_number')}
                            returnKeyType={'done'}
                            leftComponent={MobileNumberLeftComponent}
                            rightComponent={MobileNumberRightComponent}
                        />
                    </View>
                    <CustomText color={colors.lightGrey} customStyle={styles.otpWillSend}>
                        {i18n.t('otp_will_send')}
                    </CustomText>
                </View>
                <View style={styles.bottomMainContainer}>
                    <CustomButton
                        customStyle={styles.bottomButton}
                        onPress={onSignUpPress}
                        buttonLabel={
                            props?.sendOtp?.loading
                                ? i18n.t('sending_otp')
                                : i18n.t('sign_up')
                        }
                        disabled={props?.sendOtp?.loading}
                    />
                    <TouchableOpacity onPress={onBottomSubTitlePress}>
                        <CustomText
                            customStyle={styles.bottomSubTitle}
                            color={colors.lightGrey}>
                            {i18n.t('already_a_user')}
                            <CustomText
                                fontProps={['fs-medium', 'ff-roboto-medium']}
                                color={colors.primary_3}>
                                {' '}
                                {i18n.t('sign_in')}
                            </CustomText>
                        </CustomText>
                    </TouchableOpacity>
                </View>
            </CustomKeyboardAvoidingView>
            {renderLanguageSwitchButton}
        </View>
    );
};
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    languageSelectorContainer: {
        position: 'absolute',
        top: 15,
        left: 15,
    },
    wrapperContainer: {
        flex: 1,
        marginTop: globalPaddingMargin.vertical,
        alignItems: 'center',
    },
    logoContainer: {
        alignItems: 'center',
        padding: globalPaddingMargin.whole,
        marginTop: globalPaddingMargin.vertical,
    },
    pickPackageContainer: {
        marginTop: globalPaddingMargin.vertical,
        alignItems: 'center',
        width: wp(80),
    },
    bottomMainContainer: {
        marginTop: 35,
        marginVertical: globalPaddingMargin.vertical,
    },
    bottomButton: {
        marginBottom: 10,
    },
    textInputContainer: {
        marginTop: 20,
    },
    bottomSubTitle: {
        alignSelf: 'flex-end',
    },
    otpWillSend: {
        marginTop: 10,
    },
});
export default connect((state) => ({
    appLanguage: state.settings.appLanguage,
    areas: state.settings.areas,
    sendOtp: state.auth.sendOtp,
}))(SignUp);
