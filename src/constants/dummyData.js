import images from '../assets/images/images'

export const dailyActivityDummyData = [
    { key: 1, label: 'Low' },
    { key: 2, label: 'Medium' },
    { key: 3, label: 'High' },
]

export const workoutPerWeekDummyData = [
    { key: 1, label: '1 to 3 Days' },
    { key: 2, label: '3 to 5 Days' },
    { key: 3, label: '6 to 7 Days' },
]

export const workoutIntensityDummyData = [
    { key: 1, label: 'Low' },
    { key: 2, label: 'Medium' },
    { key: 3, label: 'High' },
]