const colors = {
    transparent: 'transparent',
    primary_1: '#FBB33B',
    primary_2: '#32B0BF',
    primary_3: '#F37068',
    light_primary_3: '#ed9a95',

    white: '#FFFFFF',
    overlayWhite: 'rgba(255,255,255,0.5)',

    ghostWhite: '#F7F7FB',
    desertWhite: '#F7F7F7',

    red: '#FF0000',
    beanRed: '#F26163',

    green: '#00FF00',
    whatsappGreen: '#128C7E',
    successGreen: '#4BB543',
    tawkToGreen: '#19A752',

    blue: '#0000FF',
    navyBlue: '#000080',

    black: '#000000',
    lightBlack: '#272833',
    shadowBlack: '#8D979E',
    mirageBlack: '#1D1E2C',

    // appBackground: '#FAFAFA',
    appBackground: '#EFEFEF',

    skeletonGrey: '#E3E3E3',
    borderColor: '#8A9199',
    lightGrey: '#8D8D8D',
    thinGrey: '#7D8699',
    xLightGrey: '#D3D3D3',
    charcolGrey: '#353B48',
    smokeyGrey: '#707070',
    mercury: '#E5E5E5',
};

export default colors
