import { I18nManager, Platform } from 'react-native';
import colors from './colors';

/**
 * Dev Config
 * Developer Mode
 * @enum = true | false
 */
export const DEVELOPER_MODE = __DEV__
export const IS_INDIAN_NUMBER = false
export const ENABLE_CONSOLE_LOGS = DEVELOPER_MODE;

/**
 * App Center Config
 */
export const ENABLE_CODE_PUSH = true
export const CODE_PUSH_STAGING_MODE = false
export const AppCenterConfig = {
    CODEPUSH_DEPLOYMENT_KEY: CODE_PUSH_STAGING_MODE
        ? Platform.OS === 'ios' ? '6nmxZVXSpEVeh_XWOI2dhjSlvSgKRnXoK8Yuw' : 'qoDel2mVImiquUUJ0lLrnUh0UjcfSWm68lgdb'
        : Platform.OS === 'ios' ? '4JFObxJrNsFuSIUvax-0EYuQKt9SVGlvauO9e' : 'eWTksa1z6_MIW2TMVcWQdXmuv82oYwU5nf5ll',
}

/**
 * App Configs
 */
export const WHATSAPP_CHAT_LINK = 'https://api.whatsapp.com/send/?phone=96599997371&text&app_absent=0';
export const INSTAGRAM_LINK = 'https://instagram.com/macrosq8?r=nametag';
export const APP_CURRENCY = 'KD';
export const ANDROID_CHANNEL_ID = 'macros.local_notification';
export const NO_OF_DECIMAL_POINTS_IN_AMOUNT = 2;
export const isRTL = I18nManager.isRTL;
export const isIOS = Platform.OS === 'ios';
export const isAndroid = Platform.OS === 'android';
export const globalPaddingMargin = {
    whole: 25,
    vertical: 25,
    horizontal: 25,
    left: 10,
    top: 10,
    right: 10,
    bottom: 10,
};
export const appTextInputType = {
    NUMBER: 'number',
    TEXT: 'text',
}
export const STORE_LINK = isIOS ? 'https://apps.apple.com/in/app/macros/id1615195723' : 'https://play.google.com/store/apps/details?id=com.macros';
/**
 * My Fatoorah Online Payment Configs
 */
export const ALLOW_ONLINE_PAYMENT = !DEVELOPER_MODE;
export const MY_FATOORAH_IS_LIVE_ACCOUNT = !DEVELOPER_MODE;
export const MY_FATOORAH_API_BASE_URL = MY_FATOORAH_IS_LIVE_ACCOUNT ? 'https://api.myfatoorah.com' : 'https://apitest.myfatoorah.com';
export const MY_FATOORAH_API_TOKEN = MY_FATOORAH_IS_LIVE_ACCOUNT ? '96srLSaEHg7Gsckp05ljaDX-2o6nL9m-76EEO2SvcCTCgP8k-ORymSVIX3Hp1RSna_VbrUYbjHlf9paQPTeverZCAU0kHrmXZ5LUhv0LeZFLUuQKNV408A50YfhQ0RWjMRhdPjgtMU-4CTi_GnSowg9Oz3XonCQtiFIJv868IOXc5-vxJ8s9eQUDGS-0mCyx0fKbUijtJuZrBR23ztg3SkKK-Olv9LFdZMoCVrudeYjYLovtVySaU9MDw7zvka_0uZxkAT6CuDZgR4NVLth8YjC65j-y8M4EM8rLUeG0ULOk4EgNAt8VoETZQ8o742BkU2ZG9P3kv_7EuX-4sdGBkuVQg-wqCBF_FtWuWJLkWRSbZta8hAIF30P7QHcLWeFCckRpPBiI_DsK3D7Jj86RyT1si4z9OtgUPfFKQFQ0ourTxgpwAnthKYzx1PBbVqqQwI-nrGM7WzBZGFKxjN9mqAK00FUVCcwonD29nRrQJ-ywwItdeoeJkRyGop4HoA2mp2w994NWWYOZ6XvS5MSkZKpcuzoHphPUnKW1WNvk_G46AuqmNshiVDmf3FxGZuO7Dvs_cGpNCzkHaGQVxt8B91jNd2U1kXbTyNHlcqArO5CLV2ZGblcvn0lofPWMP79n6lV9mf2g7jtVPIFDpUg6W9B4rNZf-YeVBGMAgndzXRJmI3fYiTDnZyHPp2NQI4Pgqiuahw' : 'rLtt6JWvbUHDDhsZnfpAhpYk4dxYDQkbcPTyGaKp2TYqQgG7FGZ5Th_WD53Oq8Ebz6A53njUoo1w3pjU1D4vs_ZMqFiz_j0urb_BH9Oq9VZoKFoJEDAbRZepGcQanImyYrry7Kt6MnMdgfG5jn4HngWoRdKduNNyP4kzcp3mRv7x00ahkm9LAK7ZRieg7k1PDAnBIOG3EyVSJ5kK4WLMvYr7sCwHbHcu4A5WwelxYK0GMJy37bNAarSJDFQsJ2ZvJjvMDmfWwDVFEVe_5tOomfVNt6bOg9mexbGjMrnHBnKnZR1vQbBtQieDlQepzTZMuQrSuKn-t5XZM7V6fCW7oP-uXGX-sMOajeX65JOf6XVpk29DP6ro8WTAflCDANC193yof8-f5_EYY-3hXhJj7RBXmizDpneEQDSaSz5sFk0sV5qPcARJ9zGG73vuGFyenjPPmtDtXtpx35A-BVcOSBYVIWe9kndG3nclfefjKEuZ3m4jL9Gg1h2JBvmXSMYiZtp9MR5I6pvbvylU_PP5xJFSjVTIz7IQSjcVGO41npnwIxRXNRxFOdIUHn0tjQ-7LwvEcTXyPsHXcMD8WtgBh-wxR8aKX7WPSsT1O8d8reb2aR7K3rkV3K82K_0OgawImEpwSvp9MNKynEAJQS6ZHe_J_l77652xwPNxMRTMASk1ZsJL';

/**
 * Firebase Mobile Number OTP and Default Country and Digit for Mobile Number Configs
 */
export const MOBILE_NUMBER_PREFIX = IS_INDIAN_NUMBER ? '+91' : '+965' // +965
export const MOBILE_NUMBER_LENGTH = IS_INDIAN_NUMBER ? 10 : 8 // 8
export const FIREBASE_SEND_OTP_ENABLED = !DEVELOPER_MODE;
export const FIREBASE_APP_VERIFICATION_DISABLED_FOR_TESTING = DEVELOPER_MODE;
export const DEFAULT_OTP = 123456;
export const OTP_LENGTH = 6;

/**
 * Maps Configs
 */
export const DEFAULT_COORDINATE = {
    latitude: 29.3239607,
    longitude: 48.0503343,
    latitudeDelta: 0.002,
    longitudeDelta: 0.002,
}

/**
 * Subscription And Calender Configs
 */
export const PLAN_MINIMUM_EXPIRE_DAYS = 5;
export const MINIMUM_START_DATE_DURATION_DAYS = 1;
export const WEEK_DAYS = {
    SUNDAY: 0,
    MONDAY: 1,
    TUESDAY: 2,
    WEDNESDAY: 3,
    THURSDAY: 4,
    FRIDAY: 5,
    SATURDAY: 6,
}
export const EVENT_OFF_DAY = WEEK_DAYS.FRIDAY;
export const PLAN_TYPE = {
    REGULAR: 'REGULAR',
    CUSTOM_GRAMS: 'CUSTOM_GRAMS',
}
export const calenderDayType = {
    noPlanExistDay: { key: 'planNotExist' },
    noMealSelectedDay: { key: 'noMealSelectedDay' },
    currentSelection: { key: 'currentSelection', name: 'Current Selection', color: colors.primary_3 },
    selectedDay: { key: 'selectedDay', name: 'Meal Selected', color: colors.primary_1 },
    timeGoesForSelection: { key: 'timeGoesForSelection' },
    pausedDay: { key: 'pausedDay', name: 'Paused Day', color: colors.primary_2 },
    offDay: { key: 'offDay', name: 'Off Day', color: colors.xLightGrey },
}
export const calenderDayIndicator = [
    calenderDayType.selectedDay,
    calenderDayType.pausedDay,
    calenderDayType.offDay,
]

export const diatCitationLinks = [
    { title: 'https://www.nutritionvalue.org', link: 'https://www.nutritionvalue.org' },
    { title: 'https://data.nal.usda.gov', link: 'https://data.nal.usda.gov/dataset/usda-national-nutrient-database-standard-reference-legacy-release' },
    { title: 'https://www.ars.usda.gov', link: 'https://www.ars.usda.gov/is/np/NutritiveValueofFoods/NutritiveValueofFoods.pdf' },
    { title: 'http://toolbox.foodcomp.info', link: 'http://toolbox.foodcomp.info/ToolBox_Atwater.asp' },
]
