import AsyncStorage from '@react-native-async-storage/async-storage'
import immutablePersistenceTransform from './ImmutablePersistenceTransform'

const ReduxPersistConfig = {
    key: 'primary',
    timeout: 0,
    storage: AsyncStorage,
    // Reducer keys that you do NOT want stored to persistence here.
    whitelist: ['auth', 'settings', 'subscription'],
    // Optionally, just specify the keys you DO want stored to persistence.
    // An empty array means 'don't store any reducers' -> infinitered/ignite#409
    transforms: [immutablePersistenceTransform],
}

export default ReduxPersistConfig
