import { AuthInitialState } from '../reducers/AuthReducer';
import { SettingsInitialState } from '../reducers/SettingsReducer';
import { SubscriptionInitialState } from '../reducers/SubscriptionReducer';
import { UsersInitialState } from '../reducers/UsersReducer';

const AppDefaultReducer = {
    auth: AuthInitialState,
    settings: SettingsInitialState,
    users: UsersInitialState,
    subscription: SubscriptionInitialState,
};

export default AppDefaultReducer;
