import {
    applyMiddleware, createStore,
} from 'redux';
import thunk from 'redux-thunk';
import { logger } from 'redux-logger';
import { persistStore, persistReducer } from 'redux-persist';
import ReduxPersistConfig from './persist/ReduxPersistConfig';
import rootReducer from './reducers/rootReducer';
import { ENABLE_CONSOLE_LOGS } from '../constants/constants';

const middlewares = [thunk];

if (process.env.NODE_ENV === 'development') {
    ENABLE_CONSOLE_LOGS && middlewares.push(logger);
}

const middleware = applyMiddleware(...middlewares);

const PERSIST_REDUCER = persistReducer(ReduxPersistConfig, rootReducer);
const store = createStore(PERSIST_REDUCER, middleware);
export const persistor = persistStore(store);

export default store;
