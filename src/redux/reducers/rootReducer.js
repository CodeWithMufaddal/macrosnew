import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import SettingsReducer from './SettingsReducer';
import { RESET_STORE } from '../types/ReduxTypes';
import AppDefaultReducer from '../defaults/AppDefaultReducer';
import UsersReducer from './UsersReducer';
import SubscriptionReducer from './SubscriptionReducer';
import NotificationsReducer from './NotificationsReducer';

const appReducer = combineReducers({
    auth: AuthReducer,
    settings: SettingsReducer,
    users: UsersReducer,
    subscription: SubscriptionReducer,
    notifications: NotificationsReducer,
});

export default function rootReducer(state, action) {
    let finalState = appReducer(state, action);
    if (action.type === RESET_STORE) {
        const defaultReducer = { ...AppDefaultReducer, settings: finalState.settings };
        finalState = defaultReducer;
    }
    return finalState;
}
