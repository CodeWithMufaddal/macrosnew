import * as ReduxTypes from '../types/ReduxTypes';

export const NotificationsInitialState = {
    notificationList: [],
    notificationUnreadCount: 0,
};

const NotificationsReducer = (state = NotificationsInitialState, action) => {
    switch (action.type) {
        /**
             * Set Notifications List
             */
        case ReduxTypes.ADD_NOTIFICATION: {
            return {
                ...state,
                notificationList: action.payload,
                notificationUnreadCount: state?.notificationUnreadCount + 1,
            }
        }
        case ReduxTypes.SET_NOTIFICATION_LIST: {
            return {
                ...state,
                notificationList: action.payload,
            }
        }
        case ReduxTypes.SET_NOTIFICATIONS_UNREAD_COUNT: {
            return {
                ...state,
                notificationUnreadCount: action.payload,
            }
        }
        /**
         * Defaults
         */
        default: {
            return state;
        }
    }
}

export default NotificationsReducer;
