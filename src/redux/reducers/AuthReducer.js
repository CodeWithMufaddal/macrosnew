import * as ReduxTypes from '../types/ReduxTypes';

export const AuthInitialState = {
    userData: {},
    isNewUser: false,
    tokenData: {},
    saveInbodySheet: { loading: false },
    saveMyHealthData: { loading: false },
    saveAddress: { loading: false },
    saveMyProfile: { loading: false },
    getUserData: { loading: false },
    verifyOtp: { loading: false },
    sendOtp: { loading: false },
};

const AuthReducer = (state = AuthInitialState, action) => {
    switch (action.type) {
    /**
         * Send OTP
         */
        case ReduxTypes.LOGIN_SEND_OTP_REQUEST: {
            return { ...state, sendOtp: { loading: true } }
        }
        case ReduxTypes.LOGIN_SEND_OTP_SUCCESS:
        case ReduxTypes.LOGIN_SEND_OTP_ERROR: {
            return { ...state, sendOtp: { loading: false } }
        }
        /**
     * Verify OTP
    */
        case ReduxTypes.VERIFY_OTP_REQUEST: {
            return { ...state, verifyOtp: { loading: true } }
        }
        case ReduxTypes.VERIFY_OTP_SUCCESS: {
            return {
                ...state,
                verifyOtp: { loading: false },
                userData: action.payload,
                tokenData: { token: action.payload.token },
            }
        }
        case ReduxTypes.VERIFY_OTP_ERROR: {
            return { ...state, verifyOtp: { loading: false } }
        }
        /**
     * Set Is New User
     */
        case ReduxTypes.SET_IS_NEW_USER: {
            return { ...state, isNewUser: action.payload }
        }
        /**
     * Get Referral Code and Balance
     */
        case ReduxTypes.GET_REFERRAL_CODE_AND_BALANCE_SUCCESS: {
            return { ...state, userData: { ...state.userData, ...action.payload } }
        }
        /**
     * Save Inbody Sheet
     */
        case ReduxTypes.SAVE_INBODY_SHEET_REQUEST: {
            return { ...state, saveInbodySheet: { loading: true } }
        }
        case ReduxTypes.SAVE_INBODY_SHEET_SUCCESS:
        case ReduxTypes.SAVE_INBODY_SHEET_ERROR: {
            return { ...state, saveInbodySheet: { loading: false } }
        }

        /**
     * Save My Health Data
     */
        case ReduxTypes.SAVE_MY_HEALTH_DATA_REQUEST: {
            return { ...state, saveMyHealthData: { loading: true } }
        }
        case ReduxTypes.SAVE_MY_HEALTH_DATA_SUCCESS:
        case ReduxTypes.SAVE_MY_HEALTH_DATA_ERROR: {
            return { ...state, saveMyHealthData: { loading: false } }
        }

        /**
     * Save Address
     */
        case ReduxTypes.SAVE_ADDRESS_REQUEST: {
            return { ...state, saveAddress: { loading: true } }
        }
        case ReduxTypes.SAVE_ADDRESS_SUCCESS:
        case ReduxTypes.SAVE_ADDRESS_ERROR: {
            return { ...state, saveAddress: { loading: false } }
        }

        /**
    * Save My Profile
    */
        case ReduxTypes.SAVE_MY_PROFILE_REQUEST: {
            return { ...state, saveMyProfile: { loading: true } }
        }
        case ReduxTypes.SAVE_MY_PROFILE_SUCCESS:
        case ReduxTypes.SAVE_MY_PROFILE_ERROR: {
            return { ...state, saveMyProfile: { loading: false } }
        }
        /**
     * Get User Profile
    */
        case ReduxTypes.GET_USER_PROFILE_REQUEST:
        case ReduxTypes.GET_USER_INBODY_SHEET_REQUEST: {
            return { ...state, getUserData: { loading: true } }
        }
        case ReduxTypes.GET_USER_PROFILE_SUCCESS:
        case ReduxTypes.GET_USER_INBODY_SHEET_SUCCESS: {
            return {
                ...state,
                getUserData: { loading: false },
                userData: { ...state.userData, ...action.payload },
            }
        }
        case ReduxTypes.GET_USER_PROFILE_ERROR:
        case ReduxTypes.GET_USER_INBODY_SHEET_ERROR: {
            return { ...state, getUserData: { loading: false } }
        }

        /**
     * Defaults
     */
        default: {
            return state;
        }
    }
}

export default AuthReducer;
