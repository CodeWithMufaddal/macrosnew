import * as ReduxTypes from '../types/ReduxTypes';

export const UsersInitialState = {
    generateCoupon: { loading: false },
    referralHistory: { loading: false, data: [] },
};

const UsersReducer = (state = UsersInitialState, action) => {
    switch (action.type) {
    /**
         * Generate Coupon
         */
    case ReduxTypes.GENERATE_COUPON_REQUEST: {
        return { ...state, generateCoupon: { loading: true } }
    }
    case ReduxTypes.GENERATE_COUPON_SUCCESS:
    case ReduxTypes.GENERATE_COUPON_ERROR: {
        return { ...state, generateCoupon: { loading: false } }
    }

    /**
     * Get Referral History
     */
    case ReduxTypes.GET_REFERRAL_HISTORY_REQUEST: {
        return { ...state, referralHistory: { loading: true, data: [] } }
    }
    case ReduxTypes.GET_REFERRAL_HISTORY_SUCCESS: {
        return { ...state, referralHistory: { loading: false, data: action.payload } }
    }
    case ReduxTypes.GET_REFERRAL_HISTORY_ERROR: {
        return { ...state, referralHistory: { loading: false, data: [] } }
    }
    default: {
        return state;
    }
    }
}

export default UsersReducer;
