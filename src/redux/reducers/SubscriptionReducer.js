import * as ReduxTypes from '../types/ReduxTypes';
const initialMacrosCount = {
    protein: 0,
    carbohydrates: 0,
    calories: 0,
    fats: 0
}
export const SubscriptionInitialState = {
    subscriptionDetails: { loading: false, data: null },
    goals: { loading: false, data: [] },
    allMealTypes: { loading: false, data: [] },
    allMealsByMealTypeId: { loading: false, data: [] },
    mealTypesByGoalId: { loading: false, data: [] },
    numberOfDays: { loading: false, data: [] },
    plansByDays: { loading: false, data: [] },
    myCalender: { loading: false, data: null },
    pausedOrResumePackage: { loading: false },
    selectPackageMeals: { loading: false },
    calenderRange: null,
    calenderSelectedDate: null,
    calenderSelectedDayType: null,
    selectedMealsByDate: { loading: false, data: null },
    selectedMealsByMealTypeIdAndDate: { loading: false, data: null },
    selectedMealsByMealTypeIdAndDate: { loading: false, data: null },
    totalMacros: initialMacrosCount
};

const SubscriptionReducer = (state = SubscriptionInitialState, action) => {
    switch (action.type) {
        /**
                     * Get Goals
                     */
        case ReduxTypes.GET_GOALS_REQUEST: {
            return {
                ...state,
                goals: { loading: true, data: [] },
            }
        }
        case ReduxTypes.GET_GOALS_SUCCESS: {
            return {
                ...state,
                goals: { loading: false, data: action.payload },
            }
        }
        case ReduxTypes.GET_GOALS_ERROR: {
            return {
                ...state,
                goals: { loading: false, data: [] },
            }
        }

        /**
                     * Get All Meal Types
                     */
        case ReduxTypes.GET_ALL_MEAL_TYPES_REQUEST: {
            return {
                ...state,
                allMealTypes: { ...state.allMealTypes, loading: true },
            }
        }
        case ReduxTypes.GET_ALL_MEAL_TYPES_SUCCESS: {
            return {
                ...state,
                allMealTypes: { loading: false, data: action.payload },
            }
        }
        case ReduxTypes.GET_ALL_MEAL_TYPES_ERROR: {
            return {
                ...state,
                allMealTypes: { loading: false, data: [] },
            }
        }

        /**
                 * Get All Meals By Meal Type ID
                 */
        case ReduxTypes.GET_ALL_MEALS_BY_MEALTYPEID_REQUEST: {
            return {
                ...state,
                allMealsByMealTypeId: { loading: true, data: [] },
            }
        }
        case ReduxTypes.GET_ALL_MEALS_BY_MEALTYPEID_SUCCESS: {
            return {
                ...state,
                allMealsByMealTypeId: { loading: false, data: action.payload },
            }
        }
        case ReduxTypes.GET_ALL_MEALS_BY_MEALTYPEID_ERROR: {
            return {
                ...state,
                allMealsByMealTypeId: { loading: false, data: [] },
            }
        }

        /**
                     * Get Meal Types By Goal ID
                     */
        case ReduxTypes.GET_MEAL_TYPES_BY_GOAL_ID_REQUEST: {
            return {
                ...state,
                mealTypesByGoalId: { loading: true, data: [] },
            }
        }
        case ReduxTypes.GET_MEAL_TYPES_BY_GOAL_ID_SUCCESS: {
            return {
                ...state,
                mealTypesByGoalId: { loading: false, data: action.payload },
            }
        }
        case ReduxTypes.GET_MEAL_TYPES_BY_GOAL_ID_ERROR: {
            return {
                ...state,
                mealTypesByGoalId: { loading: false, data: [] },
            }
        }

        /**
                     * Get Number Of Days
                     */
        case ReduxTypes.GET_NUMBER_OF_DAYS_REQUEST: {
            return {
                ...state,
                numberOfDays: { loading: true, data: [] },
            }
        }
        case ReduxTypes.GET_NUMBER_OF_DAYS_SUCCESS: {
            return {
                ...state,
                numberOfDays: { loading: false, data: action.payload },
            }
        }
        case ReduxTypes.GET_NUMBER_OF_DAYS_ERROR: {
            return {
                ...state,
                numberOfDays: { loading: false, data: [] },
            }
        }

        /**
                     * Get Plans By Days
                     */
        case ReduxTypes.GET_PLANS_BY_DAYS_REQUEST: {
            return {
                ...state,
                plansByDays: { loading: true, data: [] },
            }
        }
        case ReduxTypes.GET_PLANS_BY_DAYS_SUCCESS: {
            return {
                ...state,
                plansByDays: { loading: false, data: action.payload },
            }
        }
        case ReduxTypes.GET_PLANS_BY_DAYS_ERROR: {
            return {
                ...state,
                plansByDays: { loading: false, data: [] },
            }
        }

        /**
                 * My Calender
                 */
        case ReduxTypes.GET_MY_CALENDER_DATA_REQUEST: {
            return {
                ...state,
                myCalender: { ...state.myCalender, loading: true },
            }
        }
        case ReduxTypes.GET_MY_CALENDER_DATA_SUCCESS: {
            return {
                ...state,
                myCalender: { loading: false, data: action.payload },
            }
        }
        case ReduxTypes.GET_MY_CALENDER_DATA_ERROR: {
            return {
                ...state,
                myCalender: { loading: false, data: null },
            }
        }

        /**
             * Get Subscription Details
             */
        case ReduxTypes.GET_SUBSCRIPTION_DETAILS_REQUEST: {
            return {
                ...state,
                subscriptionDetails: { ...state.subscriptionDetails, loading: true },
            }
        }
        case ReduxTypes.GET_SUBSCRIPTION_DETAILS_SUCCESS: {
            return {
                ...state,
                subscriptionDetails: { loading: false, data: action.payload },
            }
        }
        case ReduxTypes.GET_SUBSCRIPTION_DETAILS_ERROR: {
            return {
                ...state,
                subscriptionDetails: { loading: false, data: null },
            }
        }
        case ReduxTypes.SAVE_MY_GRAMS_REQUEST: {
            return {
                ...state,
                saveMyGrams: { loading: true },
            }
        }
        case ReduxTypes.SAVE_MY_GRAMS_SUCCESS:
        case ReduxTypes.SAVE_MY_GRAMS_ERROR: {
            return {
                ...state,
                saveMyGrams: { loading: false },
            }
        }
        /**
             * Paused Or Resumed Package
             */
        case ReduxTypes.PAUSED_PACKAGE_REQUEST:
        case ReduxTypes.RESUME_PACKAGE_REQUEST: {
            return { ...state, pausedOrResumePackage: { loading: true } }
        }
        case ReduxTypes.PAUSED_PACKAGE_SUCCESS:
        case ReduxTypes.RESUME_PACKAGE_SUCCESS: {
            return { ...state, pausedOrResumePackage: { loading: false } }
        }
        case ReduxTypes.PAUSED_PACKAGE_ERROR:
        case ReduxTypes.RESUME_PACKAGE_ERROR: {
            return { ...state, pausedOrResumePackage: { loading: false } }
        }

        /**
             * Select Package Meals
             */
        case ReduxTypes.SELECT_PACKAGE_MEAL_REQUEST: {
            return { ...state, selectPackageMeals: { loading: true } }
        }
        case ReduxTypes.SELECT_PACKAGE_MEAL_SUCCESS:
        case ReduxTypes.SELECT_PACKAGE_MEAL_ERROR: {
            return { ...state, selectPackageMeals: { loading: false } }
        }

        /**
             * Set Calender Selected Day Type
             */
        case ReduxTypes.SET_CALENDER_SELECTED_DAY_TYPE: {
            return { ...state, calenderSelectedDayType: action.payload }
        }

        /**
             * Set Calender Range
             */
        case ReduxTypes.SET_CALENDER_RANGE: {
            return { ...state, calenderRange: action.payload }
        }

        /**
             * Set Calender Selected Date
             */
        case ReduxTypes.SET_CALENDER_SELECTED_DATE: {
            return { ...state, calenderSelectedDate: action.payload }
        }
        /**
        * Get Selected Meals By Date
        */
        case ReduxTypes.GET_SELECTED_MEALS_BY_DATE_REQUEST: {
            return {
                ...state,
                selectedMealsByDate: { loading: true, data: null },
            }
        }
        case ReduxTypes.GET_SELECTED_MEALS_BY_DATE_SUCCESS: {
            return {
                ...state,
                selectedMealsByDate: { loading: false, data: action.payload },
            }
        }
        case ReduxTypes.GET_SELECTED_MEALS_BY_DATE_ERROR: {
            return {
                ...state,
                selectedMealsByDate: { loading: false, data: null },
            }
        }

        /**
        * Get Meals By Meal Type Id and Date
        */
        case ReduxTypes.GET_MEALS_BY_MEALID_AND_DATE_REQUEST: {
            return {
                ...state,
                selectedMealsByMealTypeIdAndDate: { loading: true, data: null },
            }
        }
        case ReduxTypes.GET_MEALS_BY_MEALID_AND_DATE_SUCCESS: {
            return {
                ...state,
                selectedMealsByMealTypeIdAndDate: { loading: false, data: action.payload },
            }
        }
        case ReduxTypes.GET_MEALS_BY_MEALID_AND_DATE_ERROR: {
            return {
                ...state,
                selectedMealsByMealTypeIdAndDate: { loading: false, data: null },
            }
        }

        /**
         * Total Macros
        */
        case ReduxTypes.CALCULATE_MACROS: {
            // let mealsData = state?.selectedMealsByMealTypeIdAndDate?.data
            let mealsData = action?.payload
            let totalMacrosOnSave = {
                protein: 0,
                carbohydrates: 0,
                calories: 0,
                fats: 0
            }
            mealsData?.map((mealItem) => {
                mealItem?.meals_data?.filter(
                    subItem => {
                        if (Number(subItem?.selected_meal_qty) > 0) {
                            totalMacrosOnSave.calories += parseFloat(subItem?.calories || 0) * Number(subItem?.selected_meal_qty)
                            totalMacrosOnSave.carbohydrates += parseFloat(subItem?.carbohydrates || 0) * Number(subItem?.selected_meal_qty)
                            totalMacrosOnSave.protein += parseFloat(subItem?.protein || 0) * Number(subItem?.selected_meal_qty)
                            totalMacrosOnSave.fats += parseFloat(subItem?.fats || 0) * Number(subItem?.selected_meal_qty)
                        }
                        return Number(subItem?.selected_meal_qty) > 0
                    },
                );

            });
            return {
                ...state,
                totalMacros: {
                    ...totalMacrosOnSave
                },
            }
        }
        default: {
            return state;
        }
    }
}

export default SubscriptionReducer
