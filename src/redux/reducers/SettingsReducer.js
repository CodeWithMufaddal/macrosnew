import { APP_LANGUAGES } from '../../i18n/i18n';
import { appFlows } from '../../navigation/screens';
import * as ReduxTypes from '../types/ReduxTypes';

export const SettingsInitialState = {
    appFlow: appFlows.introFlowNavigator,
    appLanguage: APP_LANGUAGES.EN,
    appDirection: 'ltr',
    areas: { loading: false, data: [] },
    banners: { loading: false, data: [] },
    authUserData: null,
};

const SettingsReducer = (state = SettingsInitialState, action) => {
    switch (action.type) {
        /**
                       * Set App Language
                       */
        case ReduxTypes.SET_APP_LANGUAGE: {
            return {
                ...state,
                appLanguage: action.payload,
                appDirection: action.payload === APP_LANGUAGES.EN ? 'ltr' : 'rtl',
            }
        }

        case ReduxTypes.SET_AUTH_USER_DATA: {
            return {
                ...state,
                authUserData: action.payload,
            }
        }


        case ReduxTypes.SET_APP_FLOW: {
            return {
                ...state,
                appFlow: action.payload,
            }
        }

        /**
             * Get Areas
             */
        case ReduxTypes.GET_AREAS_REQUEST: {
            return {
                ...state,
                areas: { loading: true, data: [] },
            }
        }
        case ReduxTypes.GET_AREAS_SUCCESS: {
            return {
                ...state,
                areas: { loading: false, data: action.payload },
            }
        }
        case ReduxTypes.GET_AREAS_ERROR: {
            return {
                ...state,
                areas: { loading: false, data: [] },
            }
        }
        /**
         * Get Banners
         */
        case ReduxTypes.GET_BANNERS_REQUEST: {
            return {
                ...state,
                banners: { ...state.banners, loading: true },
            }
        }
        case ReduxTypes.GET_BANNERS_SUCCESS: {
            return {
                ...state,
                banners: { loading: false, data: action.payload },
            }
        }
        case ReduxTypes.GET_BANNERS_ERROR: {
            return {
                ...state,
                banners: { loading: false, data: [] },
            }
        }

        /**
                 * Default
                 */
        default: {
            return state;
        }
    }
}

export default SettingsReducer
