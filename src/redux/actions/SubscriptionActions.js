/* eslint-disable no-underscore-dangle */
import moment from 'moment';
import {
    sendDataToReducer, getSingleSubscriptionData, getWholePlanDuration, getCalenderDayType, getWeekNumberFromDate, getValidDate,
} from '../../helpers/CommonHelpers';
import * as ReduxTypes from '../types/ReduxTypes';
import * as Services from '../../services/Services';
import CustomToast from '../../components/custom/CustomToast';
import i18n from '../../i18n/i18n';
import { ALLOW_ONLINE_PAYMENT, WEEK_DAYS } from '../../constants/constants';
import CustomOneSignal from '../../onesignal/CustomOneSignal';
import { appFlows } from '../../navigation/screens';
import { changeAppFlow } from './SettingsActions';

// Get Goals
export const getGoalsAction = () => (dispatch) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.GET_GOALS_REQUEST)
    Services.getGoals().then((res) => {
        sendDataToReducer(dispatch, ReduxTypes.GET_GOALS_SUCCESS, res?.data)
        resolve(res?.data);
    }).catch((error) => {
        sendDataToReducer(dispatch, ReduxTypes.GET_GOALS_ERROR, error)
        reject(error);
    })
})

// Get All Meal Types
export const getAllMealTypesAction = () => (dispatch) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.GET_ALL_MEAL_TYPES_REQUEST)
    Services.getAllMealTypes().then((res) => {
        sendDataToReducer(dispatch, ReduxTypes.GET_ALL_MEAL_TYPES_SUCCESS, res?.data)
        resolve(res);
    }).catch((error) => {
        sendDataToReducer(dispatch, ReduxTypes.GET_ALL_MEAL_TYPES_ERROR, error)
        reject(error);
    })
})

// Get All Meal Types By Goal ID
export const getMealTypesByGoalIdAction = (goalId) => (dispatch) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.GET_MEAL_TYPES_BY_GOAL_ID_REQUEST)
    Services.getMealTypesByGoalId(goalId).then((res) => {
        sendDataToReducer(dispatch, ReduxTypes.GET_MEAL_TYPES_BY_GOAL_ID_SUCCESS, res?.data)
        resolve(res);
    }).catch((error) => {
        sendDataToReducer(dispatch, ReduxTypes.GET_MEAL_TYPES_BY_GOAL_ID_ERROR, error)
        reject(error);
    })
})

// Get Number Of Days
export const getNumberOfDaysAction = () => (dispatch, getState) => new Promise((resolve, reject) => {
    const state = getState();
    const userId = state?.auth?.userData?.id;
    sendDataToReducer(dispatch, ReduxTypes.GET_NUMBER_OF_DAYS_REQUEST)
    Services.getNumberOfDays(userId).then((res) => {
        sendDataToReducer(dispatch, ReduxTypes.GET_NUMBER_OF_DAYS_SUCCESS, res?.data)
        resolve(res);
    }).catch((error) => {
        sendDataToReducer(dispatch, ReduxTypes.GET_NUMBER_OF_DAYS_ERROR, error)
        reject(error);
    })
})

// Get Number Of Days
export const getPlanListByDaysAction = (days) => (dispatch) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.GET_PLANS_BY_DAYS_REQUEST)
    Services.getPlanListByDays(days).then((res) => {
        sendDataToReducer(dispatch, ReduxTypes.GET_PLANS_BY_DAYS_SUCCESS, res?.data)
        resolve(res);
    }).catch((error) => {
        sendDataToReducer(dispatch, ReduxTypes.GET_PLANS_BY_DAYS_ERROR, error)
        reject(error);
    })
})

// Get All Meals By Meal Type ID
export const getAllMealsByMealTypeIdAction = (mealTypeId) => (dispatch) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.GET_ALL_MEALS_BY_MEALTYPEID_REQUEST)
    Services.getAllMealsByMealTypeId(mealTypeId).then((res) => {
        sendDataToReducer(dispatch, ReduxTypes.GET_ALL_MEALS_BY_MEALTYPEID_SUCCESS, res?.data)
        resolve(res?.data);
    }).catch((error) => {
        sendDataToReducer(dispatch, ReduxTypes.GET_ALL_MEALS_BY_MEALTYPEID_ERROR, error)
        reject(error);
    })
})

// Apply Coupon
export const applyCouponAction = (subscription_id, coupons_code) => (dispatch, getState) => new Promise((resolve, reject) => {
    const state = getState();
    const couponData = { user_ids: state?.auth?.userData?.ids, coupons_code, subscription_id }
    Services.applyCoupon(couponData).then((res) => {
        resolve(res)
    }).catch((error) => {
        CustomToast.error(error?.data?.message);
        reject(error)
    })
})

// Purchase Subscription
export const purchaseSubscriptionPaymentAction = (data) => (dispatch, getState) => new Promise((resolve, reject) => {
    const orderData = data?.orderData;
    const orderPaymentStatusData = data?.orderPaymentStatusData;
    sendDataToReducer(dispatch, ReduxTypes.PURCHASE_SUBSCRIPTION_REQUEST)
    const state = getState();
    const userData = state?.auth?.userData;
    const formData = {
        is_custom: orderData?.is_custom_grams == '1' ? '1' : '0',
        renew_plan: orderData?.renew_plan,
        user_ids: userData?.ids,
        device_id: userData?.device_id,
        device_type: userData?.device_type,
        price: orderData?.price ?? 0,
        amount: orderData?.amount,
        subscription_ids: orderData?.subscription_ids,
        coupon: orderData?.coupon ?? '',
        coupon_discount: orderData?.coupon_discount ?? 0,
        start_date: orderData?.start_date,
        notes: orderData?.notes,
        redirect_status: orderPaymentStatusData?.paymentStatus,
        callback_status: orderPaymentStatusData?.paymentStatus,
        gateway_identifier: ALLOW_ONLINE_PAYMENT ? orderPaymentStatusData?.PaymentId : orderPaymentStatusData?.paymentStatus,
        gateway_event_id: ALLOW_ONLINE_PAYMENT ? orderPaymentStatusData?.TransactionId : orderPaymentStatusData?.paymentStatus,
    }
    Services.purchaseSubscriptionPayment(formData)
        .then((res) => {
            if (res?.data?.id) {
                dispatch(getSubscriptionDetailsAction())
                CustomOneSignal.userIsSubscribed(true);
                sendDataToReducer(dispatch, ReduxTypes.PURCHASE_SUBSCRIPTION_SUCCESS, res)
                CustomToast.success(i18n.t('subscribed'), res?.message);
                if (orderData?.renew_plan === 'Yes' || orderData?.isCameFromHomeDashboard) dispatch(changeAppFlow(appFlows.afterLoginFlowNavigator))
                else dispatch(changeAppFlow(appFlows.personalDetailWhilePurchaseSubscriptionFlowNavigator))
                resolve(res);
            } else {
                if (orderData?.renew_plan === 'Yes') dispatch(changeAppFlow(appFlows.afterLoginFlowNavigator))
                const error = { error: 'false' }
                reject(error)
            }
        }).catch((error) => {
            sendDataToReducer(dispatch, ReduxTypes.PURCHASE_SUBSCRIPTION_ERROR, error?.data)
            CustomToast.error(error?.data?.message);
            if (orderData?.renew_plan === 'Yes') dispatch(changeAppFlow(appFlows.afterLoginFlowNavigator));
            reject(error)
        })
})
// Subscription Action
export const subscriptionPaymentAction = (data) => (dispatch, getState) => new Promise((resolve, reject) => {
    const orderData = data?.orderData;
    sendDataToReducer(dispatch, ReduxTypes.SUBSCRIPTION_REQUEST)
    const state = getState();
    const userData = state?.auth?.userData;
    const formData = {
        is_custom: orderData?.is_custom_grams == '1' ? '1' : '0',
        renew_plan: orderData?.renew_plan,
        user_ids: userData?.ids,
        device_id: userData?.device_id,
        device_type: userData?.device_type,
        price: orderData?.price ?? 0,
        amount: orderData?.amount,
        subscription_ids: orderData?.subscription_ids,
        coupon: orderData?.coupon ?? '',
        coupon_discount: orderData?.coupon_discount ?? 0,
        start_date: orderData?.start_date,
        notes: orderData?.notes,
        payment_method: orderData?.paymentMethod,
    }
    Services.subscriptionPayment(formData)
        .then((res) => {
            if (res?.result) {
                sendDataToReducer(dispatch, ReduxTypes.SUBSCRIPTION_SUCCESS, res)
                resolve(res);
            } else {
                const error = { error: 'false' }
                reject(error)
            }
        }).catch((error) => {
            sendDataToReducer(dispatch, ReduxTypes.SUBSCRIPTION_ERROR, error?.data)
            CustomToast.error(error?.data?.message);
            reject(error)
        })
})

// Get Subscription Details
export const getSubscriptionDetailsAction = () => (dispatch, getState) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.GET_SUBSCRIPTION_DETAILS_REQUEST)
    const state = getState();
    const user_ids = state?.auth?.userData?.ids;
    Services.getSubscriptionDetails({ user_ids }).then((res) => {
        if (res?.data?.length > 0) CustomOneSignal.userIsSubscribed(true);
        const wholePlanDuration = getWholePlanDuration(res?.data);
        sendDataToReducer(dispatch, ReduxTypes.GET_SUBSCRIPTION_DETAILS_SUCCESS, res?.data)
        if (wholePlanDuration) {
            let minDate = wholePlanDuration?.start_time.toString();
            const maxDate = wholePlanDuration?.end_time.toString();
            if (new Date() > new Date(wholePlanDuration?.start_time)) minDate = getValidDate(new Date(), 'YYYY-MM-DD 00:00:00');
            const calenderRangeData = { minDate, maxDate }
            sendDataToReducer(dispatch, ReduxTypes.SET_CALENDER_RANGE, calenderRangeData);
        }
        resolve(res?.data);
    }).catch((error) => {
        sendDataToReducer(dispatch, ReduxTypes.GET_SUBSCRIPTION_DETAILS_ERROR, error)
        reject(error);
    })
})

// Get Calender Data
export const getMyCalenderDataAction = () => (dispatch, getState) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.GET_MY_CALENDER_DATA_REQUEST)
    const state = getState();
    const user_ids = state?.auth?.userData?.ids;
    Services.getMyCalenderData({ user_ids }).then((res) => {
        sendDataToReducer(dispatch, ReduxTypes.GET_MY_CALENDER_DATA_SUCCESS, { ...res?.data, active_date: res?.active_date })
        const mySelectionDate = new Date(getValidDate(new Date(state?.subscription?.calenderSelectedDate), 'YYYY-MM-DD 00:00:00'))
        const dayType = getCalenderDayType(mySelectionDate, state?.subscription?.subscriptionDetails?.data, res?.data);
        sendDataToReducer(dispatch, ReduxTypes.SET_CALENDER_SELECTED_DAY_TYPE, dayType)
        resolve(res?.data);
    }).catch((error) => {
        sendDataToReducer(dispatch, ReduxTypes.GET_MY_CALENDER_DATA_ERROR, error)
        reject(error);
    })
})

// Send Package Targert
export const sendPackageTargetAction = (package_id, meal_type_id) => (dispatch, getState) => new Promise((resolve, reject) => {
    const state = getState();
    const data = {
        user_id: state?.auth?.userData?.id,
        package_id,
        meal_type_id,
    }
    Services.sendPackageTarget(data).then(async (res) => {
        dispatch(getNumberOfDaysAction())
        resolve(res?.data);
    }).catch((error) => {
        reject(error);
    })
})

// Get Selected Package Meals By Date
export const getSelectedMealsByDateAction = (date) => (dispatch, getState) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.GET_SELECTED_MEALS_BY_DATE_REQUEST)
    const state = getState();
    const subscriptionData = getSingleSubscriptionData(state?.subscription?.subscriptionDetails?.data, date);
    const data = {
        user_ids: state?.auth?.userData?.ids,
        payment_item_ids: subscriptionData?.payment_item_ids,
        date: getValidDate(date, 'YYYY-MM-DD'),
    }

    Services.getSelectedMealsByDate(data).then(async (res) => {
        sendDataToReducer(dispatch, ReduxTypes.GET_SELECTED_MEALS_BY_DATE_SUCCESS, res?.data)
        resolve(res?.data);
    }).catch((error) => {
        CustomToast.error(i18n.t('error'), error?.data?.message);
        sendDataToReducer(dispatch, ReduxTypes.GET_SELECTED_MEALS_BY_DATE_ERROR, error)
        reject(error);
    })
})

// Paused Package
export const pausedPackageAction = (date) => (dispatch, getState) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.PAUSED_PACKAGE_REQUEST)
    let allowPause = true;
    let active_plan = 'Yes';
    const state = getState();
    const subscriptionData = getSingleSubscriptionData(state?.subscription?.subscriptionDetails?.data, date);
    const wholeSubscriptionData = state?.subscription?.subscriptionDetails?.data;
    if (wholeSubscriptionData?.length > 1) {
        const subscriptionIndex = wholeSubscriptionData?.findIndex((item) => item?.payment_item_ids === subscriptionData?.payment_item_ids && item?.goal_ids === subscriptionData?.goal_ids && item?.subscription_ids === subscriptionData?.subscription_ids);
        if (subscriptionIndex === 1) active_plan = 'No';
        if (wholeSubscriptionData?.[0]?.payment_item_ids !== wholeSubscriptionData?.[1]?.payment_item_ids) {
            const endDateOfFirstPlan = moment(moment(new Date(getValidDate(new Date(wholeSubscriptionData?.[0]?.end_time), 'YYYY-MM-DD 00:00:00'))), 'DD-MM-YYYY');
            const startDateOfSecondPlan = moment(moment(new Date(getValidDate(wholeSubscriptionData?.[1]?.start_time, 'YYYY-MM-DD 00:00:00'))), 'DD-MM-YYYY');
            const diff = startDateOfSecondPlan.diff(endDateOfFirstPlan, 'days')
            const isInBetweenFriday = (endDateOfFirstPlan.day() === WEEK_DAYS.THURSDAY && startDateOfSecondPlan.day() === WEEK_DAYS.SATURDAY) && diff === 1;
            if (diff === 0 || isInBetweenFriday) {
                CustomToast.error(i18n.t('your_plans_are_overlap'))
                allowPause = false
            }
        }
    }

    if (allowPause) {
        const data = {
            user_ids: state?.auth?.userData?.ids,
            package_ids: subscriptionData?.goal_ids,
            subscription_ids: subscriptionData?.subscription_ids,
            paused_date: getValidDate(date, 'YYYY-MM-DD'),
            active_plan,
        }
        Services.pausedPackage(data).then(async (res) => {
            await dispatch(getSubscriptionDetailsAction());
            await dispatch(getMyCalenderDataAction());
            sendDataToReducer(dispatch, ReduxTypes.PAUSED_PACKAGE_SUCCESS, res?.data)
            resolve(res?.data);
        }).catch((error) => {
            sendDataToReducer(dispatch, ReduxTypes.PAUSED_PACKAGE_ERROR, error?.data)
            reject(error?.data)
        })
    } else {
        sendDataToReducer(dispatch, ReduxTypes.PAUSED_PACKAGE_ERROR)
        reject('Not Allow to Pause');
    }
})

// Resume Package
export const resumePackageAction = (date) => (dispatch, getState) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.RESUME_PACKAGE_REQUEST)
    const state = getState();
    const subscriptionData = getSingleSubscriptionData(state?.subscription?.subscriptionDetails?.data, date);
    const wholeSubscriptionData = state?.subscription?.subscriptionDetails?.data;
    let active_plan = 'Yes';
    if (wholeSubscriptionData?.length > 1) {
        const subscriptionIndex = wholeSubscriptionData?.findIndex((item) => item?.payment_item_ids === subscriptionData?.payment_item_ids && item?.goal_ids === subscriptionData?.goal_ids && item?.subscription_ids === subscriptionData?.subscription_ids);
        if (subscriptionIndex === 1) active_plan = 'No';
    }

    const data = {
        user_ids: state?.auth?.userData?.ids,
        package_ids: subscriptionData?.goal_ids,
        subscription_ids: subscriptionData?.subscription_ids,
        paused_date: getValidDate(date, 'YYYY-MM-DD'),
        active_plan,
    }

    Services.resumePackage(data).then(async (res) => {
        Services.getSubscriptionDetails(data).then(({ data: newSubscriptionData }) => {
            Services.getMyCalenderData({ user_ids: state?.auth?.userData?.ids }).then(async ({ data: newCalenderData }) => {
                const newSingleSubscriptionData = getSingleSubscriptionData(newSubscriptionData, date);
                if (newSingleSubscriptionData) {
                    const newSubscriptionEndDate = getValidDate(newSingleSubscriptionData?.end_time, 'YYYY-MM-DD');
                    if (newCalenderData?.paused_date?.some((item) => item?.paused_date === newSubscriptionEndDate)) {
                        data.paused_date = newSubscriptionEndDate;
                        Services.resumePackage(data).then(async () => {
                            await dispatch(getSubscriptionDetailsAction());
                            await dispatch(getMyCalenderDataAction());
                            sendDataToReducer(dispatch, ReduxTypes.RESUME_PACKAGE_SUCCESS, res?.data)
                        }).catch((error) => {
                            sendDataToReducer(dispatch, ReduxTypes.RESUME_PACKAGE_ERROR, error)
                        });
                    } else {
                        await dispatch(getSubscriptionDetailsAction());
                        await dispatch(getMyCalenderDataAction());
                        sendDataToReducer(dispatch, ReduxTypes.RESUME_PACKAGE_SUCCESS, res?.data)
                    }
                }
            });
        });

        resolve(res?.data);
    }).catch((error) => {
        sendDataToReducer(dispatch, ReduxTypes.RESUME_PACKAGE_ERROR, error)
        CustomToast.error(i18n.t('error'), error?.data?.message);
        reject(error);
    })
})

const getSelectMealDataWithFormat = (mealData) => {
    const mData = [];
    for (let i = 0; i <= mealData?.length; i++) {
        for (let j = 0; j <= mealData?.[i]?.meals_data?.length; j++) {
            if (Number(mealData?.[i]?.meals_data?.[j]?.selected_meal_qty) > 0) {
                mData.push({
                    dish_category_id: mealData?.[i]?.meals_data?.[j]?.dish_category_id,
                    meal_id: mealData?.[i]?.meals_data?.[j]?.meal_id,
                    meal_qty: mealData?.[i]?.meals_data?.[j]?.selected_meal_qty,
                    substitute_id: mealData?.[i]?.meals_data?.[j]?.substitute_id ?? null,
                })
            }
        }
    }
    return mData;
}

// Select Package Meals
export const selectPackageMealsAction = (date, mealsData) => (dispatch, getState) => new Promise((resolve, reject) => {
    const meals_data = getSelectMealDataWithFormat(mealsData)
    sendDataToReducer(dispatch, ReduxTypes.SELECT_PACKAGE_MEAL_REQUEST)
    const state = getState();
    const subscriptionData = getSingleSubscriptionData(state?.subscription?.subscriptionDetails?.data, date);
    const data = {
        selected_date: getValidDate(date, 'YYYY-MM-DD'),
        data: meals_data,
        user_ids: state?.auth?.userData?.ids,
        package_ids: subscriptionData?.goal_ids,
        subscription_ids: subscriptionData?.subscription_ids,
    }

    Services.selectPackageMeals(data).then(async (res) => {
        CustomOneSignal.userMealSelected(data?.selected_date);
        await dispatch(getMyCalenderDataAction());
        sendDataToReducer(dispatch, ReduxTypes.SELECT_PACKAGE_MEAL_SUCCESS, res?.data)
        resolve(res?.data);
    }).catch((error) => {
        sendDataToReducer(dispatch, ReduxTypes.SELECT_PACKAGE_MEAL_ERROR, error)
        CustomToast.error(i18n.t('error'), error?.data?.message);
        reject(error);
    })
})

export const setCalenderSelectedDateAction = (date) => (dispatch, getState) => new Promise(() => {
    const state = getState();
    sendDataToReducer(dispatch, ReduxTypes.SET_CALENDER_SELECTED_DATE, date)
    const dayType = getCalenderDayType(date, state?.subscription?.subscriptionDetails?.data, state?.subscription?.myCalender?.data);
    sendDataToReducer(dispatch, ReduxTypes.SET_CALENDER_SELECTED_DAY_TYPE, dayType);
})

// Get Number Of Days By User Id
export const getMealsByMealTypeIdAndDateAction = (date, currentPlanType) => (dispatch, getState) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.GET_MEALS_BY_MEALID_AND_DATE_REQUEST)
    const state = getState();
    const subscriptionData = getSingleSubscriptionData(state?.subscription?.subscriptionDetails?.data, date);
    const userData = state?.auth?.userData;
    const postData = {
        user_ids: userData?.ids,
        payment_item_ids: subscriptionData?.payment_item_ids,
        meal_type_id: subscriptionData?.meal_type_id,
        subscription_ids: subscriptionData?.subscription_ids,
        date: getValidDate(date, 'YYYY-MM-DD'),
        week_no: `week_${getWeekNumberFromDate(date)}`,
    }

    Services.getMealByMealTypeIdByDate(postData)
        .then((res) => {
            sendDataToReducer(dispatch, ReduxTypes.GET_MEALS_BY_MEALID_AND_DATE_SUCCESS, res?.data)
            resolve(res?.data);
        }).catch((error) => {
            sendDataToReducer(dispatch, ReduxTypes.GET_MEALS_BY_MEALID_AND_DATE_ERROR, error?.data)
            reject(error);
        })
})

// Save My Grams
export const saveMyGramsAction = (data) => (dispatch, getState) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.SAVE_MY_GRAMS_REQUEST)
    const state = getState();
    const userData = state?.auth?.userData;
    const postData = {
        user_ids: userData?.ids,
        ...data,
    }

    Services.saveMyGrams(postData)
        .then((res) => {
            CustomToast.success(i18n.t('success'), i18n.t('grams_saved_successfully'));
            sendDataToReducer(dispatch, ReduxTypes.SAVE_MY_GRAMS_SUCCESS, res?.data)
            resolve(res?.data);
        }).catch((error) => {
            sendDataToReducer(dispatch, ReduxTypes.SAVE_MY_GRAMS_ERROR, error?.data)
            reject(error);
        })
})

// Get My Grams
export const getMyGramsAction = (data) => (dispatch, getState) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.GET_MY_GRAMS_REQUEST)
    const state = getState();
    const userData = state?.auth?.userData;
    const postData = {
        user_ids: userData?.ids,
        ...data,
    }

    Services.getMyGrams(postData)
        .then((res) => {
            sendDataToReducer(dispatch, ReduxTypes.GET_MY_GRAMS_SUCCESS, res)
            resolve(res);
        }).catch((error) => {
            sendDataToReducer(dispatch, ReduxTypes.GET_MY_GRAMS_ERROR, error?.data)
            reject(error);
        })
})

// Set Product Rating
export const setProductRatingAction = (data) => (dispatch, getState) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.SET_PRODUCT_RATING)
    const state = getState();
    const userData = state?.auth?.userData;
    const postData = {
        user_ids: userData?.ids,
        ...data,
    }

    Services.reviewRating(postData)
        .then((res) => {
            sendDataToReducer(dispatch, ReduxTypes.SET_PRODUCT_RATING, res)
            resolve(res);
        }).catch((error) => {
            sendDataToReducer(dispatch, ReduxTypes.SET_PRODUCT_RATING_ERROR, error?.data)
            reject(error);
        })
})
