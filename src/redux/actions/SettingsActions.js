import { I18nManager } from 'react-native'
import { sendDataToReducer } from '../../helpers/CommonHelpers';
import i18n from '../../i18n/i18n';
import * as ReduxTypes from '../types/ReduxTypes';
import * as Services from '../../services/Services';

// Reset Store
export const resetStore = () => (dispatch) => {
    sendDataToReducer(dispatch, ReduxTypes.RESET_STORE)
}
// Set App Language
export const changeAppLanguageAction = (language, fromRestart = false) => (dispatch, state) => new Promise((resolve) => {
    const savedLanguage = state?.settings?.appLanguage;
    let appLanguage = language
    if (fromRestart) appLanguage = savedLanguage;
    if (appLanguage) {
        sendDataToReducer(dispatch, ReduxTypes.SET_APP_LANGUAGE, appLanguage)
        i18n.changeLanguage(appLanguage);
        I18nManager.forceRTL(['ar', 'he', 'fa'].includes(appLanguage));
        resolve(appLanguage);
    }
})

// Set Auth User Data
export const setAuthUserDataAction = (data) => (dispatch) => new Promise((resolve) => {
    sendDataToReducer(dispatch, ReduxTypes.SET_AUTH_USER_DATA, data)
    resolve(data);
})

// Set App Language
export const changeAppFlow = (appFlow) => (dispatch) => new Promise((resolve) => {
    sendDataToReducer(dispatch, ReduxTypes.SET_APP_FLOW, appFlow)
    resolve(true);
})

// Get Areas
export const getAreasAction = () => (dispatch) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.GET_AREAS_REQUEST)
    Services.getAreas().then((res) => {
        sendDataToReducer(dispatch, ReduxTypes.GET_AREAS_SUCCESS, res?.data)
        resolve(res);
    }).catch((error) => {
        sendDataToReducer(dispatch, ReduxTypes.GET_AREAS_ERROR, error)
        reject(error);
    })
})

// Get Banners
export const getBannersAction = () => (dispatch) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.GET_BANNERS_REQUEST)
    Services.getBanners().then((res) => {
        sendDataToReducer(dispatch, ReduxTypes.GET_BANNERS_SUCCESS, res?.data)
        resolve(res?.data);
    }).catch((error) => {
        sendDataToReducer(dispatch, ReduxTypes.GET_BANNERS_ERROR, error)
        reject(error);
    })
})
