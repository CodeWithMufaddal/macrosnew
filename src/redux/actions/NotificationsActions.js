import AsyncStorage from '@react-native-async-storage/async-storage';
import _ from 'lodash';
import AsyncKeys from '../../constants/AsyncKeys';
import { sendDataToReducer } from '../../helpers/CommonHelpers'
import * as ReduxTypes from '../types/ReduxTypes';

/* eslint-disable no-underscore-dangle */
export const setNotificationListAction = (notificationList) => (dispatch) => new Promise(async () => {
    await AsyncStorage.setItem(AsyncKeys.NOTIFICATION_LIST, JSON.stringify(notificationList));
    sendDataToReducer(dispatch, ReduxTypes.SET_NOTIFICATION_LIST, notificationList)
})

export const addNotificationListAction = (notiData) => (dispatch, getState) => new Promise(() => {
    const state = getState();
    const notificationList = _.cloneDeep(state.notifications.notificationList);
    const isExistInArray = notificationList.some((item) => item?.notification?.notificationId === notiData?.notification?.notificationId)
    if (!isExistInArray) {
        const addableData = { ...notiData }
        notificationList.splice(0, 0, addableData)
        AsyncStorage.setItem(AsyncKeys.NOTIFICATION_LIST, JSON.stringify(notificationList));
        sendDataToReducer(dispatch, ReduxTypes.ADD_NOTIFICATION, notificationList)
    }
})

export const setNotificationUnreadCountAction = (count) => (dispatch, getState) => new Promise(() => {
    sendDataToReducer(dispatch, ReduxTypes.SET_NOTIFICATIONS_UNREAD_COUNT, count);
})

export const d = () => { }
