/* eslint-disable no-underscore-dangle */
import auth from '@react-native-firebase/auth';
import CustomCrashlytics from '../../../crashlytics/CustomCrashlytics';
import CustomToast from '../../components/custom/CustomToast';
import {
    getDeviceUniqueId,
    getOS,
    sendDataToReducer,
} from '../../helpers/CommonHelpers';
import i18n from '../../i18n/i18n';
import { navigateTo, navigateGoBack } from '../../navigation/navigation';
import screens, { appFlows } from '../../navigation/screens';
import CustomOneSignal from '../../onesignal/CustomOneSignal';
import * as Services from '../../services/Services';
import * as Validation from '../../validation/Validation';
import * as ReduxTypes from '../types/ReduxTypes';
import { changeAppFlow, setAuthUserDataAction } from './SettingsActions';
import { getAuthUserDataSelector } from '../selectors/SettingsSelectors';

// Logout
export const logoutAction = () => (dispatch) => new Promise(() => {
    dispatch(changeAppFlow(appFlows.loginFlowNavigator)).then(async () => {
        CustomOneSignal.removeExternalUserId();
        sendDataToReducer(dispatch, ReduxTypes.RESET_STORE);
    });
});

// Delete Account
export const deleteAccountAction = () => (dispatch, getState) => new Promise((resolve, reject) => {
    const state = getState();
    const data = { user_ids: state?.auth?.userData?.ids };
    Services.deleteMyAccount(data)
        .then(() => {
            CustomToast.success('Account Deleted');
            dispatch(changeAppFlow(appFlows.loginFlowNavigator)).then(async () => {
                await auth().signOut();
                CustomOneSignal.removeExternalUserId();
                sendDataToReducer(dispatch, ReduxTypes.RESET_STORE);
            });
            resolve(true);
        })
        .catch(() => {
            CustomToast.success('Something went wrong');
            reject(false);
        });
});

// Sign Up With Send Otp
export const signUpAction = (signUpData) => (dispatch) => new Promise((resolve, reject) => {
    if (Validation.signupValidation(signUpData)) {
        sendDataToReducer(dispatch, ReduxTypes.LOGIN_SEND_OTP_REQUEST);
        Services.checkMobileNumberIsRegisteredOrNot(signUpData.mobileNumber)
            .then(async (res) => {
                if (res?.data === 'No') {
                    const loginData = {
                        mobile_number: signUpData?.mobileNumber,
                    };
                    Services.sendOTP(loginData)
                        .then((response) => {
                            sendDataToReducer(dispatch, ReduxTypes.LOGIN_SEND_OTP_SUCCESS);
                            CustomToast.success(i18n.t('otp_sent'));
                            const authUserData = {
                                ...signUpData,
                                authConfirmation: response?.data?.otp,
                            };
                            dispatch(setAuthUserDataAction(authUserData));
                            navigateTo(screens.verifyOtp);
                            resolve(true);
                        })
                        .catch((error) => {
                            sendDataToReducer(
                                dispatch,
                                ReduxTypes.LOGIN_SEND_OTP_ERROR,
                                error?.message,
                            );
                            CustomCrashlytics.recordError(error);
                            CustomToast.error(i18n.t('error'), error?.message);
                            reject(error?.nativeErrorMessage);
                        });
                } else {
                    sendDataToReducer(dispatch, ReduxTypes.LOGIN_SEND_OTP_ERROR);
                    CustomToast.error(i18n.t('you_are_already_registered'));
                    navigateTo(screens.signin, {
                        mobileNumber: signUpData?.mobileNumber,
                    });
                    reject('you_are_already_registered');
                }
            })
            .catch((error) => {
                sendDataToReducer(dispatch, ReduxTypes.LOGIN_SEND_OTP_ERROR, error);
                reject(error);
            });
    }
});

// Resend Otp
export const reSendOtpAction = (mobileNumber) => (dispatch) => new Promise(async (resolve, reject) => {
    if (Validation.loginValidation(mobileNumber)) {
        sendDataToReducer(dispatch, ReduxTypes.LOGIN_SEND_OTP_REQUEST);
        const loginData = { mobile_number: mobileNumber };
        Services.sendOTP(loginData)
            .then(() => {
                sendDataToReducer(dispatch, ReduxTypes.LOGIN_SEND_OTP_SUCCESS);
                CustomToast.success(i18n.t('otp_resent'));
                resolve(true);
            })
            .catch((error) => {
                CustomCrashlytics.recordError(error);
                sendDataToReducer(
                    dispatch,
                    ReduxTypes.LOGIN_SEND_OTP_ERROR,
                    error?.message,
                );
                CustomToast.error(i18n.t('error'), error?.message);
                reject(error?.nativeErrorMessage);
            });
    }
});

// Send Otp
export const sendOtpAction = (mobileNumber, resend = false) => (dispatch) => new Promise((resolve, reject) => {
    if (Validation.loginValidation(mobileNumber)) {
        sendDataToReducer(dispatch, ReduxTypes.LOGIN_SEND_OTP_REQUEST);
        Services.checkMobileNumberIsRegisteredOrNot(mobileNumber)
            .then(async (res) => {
                if (res?.data === 'Yes') {
                    const loginData = {
                        mobile_number: mobileNumber,
                    };
                    Services.sendOTP(loginData)
                        .then((response) => {
                            sendDataToReducer(
                                dispatch,
                                ReduxTypes.LOGIN_SEND_OTP_SUCCESS,
                            );
                            const authUserData = {
                                mobileNumber,
                                authConfirmation: response?.data?.otp,
                            };
                            dispatch(setAuthUserDataAction(authUserData));
                            if (!resend) navigateTo(screens.verifyOtp);
                            CustomToast.success(
                                resend ? i18n.t('otp_resent') : i18n.t('otp_sent'),
                            );
                            resolve(true);
                        })
                        .catch((error) => {
                            CustomCrashlytics.recordError(error);
                            sendDataToReducer(
                                dispatch,
                                ReduxTypes.LOGIN_SEND_OTP_ERROR,
                                error?.message,
                            );
                            CustomToast.error(i18n.t('error'), error?.message);
                            reject(error?.nativeErrorMessage);
                        });
                } else {
                    sendDataToReducer(dispatch, ReduxTypes.LOGIN_SEND_OTP_ERROR);
                    CustomToast.error(i18n.t('you_are_not_registered'));
                    navigateGoBack();
                    reject('you_are_not_registered');
                }
            })
            .catch((error) => {
                sendDataToReducer(dispatch, ReduxTypes.LOGIN_SEND_OTP_ERROR, error);
                reject(error);
            });
    }
});

// Send Otp Only
export const sendOtpToMobileNumberAction = (mobileNumber, resend = false) => (dispatch) => new Promise((resolve, reject) => {
    if (Validation.loginValidation(mobileNumber)) {
        sendDataToReducer(dispatch, ReduxTypes.LOGIN_SEND_OTP_REQUEST);
        Services.checkMobileNumberIsRegisteredOrNot(mobileNumber)
            .then(async (res) => {
                if (res?.data === 'Yes') {
                    sendDataToReducer(dispatch, ReduxTypes.LOGIN_SEND_OTP_ERROR);
                    CustomToast.error(i18n.t('this_number_is_already_registered'));
                    reject('this_number_is_already_registered');
                } else {
                    const loginData = {
                        mobile_number: mobileNumber,
                    };
                    Services.sendOTP(loginData)
                        .then((response) => {
                            sendDataToReducer(
                                dispatch,
                                ReduxTypes.LOGIN_SEND_OTP_SUCCESS,
                            );
                            CustomToast.success(
                                resend ? i18n.t('otp_resent') : i18n.t('otp_sent'),
                            );
                            resolve({
                                mobileNumber,
                                authConfirmation: response?.data?.otp,
                            });
                        })
                        .catch((error) => {
                            CustomCrashlytics.recordError(error);
                            sendDataToReducer(
                                dispatch,
                                ReduxTypes.LOGIN_SEND_OTP_ERROR,
                                error?.message,
                            );
                            CustomToast.error(i18n.t('error'), error?.message);
                            reject(error?.nativeErrorMessage);
                        });
                }
            })
            .catch((error) => {
                sendDataToReducer(dispatch, ReduxTypes.LOGIN_SEND_OTP_ERROR, error);
                reject(error);
            });
    }
});

export const validatingUserAction = () => (dispatch, getState) => new Promise(async (resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.VERIFY_OTP_REQUEST);
    const state = getState();
    const authUserData = getAuthUserDataSelector(state);
    const data = authUserData;
    const isNewUser = !!authUserData?.name;
    const oneSignalDeviceData = await CustomOneSignal.getNotificationId();
    const deviceId = oneSignalDeviceData ?? getDeviceUniqueId();
    if (isNewUser) {
        const registerData = {
            mobile_number: data?.mobileNumber,
            area_id: data?.area,
            first_name: data?.name,
            preferred_language: state.settings.appLanguage,
            device_id: deviceId,
            device_type: getOS(),
        };
        Services.register(registerData)
            .then((registerRes) => {
                sendDataToReducer(dispatch, ReduxTypes.VERIFY_OTP_SUCCESS, {
                    ...registerRes?.data,
                    token: registerRes?.token,
                });
                sendDataToReducer(dispatch, ReduxTypes.SET_IS_NEW_USER, true);
                CustomCrashlytics.setUserId(data?.mobileNumber);
                CustomCrashlytics.setAttributes({
                    id: registerRes?.data?.id,
                    ids: registerRes?.data?.ids,
                    mobile_number: data?.mobileNumber,
                    device_id: deviceId,
                    device_type: getOS(),
                });
                CustomOneSignal.setExternalUserId(registerRes?.data?.ids);
                CustomOneSignal.userIsRegistered(true);
                CustomToast.success(i18n.t('verified_otp'));
                dispatch(changeAppFlow(appFlows.purchaseSubscriptionFlowNavigator));
                resolve(registerRes);
            })
            .catch((error) => {
                sendDataToReducer(dispatch, ReduxTypes.VERIFY_OTP_ERROR, error);
                CustomToast.error(i18n.t(error?.data?.message));
                reject(error);
            });
    } else {
        const loginData = {
            mobile_number: data?.mobileNumber,
            device_id: deviceId,
            device_type: getOS(),
        };
        Services.login(loginData)
            .then((loginRes) => {
                sendDataToReducer(dispatch, ReduxTypes.VERIFY_OTP_SUCCESS, {
                    ...loginRes?.data,
                    token: loginRes?.token,
                });
                CustomCrashlytics.setUserId(data?.mobileNumber);
                CustomCrashlytics.setAttributes({
                    id: loginRes?.data?.id,
                    ids: loginRes?.data?.ids,
                    mobile_number: data?.mobileNumber,
                    device_id: deviceId,
                    device_type: getOS(),
                });
                CustomOneSignal.setExternalUserId(loginRes?.data?.ids);
                CustomOneSignal.userIsRegistered(true);
                dispatch(changeAppFlow(appFlows.afterLoginFlowNavigator));
                CustomToast.success(i18n.t('verified_otp'));
                resolve(loginRes);
            })
            .catch((error) => {
                sendDataToReducer(dispatch, ReduxTypes.VERIFY_OTP_ERROR, error);
                CustomToast.error(i18n.t(error?.data?.message));
                reject(error);
            });
    }
});
// Verify Otp
export const verifyOtpAction = (otp) => (dispatch, getState) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.VERIFY_OTP_REQUEST);
    const state = getState();
    const authUserData = getAuthUserDataSelector(state);
    const authConfirmation = authUserData?.authConfirmation;
    const data = authUserData;
    const isNewUser = !!authUserData?.name;

    if (authConfirmation == otp) {
        dispatch(validatingUserAction(data, isNewUser));
    } else {
        sendDataToReducer(dispatch, ReduxTypes.VERIFY_OTP_ERROR, 'invalid_otp');
        CustomToast.error(i18n.t('invalid_otp'));
        reject('invalid_otp');
    }
});

// Set Is New User
export const setIsNewUserAction = (data) => (dispatch) => new Promise((resolve) => {
    sendDataToReducer(dispatch, ReduxTypes.SET_IS_NEW_USER, data);
    resolve(true);
});

// Save InbodySheet
export const saveInbodySheetAction = (data) => (dispatch, getState) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.SAVE_INBODY_SHEET_REQUEST);
    const state = getState();
    const inbodySheetData = {
        ...data,
        user_ids: state?.auth?.userData?.ids,
        calories: 0,
    };
    Services.saveInbodySheet(inbodySheetData)
        .then((res) => {
            sendDataToReducer(dispatch, ReduxTypes.SAVE_INBODY_SHEET_SUCCESS);
            dispatch(getUserInbodySheetAction());
            CustomToast.success(i18n.t('saved'));
            resolve(res);
        })
        .catch((error) => {
            sendDataToReducer(dispatch, ReduxTypes.SAVE_INBODY_SHEET_ERROR);
            CustomToast.error(error?.data?.message);
            reject(error);
        });
});

// Save Address
export const saveAddressAction = (data) => (dispatch, getState) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.SAVE_ADDRESS_REQUEST);
    const state = getState();
    const addressData = {
        ...data,
        user_ids: state?.auth?.userData?.ids,
    };
    Services.saveAddress(addressData)
        .then((res) => {
            sendDataToReducer(dispatch, ReduxTypes.SAVE_ADDRESS_SUCCESS);
            dispatch(getUserProfileAction());
            CustomToast.success(i18n.t('saved'));
            resolve(res);
        })
        .catch((error) => {
            sendDataToReducer(dispatch, ReduxTypes.SAVE_ADDRESS_ERROR);
            CustomToast.error(error?.data?.message);
            reject(error);
        });
});

// Save My Profile
export const saveMyProfileAction = (data) => (dispatch, getState) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.SAVE_MY_PROFILE_REQUEST);
    const state = getState();
    const addressData = {
        ...data,
        user_ids: state?.auth?.userData?.ids,
    };
    Services.saveMyProfile(addressData)
        .then((res) => {
            sendDataToReducer(dispatch, ReduxTypes.SAVE_MY_PROFILE_SUCCESS);
            dispatch(getUserProfileAction());
            CustomToast.success(i18n.t('saved'));
            resolve(res);
        })
        .catch((error) => {
            sendDataToReducer(dispatch, ReduxTypes.SAVE_MY_PROFILE_ERROR, error);
            CustomToast.error(error?.data?.message);
            reject(error);
        });
});

// Get User Profile
export const getUserProfileAction = () => (dispatch, getState) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.GET_USER_PROFILE_REQUEST);
    const state = getState();
    Services.getUserProfile(state?.auth?.userData?.ids)
        .then((res) => {
            sendDataToReducer(
                dispatch,
                ReduxTypes.GET_USER_PROFILE_SUCCESS,
                res?.data,
            );
            resolve(res?.data);
        })
        .catch((error) => {
            sendDataToReducer(dispatch, ReduxTypes.GET_USER_PROFILE_ERROR, error);
            reject(error);
        });
});

// Get User InbodySheet
export const getUserInbodySheetAction = () => (dispatch, getState) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.GET_USER_INBODY_SHEET_REQUEST);
    const state = getState();
    Services.getUserInbodySheet(state?.auth?.userData?.ids)
        .then((res) => {
            sendDataToReducer(
                dispatch,
                ReduxTypes.GET_USER_INBODY_SHEET_SUCCESS,
                res?.data,
            );
            resolve(res?.data);
        })
        .catch((error) => {
            sendDataToReducer(
                dispatch,
                ReduxTypes.GET_USER_INBODY_SHEET_ERROR,
                error,
            );
            reject(error);
        });
});

// Get Referral Code And Balance
export const getReferralCodeAndBalanceAction = () => (dispatch, getState) => new Promise((resolve, reject) => {
    sendDataToReducer(
        dispatch,
        ReduxTypes.GET_REFERRAL_CODE_AND_BALANCE_REQUEST,
    );
    const state = getState();
    Services.getReferralCodeAndBalance(state?.auth?.userData?.ids)
        .then((res) => {
            sendDataToReducer(
                dispatch,
                ReduxTypes.GET_REFERRAL_CODE_AND_BALANCE_SUCCESS,
                res?.data,
            );
            resolve(res);
        })
        .catch((error) => {
            sendDataToReducer(
                dispatch,
                ReduxTypes.GET_REFERRAL_CODE_AND_BALANCE_ERROR,
                error,
            );
            reject(error);
        });
});

// Save My Health Data
export const saveMyHealthDataAction = (data) => (dispatch, getState) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.SAVE_MY_HEALTH_DATA_REQUEST);
    const state = getState();
    const myHealthData = {
        ...data,
        user_ids: state?.auth?.userData?.ids,
    };
    Services.saveMyHealthData(myHealthData)
        .then((res) => {
            sendDataToReducer(dispatch, ReduxTypes.SAVE_MY_HEALTH_DATA_SUCCESS);
            dispatch(getUserInbodySheetAction());
            CustomToast.success(i18n.t('saved'));
            resolve(res);
        })
        .catch((error) => {
            sendDataToReducer(dispatch, ReduxTypes.SAVE_MY_HEALTH_DATA_ERROR);
            CustomToast.error(error?.data?.message);
            reject(error);
        });
});
