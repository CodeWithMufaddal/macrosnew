import { sendDataToReducer } from '../../helpers/CommonHelpers';
import * as Services from '../../services/Services'
import * as ReduxTypes from '../types/ReduxTypes';
import { getReferralCodeAndBalanceAction } from './AuthActions';

// Generate Coupon Code
export const generateCouponCodeAction = (amount) => (dispatch, getState) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.GENERATE_COUPON_REQUEST)
    const state = getState();
    const generateCouponData = {
        amount,
        user_ids: state?.auth?.userData?.ids,
    }
    Services.generateCouponCode(generateCouponData).then((res) => {
        sendDataToReducer(dispatch, ReduxTypes.GENERATE_COUPON_SUCCESS)
        dispatch(getReferralCodeAndBalanceAction());
        resolve(res)
    }).catch((error) => {
        sendDataToReducer(dispatch, ReduxTypes.GENERATE_COUPON_ERROR, error)
        reject(error)
    })
})

// Get Referral History
export const getReferralHistoryAction = () => (dispatch, getState) => new Promise((resolve, reject) => {
    sendDataToReducer(dispatch, ReduxTypes.GET_REFERRAL_HISTORY_REQUEST)
    const state = getState();
    Services.getReferralHistory(state?.auth?.userData?.ids).then((res) => {
        sendDataToReducer(dispatch, ReduxTypes.GET_REFERRAL_HISTORY_SUCCESS, res?.data)
        resolve(res)
    }).catch((error) => {
        sendDataToReducer(dispatch, ReduxTypes.GET_REFERRAL_HISTORY_ERROR, error)
        reject(error)
    })
})
