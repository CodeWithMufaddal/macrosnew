export const getSubscriptionDetailsSelector = (state) => state?.subscription?.subscriptionDetails?.data
