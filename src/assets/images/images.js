/* eslint-disable import/no-unresolved */
/* eslint-disable global-require */
import { BASE_URL } from '@env';
import resumeIcon from './resumeIcon.svg'
import chatIcon from './chatIcon.svg'
import profileSvg from './profileSvg.svg'
import supportSvg from './supportSvg.svg'
import mealsSvg from './mealsSvg.svg'
import calenderSvg from './calenderSvg.svg'
import homeSvg from './homeSvg.svg'
import selectedSupportSvg from './selectedSupportSvg.svg'
import selectedProfileSvg from './selectedProfileSvg.svg'
import selectedMealsSvg from './selectedMealsSvg.svg'
import selectedCalenderSvg from './selectedCalenderSvg.svg'
import bottomTabRoundShapeSvg from './bottomTabRoundShape.svg'
import redeemIconSvg from './redeemIconSvg.svg'
import historyIconSvg from './historyIconSvg.svg'
import loadingSVG from './loadingSVG.svg'
import forkSpoonPlate from './forkSpoonPlate.svg'
import instagramSVG from './instagramIcon.svg'
import whatsappSVG from './whatsappIcon.svg'
import star from './star.svg'
import starActive from './StarActive.svg'
import Breakfast from './Breakfast.svg'
import MainCourse from './MainCourse.svg'
import Snacks from './Snacks.svg'

const images = {
    MainCourse,
    Snacks,
    Breakfast,
    star,
    starActive,
    // fats,
    // carbs,
    // protien,
    loadingSVG,
    redeemIconSvg,
    historyIconSvg,
    resumeIcon,
    homeSvg,
    chatIcon,
    profileSvg,
    supportSvg,
    mealsSvg,
    instagramSVG,
    whatsappSVG,
    calenderSvg,
    selectedSupportSvg,
    selectedProfileSvg,
    bottomTabRoundShapeSvg,
    selectedMealsSvg,
    selectedCalenderSvg,
    forkSpoonPlate,
    fats: require('./fats.png'),
    carbs: require('./carbs.png'),
    protien: require('./protien.png'),
    macrosBoy: require('./macrosBoy.jpeg'),
    mapMarker: require('./mapMarker.png'),
    getRefferal: require('./getRefferal.png'),
    appSymbol: require('./appSymbol.png'),
    getStartedBackground: require('./getStartedBackground.jpg'),
    introScreendBackground: require('./introScreenBackground.jpg'),
    knetIcon: require('./knetIcon.png'),
    downArrow: require('./downArrow.png'),
    creditCardIcon: require('./creditCardIcon.png'),
    profilePicPlaceHolder: require('./profilePicPlaceHolder.jpeg'),
    loader1: require('./loader1.png'),
    loader2: require('./loader2.png'),
    loader3: require('./loader3.png'),
    macrosLogo: require('./macrosLogo.png'),
    dummy: {
        whatsYourGoal1: require('./dummy/whatsYourGoal1.jpg'),
        whatsYourGoal2: require('./dummy/whatsYourGoal2.jpg'),
        whatsYourGoal3: require('./dummy/whatsYourGoal3.jpg'),
        mealCategory1: require('./dummy/mealCategory1.jpg'),
        mealCategory2: require('./dummy/mealCategory2.jpg'),
        mealCategory3: require('./dummy/mealCategory3.jpg'),
        mealCategory4: require('./dummy/mealCategory4.jpg'),
        suggestedPlan1: require('./dummy/suggestedPlan1.png'),
        dummyProfilePic: require('./dummy/dummyProfilePic.png'),
    },
}

export default images

export const imagesMode = {
    center: 'center',
    contain: 'contain',
    cover: 'cover',
    stretch: 'stretch',
}

export const imagePathType = {
    subscriptions: 'subscriptions',
    meals: 'meals',
    mealTypes: 'mealtypes',
    packages: 'packages',
    avatar: 'avatar',
    bodysheet: 'bodysheet',
    banners: 'banners',
}

export const getImagePath = (type, imageName) => (imageName ? `${BASE_URL}/upload/${type}/${imageName}` : '');
