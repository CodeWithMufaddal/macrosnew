    # Macros app
```bash
Keystore alias      : macros
Keystore Password   : macros
```
## Minimum Required (Successfully run on this Versions Lastly) 
```bash
Node Version   : 20.10.0
NPM Version    : 10.2.3
JAVA Version   : 17.0.9
XCODE Version  : 15.1.0
Android Studio : Hedgehog 2023.1.1 Patch 2
MacOS Version  : macOS Ventura 13.6.1
```


# Custom Text
```bash
ff  -   Font Family
fs  -   Font Size
```

# Font Name: Roboto
```bash
ff-regular  -   Regular
ff-thin     -   Thin
ff-medium   -   Medium
ff-bold     -   Bold
ff-black    -   Extra Bold
```

# Font Sizes
```bash
fs-xsmall   -   12
fs-small    -   14
fs-medium   -   16
fs-large    -   20
fs-xlarge   -   24
```

# Custom Font Size
```bash
fs-15   -   15
fs-30   -   30
```