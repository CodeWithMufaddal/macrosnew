import Reactotron, {asyncStorage, networking} from "reactotron-react-native";

const reactotron = Reactotron.configure("127.0.0.1") // controls connection & communication settings
  .useReactNative(asyncStorage()) // add all built-in react native plugins
  .use(networking())
  .connect();

export default reactotron;