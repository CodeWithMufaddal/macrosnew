import { ONESIGNAL_APP_ID } from '@env';
import { AppRegistry, LogBox } from 'react-native';
import CodePush from 'react-native-code-push'
import App from './App';
import { name as appName } from './app.json';
import CustomOneSignal from './src/onesignal/CustomOneSignal';
import { ENABLE_CODE_PUSH } from './src/constants/constants';

if (__DEV__) {
    import("./Reactotron.js").then(() =>
        console.log("Reactotron Configured Yehhh"),
    );
}
CustomOneSignal.setLogLevel(6);
CustomOneSignal.setAppId(ONESIGNAL_APP_ID);
LogBox.ignoreLogs(['Remote debugger']);
LogBox.ignoreLogs(['VirtualizedLists should never be nested']);

// Codepush Configs
const CODE_PUSH_OPTIONS = { checkFrequency: CodePush.CheckFrequency.MANUAL }

const CodePushedApp = (ENABLE_CODE_PUSH && !__DEV__) ? CodePush(CODE_PUSH_OPTIONS)(App) : App;

AppRegistry.registerComponent(appName, () => CodePushedApp)
