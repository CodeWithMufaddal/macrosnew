/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useCallback, useEffect } from 'react';
import {
  AppState,
  SafeAreaView,
  StatusBar,
} from 'react-native';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider, useDispatch } from 'react-redux';
import Toast from 'react-native-toast-message';
import SplashScreen from 'react-native-splash-screen'
import store, { persistor } from './src/redux/store';
import MainScreen from './src/screens/main/MainScreen';
import { useStatusBar } from './src/hooks/useStatusBar';
import CustomPushNotification from './src/onesignal/CustomPushNotification';
import NoInternetModal from './src/screens/common/NoInternetModal';

// if (global.__fbBatchedBridge) {
//   const origMessageQueue = global.__fbBatchedBridge;
//   const modules = origMessageQueue._remoteModuleTable;
//   const methods = origMessageQueue._remoteMethodTable;
//   global.findModuleByModuleAndMethodIds = (moduleId, methodId) => {
//     console.log(`The problematic line code is in: ${modules[moduleId]}.${methods[moduleId] ? methods[moduleId][methodId] : ''}`)
//   }
// }

const App = () => {
  const statusBarBackgroundColor = useStatusBar();
  const appStateChange = useCallback((newState) => {
    CustomPushNotification.handleAppStateChange(newState)
  }, [])
  useEffect(() => {
    const appStateListener = AppState.addEventListener('change', appStateChange);
    SplashScreen.hide();
    return () => {
      appStateListener.remove();
    };
  }, [])

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: statusBarBackgroundColor }}>
      <NoInternetModal />
      <StatusBar backgroundColor={statusBarBackgroundColor} barStyle={'dark-content'} />
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <MainScreen />
        </PersistGate>
      </Provider>
      <Toast />
    </SafeAreaView>
  )
};

export default App;
