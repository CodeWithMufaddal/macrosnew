import crashlytics from '@react-native-firebase/crashlytics';

const CustomCrashlytics = {
    log: (message) => crashlytics().log(message),
    crash: () => crashlytics().crash(),
    recordError: (error) => crashlytics().recordError(error),
    setUserId: (userId) => crashlytics().setUserId(userId),
    setAttributes: (attributes) => crashlytics().setAttributes(attributes),
}

export default CustomCrashlytics;